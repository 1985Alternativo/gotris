;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module enter_score_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGSFXStop
	.globl _PSGStop
	.globl _saveChanges
	.globl _update_highscore_position
	.globl _get_highscore_position_char
	.globl _insert_highscore_position
	.globl _get_highscore_position
	.globl _waitForFrame
	.globl _getButtonsPressed
	.globl _update_input
	.globl _clear_input
	.globl _transition_to_state
	.globl _play_fx
	.globl _play_song
	.globl _start_fadeout_music
	.globl _start_fadein_music
	.globl _print_string
	.globl _highscore_fade_in
	.globl _draw_highscore_table
	.globl _draw_highscore_row
	.globl _general_fade_out
	.globl _load_highscore_assets
	.globl _space
	.globl _currentScore
	.globl _enter_score_status
	.globl _charPosition
	.globl _charValue
	.globl _scorePosition
	.globl _KEY_TWO
	.globl _KEY_ONE
	.globl _KEY_RIGHT
	.globl _KEY_LEFT
	.globl _KEY_DOWN
	.globl _enter_score_state_start
	.globl _enter_score_state_update
	.globl _enter_score_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$KEY_DOWN$0_0$0==.
_KEY_DOWN::
	.ds 2
G$KEY_LEFT$0_0$0==.
_KEY_LEFT::
	.ds 2
G$KEY_RIGHT$0_0$0==.
_KEY_RIGHT::
	.ds 2
G$KEY_ONE$0_0$0==.
_KEY_ONE::
	.ds 2
G$KEY_TWO$0_0$0==.
_KEY_TWO::
	.ds 2
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$scorePosition$0_0$0==.
_scorePosition::
	.ds 1
G$charValue$0_0$0==.
_charValue::
	.ds 1
G$charPosition$0_0$0==.
_charPosition::
	.ds 1
G$enter_score_status$0_0$0==.
_enter_score_status::
	.ds 1
G$currentScore$0_0$0==.
_currentScore::
	.ds 2
G$space$0_0$0==.
_space::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$enter_score_state_start$0$0	= .
	.globl	G$enter_score_state_start$0$0
	C$enter_score_state.c$24$0_0$135	= .
	.globl	C$enter_score_state.c$24$0_0$135
;src/states/enter_score_state.c:24: void enter_score_state_start(void) {
;	---------------------------------
; Function enter_score_state_start
; ---------------------------------
_enter_score_state_start::
	C$enter_score_state.c$25$1_0$135	= .
	.globl	C$enter_score_state.c$25$1_0$135
;src/states/enter_score_state.c:25: scorePosition = 0;
	ld	hl, #_scorePosition
	ld	(hl), #0x00
	C$enter_score_state.c$26$1_0$135	= .
	.globl	C$enter_score_state.c$26$1_0$135
;src/states/enter_score_state.c:26: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$enter_score_state.c$27$1_0$135	= .
	.globl	C$enter_score_state.c$27$1_0$135
;src/states/enter_score_state.c:27: enter_score_status = 0;
	ld	iy, #_enter_score_status
	ld	0 (iy), #0x00
	C$enter_score_state.c$28$1_0$135	= .
	.globl	C$enter_score_state.c$28$1_0$135
;src/states/enter_score_state.c:28: load_highscore_assets();    
	C$enter_score_state.c$29$1_0$135	= .
	.globl	C$enter_score_state.c$29$1_0$135
;src/states/enter_score_state.c:29: }
	C$enter_score_state.c$29$1_0$135	= .
	.globl	C$enter_score_state.c$29$1_0$135
	XG$enter_score_state_start$0$0	= .
	.globl	XG$enter_score_state_start$0$0
	jp	_load_highscore_assets
	G$enter_score_state_update$0$0	= .
	.globl	G$enter_score_state_update$0$0
	C$enter_score_state.c$31$1_0$137	= .
	.globl	C$enter_score_state.c$31$1_0$137
;src/states/enter_score_state.c:31: void enter_score_state_update(void) {
;	---------------------------------
; Function enter_score_state_update
; ---------------------------------
_enter_score_state_update::
	C$enter_score_state.c$32$1_0$137	= .
	.globl	C$enter_score_state.c$32$1_0$137
;src/states/enter_score_state.c:32: if(enter_score_status == 0) {
	ld	a, (_enter_score_status+0)
	or	a, a
	jr	NZ, 00159$
	C$enter_score_state.c$33$2_0$138	= .
	.globl	C$enter_score_state.c$33$2_0$138
;src/states/enter_score_state.c:33: start_fadein_music();
	call	_start_fadein_music
	C$enter_score_state.c$34$2_0$138	= .
	.globl	C$enter_score_state.c$34$2_0$138
;src/states/enter_score_state.c:34: highscore_fade_in();
	call	_highscore_fade_in
	C$enter_score_state.c$35$2_0$138	= .
	.globl	C$enter_score_state.c$35$2_0$138
;src/states/enter_score_state.c:35: current_player = PLAYER_1;
	ld	hl, #_current_player
	ld	(hl), #0x00
	C$enter_score_state.c$36$2_0$138	= .
	.globl	C$enter_score_state.c$36$2_0$138
;src/states/enter_score_state.c:36: currentScore = score_p1;
	ld	hl, (_score_p1)
	ld	(_currentScore), hl
	C$enter_score_state.c$37$2_0$138	= .
	.globl	C$enter_score_state.c$37$2_0$138
;src/states/enter_score_state.c:37: enter_score_status = 1;
	ld	iy, #_enter_score_status
	ld	0 (iy), #0x01
	C$enter_score_state.c$38$2_0$138	= .
	.globl	C$enter_score_state.c$38$2_0$138
;src/states/enter_score_state.c:38: KEY_DOWN = KEY_DOWN_P1;
	ld	hl, #0x0002
	ld	(_KEY_DOWN), hl
	C$enter_score_state.c$39$2_0$138	= .
	.globl	C$enter_score_state.c$39$2_0$138
;src/states/enter_score_state.c:39: KEY_LEFT = KEY_LEFT_P1;
	ld	l, #0x04
	ld	(_KEY_LEFT), hl
	C$enter_score_state.c$40$2_0$138	= .
	.globl	C$enter_score_state.c$40$2_0$138
;src/states/enter_score_state.c:40: KEY_RIGHT = KEY_RIGHT_P1;
	ld	l, #0x08
	ld	(_KEY_RIGHT), hl
	C$enter_score_state.c$41$2_0$138	= .
	.globl	C$enter_score_state.c$41$2_0$138
;src/states/enter_score_state.c:41: KEY_ONE = KEY_ONE_P1;
	ld	l, #0x10
	ld	(_KEY_ONE), hl
	C$enter_score_state.c$42$2_0$138	= .
	.globl	C$enter_score_state.c$42$2_0$138
;src/states/enter_score_state.c:42: KEY_TWO = KEY_TWO_P1;
	ld	l, #0x20
	ld	(_KEY_TWO), hl
	C$enter_score_state.c$43$2_0$138	= .
	.globl	C$enter_score_state.c$43$2_0$138
;src/states/enter_score_state.c:43: play_song(SCORE_OST);
	ld	a, #0x03
	jp	_play_song
00159$:
	C$enter_score_state.c$44$1_0$137	= .
	.globl	C$enter_score_state.c$44$1_0$137
;src/states/enter_score_state.c:44: } else if(enter_score_status == 1){
	ld	a, (_enter_score_status+0)
	dec	a
	jp	NZ,00156$
	C$enter_score_state.c$45$2_0$139	= .
	.globl	C$enter_score_state.c$45$2_0$139
;src/states/enter_score_state.c:45: draw_highscore_table();
	call	_draw_highscore_table
	C$enter_score_state.c$46$2_0$139	= .
	.globl	C$enter_score_state.c$46$2_0$139
;src/states/enter_score_state.c:46: scorePosition = get_highscore_position(currentScore);
	ld	hl, (_currentScore)
	call	_get_highscore_position
	C$enter_score_state.c$47$2_0$139	= .
	.globl	C$enter_score_state.c$47$2_0$139
;src/states/enter_score_state.c:47: if(scorePosition < 10) {
	ld	(_scorePosition+0), a
	sub	a, #0x0a
	jr	NC, 00114$
	C$enter_score_state.c$48$3_0$140	= .
	.globl	C$enter_score_state.c$48$3_0$140
;src/states/enter_score_state.c:48: insert_highscore_position(scorePosition, currentScore);
	ld	de, (_currentScore)
	ld	a, (_scorePosition+0)
	call	_insert_highscore_position
	C$enter_score_state.c$49$3_0$140	= .
	.globl	C$enter_score_state.c$49$3_0$140
;src/states/enter_score_state.c:49: draw_highscore_table();
	call	_draw_highscore_table
	C$enter_score_state.c$50$3_0$140	= .
	.globl	C$enter_score_state.c$50$3_0$140
;src/states/enter_score_state.c:50: charValue = get_highscore_position_char(scorePosition, charPosition);
	ld	a, (_charPosition+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_scorePosition+0)
	call	_get_highscore_position_char
	ld	(_charValue+0), a
	C$enter_score_state.c$51$3_0$140	= .
	.globl	C$enter_score_state.c$51$3_0$140
;src/states/enter_score_state.c:51: enter_score_status = 3;
	ld	hl, #_enter_score_status
	ld	(hl), #0x03
	C$enter_score_state.c$52$3_0$140	= .
	.globl	C$enter_score_state.c$52$3_0$140
;src/states/enter_score_state.c:52: if(number_human_players == 2) {
	ld	a, (_number_human_players+0)
	sub	a, #0x02
	ret	NZ
	C$enter_score_state.c$53$4_0$141	= .
	.globl	C$enter_score_state.c$53$4_0$141
;src/states/enter_score_state.c:53: if(current_player == PLAYER_1) {
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00102$
	C$enter_score_state.c$54$5_0$142	= .
	.globl	C$enter_score_state.c$54$5_0$142
;src/states/enter_score_state.c:54: print_string(4,10,"PLAYER]A^");
	ld	hl, #___str_0
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x04
	call	_print_string
	ret
00102$:
	C$enter_score_state.c$56$5_0$143	= .
	.globl	C$enter_score_state.c$56$5_0$143
;src/states/enter_score_state.c:56: print_string(4,10,"PLAYER]B^");
	ld	hl, #___str_1
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x04
	call	_print_string
	ret
00114$:
	C$enter_score_state.c$60$3_0$144	= .
	.globl	C$enter_score_state.c$60$3_0$144
;src/states/enter_score_state.c:60: if(current_player == PLAYER_1 && number_human_players == 2) {
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00106$
	ld	a, (_number_human_players+0)
	sub	a, #0x02
	jr	NZ, 00106$
	C$enter_score_state.c$61$4_0$145	= .
	.globl	C$enter_score_state.c$61$4_0$145
;src/states/enter_score_state.c:61: current_player = PLAYER_2;
	ld	hl, #_current_player
	ld	(hl), #0x01
	C$enter_score_state.c$62$4_0$145	= .
	.globl	C$enter_score_state.c$62$4_0$145
;src/states/enter_score_state.c:62: currentScore = score_p2;
	ld	hl, (_score_p2)
	ld	(_currentScore), hl
	C$enter_score_state.c$63$4_0$145	= .
	.globl	C$enter_score_state.c$63$4_0$145
;src/states/enter_score_state.c:63: scorePosition = 0;
	ld	hl, #_scorePosition
	ld	(hl), #0x00
	C$enter_score_state.c$64$4_0$145	= .
	.globl	C$enter_score_state.c$64$4_0$145
;src/states/enter_score_state.c:64: KEY_DOWN = KEY_DOWN_P2;
	ld	hl, #0x0080
	ld	(_KEY_DOWN), hl
	C$enter_score_state.c$65$4_0$145	= .
	.globl	C$enter_score_state.c$65$4_0$145
;src/states/enter_score_state.c:65: KEY_LEFT = KEY_LEFT_P2;
	ld	hl, #0x0100
	ld	(_KEY_LEFT), hl
	C$enter_score_state.c$66$4_0$145	= .
	.globl	C$enter_score_state.c$66$4_0$145
;src/states/enter_score_state.c:66: KEY_RIGHT = KEY_RIGHT_P2;
	ld	h, #0x02
	ld	(_KEY_RIGHT), hl
	C$enter_score_state.c$67$4_0$145	= .
	.globl	C$enter_score_state.c$67$4_0$145
;src/states/enter_score_state.c:67: KEY_ONE = KEY_ONE_P2;
	ld	h, #0x04
	ld	(_KEY_ONE), hl
	C$enter_score_state.c$68$4_0$145	= .
	.globl	C$enter_score_state.c$68$4_0$145
;src/states/enter_score_state.c:68: KEY_TWO = KEY_TWO_P2;
	ld	h, #0x08
	ld	(_KEY_TWO), hl
	ret
	C$enter_score_state.c$71$4_0$146	= .
	.globl	C$enter_score_state.c$71$4_0$146
;src/states/enter_score_state.c:71: while(frame_cnt < M_PERIOD) {
00106$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00108$
	C$enter_score_state.c$72$5_0$147	= .
	.globl	C$enter_score_state.c$72$5_0$147
;src/states/enter_score_state.c:72: waitForFrame();
	call	_waitForFrame
	jr	00106$
00108$:
	C$enter_score_state.c$74$4_0$146	= .
	.globl	C$enter_score_state.c$74$4_0$146
;src/states/enter_score_state.c:74: start_fadeout_music();
	call	_start_fadeout_music
	C$enter_score_state.c$75$4_0$146	= .
	.globl	C$enter_score_state.c$75$4_0$146
;src/states/enter_score_state.c:75: general_fade_out();
	call	_general_fade_out
	C$enter_score_state.c$76$4_0$146	= .
	.globl	C$enter_score_state.c$76$4_0$146
;src/states/enter_score_state.c:76: enter_score_status = 4;
	ld	hl, #_enter_score_status
	ld	(hl), #0x04
	ret
00156$:
	C$enter_score_state.c$79$1_0$137	= .
	.globl	C$enter_score_state.c$79$1_0$137
;src/states/enter_score_state.c:79: }  else if(enter_score_status == 3){
	ld	a, (_enter_score_status+0)
	sub	a, #0x03
	jp	NZ,00153$
	C$enter_score_state.c$80$2_0$148	= .
	.globl	C$enter_score_state.c$80$2_0$148
;src/states/enter_score_state.c:80: if(charPosition < 3) {
	ld	a, (_charPosition+0)
	sub	a, #0x03
	jp	NC, 00148$
	C$enter_score_state.c$81$3_0$149	= .
	.globl	C$enter_score_state.c$81$3_0$149
;src/states/enter_score_state.c:81: if(frame_cnt > XXS_PERIOD) {
	ld	a, #0x0c
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jr	NC, 00120$
	C$enter_score_state.c$82$4_0$150	= .
	.globl	C$enter_score_state.c$82$4_0$150
;src/states/enter_score_state.c:82: if(space) {
	ld	hl, #_space
	bit	0, (hl)
	jr	Z, 00117$
	C$enter_score_state.c$83$5_0$151	= .
	.globl	C$enter_score_state.c$83$5_0$151
;src/states/enter_score_state.c:83: update_highscore_position(scorePosition, charPosition, charValue);
	ld	a, (_charValue+0)
	push	af
	inc	sp
	ld	a, (_charPosition+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_scorePosition+0)
	call	_update_highscore_position
	C$enter_score_state.c$84$5_0$151	= .
	.globl	C$enter_score_state.c$84$5_0$151
;src/states/enter_score_state.c:84: space = false;  
	ld	hl, #_space
	ld	(hl), #0x00
	jr	00118$
00117$:
	C$enter_score_state.c$86$5_0$152	= .
	.globl	C$enter_score_state.c$86$5_0$152
;src/states/enter_score_state.c:86: update_highscore_position(scorePosition, charPosition, 27);
	ld	a, #0x1b
	push	af
	inc	sp
	ld	a, (_charPosition+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_scorePosition+0)
	call	_update_highscore_position
	C$enter_score_state.c$87$5_0$152	= .
	.globl	C$enter_score_state.c$87$5_0$152
;src/states/enter_score_state.c:87: space = true;
	ld	hl, #_space
	ld	(hl), #0x01
00118$:
	C$enter_score_state.c$89$4_0$150	= .
	.globl	C$enter_score_state.c$89$4_0$150
;src/states/enter_score_state.c:89: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
00120$:
	C$enter_score_state.c$91$3_0$149	= .
	.globl	C$enter_score_state.c$91$3_0$149
;src/states/enter_score_state.c:91: draw_highscore_row(scorePosition);
	ld	a, (_scorePosition+0)
	call	_draw_highscore_row
	C$enter_score_state.c$92$3_0$149	= .
	.globl	C$enter_score_state.c$92$3_0$149
;src/states/enter_score_state.c:92: clear_input();
	call	_clear_input
	C$enter_score_state.c$93$3_0$149	= .
	.globl	C$enter_score_state.c$93$3_0$149
;src/states/enter_score_state.c:93: update_input();
	call	_update_input
	C$enter_score_state.c$94$3_0$149	= .
	.globl	C$enter_score_state.c$94$3_0$149
;src/states/enter_score_state.c:94: if((getButtonsPressed() & KEY_LEFT) && charValue > 0) {
	call	_getButtonsPressed
	ld	a, e
	ld	iy, #_KEY_LEFT
	and	a, 0 (iy)
	ld	c, a
	ld	a, d
	and	a, 1 (iy)
	or	a, c
	jr	Z, 00125$
	ld	a, (_charValue+0)
	or	a, a
	jr	Z, 00125$
	C$enter_score_state.c$95$4_0$153	= .
	.globl	C$enter_score_state.c$95$4_0$153
;src/states/enter_score_state.c:95: charValue--;
	ld	hl, #_charValue
	dec	(hl)
	jr	00126$
00125$:
	C$enter_score_state.c$97$3_0$149	= .
	.globl	C$enter_score_state.c$97$3_0$149
;src/states/enter_score_state.c:97: } else if((getButtonsPressed() & KEY_RIGHT) && charValue < 25) {
	call	_getButtonsPressed
	ld	a, e
	ld	iy, #_KEY_RIGHT
	and	a, 0 (iy)
	ld	c, a
	ld	a, d
	and	a, 1 (iy)
	or	a, c
	jr	Z, 00126$
	ld	a, (_charValue+0)
	sub	a, #0x19
	jr	NC, 00126$
	C$enter_score_state.c$98$4_0$154	= .
	.globl	C$enter_score_state.c$98$4_0$154
;src/states/enter_score_state.c:98: charValue++; 
	ld	hl, #_charValue
	inc	(hl)
00126$:
	C$enter_score_state.c$101$3_0$149	= .
	.globl	C$enter_score_state.c$101$3_0$149
;src/states/enter_score_state.c:101: if(getButtonsPressed() & KEY_ONE) {
	call	_getButtonsPressed
	ld	a, e
	ld	iy, #_KEY_ONE
	and	a, 0 (iy)
	ld	c, a
	ld	a, d
	and	a, 1 (iy)
	or	a, c
	jr	Z, 00135$
	C$enter_score_state.c$102$4_0$155	= .
	.globl	C$enter_score_state.c$102$4_0$155
;src/states/enter_score_state.c:102: update_highscore_position(scorePosition, charPosition, charValue); 
	ld	a, (_charValue+0)
	push	af
	inc	sp
	ld	a, (_charPosition+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_scorePosition+0)
	call	_update_highscore_position
	C$enter_score_state.c$103$4_0$155	= .
	.globl	C$enter_score_state.c$103$4_0$155
;src/states/enter_score_state.c:103: charPosition++;
	ld	iy, #_charPosition
	inc	0 (iy)
	C$enter_score_state.c$104$4_0$155	= .
	.globl	C$enter_score_state.c$104$4_0$155
;src/states/enter_score_state.c:104: if(charPosition < 3) {
	ld	a, (_charPosition+0)
	sub	a, #0x03
	jp	NC,_waitForFrame
	C$enter_score_state.c$105$5_0$156	= .
	.globl	C$enter_score_state.c$105$5_0$156
;src/states/enter_score_state.c:105: play_fx(FALL_FX);
	ld	a, #0x03
	call	_play_fx
	C$enter_score_state.c$106$5_0$156	= .
	.globl	C$enter_score_state.c$106$5_0$156
;src/states/enter_score_state.c:106: charValue = get_highscore_position_char(scorePosition, charPosition);
	ld	a, (_charPosition+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_scorePosition+0)
	call	_get_highscore_position_char
	ld	(_charValue+0), a
	jp	_waitForFrame
00135$:
	C$enter_score_state.c$108$3_0$149	= .
	.globl	C$enter_score_state.c$108$3_0$149
;src/states/enter_score_state.c:108: } else if(getButtonsPressed() & KEY_TWO) {
	call	_getButtonsPressed
	ld	a, e
	ld	iy, #_KEY_TWO
	and	a, 0 (iy)
	ld	c, a
	ld	a, d
	and	a, 1 (iy)
	ld	b, a
	or	a, c
	jp	Z,_waitForFrame
	C$enter_score_state.c$109$4_0$157	= .
	.globl	C$enter_score_state.c$109$4_0$157
;src/states/enter_score_state.c:109: update_highscore_position(scorePosition, charPosition, charValue); 
	ld	a, (_charValue+0)
	push	af
	inc	sp
	ld	a, (_charPosition+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_scorePosition+0)
	call	_update_highscore_position
	C$enter_score_state.c$110$4_0$157	= .
	.globl	C$enter_score_state.c$110$4_0$157
;src/states/enter_score_state.c:110: if(charPosition > 0) {
	ld	a, (_charPosition+0)
	or	a, a
	jp	Z,_waitForFrame
	C$enter_score_state.c$111$5_0$158	= .
	.globl	C$enter_score_state.c$111$5_0$158
;src/states/enter_score_state.c:111: charPosition--;
	ld	hl, #_charPosition
	dec	(hl)
	C$enter_score_state.c$112$5_0$158	= .
	.globl	C$enter_score_state.c$112$5_0$158
;src/states/enter_score_state.c:112: play_fx(FALL_FX);
	ld	a, #0x03
	call	_play_fx
	C$enter_score_state.c$113$5_0$158	= .
	.globl	C$enter_score_state.c$113$5_0$158
;src/states/enter_score_state.c:113: charValue = get_highscore_position_char(scorePosition, charPosition);
	ld	a, (_charPosition+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_scorePosition+0)
	call	_get_highscore_position_char
	C$enter_score_state.c$116$3_0$149	= .
	.globl	C$enter_score_state.c$116$3_0$149
;src/states/enter_score_state.c:116: waitForFrame();
	ld	(_charValue+0), a
	jp	_waitForFrame
00148$:
	C$enter_score_state.c$118$3_0$159	= .
	.globl	C$enter_score_state.c$118$3_0$159
;src/states/enter_score_state.c:118: draw_highscore_table();
	call	_draw_highscore_table
	C$enter_score_state.c$119$3_0$159	= .
	.globl	C$enter_score_state.c$119$3_0$159
;src/states/enter_score_state.c:119: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$enter_score_state.c$120$3_0$159	= .
	.globl	C$enter_score_state.c$120$3_0$159
;src/states/enter_score_state.c:120: play_fx(START_FX);
	xor	a, a
	call	_play_fx
	C$enter_score_state.c$121$3_0$159	= .
	.globl	C$enter_score_state.c$121$3_0$159
;src/states/enter_score_state.c:121: if(current_player == PLAYER_1 && number_human_players == 2) {
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00140$
	ld	a, (_number_human_players+0)
	sub	a, #0x02
	jr	NZ, 00140$
	C$enter_score_state.c$122$4_0$160	= .
	.globl	C$enter_score_state.c$122$4_0$160
;src/states/enter_score_state.c:122: while(frame_cnt < M_PERIOD) {
00137$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00139$
	C$enter_score_state.c$123$5_0$161	= .
	.globl	C$enter_score_state.c$123$5_0$161
;src/states/enter_score_state.c:123: waitForFrame();
	call	_waitForFrame
	jr	00137$
00139$:
	C$enter_score_state.c$125$4_0$160	= .
	.globl	C$enter_score_state.c$125$4_0$160
;src/states/enter_score_state.c:125: current_player = PLAYER_2;
	ld	hl, #_current_player
	ld	(hl), #0x01
	C$enter_score_state.c$126$4_0$160	= .
	.globl	C$enter_score_state.c$126$4_0$160
;src/states/enter_score_state.c:126: currentScore = score_p2;
	ld	hl, (_score_p2)
	ld	(_currentScore), hl
	C$enter_score_state.c$127$4_0$160	= .
	.globl	C$enter_score_state.c$127$4_0$160
;src/states/enter_score_state.c:127: enter_score_status = 1;
	ld	hl, #_enter_score_status
	ld	(hl), #0x01
	C$enter_score_state.c$128$4_0$160	= .
	.globl	C$enter_score_state.c$128$4_0$160
;src/states/enter_score_state.c:128: scorePosition = 0;
	ld	hl, #_scorePosition
	ld	(hl), #0x00
	C$enter_score_state.c$129$4_0$160	= .
	.globl	C$enter_score_state.c$129$4_0$160
;src/states/enter_score_state.c:129: charPosition = 0;
	ld	hl, #_charPosition
	ld	(hl), #0x00
	C$enter_score_state.c$130$4_0$160	= .
	.globl	C$enter_score_state.c$130$4_0$160
;src/states/enter_score_state.c:130: KEY_DOWN = KEY_DOWN_P2;
	ld	hl, #0x0080
	ld	(_KEY_DOWN), hl
	C$enter_score_state.c$131$4_0$160	= .
	.globl	C$enter_score_state.c$131$4_0$160
;src/states/enter_score_state.c:131: KEY_LEFT = KEY_LEFT_P2;
	ld	hl, #0x0100
	ld	(_KEY_LEFT), hl
	C$enter_score_state.c$132$4_0$160	= .
	.globl	C$enter_score_state.c$132$4_0$160
;src/states/enter_score_state.c:132: KEY_RIGHT = KEY_RIGHT_P2;
	ld	h, #0x02
	ld	(_KEY_RIGHT), hl
	C$enter_score_state.c$133$4_0$160	= .
	.globl	C$enter_score_state.c$133$4_0$160
;src/states/enter_score_state.c:133: KEY_ONE = KEY_ONE_P2;
	ld	h, #0x04
	ld	(_KEY_ONE), hl
	C$enter_score_state.c$134$4_0$160	= .
	.globl	C$enter_score_state.c$134$4_0$160
;src/states/enter_score_state.c:134: KEY_TWO = KEY_TWO_P2;
	ld	h, #0x08
	ld	(_KEY_TWO), hl
	ret
	C$enter_score_state.c$136$4_0$162	= .
	.globl	C$enter_score_state.c$136$4_0$162
;src/states/enter_score_state.c:136: while(frame_cnt < XXL_PERIOD) {
00140$:
	ld	de, #0x01e0
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00142$
	C$enter_score_state.c$137$5_0$163	= .
	.globl	C$enter_score_state.c$137$5_0$163
;src/states/enter_score_state.c:137: waitForFrame();
	call	_waitForFrame
	jr	00140$
00142$:
	C$enter_score_state.c$139$4_0$162	= .
	.globl	C$enter_score_state.c$139$4_0$162
;src/states/enter_score_state.c:139: clear_input();
	call	_clear_input
	C$enter_score_state.c$140$4_0$162	= .
	.globl	C$enter_score_state.c$140$4_0$162
;src/states/enter_score_state.c:140: start_fadeout_music();
	call	_start_fadeout_music
	C$enter_score_state.c$141$4_0$162	= .
	.globl	C$enter_score_state.c$141$4_0$162
;src/states/enter_score_state.c:141: general_fade_out();
	call	_general_fade_out
	C$enter_score_state.c$142$4_0$162	= .
	.globl	C$enter_score_state.c$142$4_0$162
;src/states/enter_score_state.c:142: saveChanges();
	call	_saveChanges
	C$enter_score_state.c$143$4_0$162	= .
	.globl	C$enter_score_state.c$143$4_0$162
;src/states/enter_score_state.c:143: enter_score_status = 4;
	ld	hl, #_enter_score_status
	ld	(hl), #0x04
	ret
00153$:
	C$enter_score_state.c$147$1_0$137	= .
	.globl	C$enter_score_state.c$147$1_0$137
;src/states/enter_score_state.c:147: } else if(enter_score_status == 4) {
	ld	a, (_enter_score_status+0)
	sub	a, #0x04
	ret	NZ
	C$enter_score_state.c$148$2_0$164	= .
	.globl	C$enter_score_state.c$148$2_0$164
;src/states/enter_score_state.c:148: highScoreReached = false;
	ld	iy, #_highScoreReached
	ld	0 (iy), #0x00
	C$enter_score_state.c$149$2_0$164	= .
	.globl	C$enter_score_state.c$149$2_0$164
;src/states/enter_score_state.c:149: transition_to_state(TITLE_STATE);
	ld	a, #0x03
	C$enter_score_state.c$151$1_0$137	= .
	.globl	C$enter_score_state.c$151$1_0$137
;src/states/enter_score_state.c:151: }
	C$enter_score_state.c$151$1_0$137	= .
	.globl	C$enter_score_state.c$151$1_0$137
	XG$enter_score_state_update$0$0	= .
	.globl	XG$enter_score_state_update$0$0
	jp	_transition_to_state
Fenter_score_state$__str_0$0_0$0 == .
___str_0:
	.ascii "PLAYER]A^"
	.db 0x00
Fenter_score_state$__str_1$0_0$0 == .
___str_1:
	.ascii "PLAYER]B^"
	.db 0x00
	G$enter_score_state_stop$0$0	= .
	.globl	G$enter_score_state_stop$0$0
	C$enter_score_state.c$153$1_0$166	= .
	.globl	C$enter_score_state.c$153$1_0$166
;src/states/enter_score_state.c:153: void enter_score_state_stop(void) {
;	---------------------------------
; Function enter_score_state_stop
; ---------------------------------
_enter_score_state_stop::
	C$enter_score_state.c$154$1_0$166	= .
	.globl	C$enter_score_state.c$154$1_0$166
;src/states/enter_score_state.c:154: stop_music();
	call	_PSGStop
	C$enter_score_state.c$155$1_0$166	= .
	.globl	C$enter_score_state.c$155$1_0$166
;src/states/enter_score_state.c:155: stop_fx();
	C$enter_score_state.c$156$1_0$166	= .
	.globl	C$enter_score_state.c$156$1_0$166
;src/states/enter_score_state.c:156: }
	C$enter_score_state.c$156$1_0$166	= .
	.globl	C$enter_score_state.c$156$1_0$166
	XG$enter_score_state_stop$0$0	= .
	.globl	XG$enter_score_state_stop$0$0
	jp	_PSGSFXStop
	.area _CODE
	.area _INITIALIZER
Fenter_score_state$__xinit_scorePosition$0_0$0 == .
__xinit__scorePosition:
	.db #0x00	; 0
Fenter_score_state$__xinit_charValue$0_0$0 == .
__xinit__charValue:
	.db #0x00	; 0
Fenter_score_state$__xinit_charPosition$0_0$0 == .
__xinit__charPosition:
	.db #0x00	; 0
Fenter_score_state$__xinit_enter_score_status$0_0$0 == .
__xinit__enter_score_status:
	.db #0x00	; 0
Fenter_score_state$__xinit_currentScore$0_0$0 == .
__xinit__currentScore:
	.dw #0x0000
Fenter_score_state$__xinit_space$0_0$0 == .
__xinit__space:
	.db #0x00	;  0
	.area _CABS (ABS)
