;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module input
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _SMS_getKeysReleased
	.globl _SMS_getKeysHeld
	.globl _SMS_getKeysPressed
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _clear_input
	.globl _update_input
	.globl _getDirection
	.globl _getRotation
	.globl _getButtonsPressed
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$clear_input$0$0	= .
	.globl	G$clear_input$0$0
	C$input.c$8$0_0$84	= .
	.globl	C$input.c$8$0_0$84
;src/engine/input.c:8: void clear_input(void) {
;	---------------------------------
; Function clear_input
; ---------------------------------
_clear_input::
	C$input.c$9$1_0$84	= .
	.globl	C$input.c$9$1_0$84
;src/engine/input.c:9: movement_direction = 0; 
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$10$1_0$84	= .
	.globl	C$input.c$10$1_0$84
;src/engine/input.c:10: rotation_input = 0;
	ld	(_rotation_input), hl
	C$input.c$11$1_0$84	= .
	.globl	C$input.c$11$1_0$84
;src/engine/input.c:11: button_input = 0;
	ld	(_button_input), hl
	C$input.c$12$1_0$84	= .
	.globl	C$input.c$12$1_0$84
;src/engine/input.c:12: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$13$1_0$84	= .
	.globl	C$input.c$13$1_0$84
;src/engine/input.c:13: repeat_rotation = 0; 
	ld	(_repeat_rotation), hl
	C$input.c$14$1_0$84	= .
	.globl	C$input.c$14$1_0$84
;src/engine/input.c:14: }
	C$input.c$14$1_0$84	= .
	.globl	C$input.c$14$1_0$84
	XG$clear_input$0$0	= .
	.globl	XG$clear_input$0$0
	ret
	G$update_input$0$0	= .
	.globl	G$update_input$0$0
	C$input.c$16$1_0$86	= .
	.globl	C$input.c$16$1_0$86
;src/engine/input.c:16: void update_input(void) {
;	---------------------------------
; Function update_input
; ---------------------------------
_update_input::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-22
	add	hl, sp
	ld	sp, hl
	C$input.c$17$1_0$86	= .
	.globl	C$input.c$17$1_0$86
;src/engine/input.c:17: u16 keys_pressed = SMS_getKeysPressed();
	call	_SMS_getKeysPressed
	inc	sp
	inc	sp
	push	de
	C$input.c$18$1_0$86	= .
	.globl	C$input.c$18$1_0$86
;src/engine/input.c:18: u16 keys_held = SMS_getKeysHeld(); 
	call	_SMS_getKeysHeld
	ld	-20 (ix), e
	ld	-19 (ix), d
	C$input.c$19$1_0$86	= .
	.globl	C$input.c$19$1_0$86
;src/engine/input.c:19: u16 keys_released = SMS_getKeysReleased();
	call	_SMS_getKeysReleased
	ld	-18 (ix), e
	ld	-17 (ix), d
	C$input.c$22$1_0$86	= .
	.globl	C$input.c$22$1_0$86
;src/engine/input.c:22: if(current_player == PLAYER_1) {
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00102$
	C$input.c$23$2_0$87	= .
	.globl	C$input.c$23$2_0$87
;src/engine/input.c:23: KEY_DOWN = KEY_DOWN_P1;
	ld	-16 (ix), #0x02
	ld	-15 (ix), #0
	C$input.c$24$2_0$87	= .
	.globl	C$input.c$24$2_0$87
;src/engine/input.c:24: KEY_LEFT = KEY_LEFT_P1;
	ld	-14 (ix), #0x04
	ld	-13 (ix), #0
	C$input.c$25$2_0$87	= .
	.globl	C$input.c$25$2_0$87
;src/engine/input.c:25: KEY_RIGHT = KEY_RIGHT_P1;
	ld	-12 (ix), #0x08
	ld	-11 (ix), #0
	C$input.c$26$2_0$87	= .
	.globl	C$input.c$26$2_0$87
;src/engine/input.c:26: KEY_ONE = KEY_ONE_P1;
	ld	-10 (ix), #0x10
	ld	-9 (ix), #0
	C$input.c$27$2_0$87	= .
	.globl	C$input.c$27$2_0$87
;src/engine/input.c:27: KEY_TWO = KEY_TWO_P1;
	ld	-8 (ix), #0x20
	ld	-7 (ix), #0
	jr	00103$
00102$:
	C$input.c$29$2_0$88	= .
	.globl	C$input.c$29$2_0$88
;src/engine/input.c:29: KEY_DOWN = KEY_DOWN_P2;
	ld	-16 (ix), #0x80
	ld	-15 (ix), #0
	C$input.c$30$2_0$88	= .
	.globl	C$input.c$30$2_0$88
;src/engine/input.c:30: KEY_LEFT = KEY_LEFT_P2;
	ld	-14 (ix), #0x00
	ld	-13 (ix), #0x01
	C$input.c$31$2_0$88	= .
	.globl	C$input.c$31$2_0$88
;src/engine/input.c:31: KEY_RIGHT = KEY_RIGHT_P2;
	ld	-12 (ix), #0x00
	ld	-11 (ix), #0x02
	C$input.c$32$2_0$88	= .
	.globl	C$input.c$32$2_0$88
;src/engine/input.c:32: KEY_ONE = KEY_ONE_P2;
	ld	-10 (ix), #0x00
	ld	-9 (ix), #0x04
	C$input.c$33$2_0$88	= .
	.globl	C$input.c$33$2_0$88
;src/engine/input.c:33: KEY_TWO = KEY_TWO_P2;
	ld	-8 (ix), #0x00
	ld	-7 (ix), #0x08
00103$:
	C$input.c$36$1_0$86	= .
	.globl	C$input.c$36$1_0$86
;src/engine/input.c:36: if(keys_pressed != 0) {
	ld	a, -21 (ix)
	or	a, -22 (ix)
	jp	Z, 00155$
	C$input.c$37$2_0$89	= .
	.globl	C$input.c$37$2_0$89
;src/engine/input.c:37: button_input = keys_pressed;
	pop	hl
	push	hl
	ld	(_button_input), hl
	C$input.c$39$1_0$86	= .
	.globl	C$input.c$39$1_0$86
;src/engine/input.c:39: if(keys_pressed & KEY_DOWN) {
	ld	a, -22 (ix)
	and	a, -16 (ix)
	ld	-6 (ix), a
	ld	a, -21 (ix)
	and	a, -15 (ix)
	ld	-5 (ix), a
	C$input.c$43$1_0$86	= .
	.globl	C$input.c$43$1_0$86
;src/engine/input.c:43: } else if((keys_pressed & KEY_LEFT) && !(keys_pressed & KEY_RIGHT)) {
	ld	a, -22 (ix)
	and	a, -14 (ix)
	ld	-4 (ix), a
	ld	a, -21 (ix)
	and	a, -13 (ix)
	ld	-3 (ix), a
	ld	a, -22 (ix)
	and	a, -12 (ix)
	ld	-2 (ix), a
	ld	a, -21 (ix)
	and	a, -11 (ix)
	ld	-1 (ix), a
	C$input.c$38$2_0$89	= .
	.globl	C$input.c$38$2_0$89
;src/engine/input.c:38: if(movement_direction == 0) {
	ld	a, (_movement_direction+1)
	ld	hl, #_movement_direction
	or	a, (hl)
	jr	NZ, 00129$
	C$input.c$39$3_0$90	= .
	.globl	C$input.c$39$3_0$90
;src/engine/input.c:39: if(keys_pressed & KEY_DOWN) {
	ld	a, -5 (ix)
	or	a, -6 (ix)
	jr	Z, 00112$
	C$input.c$40$4_0$91	= .
	.globl	C$input.c$40$4_0$91
;src/engine/input.c:40: movement_direction = DOWN_ACTION;
	ld	hl, #0x0002
	ld	(_movement_direction), hl
	C$input.c$41$4_0$91	= .
	.globl	C$input.c$41$4_0$91
;src/engine/input.c:41: input_cnt_direction =  0;
	ld	l, h
	ld	(_input_cnt_direction), hl
	C$input.c$42$4_0$91	= .
	.globl	C$input.c$42$4_0$91
;src/engine/input.c:42: repeat_movement = 0;
	ld	(_repeat_movement), hl
	jp	00130$
00112$:
	C$input.c$43$3_0$90	= .
	.globl	C$input.c$43$3_0$90
;src/engine/input.c:43: } else if((keys_pressed & KEY_LEFT) && !(keys_pressed & KEY_RIGHT)) {
	ld	a, -3 (ix)
	or	a, -4 (ix)
	jr	Z, 00108$
	ld	a, -1 (ix)
	or	a, -2 (ix)
	jr	NZ, 00108$
	C$input.c$44$4_0$92	= .
	.globl	C$input.c$44$4_0$92
;src/engine/input.c:44: movement_direction = LEFT_ACTION;
	ld	hl, #0x0004
	ld	(_movement_direction), hl
	C$input.c$45$4_0$92	= .
	.globl	C$input.c$45$4_0$92
;src/engine/input.c:45: input_cnt_direction =  0;
	ld	l, h
	ld	(_input_cnt_direction), hl
	C$input.c$46$4_0$92	= .
	.globl	C$input.c$46$4_0$92
;src/engine/input.c:46: repeat_movement = 0;
	ld	(_repeat_movement), hl
	jp	00130$
00108$:
	C$input.c$47$3_0$90	= .
	.globl	C$input.c$47$3_0$90
;src/engine/input.c:47: } else if((keys_pressed & KEY_RIGHT) && !(keys_pressed & KEY_LEFT)) {
	ld	a, -1 (ix)
	or	a, -2 (ix)
	jp	Z, 00130$
	ld	a, -3 (ix)
	or	a, -4 (ix)
	jp	NZ, 00130$
	C$input.c$48$4_0$93	= .
	.globl	C$input.c$48$4_0$93
;src/engine/input.c:48: movement_direction = RIGHT_ACTION;
	ld	hl, #0x0008
	ld	(_movement_direction), hl
	C$input.c$49$4_0$93	= .
	.globl	C$input.c$49$4_0$93
;src/engine/input.c:49: input_cnt_direction =  0;
	ld	l, h
	ld	(_input_cnt_direction), hl
	C$input.c$50$4_0$93	= .
	.globl	C$input.c$50$4_0$93
;src/engine/input.c:50: repeat_movement = 0;
	ld	(_repeat_movement), hl
	jr	00130$
00129$:
	C$input.c$53$3_0$94	= .
	.globl	C$input.c$53$3_0$94
;src/engine/input.c:53: if(keys_pressed & KEY_DOWN) {
	ld	a, -5 (ix)
	or	a, -6 (ix)
	jr	Z, 00126$
	C$input.c$54$4_0$95	= .
	.globl	C$input.c$54$4_0$95
;src/engine/input.c:54: movement_direction = DOWN_ACTION;
	ld	hl, #0x0002
	ld	(_movement_direction), hl
	C$input.c$55$4_0$95	= .
	.globl	C$input.c$55$4_0$95
;src/engine/input.c:55: repeat_movement = 0;
	ld	l, h
	ld	(_repeat_movement), hl
	C$input.c$56$4_0$95	= .
	.globl	C$input.c$56$4_0$95
;src/engine/input.c:56: input_cnt_direction = 0;
	ld	(_input_cnt_direction), hl
	jr	00130$
00126$:
	C$input.c$57$3_0$94	= .
	.globl	C$input.c$57$3_0$94
;src/engine/input.c:57: } else if(movement_direction & KEY_LEFT) {
	ld	a, (_movement_direction+0)
	and	a, -14 (ix)
	ld	c, a
	ld	a, (_movement_direction+1)
	and	a, -13 (ix)
	or	a, c
	jr	Z, 00123$
	C$input.c$58$4_0$96	= .
	.globl	C$input.c$58$4_0$96
;src/engine/input.c:58: if((keys_pressed & KEY_RIGHT) && !(keys_pressed & KEY_LEFT)) {
	ld	a, -1 (ix)
	or	a, -2 (ix)
	jr	Z, 00130$
	ld	a, -3 (ix)
	or	a, -4 (ix)
	jr	NZ, 00130$
	C$input.c$59$5_0$97	= .
	.globl	C$input.c$59$5_0$97
;src/engine/input.c:59: movement_direction = 0;
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$60$5_0$97	= .
	.globl	C$input.c$60$5_0$97
;src/engine/input.c:60: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$61$5_0$97	= .
	.globl	C$input.c$61$5_0$97
;src/engine/input.c:61: input_cnt_direction = 0;
	ld	(_input_cnt_direction), hl
	jr	00130$
00123$:
	C$input.c$63$3_0$94	= .
	.globl	C$input.c$63$3_0$94
;src/engine/input.c:63: } else if(movement_direction & KEY_RIGHT) {
	ld	a, (_movement_direction+0)
	and	a, -12 (ix)
	ld	c, a
	ld	a, (_movement_direction+1)
	and	a, -11 (ix)
	or	a, c
	jr	Z, 00130$
	C$input.c$64$4_0$98	= .
	.globl	C$input.c$64$4_0$98
;src/engine/input.c:64: if((keys_pressed & KEY_LEFT) && !(keys_pressed & KEY_RIGHT)) {
	ld	a, -3 (ix)
	or	a, -4 (ix)
	jr	Z, 00130$
	ld	a, -1 (ix)
	or	a, -2 (ix)
	jr	NZ, 00130$
	C$input.c$65$5_0$99	= .
	.globl	C$input.c$65$5_0$99
;src/engine/input.c:65: movement_direction = 0;
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$66$5_0$99	= .
	.globl	C$input.c$66$5_0$99
;src/engine/input.c:66: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$67$5_0$99	= .
	.globl	C$input.c$67$5_0$99
;src/engine/input.c:67: input_cnt_direction = 0;
	ld	(_input_cnt_direction), hl
00130$:
	C$input.c$72$1_0$86	= .
	.globl	C$input.c$72$1_0$86
;src/engine/input.c:72: if((keys_pressed & KEY_ONE) && !(keys_pressed & KEY_TWO)) {
	ld	a, -22 (ix)
	and	a, -10 (ix)
	ld	c, a
	ld	a, -21 (ix)
	and	a, -9 (ix)
	ld	b, a
	ld	a, -22 (ix)
	and	a, -8 (ix)
	ld	e, a
	ld	a, -21 (ix)
	and	a, -7 (ix)
	ld	d, a
	C$input.c$71$2_0$89	= .
	.globl	C$input.c$71$2_0$89
;src/engine/input.c:71: if(rotation_input == 0) {
	ld	a, (_rotation_input+1)
	ld	hl, #_rotation_input
	or	a, (hl)
	jr	NZ, 00150$
	C$input.c$72$3_0$100	= .
	.globl	C$input.c$72$3_0$100
;src/engine/input.c:72: if((keys_pressed & KEY_ONE) && !(keys_pressed & KEY_TWO)) {
	ld	a, b
	or	a, c
	jr	Z, 00135$
	ld	a, d
	or	a, e
	jr	NZ, 00135$
	C$input.c$73$4_0$101	= .
	.globl	C$input.c$73$4_0$101
;src/engine/input.c:73: rotation_input = ROTATION_ONE;
	ld	hl, #0x0010
	ld	(_rotation_input), hl
	C$input.c$74$4_0$101	= .
	.globl	C$input.c$74$4_0$101
;src/engine/input.c:74: repeat_rotation = 0;
	ld	l, h
	ld	(_repeat_rotation), hl
	C$input.c$75$4_0$101	= .
	.globl	C$input.c$75$4_0$101
;src/engine/input.c:75: input_cnt_rotation =  0;
	ld	(_input_cnt_rotation), hl
	jr	00151$
00135$:
	C$input.c$76$3_0$100	= .
	.globl	C$input.c$76$3_0$100
;src/engine/input.c:76: } else if((keys_pressed & KEY_TWO) && !(keys_pressed & KEY_ONE)) {
	ld	a, d
	or	a, e
	jr	Z, 00151$
	ld	a, b
	or	a, c
	jr	NZ, 00151$
	C$input.c$77$4_0$102	= .
	.globl	C$input.c$77$4_0$102
;src/engine/input.c:77: rotation_input = ROTATION_TWO;
	ld	hl, #0x0020
	ld	(_rotation_input), hl
	C$input.c$78$4_0$102	= .
	.globl	C$input.c$78$4_0$102
;src/engine/input.c:78: repeat_rotation = 0;
	ld	l, h
	ld	(_repeat_rotation), hl
	C$input.c$79$4_0$102	= .
	.globl	C$input.c$79$4_0$102
;src/engine/input.c:79: input_cnt_rotation =  0;
	ld	(_input_cnt_rotation), hl
	jr	00151$
00150$:
	C$input.c$82$3_0$103	= .
	.globl	C$input.c$82$3_0$103
;src/engine/input.c:82: if(rotation_input & KEY_ONE) {
	ld	a, (_rotation_input+0)
	and	a, -10 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_rotation_input+1)
	and	a, -9 (ix)
	or	a, l
	jr	Z, 00147$
	C$input.c$83$4_0$104	= .
	.globl	C$input.c$83$4_0$104
;src/engine/input.c:83: if((keys_pressed & KEY_TWO) && !(keys_pressed & KEY_ONE)) {
	ld	a, d
	or	a, e
	jr	Z, 00151$
	ld	a, b
	or	a, c
	jr	NZ, 00151$
	C$input.c$84$5_0$105	= .
	.globl	C$input.c$84$5_0$105
;src/engine/input.c:84: rotation_input = 0;
	ld	hl, #0x0000
	ld	(_rotation_input), hl
	C$input.c$85$5_0$105	= .
	.globl	C$input.c$85$5_0$105
;src/engine/input.c:85: repeat_rotation = 0;
	ld	(_repeat_rotation), hl
	C$input.c$86$5_0$105	= .
	.globl	C$input.c$86$5_0$105
;src/engine/input.c:86: input_cnt_rotation = 0;
	ld	(_input_cnt_rotation), hl
	jr	00151$
00147$:
	C$input.c$88$3_0$103	= .
	.globl	C$input.c$88$3_0$103
;src/engine/input.c:88: } else if(rotation_input & KEY_TWO) {
	ld	a, (_rotation_input+0)
	and	a, -8 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_rotation_input+1)
	and	a, -7 (ix)
	or	a, l
	jr	Z, 00151$
	C$input.c$89$4_0$106	= .
	.globl	C$input.c$89$4_0$106
;src/engine/input.c:89: if((keys_pressed & KEY_ONE) && !(keys_pressed & KEY_TWO)) {
	ld	a, b
	or	a, c
	jr	Z, 00151$
	ld	a, d
	or	a, e
	jr	NZ, 00151$
	C$input.c$90$5_0$107	= .
	.globl	C$input.c$90$5_0$107
;src/engine/input.c:90: rotation_input = 0;
	ld	hl, #0x0000
	ld	(_rotation_input), hl
	C$input.c$91$5_0$107	= .
	.globl	C$input.c$91$5_0$107
;src/engine/input.c:91: repeat_rotation = 0;
	ld	(_repeat_rotation), hl
	C$input.c$92$5_0$107	= .
	.globl	C$input.c$92$5_0$107
;src/engine/input.c:92: input_cnt_rotation = 0; 
	ld	(_input_cnt_rotation), hl
00151$:
	C$input.c$96$2_0$89	= .
	.globl	C$input.c$96$2_0$89
;src/engine/input.c:96: if(keys_pressed & RESET_KEY) {
	bit	4, -21 (ix)
	jr	Z, 00155$
	C$input.c$97$3_0$108	= .
	.globl	C$input.c$97$3_0$108
;src/engine/input.c:97: resetPressed = true;
	ld	hl, #_resetPressed
	ld	(hl), #0x01
00155$:
	C$input.c$100$1_0$86	= .
	.globl	C$input.c$100$1_0$86
;src/engine/input.c:100: if(keys_held != 0) {
	ld	a, -19 (ix)
	or	a, -20 (ix)
	jp	Z, 00182$
	C$input.c$101$2_0$109	= .
	.globl	C$input.c$101$2_0$109
;src/engine/input.c:101: if(keys_held & KEY_DOWN) {
	ld	a, -20 (ix)
	and	a, -16 (ix)
	ld	c, a
	ld	a, -19 (ix)
	and	a, -15 (ix)
	or	a, c
	jr	Z, 00168$
	C$input.c$102$3_0$110	= .
	.globl	C$input.c$102$3_0$110
;src/engine/input.c:102: movement_direction = DOWN_ACTION;
	ld	hl, #0x0002
	ld	(_movement_direction), hl
	C$input.c$103$3_0$110	= .
	.globl	C$input.c$103$3_0$110
;src/engine/input.c:103: repeat_movement = 0;
	ld	l, h
	ld	(_repeat_movement), hl
	C$input.c$104$3_0$110	= .
	.globl	C$input.c$104$3_0$110
;src/engine/input.c:104: input_cnt_direction = 0;
	ld	(_input_cnt_direction), hl
	jr	00169$
00168$:
	C$input.c$106$1_0$86	= .
	.globl	C$input.c$106$1_0$86
;src/engine/input.c:106: if((keys_held & KEY_RIGHT) && !(keys_held & KEY_LEFT)) {
	ld	a, -20 (ix)
	and	a, -12 (ix)
	ld	c, a
	ld	a, -19 (ix)
	and	a, -11 (ix)
	ld	b, a
	ld	a, -20 (ix)
	and	a, -14 (ix)
	ld	e, a
	ld	a, -19 (ix)
	and	a, -13 (ix)
	ld	d, a
	C$input.c$105$2_0$109	= .
	.globl	C$input.c$105$2_0$109
;src/engine/input.c:105: } else if(movement_direction & LEFT_ACTION) {
	ld	hl, #_movement_direction
	bit	2, (hl)
	jr	Z, 00165$
	C$input.c$106$3_0$111	= .
	.globl	C$input.c$106$3_0$111
;src/engine/input.c:106: if((keys_held & KEY_RIGHT) && !(keys_held & KEY_LEFT)) {
	ld	a, b
	or	a, c
	jr	Z, 00169$
	ld	a, d
	or	a, e
	jr	NZ, 00169$
	C$input.c$107$4_0$112	= .
	.globl	C$input.c$107$4_0$112
;src/engine/input.c:107: movement_direction = 0;
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$108$4_0$112	= .
	.globl	C$input.c$108$4_0$112
;src/engine/input.c:108: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$109$4_0$112	= .
	.globl	C$input.c$109$4_0$112
;src/engine/input.c:109: input_cnt_direction = 0;
	ld	(_input_cnt_direction), hl
	jr	00169$
00165$:
	C$input.c$111$2_0$109	= .
	.globl	C$input.c$111$2_0$109
;src/engine/input.c:111: } else if(movement_direction & RIGHT_ACTION) {
	ld	hl, #_movement_direction
	bit	3, (hl)
	jr	Z, 00169$
	C$input.c$112$3_0$113	= .
	.globl	C$input.c$112$3_0$113
;src/engine/input.c:112: if((keys_held & KEY_LEFT) && !(keys_held & KEY_RIGHT)) {
	ld	a, d
	or	a, e
	jr	Z, 00169$
	ld	a, b
	or	a, c
	jr	NZ, 00169$
	C$input.c$113$4_0$114	= .
	.globl	C$input.c$113$4_0$114
;src/engine/input.c:113: movement_direction = 0;
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$114$4_0$114	= .
	.globl	C$input.c$114$4_0$114
;src/engine/input.c:114: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$115$4_0$114	= .
	.globl	C$input.c$115$4_0$114
;src/engine/input.c:115: input_cnt_direction = 0;
	ld	(_input_cnt_direction), hl
00169$:
	C$input.c$120$1_0$86	= .
	.globl	C$input.c$120$1_0$86
;src/engine/input.c:120: if((keys_held & KEY_TWO) && !(keys_held & KEY_ONE)) {
	ld	a, -20 (ix)
	and	a, -8 (ix)
	ld	c, a
	ld	a, -19 (ix)
	and	a, -7 (ix)
	ld	b, a
	ld	a, -20 (ix)
	and	a, -10 (ix)
	ld	e, a
	ld	a, -19 (ix)
	and	a, -9 (ix)
	ld	d, a
	C$input.c$119$2_0$109	= .
	.globl	C$input.c$119$2_0$109
;src/engine/input.c:119: if(rotation_input & ROTATION_ONE) {
	ld	hl, #_rotation_input
	bit	4, (hl)
	jr	Z, 00179$
	C$input.c$120$3_0$115	= .
	.globl	C$input.c$120$3_0$115
;src/engine/input.c:120: if((keys_held & KEY_TWO) && !(keys_held & KEY_ONE)) {
	ld	a, b
	or	a, c
	jr	Z, 00182$
	ld	a, d
	or	a, e
	jr	NZ, 00182$
	C$input.c$121$4_0$116	= .
	.globl	C$input.c$121$4_0$116
;src/engine/input.c:121: rotation_input = 0;
	ld	hl, #0x0000
	ld	(_rotation_input), hl
	C$input.c$122$4_0$116	= .
	.globl	C$input.c$122$4_0$116
;src/engine/input.c:122: repeat_rotation = 0;
	ld	(_repeat_rotation), hl
	C$input.c$123$4_0$116	= .
	.globl	C$input.c$123$4_0$116
;src/engine/input.c:123: input_cnt_rotation = 0;
	ld	(_input_cnt_rotation), hl
	jr	00182$
00179$:
	C$input.c$125$2_0$109	= .
	.globl	C$input.c$125$2_0$109
;src/engine/input.c:125: } else if(rotation_input & ROTATION_TWO) {
	ld	hl, #_rotation_input
	bit	5, (hl)
	jr	Z, 00182$
	C$input.c$126$3_0$117	= .
	.globl	C$input.c$126$3_0$117
;src/engine/input.c:126: if((keys_held & KEY_ONE) && !(keys_held & KEY_TWO)) {
	ld	a, d
	or	a, e
	jr	Z, 00182$
	ld	a, b
	or	a, c
	jr	NZ, 00182$
	C$input.c$127$4_0$118	= .
	.globl	C$input.c$127$4_0$118
;src/engine/input.c:127: rotation_input = 0;
	ld	hl, #0x0000
	ld	(_rotation_input), hl
	C$input.c$128$4_0$118	= .
	.globl	C$input.c$128$4_0$118
;src/engine/input.c:128: repeat_rotation = 0;
	ld	(_repeat_rotation), hl
	C$input.c$129$4_0$118	= .
	.globl	C$input.c$129$4_0$118
;src/engine/input.c:129: input_cnt_rotation = 0; 
	ld	(_input_cnt_rotation), hl
00182$:
	C$input.c$133$1_0$86	= .
	.globl	C$input.c$133$1_0$86
;src/engine/input.c:133: if(keys_released != 0) {
	ld	a, -17 (ix)
	or	a, -18 (ix)
	jp	Z, 00204$
	C$input.c$134$2_0$119	= .
	.globl	C$input.c$134$2_0$119
;src/engine/input.c:134: if(keys_released & KEY_DOWN) {
	ld	a, -18 (ix)
	and	a, -16 (ix)
	ld	c, a
	ld	a, -17 (ix)
	and	a, -15 (ix)
	or	a, c
	jr	Z, 00186$
	C$input.c$135$3_0$120	= .
	.globl	C$input.c$135$3_0$120
;src/engine/input.c:135: if(movement_direction & DOWN_ACTION) {
	ld	hl, #_movement_direction
	bit	1, (hl)
	jr	Z, 00186$
	C$input.c$136$4_0$121	= .
	.globl	C$input.c$136$4_0$121
;src/engine/input.c:136: movement_direction = 0;
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$137$4_0$121	= .
	.globl	C$input.c$137$4_0$121
;src/engine/input.c:137: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$138$4_0$121	= .
	.globl	C$input.c$138$4_0$121
;src/engine/input.c:138: input_cnt_direction = 0;
	ld	(_input_cnt_direction), hl
00186$:
	C$input.c$141$2_0$119	= .
	.globl	C$input.c$141$2_0$119
;src/engine/input.c:141: if(keys_released & KEY_LEFT) {
	ld	a, -18 (ix)
	and	a, -14 (ix)
	ld	c, a
	ld	a, -17 (ix)
	and	a, -13 (ix)
	or	a, c
	jr	Z, 00190$
	C$input.c$142$3_0$122	= .
	.globl	C$input.c$142$3_0$122
;src/engine/input.c:142: if(movement_direction & LEFT_ACTION) {
	ld	hl, #_movement_direction
	bit	2, (hl)
	jr	Z, 00190$
	C$input.c$143$4_0$123	= .
	.globl	C$input.c$143$4_0$123
;src/engine/input.c:143: movement_direction = 0;
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$144$4_0$123	= .
	.globl	C$input.c$144$4_0$123
;src/engine/input.c:144: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$145$4_0$123	= .
	.globl	C$input.c$145$4_0$123
;src/engine/input.c:145: input_cnt_rotation = 0;
	ld	(_input_cnt_rotation), hl
00190$:
	C$input.c$148$2_0$119	= .
	.globl	C$input.c$148$2_0$119
;src/engine/input.c:148: if(keys_released & KEY_RIGHT) {
	ld	a, -18 (ix)
	and	a, -12 (ix)
	ld	c, a
	ld	a, -17 (ix)
	and	a, -11 (ix)
	or	a, c
	jr	Z, 00194$
	C$input.c$149$3_0$124	= .
	.globl	C$input.c$149$3_0$124
;src/engine/input.c:149: if(movement_direction & RIGHT_ACTION) {
	ld	hl, #_movement_direction
	bit	3, (hl)
	jr	Z, 00194$
	C$input.c$150$4_0$125	= .
	.globl	C$input.c$150$4_0$125
;src/engine/input.c:150: movement_direction = 0;
	ld	hl, #0x0000
	ld	(_movement_direction), hl
	C$input.c$151$4_0$125	= .
	.globl	C$input.c$151$4_0$125
;src/engine/input.c:151: repeat_movement = 0;
	ld	(_repeat_movement), hl
	C$input.c$152$4_0$125	= .
	.globl	C$input.c$152$4_0$125
;src/engine/input.c:152: input_cnt_rotation = 0;
	ld	(_input_cnt_rotation), hl
00194$:
	C$input.c$155$2_0$119	= .
	.globl	C$input.c$155$2_0$119
;src/engine/input.c:155: if(keys_released & KEY_ONE) {
	ld	a, -18 (ix)
	and	a, -10 (ix)
	ld	c, a
	ld	a, -17 (ix)
	and	a, -9 (ix)
	or	a, c
	jr	Z, 00198$
	C$input.c$156$3_0$126	= .
	.globl	C$input.c$156$3_0$126
;src/engine/input.c:156: if(rotation_input & ROTATION_ONE) {
	ld	hl, #_rotation_input
	bit	4, (hl)
	jr	Z, 00198$
	C$input.c$157$4_0$127	= .
	.globl	C$input.c$157$4_0$127
;src/engine/input.c:157: rotation_input = 0;
	ld	hl, #0x0000
	ld	(_rotation_input), hl
	C$input.c$158$4_0$127	= .
	.globl	C$input.c$158$4_0$127
;src/engine/input.c:158: repeat_rotation = 0;
	ld	(_repeat_rotation), hl
	C$input.c$159$4_0$127	= .
	.globl	C$input.c$159$4_0$127
;src/engine/input.c:159: input_cnt_rotation = 0;
	ld	(_input_cnt_rotation), hl
00198$:
	C$input.c$162$2_0$119	= .
	.globl	C$input.c$162$2_0$119
;src/engine/input.c:162: if(keys_released & KEY_TWO) {
	ld	a, -18 (ix)
	and	a, -8 (ix)
	ld	c, a
	ld	a, -17 (ix)
	and	a, -7 (ix)
	or	a, c
	jr	Z, 00204$
	C$input.c$163$3_0$128	= .
	.globl	C$input.c$163$3_0$128
;src/engine/input.c:163: if(rotation_input & ROTATION_TWO) {
	ld	hl, #_rotation_input
	bit	5, (hl)
	jr	Z, 00204$
	C$input.c$164$4_0$129	= .
	.globl	C$input.c$164$4_0$129
;src/engine/input.c:164: rotation_input = 0;
	ld	hl, #0x0000
	ld	(_rotation_input), hl
	C$input.c$165$4_0$129	= .
	.globl	C$input.c$165$4_0$129
;src/engine/input.c:165: repeat_rotation = 0;
	ld	(_repeat_rotation), hl
	C$input.c$166$4_0$129	= .
	.globl	C$input.c$166$4_0$129
;src/engine/input.c:166: input_cnt_rotation = 0;
	ld	(_input_cnt_rotation), hl
00204$:
	C$input.c$171$1_0$86	= .
	.globl	C$input.c$171$1_0$86
;src/engine/input.c:171: input_cnt_direction++;
	ld	hl, (_input_cnt_direction)
	inc	hl
	ld	(_input_cnt_direction), hl
	C$input.c$172$1_0$86	= .
	.globl	C$input.c$172$1_0$86
;src/engine/input.c:172: input_cnt_rotation++;
	ld	hl, (_input_cnt_rotation)
	inc	hl
	ld	(_input_cnt_rotation), hl
	C$input.c$173$1_0$86	= .
	.globl	C$input.c$173$1_0$86
;src/engine/input.c:173: }
	ld	sp, ix
	pop	ix
	C$input.c$173$1_0$86	= .
	.globl	C$input.c$173$1_0$86
	XG$update_input$0$0	= .
	.globl	XG$update_input$0$0
	ret
	G$getDirection$0$0	= .
	.globl	G$getDirection$0$0
	C$input.c$175$1_0$131	= .
	.globl	C$input.c$175$1_0$131
;src/engine/input.c:175: u16 getDirection(void) {
;	---------------------------------
; Function getDirection
; ---------------------------------
_getDirection::
	C$input.c$176$1_0$131	= .
	.globl	C$input.c$176$1_0$131
;src/engine/input.c:176: if(repeat_movement == 0){
	ld	a, (_repeat_movement+1)
	ld	hl, #_repeat_movement
	or	a, (hl)
	jr	NZ, 00113$
	C$input.c$177$2_0$132	= .
	.globl	C$input.c$177$2_0$132
;src/engine/input.c:177: if(movement_direction != NEUTRAL) 
	ld	a, (_movement_direction+1)
	ld	hl, #_movement_direction
	or	a, (hl)
	jr	Z, 00102$
	C$input.c$178$2_0$132	= .
	.globl	C$input.c$178$2_0$132
;src/engine/input.c:178: repeat_movement = 1;
	ld	hl, #0x0001
	ld	(_repeat_movement), hl
00102$:
	C$input.c$179$2_0$132	= .
	.globl	C$input.c$179$2_0$132
;src/engine/input.c:179: return movement_direction;
	ld	de, (_movement_direction)
	ret
00113$:
	C$input.c$183$1_0$131	= .
	.globl	C$input.c$183$1_0$131
;src/engine/input.c:183: repeat_movement++;
	ld	bc, (_repeat_movement)
	inc	bc
	C$input.c$180$1_0$131	= .
	.globl	C$input.c$180$1_0$131
;src/engine/input.c:180: } else if(repeat_movement == 1){
	ld	a, (_repeat_movement+0)
	dec	a
	ld	hl, #_repeat_movement + 1
	or	a, (hl)
	jr	NZ, 00110$
	C$input.c$181$2_0$133	= .
	.globl	C$input.c$181$2_0$133
;src/engine/input.c:181: if(input_cnt_direction > FIRST_REPEAT_KEY_DELAY) {
	ld	a, #0x14
	ld	iy, #_input_cnt_direction
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jr	NC, 00104$
	C$input.c$182$3_0$134	= .
	.globl	C$input.c$182$3_0$134
;src/engine/input.c:182: input_cnt_direction = 0;
	ld	hl, #0x0000
	ld	(_input_cnt_direction), hl
	C$input.c$183$3_0$134	= .
	.globl	C$input.c$183$3_0$134
;src/engine/input.c:183: repeat_movement++;
	ld	(_repeat_movement), bc
	C$input.c$184$3_0$134	= .
	.globl	C$input.c$184$3_0$134
;src/engine/input.c:184: return movement_direction;
	ld	de, (_movement_direction)
	ret
00104$:
	C$input.c$186$3_0$135	= .
	.globl	C$input.c$186$3_0$135
;src/engine/input.c:186: return NEUTRAL;
	ld	de, #0x0000
	ret
00110$:
	C$input.c$189$2_0$136	= .
	.globl	C$input.c$189$2_0$136
;src/engine/input.c:189: if(input_cnt_direction > REPEAT_KEY_DELAY) {
	ld	a, #0x0a
	ld	iy, #_input_cnt_direction
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jr	NC, 00107$
	C$input.c$190$3_0$137	= .
	.globl	C$input.c$190$3_0$137
;src/engine/input.c:190: input_cnt_direction = 0;
	ld	hl, #0x0000
	ld	(_input_cnt_direction), hl
	C$input.c$191$3_0$137	= .
	.globl	C$input.c$191$3_0$137
;src/engine/input.c:191: repeat_movement++;
	ld	(_repeat_movement), bc
	C$input.c$192$3_0$137	= .
	.globl	C$input.c$192$3_0$137
;src/engine/input.c:192: return movement_direction;
	ld	de, (_movement_direction)
	ret
00107$:
	C$input.c$194$3_0$138	= .
	.globl	C$input.c$194$3_0$138
;src/engine/input.c:194: return NEUTRAL;
	ld	de, #0x0000
	C$input.c$197$1_0$131	= .
	.globl	C$input.c$197$1_0$131
;src/engine/input.c:197: }
	C$input.c$197$1_0$131	= .
	.globl	C$input.c$197$1_0$131
	XG$getDirection$0$0	= .
	.globl	XG$getDirection$0$0
	ret
	G$getRotation$0$0	= .
	.globl	G$getRotation$0$0
	C$input.c$199$1_0$140	= .
	.globl	C$input.c$199$1_0$140
;src/engine/input.c:199: u16 getRotation(void) {
;	---------------------------------
; Function getRotation
; ---------------------------------
_getRotation::
	C$input.c$200$1_0$140	= .
	.globl	C$input.c$200$1_0$140
;src/engine/input.c:200: if(repeat_rotation == 0){
	ld	a, (_repeat_rotation+1)
	ld	hl, #_repeat_rotation
	or	a, (hl)
	jr	NZ, 00113$
	C$input.c$201$2_0$141	= .
	.globl	C$input.c$201$2_0$141
;src/engine/input.c:201: if(rotation_input != NEUTRAL)
	ld	a, (_rotation_input+1)
	ld	hl, #_rotation_input
	or	a, (hl)
	jr	Z, 00102$
	C$input.c$202$2_0$141	= .
	.globl	C$input.c$202$2_0$141
;src/engine/input.c:202: repeat_rotation = 1;
	ld	hl, #0x0001
	ld	(_repeat_rotation), hl
00102$:
	C$input.c$203$2_0$141	= .
	.globl	C$input.c$203$2_0$141
;src/engine/input.c:203: return rotation_input;
	ld	de, (_rotation_input)
	ret
00113$:
	C$input.c$204$1_0$140	= .
	.globl	C$input.c$204$1_0$140
;src/engine/input.c:204: } else if(repeat_rotation == 1){
	ld	a, (_repeat_rotation+0)
	dec	a
	ld	hl, #_repeat_rotation + 1
	or	a, (hl)
	jr	NZ, 00110$
	C$input.c$205$2_0$142	= .
	.globl	C$input.c$205$2_0$142
;src/engine/input.c:205: if(input_cnt_rotation > FIRST_REPEAT_KEY_DELAY) {
	ld	a, #0x14
	ld	iy, #_input_cnt_rotation
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jr	NC, 00104$
	C$input.c$206$3_0$143	= .
	.globl	C$input.c$206$3_0$143
;src/engine/input.c:206: input_cnt_rotation = 0;
	ld	hl, #0x0000
	ld	(_input_cnt_rotation), hl
	C$input.c$207$3_0$143	= .
	.globl	C$input.c$207$3_0$143
;src/engine/input.c:207: return rotation_input;
	ld	de, (_rotation_input)
	ret
00104$:
	C$input.c$209$3_0$144	= .
	.globl	C$input.c$209$3_0$144
;src/engine/input.c:209: return NEUTRAL;
	ld	de, #0x0000
	ret
00110$:
	C$input.c$212$2_0$145	= .
	.globl	C$input.c$212$2_0$145
;src/engine/input.c:212: if(input_cnt_rotation > REPEAT_KEY_DELAY) {
	ld	a, #0x0a
	ld	iy, #_input_cnt_rotation
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jr	NC, 00107$
	C$input.c$213$3_0$146	= .
	.globl	C$input.c$213$3_0$146
;src/engine/input.c:213: input_cnt_rotation = 0;
	ld	hl, #0x0000
	ld	(_input_cnt_rotation), hl
	C$input.c$214$3_0$146	= .
	.globl	C$input.c$214$3_0$146
;src/engine/input.c:214: return rotation_input;
	ld	de, (_rotation_input)
	ret
00107$:
	C$input.c$216$3_0$147	= .
	.globl	C$input.c$216$3_0$147
;src/engine/input.c:216: return NEUTRAL;
	ld	de, #0x0000
	C$input.c$219$1_0$140	= .
	.globl	C$input.c$219$1_0$140
;src/engine/input.c:219: }
	C$input.c$219$1_0$140	= .
	.globl	C$input.c$219$1_0$140
	XG$getRotation$0$0	= .
	.globl	XG$getRotation$0$0
	ret
	G$getButtonsPressed$0$0	= .
	.globl	G$getButtonsPressed$0$0
	C$input.c$221$1_0$149	= .
	.globl	C$input.c$221$1_0$149
;src/engine/input.c:221: u16 getButtonsPressed(void) {
;	---------------------------------
; Function getButtonsPressed
; ---------------------------------
_getButtonsPressed::
	C$input.c$222$1_0$149	= .
	.globl	C$input.c$222$1_0$149
;src/engine/input.c:222: return button_input;
	ld	de, (_button_input)
	C$input.c$223$1_0$149	= .
	.globl	C$input.c$223$1_0$149
;src/engine/input.c:223: }
	C$input.c$223$1_0$149	= .
	.globl	C$input.c$223$1_0$149
	XG$getButtonsPressed$0$0	= .
	.globl	XG$getButtonsPressed$0$0
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
