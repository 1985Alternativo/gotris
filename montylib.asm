;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module montylib
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGSFXFrame
	.globl _PSGFrame
	.globl _PSGSFXGetStatus
	.globl _PSGSetMusicVolumeAttenuation
	.globl _PSGGetStatus
	.globl _SMS_VRAMmemcpy
	.globl _SMS_copySpritestoSAT
	.globl _SMS_crt0_RST18
	.globl _SMS_crt0_RST08
	.globl _SMS_waitForVBlank
	.globl _SMS_setSpriteMode
	.globl _SMS_useFirstHalfTilesforSprites
	.globl _SMS_VDPturnOnFeature
	.globl _SMS_init
	.globl _free
	.globl _malloc
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _init_console
	.globl _clear_tilemap
	.globl _waitForFrame
	.globl _waitForFrameNoMusic
	.globl _get_highscore_position
	.globl _insert_highscore_position
	.globl _get_highscore_position_char
	.globl _update_highscore_position
	.globl _get_rand
	.globl _setTileMapToHighPriority
	.globl _createList
	.globl _addElementBeginingList
	.globl _addElementEndList
	.globl _deleteElementBeginingList
	.globl _deleteElementEndList
	.globl _deleteElementPosList
	.globl _deleteElement_Unsafe
	.globl _destroyList
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$init_console$0$0	= .
	.globl	G$init_console$0$0
	C$montylib.c$14$0_0$169	= .
	.globl	C$montylib.c$14$0_0$169
;src/libs/montylib.c:14: void init_console(void) {
;	---------------------------------
; Function init_console
; ---------------------------------
_init_console::
	C$montylib.c$15$1_0$169	= .
	.globl	C$montylib.c$15$1_0$169
;src/libs/montylib.c:15: SMS_init();
	call	_SMS_init
	C$montylib.c$16$1_0$169	= .
	.globl	C$montylib.c$16$1_0$169
;src/libs/montylib.c:16: SMS_setSpriteMode(SPRITEMODE_NORMAL);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setSpriteMode
	C$montylib.c$17$1_0$169	= .
	.globl	C$montylib.c$17$1_0$169
;src/libs/montylib.c:17: SMS_useFirstHalfTilesforSprites(true);
	ld	l, #0x01
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_useFirstHalfTilesforSprites
	C$montylib.c$18$1_0$169	= .
	.globl	C$montylib.c$18$1_0$169
;src/libs/montylib.c:18: SMS_VDPturnOnFeature(VDPFEATURE_LEFTCOLBLANK);
	ld	hl, #0x0020
	call	_SMS_VDPturnOnFeature
	C$montylib.c$19$1_0$169	= .
	.globl	C$montylib.c$19$1_0$169
;src/libs/montylib.c:19: SMS_displayOn();
	ld	hl, #0x0140
	call	_SMS_VDPturnOnFeature
	C$montylib.c$20$1_0$169	= .
	.globl	C$montylib.c$20$1_0$169
;src/libs/montylib.c:20: pause = false;
	ld	hl, #_pause
	ld	(hl), #0x00
	C$montylib.c$21$1_0$169	= .
	.globl	C$montylib.c$21$1_0$169
;src/libs/montylib.c:21: rand_index = 0;
	ld	hl, #_rand_index
	ld	(hl), #0x00
	C$montylib.c$22$1_0$169	= .
	.globl	C$montylib.c$22$1_0$169
;src/libs/montylib.c:22: current_music_bank = 0;
	ld	hl, #_current_music_bank
	ld	(hl), #0x00
	C$montylib.c$23$1_0$169	= .
	.globl	C$montylib.c$23$1_0$169
;src/libs/montylib.c:23: current_fx_bank = 0;
	ld	hl, #_current_fx_bank
	ld	(hl), #0x00
	C$montylib.c$24$1_0$169	= .
	.globl	C$montylib.c$24$1_0$169
;src/libs/montylib.c:24: current_resource_bank = 0;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x00
	C$montylib.c$25$1_0$169	= .
	.globl	C$montylib.c$25$1_0$169
;src/libs/montylib.c:25: }
	C$montylib.c$25$1_0$169	= .
	.globl	C$montylib.c$25$1_0$169
	XG$init_console$0$0	= .
	.globl	XG$init_console$0$0
	ret
	G$clear_tilemap$0$0	= .
	.globl	G$clear_tilemap$0$0
	C$montylib.c$27$1_0$171	= .
	.globl	C$montylib.c$27$1_0$171
;src/libs/montylib.c:27: void clear_tilemap(void) {
;	---------------------------------
; Function clear_tilemap
; ---------------------------------
_clear_tilemap::
	C$montylib.c$29$1_0$171	= .
	.globl	C$montylib.c$29$1_0$171
;src/libs/montylib.c:29: SMS_setNextTileatXY(0,0);
	ld	hl, #0x7800
	rst	#0x08
	C$montylib.c$30$2_0$171	= .
	.globl	C$montylib.c$30$2_0$171
;src/libs/montylib.c:30: for(j=0;j<SCREEN_ROWS;j++) {
	ld	c, #0x00
	C$montylib.c$31$2_0$171	= .
	.globl	C$montylib.c$31$2_0$171
;src/libs/montylib.c:31: for(i=0;i<SCREEN_COLUMNS;i++) {
00109$:
	ld	b, #0x00
00103$:
	C$montylib.c$32$5_0$175	= .
	.globl	C$montylib.c$32$5_0$175
;src/libs/montylib.c:32: SMS_setTile(0);
	ld	hl, #0x0000
	rst	#0x18
	C$montylib.c$31$4_0$174	= .
	.globl	C$montylib.c$31$4_0$174
;src/libs/montylib.c:31: for(i=0;i<SCREEN_COLUMNS;i++) {
	inc	b
	ld	a, b
	sub	a, #0x20
	jr	C, 00103$
	C$montylib.c$30$2_0$172	= .
	.globl	C$montylib.c$30$2_0$172
;src/libs/montylib.c:30: for(j=0;j<SCREEN_ROWS;j++) {
	inc	c
	ld	a, c
	sub	a, #0x18
	jr	C, 00109$
	C$montylib.c$35$2_0$171	= .
	.globl	C$montylib.c$35$2_0$171
;src/libs/montylib.c:35: }
	C$montylib.c$35$2_0$171	= .
	.globl	C$montylib.c$35$2_0$171
	XG$clear_tilemap$0$0	= .
	.globl	XG$clear_tilemap$0$0
	ret
	G$waitForFrame$0$0	= .
	.globl	G$waitForFrame$0$0
	C$montylib.c$40$2_0$177	= .
	.globl	C$montylib.c$40$2_0$177
;src/libs/montylib.c:40: void waitForFrame(void) {
;	---------------------------------
; Function waitForFrame
; ---------------------------------
_waitForFrame::
	C$montylib.c$41$1_0$177	= .
	.globl	C$montylib.c$41$1_0$177
;src/libs/montylib.c:41: SMS_mapROMBank(current_music_bank);
	ld	a, (_current_music_bank+0)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$montylib.c$42$1_0$177	= .
	.globl	C$montylib.c$42$1_0$177
;src/libs/montylib.c:42: if(PSGGetStatus() == PSG_PLAYING){
	call	_PSGGetStatus
	dec	a
	jr	NZ, 00107$
	C$montylib.c$44$1_0$177	= .
	.globl	C$montylib.c$44$1_0$177
;src/libs/montylib.c:44: current_atenuation = current_atenuation - 4;
	ld	a, (_current_atenuation+0)
	ld	c, a
	C$montylib.c$43$2_0$178	= .
	.globl	C$montylib.c$43$2_0$178
;src/libs/montylib.c:43: if(target_atenuation < current_atenuation) {
	ld	a, (_target_atenuation+0)
	ld	iy, #_current_atenuation
	sub	a, 0 (iy)
	jr	NC, 00104$
	C$montylib.c$44$3_0$179	= .
	.globl	C$montylib.c$44$3_0$179
;src/libs/montylib.c:44: current_atenuation = current_atenuation - 4;
	ld	hl, #_current_atenuation
	ld	a, c
	add	a, #0xfc
	ld	(hl), a
	C$montylib.c$45$3_0$179	= .
	.globl	C$montylib.c$45$3_0$179
;src/libs/montylib.c:45: PSGSetMusicVolumeAttenuation(current_atenuation>>4);
	ld	c, 0 (iy)
	srl	c
	srl	c
	srl	c
	srl	c
	ld	a, c
	call	_PSGSetMusicVolumeAttenuation
	jr	00105$
00104$:
	C$montylib.c$46$2_0$178	= .
	.globl	C$montylib.c$46$2_0$178
;src/libs/montylib.c:46: } else if(target_atenuation > current_atenuation) {
	ld	a, (_current_atenuation+0)
	ld	hl, #_target_atenuation
	sub	a, (hl)
	jr	NC, 00105$
	C$montylib.c$47$3_0$180	= .
	.globl	C$montylib.c$47$3_0$180
;src/libs/montylib.c:47: current_atenuation = current_atenuation + 4;
	ld	a, c
	add	a, #0x04
	ld	(_current_atenuation+0), a
	C$montylib.c$48$3_0$180	= .
	.globl	C$montylib.c$48$3_0$180
;src/libs/montylib.c:48: PSGSetMusicVolumeAttenuation(current_atenuation>>4);
	ld	a, (#_current_atenuation + 0)
	ld	c, a
	srl	c
	srl	c
	srl	c
	srl	c
	ld	a, c
	call	_PSGSetMusicVolumeAttenuation
00105$:
	C$montylib.c$50$2_0$178	= .
	.globl	C$montylib.c$50$2_0$178
;src/libs/montylib.c:50: PSGFrame();
	call	_PSGFrame
00107$:
	C$montylib.c$52$1_0$177	= .
	.globl	C$montylib.c$52$1_0$177
;src/libs/montylib.c:52: SMS_mapROMBank(current_fx_bank);
	ld	a, (_current_fx_bank+0)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$montylib.c$53$1_0$177	= .
	.globl	C$montylib.c$53$1_0$177
;src/libs/montylib.c:53: if(PSGSFXGetStatus() == PSG_PLAYING){
	call	_PSGSFXGetStatus
	dec	a
	jr	NZ, 00109$
	C$montylib.c$54$2_0$181	= .
	.globl	C$montylib.c$54$2_0$181
;src/libs/montylib.c:54: PSGSFXFrame();
	call	_PSGSFXFrame
00109$:
	C$montylib.c$56$1_0$177	= .
	.globl	C$montylib.c$56$1_0$177
;src/libs/montylib.c:56: SMS_mapROMBank(current_resource_bank); 
	ld	a, (_current_resource_bank+0)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$montylib.c$57$1_0$177	= .
	.globl	C$montylib.c$57$1_0$177
;src/libs/montylib.c:57: waitForFrameNoMusic();
	C$montylib.c$58$1_0$177	= .
	.globl	C$montylib.c$58$1_0$177
;src/libs/montylib.c:58: }
	C$montylib.c$58$1_0$177	= .
	.globl	C$montylib.c$58$1_0$177
	XG$waitForFrame$0$0	= .
	.globl	XG$waitForFrame$0$0
	jp	_waitForFrameNoMusic
	G$waitForFrameNoMusic$0$0	= .
	.globl	G$waitForFrameNoMusic$0$0
	C$montylib.c$60$1_0$183	= .
	.globl	C$montylib.c$60$1_0$183
;src/libs/montylib.c:60: void waitForFrameNoMusic(void) {
;	---------------------------------
; Function waitForFrameNoMusic
; ---------------------------------
_waitForFrameNoMusic::
	C$montylib.c$62$1_0$183	= .
	.globl	C$montylib.c$62$1_0$183
;src/libs/montylib.c:62: SMS_waitForVBlank();
	call	_SMS_waitForVBlank
	C$montylib.c$63$1_0$183	= .
	.globl	C$montylib.c$63$1_0$183
;src/libs/montylib.c:63: SMS_copySpritestoSAT();
	call	_SMS_copySpritestoSAT
	C$montylib.c$64$1_0$183	= .
	.globl	C$montylib.c$64$1_0$183
;src/libs/montylib.c:64: frame_cnt++;
	ld	hl, (_frame_cnt)
	inc	hl
	ld	(_frame_cnt), hl
	C$montylib.c$65$1_0$183	= .
	.globl	C$montylib.c$65$1_0$183
;src/libs/montylib.c:65: }
	C$montylib.c$65$1_0$183	= .
	.globl	C$montylib.c$65$1_0$183
	XG$waitForFrameNoMusic$0$0	= .
	.globl	XG$waitForFrameNoMusic$0$0
	ret
	G$get_highscore_position$0$0	= .
	.globl	G$get_highscore_position$0$0
	C$montylib.c$68$1_0$185	= .
	.globl	C$montylib.c$68$1_0$185
;src/libs/montylib.c:68: u8 get_highscore_position(u16 score) {
;	---------------------------------
; Function get_highscore_position
; ---------------------------------
_get_highscore_position::
	ex	de, hl
	C$montylib.c$70$1_0$185	= .
	.globl	C$montylib.c$70$1_0$185
;src/libs/montylib.c:70: if( highscore_table.marker1!= 69 || highscore_table.marker2 != 148) {
	ld	a, (#_highscore_table + 0)
	sub	a, #0x45
	jr	NZ, 00104$
	ld	a, (#_highscore_table + 1)
	sub	a, #0x94
	jr	Z, 00105$
00104$:
	C$montylib.c$71$2_0$186	= .
	.globl	C$montylib.c$71$2_0$186
;src/libs/montylib.c:71: return 69;
	ld	a, #0x45
	ret
00105$:
	C$montylib.c$73$3_0$188	= .
	.globl	C$montylib.c$73$3_0$188
;src/libs/montylib.c:73: for(sramIndex=9;sramIndex>=0;sramIndex--) {
	ld	bc, #0x909
00108$:
	C$montylib.c$74$4_0$189	= .
	.globl	C$montylib.c$74$4_0$189
;src/libs/montylib.c:74: if(highscore_table.table[sramIndex].score > score) {
	ld	a, c
	add	a, a
	add	a, a
	add	a, c
	add	a, #<((_highscore_table + 2))
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x00
	adc	a, #>((_highscore_table + 2))
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	inc	hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, e
	sub	a, l
	ld	a, d
	sbc	a, h
	jr	NC, 00109$
	C$montylib.c$75$5_0$190	= .
	.globl	C$montylib.c$75$5_0$190
;src/libs/montylib.c:75: SMS_disableSRAM();
	ld	hl, #_SRAM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x00
	C$montylib.c$76$5_0$190	= .
	.globl	C$montylib.c$76$5_0$190
;src/libs/montylib.c:76: return sramIndex + 1;
	ld	a, b
	inc	a
	ret
00109$:
	C$montylib.c$73$3_0$188	= .
	.globl	C$montylib.c$73$3_0$188
;src/libs/montylib.c:73: for(sramIndex=9;sramIndex>=0;sramIndex--) {
	dec	c
	ld	b, c
	bit	7, c
	jr	Z, 00108$
	C$montylib.c$79$2_0$187	= .
	.globl	C$montylib.c$79$2_0$187
;src/libs/montylib.c:79: return 0;
	xor	a, a
	C$montylib.c$81$1_0$185	= .
	.globl	C$montylib.c$81$1_0$185
;src/libs/montylib.c:81: }
	C$montylib.c$81$1_0$185	= .
	.globl	C$montylib.c$81$1_0$185
	XG$get_highscore_position$0$0	= .
	.globl	XG$get_highscore_position$0$0
	ret
	G$insert_highscore_position$0$0	= .
	.globl	G$insert_highscore_position$0$0
	C$montylib.c$83$1_0$192	= .
	.globl	C$montylib.c$83$1_0$192
;src/libs/montylib.c:83: void insert_highscore_position(u8 scorePosition, u16 score) {
;	---------------------------------
; Function insert_highscore_position
; ---------------------------------
_insert_highscore_position::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-8
	add	hl, sp
	ld	sp, hl
	ld	-2 (ix), a
	C$montylib.c$86$3_0$194	= .
	.globl	C$montylib.c$86$3_0$194
;src/libs/montylib.c:86: for(sramIndex=8;sramIndex>=scorePosition;sramIndex--) {
	ld	-1 (ix), #0x08
00103$:
	ld	a, -1 (ix)
	sub	a, -2 (ix)
	jp	C, 00101$
	C$montylib.c$87$3_0$194	= .
	.globl	C$montylib.c$87$3_0$194
;src/libs/montylib.c:87: highscore_table.table[sramIndex+1].name[0] = highscore_table.table[sramIndex].name[0];
	ld	a, -1 (ix)
	inc	a
	ld	c, a
	add	a, a
	add	a, a
	add	a, c
	ld	c, a
	add	a, #<((_highscore_table + 2))
	ld	-8 (ix), a
	ld	a, #0x00
	adc	a, #>((_highscore_table + 2))
	ld	-7 (ix), a
	push	de
	ld	a, -1 (ix)
	ld	e, a
	add	a, a
	add	a, a
	add	a, e
	pop	de
	ld	-3 (ix), a
	add	a, #<((_highscore_table + 2))
	ld	-6 (ix), a
	ld	a, #0x00
	adc	a, #>((_highscore_table + 2))
	ld	-5 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	pop	hl
	push	hl
	ld	(hl), a
	C$montylib.c$88$3_0$194	= .
	.globl	C$montylib.c$88$3_0$194
;src/libs/montylib.c:88: highscore_table.table[sramIndex+1].name[1] = highscore_table.table[sramIndex].name[1];
	ld	iy, #(_highscore_table + 2)
	ld	b, #0x00
	add	iy, bc
	push	iy
	pop	bc
	inc	bc
	ld	a, -3 (ix)
	add	a, #<((_highscore_table + 2))
	ld	-4 (ix), a
	ld	a, #0x00
	adc	a, #>((_highscore_table + 2))
	ld	-3 (ix), a
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	ld	a, (hl)
	ld	(bc), a
	C$montylib.c$89$3_0$194	= .
	.globl	C$montylib.c$89$3_0$194
;src/libs/montylib.c:89: highscore_table.table[sramIndex+1].name[2] = highscore_table.table[sramIndex].name[2];
	push	iy
	pop	bc
	inc	bc
	inc	bc
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	a, (hl)
	ld	(bc), a
	C$montylib.c$90$3_0$194	= .
	.globl	C$montylib.c$90$3_0$194
;src/libs/montylib.c:90: highscore_table.table[sramIndex+1].score = highscore_table.table[sramIndex].score;
	pop	bc
	push	bc
	inc	bc
	inc	bc
	inc	bc
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	inc	hl
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	ld	a, -4 (ix)
	ld	(bc), a
	inc	bc
	ld	a, -3 (ix)
	ld	(bc), a
	C$montylib.c$86$2_0$193	= .
	.globl	C$montylib.c$86$2_0$193
;src/libs/montylib.c:86: for(sramIndex=8;sramIndex>=scorePosition;sramIndex--) {
	dec	-1 (ix)
	jp	00103$
00101$:
	C$montylib.c$92$1_0$192	= .
	.globl	C$montylib.c$92$1_0$192
;src/libs/montylib.c:92: highscore_table.table[scorePosition].name[0] = 0;
	ld	a, -2 (ix)
	ld	c, a
	add	a, a
	add	a, a
	add	a, c
	ld	-1 (ix), a
	add	a, #<((_highscore_table + 2))
	ld	c, a
	ld	a, #0x00
	adc	a, #>((_highscore_table + 2))
	ld	b, a
	xor	a, a
	ld	(bc), a
	C$montylib.c$93$1_0$192	= .
	.globl	C$montylib.c$93$1_0$192
;src/libs/montylib.c:93: highscore_table.table[scorePosition].name[1] = 0;
	ld	iy, #(_highscore_table + 2)
	push	bc
	ld	c, -1 (ix)
	ld	b, #0x00
	add	iy, bc
	pop	bc
	push	iy
	pop	hl
	inc	hl
	ld	(hl), #0x00
	C$montylib.c$94$1_0$192	= .
	.globl	C$montylib.c$94$1_0$192
;src/libs/montylib.c:94: highscore_table.table[scorePosition].name[2] = 0;
	push	iy
	pop	hl
	inc	hl
	inc	hl
	ld	(hl), #0x00
	C$montylib.c$95$1_0$192	= .
	.globl	C$montylib.c$95$1_0$192
;src/libs/montylib.c:95: highscore_table.table[scorePosition].score = score;
	inc	bc
	inc	bc
	inc	bc
	ld	a, e
	ld	(bc), a
	inc	bc
	ld	a, d
	ld	(bc), a
	C$montylib.c$97$1_0$192	= .
	.globl	C$montylib.c$97$1_0$192
;src/libs/montylib.c:97: }
	ld	sp, ix
	pop	ix
	C$montylib.c$97$1_0$192	= .
	.globl	C$montylib.c$97$1_0$192
	XG$insert_highscore_position$0$0	= .
	.globl	XG$insert_highscore_position$0$0
	ret
	G$get_highscore_position_char$0$0	= .
	.globl	G$get_highscore_position_char$0$0
	C$montylib.c$99$1_0$196	= .
	.globl	C$montylib.c$99$1_0$196
;src/libs/montylib.c:99: u8 get_highscore_position_char(u8 scorePosition, u8 charPosition) {
;	---------------------------------
; Function get_highscore_position_char
; ---------------------------------
_get_highscore_position_char::
	ld	c, a
	ld	e, l
	C$montylib.c$101$1_0$196	= .
	.globl	C$montylib.c$101$1_0$196
;src/libs/montylib.c:101: charValue = highscore_table.table[scorePosition].name[charPosition];
	ld	a, c
	add	a, a
	add	a, a
	add	a, c
	ld	c, a
	ld	hl, #(_highscore_table + 2)
	ld	b, #0x00
	add	hl, bc
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	C$montylib.c$102$1_0$196	= .
	.globl	C$montylib.c$102$1_0$196
;src/libs/montylib.c:102: return charValue;
	C$montylib.c$103$1_0$196	= .
	.globl	C$montylib.c$103$1_0$196
;src/libs/montylib.c:103: }
	C$montylib.c$103$1_0$196	= .
	.globl	C$montylib.c$103$1_0$196
	XG$get_highscore_position_char$0$0	= .
	.globl	XG$get_highscore_position_char$0$0
	ret
	G$update_highscore_position$0$0	= .
	.globl	G$update_highscore_position$0$0
	C$montylib.c$105$1_0$198	= .
	.globl	C$montylib.c$105$1_0$198
;src/libs/montylib.c:105: void update_highscore_position(u8 scorePosition, u8 charPosition, u8 charValue) {  
;	---------------------------------
; Function update_highscore_position
; ---------------------------------
_update_highscore_position::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	c, a
	ld	e, l
	C$montylib.c$106$1_0$198	= .
	.globl	C$montylib.c$106$1_0$198
;src/libs/montylib.c:106: highscore_table.table[scorePosition].name[charPosition] = charValue;
	ld	a, c
	add	a, a
	add	a, a
	add	a, c
	ld	c, a
	ld	hl, #(_highscore_table + 2)
	ld	b, #0x00
	add	hl, bc
	ld	d, #0x00
	add	hl, de
	ld	a, 4 (ix)
	ld	(hl), a
	C$montylib.c$107$1_0$198	= .
	.globl	C$montylib.c$107$1_0$198
;src/libs/montylib.c:107: }
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$get_rand$0$0	= .
	.globl	G$get_rand$0$0
	C$montylib.c$109$1_0$200	= .
	.globl	C$montylib.c$109$1_0$200
;src/libs/montylib.c:109: u8 get_rand(void) {
;	---------------------------------
; Function get_rand
; ---------------------------------
_get_rand::
	C$montylib.c$110$1_0$200	= .
	.globl	C$montylib.c$110$1_0$200
;src/libs/montylib.c:110: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$montylib.c$111$1_0$200	= .
	.globl	C$montylib.c$111$1_0$200
;src/libs/montylib.c:111: return randLUT[rand_index++]; 
	ld	a, (_rand_index+0)
	ld	c, a
	ld	hl, #_rand_index
	inc	(hl)
	ld	hl, #_randLUT
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	C$montylib.c$112$1_0$200	= .
	.globl	C$montylib.c$112$1_0$200
;src/libs/montylib.c:112: }
	C$montylib.c$112$1_0$200	= .
	.globl	C$montylib.c$112$1_0$200
	XG$get_rand$0$0	= .
	.globl	XG$get_rand$0$0
	ret
	G$setTileMapToHighPriority$0$0	= .
	.globl	G$setTileMapToHighPriority$0$0
	C$montylib.c$114$1_0$202	= .
	.globl	C$montylib.c$114$1_0$202
;src/libs/montylib.c:114: void setTileMapToHighPriority(u8 x, u8 y) {
;	---------------------------------
; Function setTileMapToHighPriority
; ---------------------------------
_setTileMapToHighPriority::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, a
	ld	b, l
	C$montylib.c$115$2_0$202	= .
	.globl	C$montylib.c$115$2_0$202
;src/libs/montylib.c:115: u8 value[1] = {0x10}; //FIXME: It only works by chance...
	ld	-1 (ix), #0x10
	C$montylib.c$116$1_0$202	= .
	.globl	C$montylib.c$116$1_0$202
;src/libs/montylib.c:116: SMS_VRAMmemcpy(XYtoADDR((x),(y))+1,value,1);
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	ld	de, #0x0001
	ex	de, hl
	push	hl
	ld	hl, #2
	add	hl, sp
	ex	de, hl
	call	_SMS_VRAMmemcpy
	C$montylib.c$117$1_0$202	= .
	.globl	C$montylib.c$117$1_0$202
;src/libs/montylib.c:117: }
	inc	sp
	pop	ix
	C$montylib.c$117$1_0$202	= .
	.globl	C$montylib.c$117$1_0$202
	XG$setTileMapToHighPriority$0$0	= .
	.globl	XG$setTileMapToHighPriority$0$0
	ret
	G$createList$0$0	= .
	.globl	G$createList$0$0
	C$montylib.c$119$1_0$204	= .
	.globl	C$montylib.c$119$1_0$204
;src/libs/montylib.c:119: linked_list_t* createList(int typeSize) {
;	---------------------------------
; Function createList
; ---------------------------------
_createList::
	C$montylib.c$120$1_0$204	= .
	.globl	C$montylib.c$120$1_0$204
;src/libs/montylib.c:120: linked_list_t* newList = malloc(sizeof(linked_list_t));
	push	hl
	ld	hl, #0x0007
	call	_malloc
	pop	bc
	C$montylib.c$121$1_0$204	= .
	.globl	C$montylib.c$121$1_0$204
;src/libs/montylib.c:121: newList->head = NULL;
	ld	l, e
	ld	h, d
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$122$1_0$204	= .
	.globl	C$montylib.c$122$1_0$204
;src/libs/montylib.c:122: newList->tail = NULL;
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$123$1_0$204	= .
	.globl	C$montylib.c$123$1_0$204
;src/libs/montylib.c:123: newList->numElements = 0;
	ld	hl, #0x0004
	add	hl, de
	ld	(hl), #0x00
	C$montylib.c$124$1_0$204	= .
	.globl	C$montylib.c$124$1_0$204
;src/libs/montylib.c:124: newList->typeSize = typeSize;
	ld	hl, #0x0005
	add	hl, de
	ld	(hl), c
	inc	hl
	ld	(hl), b
	C$montylib.c$125$1_0$204	= .
	.globl	C$montylib.c$125$1_0$204
;src/libs/montylib.c:125: return newList;
	C$montylib.c$126$1_0$204	= .
	.globl	C$montylib.c$126$1_0$204
;src/libs/montylib.c:126: }
	C$montylib.c$126$1_0$204	= .
	.globl	C$montylib.c$126$1_0$204
	XG$createList$0$0	= .
	.globl	XG$createList$0$0
	ret
	G$addElementBeginingList$0$0	= .
	.globl	G$addElementBeginingList$0$0
	C$montylib.c$127$1_0$206	= .
	.globl	C$montylib.c$127$1_0$206
;src/libs/montylib.c:127: void addElementBeginingList(linked_list_t* list, void * element) {
;	---------------------------------
; Function addElementBeginingList
; ---------------------------------
_addElementBeginingList::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	push	af
	ld	c, l
	ld	b, h
	ld	-2 (ix), e
	ld	-1 (ix), d
	C$montylib.c$129$1_0$206	= .
	.globl	C$montylib.c$129$1_0$206
;src/libs/montylib.c:129: new_head = (list_node_t *) malloc(sizeof(list_node_t));
	push	bc
	ld	hl, #0x0006
	call	_malloc
	pop	bc
	C$montylib.c$130$1_0$206	= .
	.globl	C$montylib.c$130$1_0$206
;src/libs/montylib.c:130: if(new_head == NULL) {
	ld	a, d
	or	a, e
	C$montylib.c$131$2_0$207	= .
	.globl	C$montylib.c$131$2_0$207
;src/libs/montylib.c:131: return;
	jp	Z,00107$
	C$montylib.c$133$1_0$206	= .
	.globl	C$montylib.c$133$1_0$206
;src/libs/montylib.c:133: new_head->data = malloc(list->typeSize);
	ld	hl, #0x0005
	add	hl, bc
	ex	(sp), hl
	pop	hl
	push	hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	push	de
	call	_malloc
	ld	-4 (ix), e
	ld	-3 (ix), d
	pop	de
	pop	bc
	ld	l, e
	ld	h, d
	ld	a, -4 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -3 (ix)
	ld	(hl), a
	C$montylib.c$134$1_0$206	= .
	.globl	C$montylib.c$134$1_0$206
;src/libs/montylib.c:134: memcpy(new_head->data, element, list->typeSize);
	pop	hl
	push	hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	de
	push	bc
	ld	c, l
	ld	b, h
	ld	e, -4 (ix)
	ld	d, -3 (ix)
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, b
	or	a, c
	jr	Z, 00124$
	ldir
00124$:
	pop	bc
	pop	de
	C$montylib.c$135$1_0$206	= .
	.globl	C$montylib.c$135$1_0$206
;src/libs/montylib.c:135: new_head->next = list->head;
	ld	hl, #0x0002
	add	hl, de
	ex	(sp), hl
	ld	a, (bc)
	ld	-4 (ix), a
	inc	bc
	ld	a, (bc)
	ld	-3 (ix), a
	dec	bc
	pop	hl
	push	hl
	ld	a, -4 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -3 (ix)
	ld	(hl), a
	C$montylib.c$136$1_0$206	= .
	.globl	C$montylib.c$136$1_0$206
;src/libs/montylib.c:136: new_head->prev = NULL;
	ld	hl, #0x0004
	add	hl, de
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$137$1_0$206	= .
	.globl	C$montylib.c$137$1_0$206
;src/libs/montylib.c:137: if(list->head != NULL)
	ld	l, c
	ld	h, b
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
;	spillPairReg hl
;	spillPairReg hl
	ld	l,a
	or	a,h
	jr	Z, 00104$
	C$montylib.c$138$1_0$206	= .
	.globl	C$montylib.c$138$1_0$206
;src/libs/montylib.c:138: list->head->prev = new_head;
	inc	hl
	inc	hl
	inc	hl
	inc	hl
	ld	(hl), e
	inc	hl
	ld	(hl), d
00104$:
	C$montylib.c$139$1_0$206	= .
	.globl	C$montylib.c$139$1_0$206
;src/libs/montylib.c:139: if(list->tail == NULL)
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	dec	hl
	ld	a, -3 (ix)
	or	a, -4 (ix)
	jr	NZ, 00106$
	C$montylib.c$140$1_0$206	= .
	.globl	C$montylib.c$140$1_0$206
;src/libs/montylib.c:140: list->tail = new_head;
	ld	(hl), e
	inc	hl
	ld	(hl), d
00106$:
	C$montylib.c$141$1_0$206	= .
	.globl	C$montylib.c$141$1_0$206
;src/libs/montylib.c:141: list->head = new_head;
	ld	l, c
	ld	h, b
	ld	(hl), e
	inc	hl
	ld	(hl), d
	C$montylib.c$142$1_0$206	= .
	.globl	C$montylib.c$142$1_0$206
;src/libs/montylib.c:142: list->numElements++;
	inc	bc
	inc	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	inc	a
	ld	(bc), a
00107$:
	C$montylib.c$143$1_0$206	= .
	.globl	C$montylib.c$143$1_0$206
;src/libs/montylib.c:143: }
	ld	sp, ix
	pop	ix
	C$montylib.c$143$1_0$206	= .
	.globl	C$montylib.c$143$1_0$206
	XG$addElementBeginingList$0$0	= .
	.globl	XG$addElementBeginingList$0$0
	ret
	G$addElementEndList$0$0	= .
	.globl	G$addElementEndList$0$0
	C$montylib.c$145$1_0$209	= .
	.globl	C$montylib.c$145$1_0$209
;src/libs/montylib.c:145: void addElementEndList(linked_list_t* list, void * element) {
;	---------------------------------
; Function addElementEndList
; ---------------------------------
_addElementEndList::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-8
	add	iy, sp
	ld	sp, iy
	ld	c, l
	ld	b, h
	ld	-2 (ix), e
	ld	-1 (ix), d
	C$montylib.c$147$1_0$209	= .
	.globl	C$montylib.c$147$1_0$209
;src/libs/montylib.c:147: new_tail = (list_node_t *) malloc(sizeof(list_node_t));
	push	bc
	ld	hl, #0x0006
	call	_malloc
	pop	bc
	C$montylib.c$148$1_0$209	= .
	.globl	C$montylib.c$148$1_0$209
;src/libs/montylib.c:148: if(new_tail == NULL) {
	ld	a, d
	or	a, e
	C$montylib.c$149$2_0$210	= .
	.globl	C$montylib.c$149$2_0$210
;src/libs/montylib.c:149: return;
	jp	Z,00107$
	C$montylib.c$151$1_0$209	= .
	.globl	C$montylib.c$151$1_0$209
;src/libs/montylib.c:151: new_tail->data = malloc(list->typeSize);
	ld	iy, #0x0005
	add	iy, bc
	ld	l, 0 (iy)
;	spillPairReg hl
	ld	h, 1 (iy)
;	spillPairReg hl
	push	bc
	push	de
	push	iy
	call	_malloc
	ld	-4 (ix), e
	ld	-3 (ix), d
	pop	iy
	pop	de
	pop	bc
	ld	l, e
	ld	h, d
	ld	a, -4 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -3 (ix)
	ld	(hl), a
	C$montylib.c$152$1_0$209	= .
	.globl	C$montylib.c$152$1_0$209
;src/libs/montylib.c:152: memcpy(new_tail->data, element, list->typeSize);
	ld	l, 0 (iy)
;	spillPairReg hl
	ld	h, 1 (iy)
;	spillPairReg hl
	push	de
	push	bc
	ld	c, l
	ld	b, h
	ld	e, -4 (ix)
	ld	d, -3 (ix)
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, b
	or	a, c
	jr	Z, 00124$
	ldir
00124$:
	pop	bc
	pop	de
	C$montylib.c$153$1_0$209	= .
	.globl	C$montylib.c$153$1_0$209
;src/libs/montylib.c:153: new_tail->prev = list->tail;
	ld	hl, #0x0004
	add	hl, de
	ex	(sp), hl
	ld	hl, #0x0002
	add	hl, bc
	ld	-6 (ix), l
	ld	-5 (ix), h
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	pop	hl
	push	hl
	ld	a, -4 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -3 (ix)
	ld	(hl), a
	C$montylib.c$154$1_0$209	= .
	.globl	C$montylib.c$154$1_0$209
;src/libs/montylib.c:154: new_tail->next = NULL;
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$155$1_0$209	= .
	.globl	C$montylib.c$155$1_0$209
;src/libs/montylib.c:155: if(list->tail != NULL) 
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
;	spillPairReg hl
;	spillPairReg hl
	ld	l,a
	or	a,h
	jr	Z, 00104$
	C$montylib.c$156$1_0$209	= .
	.globl	C$montylib.c$156$1_0$209
;src/libs/montylib.c:156: list->tail->next = new_tail;
	inc	hl
	inc	hl
	ld	(hl), e
	inc	hl
	ld	(hl), d
00104$:
	C$montylib.c$157$1_0$209	= .
	.globl	C$montylib.c$157$1_0$209
;src/libs/montylib.c:157: if(list->head == NULL)
	ld	l, c
	ld	h, b
	inc	hl
	ld	a, (hl)
	dec	hl
	ld	l, (hl)
;	spillPairReg hl
	or	a, l
	jr	NZ, 00106$
	C$montylib.c$158$1_0$209	= .
	.globl	C$montylib.c$158$1_0$209
;src/libs/montylib.c:158: list->head = new_tail;
	ld	l, c
	ld	h, b
	ld	(hl), e
	inc	hl
	ld	(hl), d
00106$:
	C$montylib.c$159$1_0$209	= .
	.globl	C$montylib.c$159$1_0$209
;src/libs/montylib.c:159: list->tail = new_tail;
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	(hl), e
	inc	hl
	ld	(hl), d
	C$montylib.c$160$1_0$209	= .
	.globl	C$montylib.c$160$1_0$209
;src/libs/montylib.c:160: list->numElements++;
	inc	bc
	inc	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	inc	a
	ld	(bc), a
00107$:
	C$montylib.c$161$1_0$209	= .
	.globl	C$montylib.c$161$1_0$209
;src/libs/montylib.c:161: }
	ld	sp, ix
	pop	ix
	C$montylib.c$161$1_0$209	= .
	.globl	C$montylib.c$161$1_0$209
	XG$addElementEndList$0$0	= .
	.globl	XG$addElementEndList$0$0
	ret
	G$deleteElementBeginingList$0$0	= .
	.globl	G$deleteElementBeginingList$0$0
	C$montylib.c$164$1_0$212	= .
	.globl	C$montylib.c$164$1_0$212
;src/libs/montylib.c:164: void deleteElementBeginingList(linked_list_t* list) {
;	---------------------------------
; Function deleteElementBeginingList
; ---------------------------------
_deleteElementBeginingList::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	ld	c, l
	ld	b, h
	C$montylib.c$165$1_0$212	= .
	.globl	C$montylib.c$165$1_0$212
;src/libs/montylib.c:165: if(list->numElements == 0)
	ld	hl, #0x0004
	add	hl, bc
	ex	de, hl
	ld	a, (de)
	or	a, a
	C$montylib.c$166$1_0$212	= .
	.globl	C$montylib.c$166$1_0$212
;src/libs/montylib.c:166: return;
	jr	Z, 00106$
	C$montylib.c$167$1_1$213	= .
	.globl	C$montylib.c$167$1_1$213
;src/libs/montylib.c:167: list_node_t* current_element = list->head;
	ld	a, (bc)
	ld	-4 (ix), a
	inc	bc
	ld	a, (bc)
	ld	-3 (ix), a
	dec	bc
	C$montylib.c$168$1_1$213	= .
	.globl	C$montylib.c$168$1_1$213
;src/libs/montylib.c:168: if(current_element->next != NULL) {
	ld	a, -4 (ix)
	add	a, #0x02
	ld	-2 (ix), a
	ld	a, -3 (ix)
	adc	a, #0x00
	ld	-1 (ix), a
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
;	spillPairReg hl
;	spillPairReg hl
	ld	l,a
	or	a,h
	jr	Z, 00104$
	C$montylib.c$169$2_1$214	= .
	.globl	C$montylib.c$169$2_1$214
;src/libs/montylib.c:169: current_element->next->prev = NULL;
	inc	hl
	inc	hl
	inc	hl
	inc	hl
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$170$2_1$214	= .
	.globl	C$montylib.c$170$2_1$214
;src/libs/montylib.c:170: list->head = current_element->next;
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, (hl)
	ld	-2 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-1 (ix), a
	ld	a, -2 (ix)
	ld	(bc), a
	inc	bc
	ld	a, -1 (ix)
	ld	(bc), a
	jr	00105$
00104$:
	C$montylib.c$172$2_1$215	= .
	.globl	C$montylib.c$172$2_1$215
;src/libs/montylib.c:172: list->head = NULL;
	ld	l, c
	ld	h, b
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$173$2_1$215	= .
	.globl	C$montylib.c$173$2_1$215
;src/libs/montylib.c:173: list->tail = NULL;
	inc	bc
	inc	bc
	xor	a, a
	ld	(bc), a
	inc	bc
	ld	(bc), a
00105$:
	C$montylib.c$175$1_1$213	= .
	.globl	C$montylib.c$175$1_1$213
;src/libs/montylib.c:175: free(current_element->data);
	pop	hl
	push	hl
	ld	c, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	push	de
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	call	_free
	pop	de
	C$montylib.c$176$1_1$213	= .
	.globl	C$montylib.c$176$1_1$213
;src/libs/montylib.c:176: free(current_element);
	pop	hl
	push	hl
	push	de
	call	_free
	pop	de
	C$montylib.c$177$1_1$213	= .
	.globl	C$montylib.c$177$1_1$213
;src/libs/montylib.c:177: list->numElements--;
	ld	a, (de)
	dec	a
	ld	(de), a
00106$:
	C$montylib.c$178$1_1$212	= .
	.globl	C$montylib.c$178$1_1$212
;src/libs/montylib.c:178: }
	ld	sp, ix
	pop	ix
	C$montylib.c$178$1_1$212	= .
	.globl	C$montylib.c$178$1_1$212
	XG$deleteElementBeginingList$0$0	= .
	.globl	XG$deleteElementBeginingList$0$0
	ret
	G$deleteElementEndList$0$0	= .
	.globl	G$deleteElementEndList$0$0
	C$montylib.c$180$1_1$217	= .
	.globl	C$montylib.c$180$1_1$217
;src/libs/montylib.c:180: void deleteElementEndList(linked_list_t* list) {
;	---------------------------------
; Function deleteElementEndList
; ---------------------------------
_deleteElementEndList::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-12
	add	iy, sp
	ld	sp, iy
	ld	-2 (ix), l
	ld	-1 (ix), h
	C$montylib.c$181$1_0$217	= .
	.globl	C$montylib.c$181$1_0$217
;src/libs/montylib.c:181: if(list->numElements == 0)
	ld	a, -2 (ix)
	add	a, #0x04
	ld	-12 (ix), a
	ld	a, -1 (ix)
	adc	a, #0x00
	ld	-11 (ix), a
	pop	hl
	push	hl
	ld	a, (hl)
	ld	-3 (ix), a
	or	a, a
	C$montylib.c$182$1_0$217	= .
	.globl	C$montylib.c$182$1_0$217
;src/libs/montylib.c:182: return;
	jp	Z,00106$
	C$montylib.c$183$1_1$218	= .
	.globl	C$montylib.c$183$1_1$218
;src/libs/montylib.c:183: list_node_t* current_element = list->tail;
	ld	a, -2 (ix)
	add	a, #0x02
	ld	-10 (ix), a
	ld	a, -1 (ix)
	adc	a, #0x00
	ld	-9 (ix), a
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	ld	a, (hl)
	ld	-8 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-7 (ix), a
	C$montylib.c$184$1_1$218	= .
	.globl	C$montylib.c$184$1_1$218
;src/libs/montylib.c:184: if(current_element->prev != NULL) {
	ld	a, -8 (ix)
	add	a, #0x04
	ld	-6 (ix), a
	ld	a, -7 (ix)
	adc	a, #0x00
	ld	-5 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	or	a, -4 (ix)
	jr	Z, 00104$
	C$montylib.c$185$2_1$219	= .
	.globl	C$montylib.c$185$2_1$219
;src/libs/montylib.c:185: current_element->prev->next = NULL;
	ld	c, -4 (ix)
	ld	b, -3 (ix)
	inc	bc
	inc	bc
	xor	a, a
	ld	(bc), a
	inc	bc
	ld	(bc), a
	C$montylib.c$186$2_1$219	= .
	.globl	C$montylib.c$186$2_1$219
;src/libs/montylib.c:186: list->tail = current_element->prev;
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	c, (hl)
	inc	hl
	ld	b, (hl)
	pop	de
	pop	hl
	push	hl
	push	de
	ld	(hl), c
	inc	hl
	ld	(hl), b
	jr	00105$
00104$:
	C$montylib.c$188$2_1$220	= .
	.globl	C$montylib.c$188$2_1$220
;src/libs/montylib.c:188: list->head = NULL;
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$189$2_1$220	= .
	.globl	C$montylib.c$189$2_1$220
;src/libs/montylib.c:189: list->tail = NULL;
	pop	bc
	pop	hl
	push	hl
	push	bc
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
00105$:
	C$montylib.c$191$1_1$218	= .
	.globl	C$montylib.c$191$1_1$218
;src/libs/montylib.c:191: free(current_element->data);
	ld	l, -8 (ix)
	ld	h, -7 (ix)
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_free
	C$montylib.c$192$1_1$218	= .
	.globl	C$montylib.c$192$1_1$218
;src/libs/montylib.c:192: free(current_element);
	ld	a, -8 (ix)
	ld	-4 (ix), a
	ld	a, -7 (ix)
	ld	-3 (ix), a
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_free
	C$montylib.c$193$1_1$218	= .
	.globl	C$montylib.c$193$1_1$218
;src/libs/montylib.c:193: list->numElements--;    
	pop	hl
	push	hl
	ld	a, (hl)
	dec	a
	pop	hl
	push	hl
	ld	(hl), a
00106$:
	C$montylib.c$194$1_1$217	= .
	.globl	C$montylib.c$194$1_1$217
;src/libs/montylib.c:194: }
	ld	sp, ix
	pop	ix
	C$montylib.c$194$1_1$217	= .
	.globl	C$montylib.c$194$1_1$217
	XG$deleteElementEndList$0$0	= .
	.globl	XG$deleteElementEndList$0$0
	ret
	G$deleteElementPosList$0$0	= .
	.globl	G$deleteElementPosList$0$0
	C$montylib.c$196$1_1$222	= .
	.globl	C$montylib.c$196$1_1$222
;src/libs/montylib.c:196: void deleteElementPosList(linked_list_t* list, u8 pos) {
;	---------------------------------
; Function deleteElementPosList
; ---------------------------------
_deleteElementPosList::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-10
	add	iy, sp
	ld	sp, iy
	ld	-2 (ix), l
	ld	-1 (ix), h
	C$montylib.c$198$1_0$222	= .
	.globl	C$montylib.c$198$1_0$222
;src/libs/montylib.c:198: if(list->numElements == 0)
	ld	a, -2 (ix)
	add	a, #0x04
	ld	-10 (ix), a
	ld	a, -1 (ix)
	adc	a, #0x00
	ld	-9 (ix), a
	pop	hl
	push	hl
	ld	c, (hl)
	ld	a, c
	or	a, a
	C$montylib.c$199$1_0$222	= .
	.globl	C$montylib.c$199$1_0$222
;src/libs/montylib.c:199: return;
	jp	Z,00122$
	C$montylib.c$200$1_0$222	= .
	.globl	C$montylib.c$200$1_0$222
;src/libs/montylib.c:200: if(pos==0) {
	ld	a, 4 (ix)
	or	a, a
	jr	NZ, 00104$
	C$montylib.c$201$2_0$223	= .
	.globl	C$montylib.c$201$2_0$223
;src/libs/montylib.c:201: deleteElementBeginingList(list);
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_deleteElementBeginingList
	C$montylib.c$202$2_0$223	= .
	.globl	C$montylib.c$202$2_0$223
;src/libs/montylib.c:202: return;
	jp	00122$
00104$:
	C$montylib.c$204$1_0$222	= .
	.globl	C$montylib.c$204$1_0$222
;src/libs/montylib.c:204: if(pos == (list->numElements-1)) {
	ld	b, #0x00
	dec	bc
	ld	l, 4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	cp	a, a
	sbc	hl, bc
	jr	NZ, 00106$
	C$montylib.c$205$2_0$224	= .
	.globl	C$montylib.c$205$2_0$224
;src/libs/montylib.c:205: deleteElementEndList(list);
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_deleteElementEndList
	C$montylib.c$206$2_0$224	= .
	.globl	C$montylib.c$206$2_0$224
;src/libs/montylib.c:206: return;
	jp	00122$
00106$:
	C$montylib.c$208$1_1$225	= .
	.globl	C$montylib.c$208$1_1$225
;src/libs/montylib.c:208: list_node_t* current_element = list->head;
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	C$montylib.c$209$1_1$222	= .
	.globl	C$montylib.c$209$1_1$222
;src/libs/montylib.c:209: while(current_element->next != NULL) {
	ld	c, #0x00
00109$:
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	or	a, -4 (ix)
	jr	Z, 00111$
	C$montylib.c$210$2_1$226	= .
	.globl	C$montylib.c$210$2_1$226
;src/libs/montylib.c:210: current_element = current_element->next;
	ld	e, -4 (ix)
	ld	d, -3 (ix)
	C$montylib.c$211$2_1$226	= .
	.globl	C$montylib.c$211$2_1$226
;src/libs/montylib.c:211: currentPos++;
	inc	c
	C$montylib.c$212$2_1$226	= .
	.globl	C$montylib.c$212$2_1$226
;src/libs/montylib.c:212: if(currentPos == pos) 
	ld	a, 4 (ix)
	sub	a, c
	jr	NZ, 00109$
	C$montylib.c$213$1_1$225	= .
	.globl	C$montylib.c$213$1_1$225
;src/libs/montylib.c:213: break;
00111$:
	C$montylib.c$216$1_1$225	= .
	.globl	C$montylib.c$216$1_1$225
;src/libs/montylib.c:216: if(currentPos!=pos)
	ld	a, 4 (ix)
	sub	a, c
	C$montylib.c$217$1_1$225	= .
	.globl	C$montylib.c$217$1_1$225
;src/libs/montylib.c:217: return;
	jp	NZ,00122$
	C$montylib.c$218$1_1$225	= .
	.globl	C$montylib.c$218$1_1$225
;src/libs/montylib.c:218: if(current_element->next != NULL) {
	ld	c, e
	ld	b, d
	inc	bc
	inc	bc
	ld	a, (bc)
	ld	-8 (ix), a
	inc	bc
	ld	a, (bc)
	ld	-7 (ix), a
	dec	bc
	C$montylib.c$219$1_1$222	= .
	.globl	C$montylib.c$219$1_1$222
;src/libs/montylib.c:219: if(current_element->prev != NULL) {
	ld	hl, #0x0004
	add	hl, de
	ld	-6 (ix), l
	ld	-5 (ix), h
	C$montylib.c$218$1_1$225	= .
	.globl	C$montylib.c$218$1_1$225
;src/libs/montylib.c:218: if(current_element->next != NULL) {
	ld	a, -7 (ix)
	or	a, -8 (ix)
	jr	Z, 00117$
	C$montylib.c$219$2_1$227	= .
	.globl	C$montylib.c$219$2_1$227
;src/libs/montylib.c:219: if(current_element->prev != NULL) {
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	or	a, -4 (ix)
	jr	Z, 00117$
	C$montylib.c$220$3_1$228	= .
	.globl	C$montylib.c$220$3_1$228
;src/libs/montylib.c:220: current_element->next->prev = current_element->prev;
	ld	a, -8 (ix)
	add	a, #0x04
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -7 (ix)
	adc	a, #0x00
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -4 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -3 (ix)
	ld	(hl), a
00117$:
	C$montylib.c$223$1_1$225	= .
	.globl	C$montylib.c$223$1_1$225
;src/libs/montylib.c:223: if(current_element->prev != NULL) {
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	or	a, -4 (ix)
	jr	Z, 00121$
	C$montylib.c$224$2_1$229	= .
	.globl	C$montylib.c$224$2_1$229
;src/libs/montylib.c:224: if(current_element->next != NULL) {
	ld	l, c
	ld	h, b
	ld	c, (hl)
	inc	hl
	ld	b, (hl)
	ld	a, b
	or	a, c
	jr	Z, 00121$
	C$montylib.c$225$3_1$230	= .
	.globl	C$montylib.c$225$3_1$230
;src/libs/montylib.c:225: current_element->prev->next = current_element->next;
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	(hl), c
	inc	hl
	ld	(hl), b
00121$:
	C$montylib.c$228$1_1$225	= .
	.globl	C$montylib.c$228$1_1$225
;src/libs/montylib.c:228: free(current_element->data);
	ld	l, e
	ld	h, d
	ld	c, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	push	de
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	call	_free
	pop	de
	C$montylib.c$229$1_1$225	= .
	.globl	C$montylib.c$229$1_1$225
;src/libs/montylib.c:229: free(current_element);
	ex	de, hl
	call	_free
	C$montylib.c$230$1_1$225	= .
	.globl	C$montylib.c$230$1_1$225
;src/libs/montylib.c:230: list->numElements--;
	pop	hl
	push	hl
	ld	a, (hl)
	dec	a
	pop	hl
	push	hl
	ld	(hl), a
00122$:
	C$montylib.c$231$1_1$222	= .
	.globl	C$montylib.c$231$1_1$222
;src/libs/montylib.c:231: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$deleteElement_Unsafe$0$0	= .
	.globl	G$deleteElement_Unsafe$0$0
	C$montylib.c$233$1_1$232	= .
	.globl	C$montylib.c$233$1_1$232
;src/libs/montylib.c:233: void deleteElement_Unsafe(linked_list_t* list, list_node_t* element_to_delete) {
;	---------------------------------
; Function deleteElement_Unsafe
; ---------------------------------
_deleteElement_Unsafe::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-8
	add	iy, sp
	ld	sp, iy
	ld	c, l
	ld	b, h
	C$montylib.c$234$1_0$232	= .
	.globl	C$montylib.c$234$1_0$232
;src/libs/montylib.c:234: if(element_to_delete->next == NULL && element_to_delete->prev == NULL) {
	ld	hl, #0x0002
	add	hl, de
	ld	-2 (ix), l
	ld	-1 (ix), h
	ld	a, (hl)
	ld	-8 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-7 (ix), a
	ld	iy, #0x0004
	add	iy, de
	C$montylib.c$236$1_0$232	= .
	.globl	C$montylib.c$236$1_0$232
;src/libs/montylib.c:236: list->tail = NULL;
	ld	hl, #0x0002
	add	hl, bc
	ld	-6 (ix), l
	ld	-5 (ix), h
	C$montylib.c$247$1_0$232	= .
	.globl	C$montylib.c$247$1_0$232
;src/libs/montylib.c:247: if(element_to_delete->prev != NULL) {
	ld	a, 0 (iy)
	ld	-4 (ix), a
	ld	a, 1 (iy)
	ld	-3 (ix), a
	C$montylib.c$234$1_0$232	= .
	.globl	C$montylib.c$234$1_0$232
;src/libs/montylib.c:234: if(element_to_delete->next == NULL && element_to_delete->prev == NULL) {
	ld	a, -7 (ix)
	or	a, -8 (ix)
	jr	NZ, 00112$
	ld	a, -3 (ix)
	or	a, -4 (ix)
	jr	NZ, 00112$
	C$montylib.c$235$2_0$233	= .
	.globl	C$montylib.c$235$2_0$233
;src/libs/montylib.c:235: list->head = NULL;
	ld	l, c
	ld	h, b
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$236$2_0$233	= .
	.globl	C$montylib.c$236$2_0$233
;src/libs/montylib.c:236: list->tail = NULL;
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	jp	00113$
00112$:
	C$montylib.c$238$2_0$234	= .
	.globl	C$montylib.c$238$2_0$234
;src/libs/montylib.c:238: if(element_to_delete->next != NULL) {
	ld	a, -7 (ix)
	or	a, -8 (ix)
	jr	Z, 00105$
	C$montylib.c$240$1_0$232	= .
	.globl	C$montylib.c$240$1_0$232
;src/libs/montylib.c:240: element_to_delete->next->prev = element_to_delete->prev;
	ld	a, -8 (ix)
	add	a, #0x04
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -7 (ix)
	adc	a, #0x00
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	C$montylib.c$239$3_0$235	= .
	.globl	C$montylib.c$239$3_0$235
;src/libs/montylib.c:239: if(element_to_delete->prev != NULL) {
	ld	a, -3 (ix)
	or	a, -4 (ix)
	jr	Z, 00102$
	C$montylib.c$240$4_0$236	= .
	.globl	C$montylib.c$240$4_0$236
;src/libs/montylib.c:240: element_to_delete->next->prev = element_to_delete->prev;
	ld	a, -4 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -3 (ix)
	ld	(hl), a
	jr	00105$
00102$:
	C$montylib.c$242$4_0$237	= .
	.globl	C$montylib.c$242$4_0$237
;src/libs/montylib.c:242: element_to_delete->next->prev = NULL;
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$243$4_0$237	= .
	.globl	C$montylib.c$243$4_0$237
;src/libs/montylib.c:243: list->head = element_to_delete->next;
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, (hl)
	ld	-4 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-3 (ix), a
	ld	l, c
	ld	h, b
	ld	a, -4 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -3 (ix)
	ld	(hl), a
00105$:
	C$montylib.c$247$2_0$234	= .
	.globl	C$montylib.c$247$2_0$234
;src/libs/montylib.c:247: if(element_to_delete->prev != NULL) {
	ld	a, 0 (iy)
	ld	-4 (ix), a
	ld	a, 1 (iy)
	ld	-3 (ix), a
	or	a, -4 (ix)
	jr	Z, 00113$
	C$montylib.c$248$3_0$238	= .
	.globl	C$montylib.c$248$3_0$238
;src/libs/montylib.c:248: if(element_to_delete->next != NULL) {
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, (hl)
	ld	-2 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-1 (ix), a
	C$montylib.c$249$1_0$232	= .
	.globl	C$montylib.c$249$1_0$232
;src/libs/montylib.c:249: element_to_delete->prev->next = element_to_delete->next;
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	C$montylib.c$248$3_0$238	= .
	.globl	C$montylib.c$248$3_0$238
;src/libs/montylib.c:248: if(element_to_delete->next != NULL) {
	ld	a, -1 (ix)
	or	a, -2 (ix)
	jr	Z, 00107$
	C$montylib.c$249$4_0$239	= .
	.globl	C$montylib.c$249$4_0$239
;src/libs/montylib.c:249: element_to_delete->prev->next = element_to_delete->next;
	ld	a, -2 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -1 (ix)
	ld	(hl), a
	jr	00113$
00107$:
	C$montylib.c$251$4_0$240	= .
	.globl	C$montylib.c$251$4_0$240
;src/libs/montylib.c:251: element_to_delete->prev->next = NULL;
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$montylib.c$252$4_0$240	= .
	.globl	C$montylib.c$252$4_0$240
;src/libs/montylib.c:252: list->tail = element_to_delete->prev;
	ld	a, 0 (iy)
	ld	-2 (ix), a
	ld	a, 1 (iy)
	ld	-1 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, -2 (ix)
	ld	(hl), a
	inc	hl
	ld	a, -1 (ix)
	ld	(hl), a
00113$:
	C$montylib.c$256$1_0$232	= .
	.globl	C$montylib.c$256$1_0$232
;src/libs/montylib.c:256: free(element_to_delete->data);
	ld	l, e
	ld	h, d
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	push	de
	call	_free
	pop	de
	ex	de, hl
	call	_free
	pop	bc
	C$montylib.c$258$1_0$232	= .
	.globl	C$montylib.c$258$1_0$232
;src/libs/montylib.c:258: list->numElements--;
	inc	bc
	inc	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	dec	a
	ld	(bc), a
	C$montylib.c$259$1_0$232	= .
	.globl	C$montylib.c$259$1_0$232
;src/libs/montylib.c:259: }
	ld	sp, ix
	pop	ix
	C$montylib.c$259$1_0$232	= .
	.globl	C$montylib.c$259$1_0$232
	XG$deleteElement_Unsafe$0$0	= .
	.globl	XG$deleteElement_Unsafe$0$0
	ret
	G$destroyList$0$0	= .
	.globl	G$destroyList$0$0
	C$montylib.c$261$1_0$242	= .
	.globl	C$montylib.c$261$1_0$242
;src/libs/montylib.c:261: void destroyList(linked_list_t* list) {
;	---------------------------------
; Function destroyList
; ---------------------------------
_destroyList::
	ex	de, hl
	C$montylib.c$262$1_0$242	= .
	.globl	C$montylib.c$262$1_0$242
;src/libs/montylib.c:262: while(list->numElements) {
	ld	c, e
	ld	b, d
00101$:
	ld	hl, #4
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	Z, 00103$
	C$montylib.c$263$2_0$243	= .
	.globl	C$montylib.c$263$2_0$243
;src/libs/montylib.c:263: deleteElementBeginingList(list);
	push	bc
;	spillPairReg hl
;	spillPairReg hl
	ex	de, hl
	push	hl
;	spillPairReg hl
;	spillPairReg hl
	call	_deleteElementBeginingList
	pop	de
	pop	bc
	jr	00101$
00103$:
	C$montylib.c$265$1_0$242	= .
	.globl	C$montylib.c$265$1_0$242
;src/libs/montylib.c:265: free(list);
	ex	de, hl
	C$montylib.c$266$1_0$242	= .
	.globl	C$montylib.c$266$1_0$242
;src/libs/montylib.c:266: }
	C$montylib.c$266$1_0$242	= .
	.globl	C$montylib.c$266$1_0$242
	XG$destroyList$0$0	= .
	.globl	XG$destroyList$0$0
	jp	_free
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
