;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module end_game_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGSFXStop
	.globl _PSGStop
	.globl _waitForFrame
	.globl _clear_tilemap
	.globl _transition_to_state
	.globl _play_song
	.globl _start_fadeout_music
	.globl _start_fadein_music
	.globl _update_dialog_box
	.globl _create_dialog_box
	.globl _clear_cursor_sprites
	.globl _ending_update_face
	.globl _endingScrollFinish
	.globl _scroll_ending
	.globl _ending_sprites
	.globl _load_ending2_assets
	.globl _load_ending1_assets
	.globl _sprite_fade_in
	.globl _background_fade_in
	.globl _sprite_fade_out
	.globl _background_fade_out
	.globl _load_tilemap
	.globl _load_tileset
	.globl _end_currentLine
	.globl _end_state_status
	.globl _end_game_state_start
	.globl _end_game_state_update
	.globl _end_game_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$end_state_status$0_0$0==.
_end_state_status::
	.ds 1
G$end_currentLine$0_0$0==.
_end_currentLine::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$end_game_state_start$0$0	= .
	.globl	G$end_game_state_start$0$0
	C$end_game_state.c$16$0_0$133	= .
	.globl	C$end_game_state.c$16$0_0$133
;src/states/end_game_state.c:16: void end_game_state_start(void) {
;	---------------------------------
; Function end_game_state_start
; ---------------------------------
_end_game_state_start::
	C$end_game_state.c$17$1_0$133	= .
	.globl	C$end_game_state.c$17$1_0$133
;src/states/end_game_state.c:17: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$end_game_state.c$18$1_0$133	= .
	.globl	C$end_game_state.c$18$1_0$133
;src/states/end_game_state.c:18: end_state_status = 0;
	ld	hl, #_end_state_status
	ld	(hl), #0x00
	C$end_game_state.c$19$1_0$133	= .
	.globl	C$end_game_state.c$19$1_0$133
;src/states/end_game_state.c:19: end_currentLine = 0;
	ld	hl, #_end_currentLine
	ld	(hl), #0x00
	C$end_game_state.c$20$1_0$133	= .
	.globl	C$end_game_state.c$20$1_0$133
;src/states/end_game_state.c:20: clear_tilemap();
	call	_clear_tilemap
	C$end_game_state.c$21$1_0$133	= .
	.globl	C$end_game_state.c$21$1_0$133
;src/states/end_game_state.c:21: load_ending1_assets();
	call	_load_ending1_assets
	C$end_game_state.c$22$1_0$133	= .
	.globl	C$end_game_state.c$22$1_0$133
;src/states/end_game_state.c:22: load_tileset(SIMPLE_FONT_ASSET,DIGITS_BASE_ADDRESS);
	ld	l, #0x18
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_load_tileset
	C$end_game_state.c$23$1_0$133	= .
	.globl	C$end_game_state.c$23$1_0$133
;src/states/end_game_state.c:23: load_tileset(ENDING1_ASSETS,BACKGROUND_BASE_ADRESS);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x08
	call	_load_tileset
	C$end_game_state.c$24$1_0$133	= .
	.globl	C$end_game_state.c$24$1_0$133
;src/states/end_game_state.c:24: load_tilemap(ENDING1_ASSETS);
	ld	a, #0x08
	C$end_game_state.c$25$1_0$133	= .
	.globl	C$end_game_state.c$25$1_0$133
;src/states/end_game_state.c:25: }
	C$end_game_state.c$25$1_0$133	= .
	.globl	C$end_game_state.c$25$1_0$133
	XG$end_game_state_start$0$0	= .
	.globl	XG$end_game_state_start$0$0
	jp	_load_tilemap
	G$end_game_state_update$0$0	= .
	.globl	G$end_game_state_update$0$0
	C$end_game_state.c$27$1_0$135	= .
	.globl	C$end_game_state.c$27$1_0$135
;src/states/end_game_state.c:27: void end_game_state_update(void) {
;	---------------------------------
; Function end_game_state_update
; ---------------------------------
_end_game_state_update::
	C$end_game_state.c$28$1_0$135	= .
	.globl	C$end_game_state.c$28$1_0$135
;src/states/end_game_state.c:28: if(end_state_status == 0) {
	ld	a, (_end_state_status+0)
	or	a, a
	jr	NZ, 00134$
	C$end_game_state.c$29$2_0$136	= .
	.globl	C$end_game_state.c$29$2_0$136
;src/states/end_game_state.c:29: play_song(INTRO_OST);
	ld	a, #0x02
	call	_play_song
	C$end_game_state.c$30$2_0$136	= .
	.globl	C$end_game_state.c$30$2_0$136
;src/states/end_game_state.c:30: ending_sprites();
	call	_ending_sprites
	C$end_game_state.c$31$2_0$136	= .
	.globl	C$end_game_state.c$31$2_0$136
;src/states/end_game_state.c:31: start_fadein_music();
	call	_start_fadein_music
	C$end_game_state.c$32$2_0$136	= .
	.globl	C$end_game_state.c$32$2_0$136
;src/states/end_game_state.c:32: background_fade_in(ENDING1_PALETTE,2);
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0b
	call	_background_fade_in
	C$end_game_state.c$33$2_0$136	= .
	.globl	C$end_game_state.c$33$2_0$136
;src/states/end_game_state.c:33: sprite_fade_in(PIECES_PALETTE,1);
	ld	l, #0x01
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0a
	call	_sprite_fade_in
	C$end_game_state.c$34$2_0$136	= .
	.globl	C$end_game_state.c$34$2_0$136
;src/states/end_game_state.c:34: create_dialog_box();
	call	_create_dialog_box
	C$end_game_state.c$35$2_0$136	= .
	.globl	C$end_game_state.c$35$2_0$136
;src/states/end_game_state.c:35: end_state_status = 1;
	ld	hl, #_end_state_status
	ld	(hl), #0x01
	ret
00134$:
	C$end_game_state.c$36$1_0$135	= .
	.globl	C$end_game_state.c$36$1_0$135
;src/states/end_game_state.c:36: } else if(end_state_status == 1){
	ld	a, (_end_state_status+0)
	dec	a
	jp	NZ,00131$
	C$end_game_state.c$37$2_0$137	= .
	.globl	C$end_game_state.c$37$2_0$137
;src/states/end_game_state.c:37: if((end_currentLine>>1) > NUMBER_ENDGAME_SENTENCES) {
	ld	iy, #_end_currentLine
	ld	c, 0 (iy)
	srl	c
	ld	a, #0x09
	sub	a, c
	jr	NC, 00102$
	C$end_game_state.c$38$3_0$138	= .
	.globl	C$end_game_state.c$38$3_0$138
;src/states/end_game_state.c:38: end_state_status = 2;
	ld	hl, #_end_state_status
	ld	(hl), #0x02
	C$end_game_state.c$39$3_0$138	= .
	.globl	C$end_game_state.c$39$3_0$138
;src/states/end_game_state.c:39: scroll_x[0] = 255;
	ld	hl, #0x00ff
	ld	(_scroll_x), hl
	C$end_game_state.c$40$3_0$138	= .
	.globl	C$end_game_state.c$40$3_0$138
;src/states/end_game_state.c:40: currentScrollColumn = scroll_x[0] >> 3;
	ld	hl, (#_scroll_x + 0)
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	ld	iy, #_currentScrollColumn
	ld	0 (iy), l
00102$:
	C$end_game_state.c$42$2_0$137	= .
	.globl	C$end_game_state.c$42$2_0$137
;src/states/end_game_state.c:42: if((frame_cnt & 63) == 0) {
	ld	a, (_frame_cnt+0)
	and	a, #0x3f
	jp	NZ,_waitForFrame
	C$end_game_state.c$43$3_0$139	= .
	.globl	C$end_game_state.c$43$3_0$139
;src/states/end_game_state.c:43: update_dialog_box(end_currentLine>>1, end_currentLine&1,1);
	ld	a, (_end_currentLine+0)
	and	a, #0x01
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (#_end_currentLine + 0)
	ld	c, a
	srl	c
	ld	a, #0x01
	push	af
	inc	sp
	ld	a, c
	call	_update_dialog_box
	C$end_game_state.c$44$3_0$139	= .
	.globl	C$end_game_state.c$44$3_0$139
;src/states/end_game_state.c:44: end_currentLine++;
	ld	iy, #_end_currentLine
	inc	0 (iy)
	C$end_game_state.c$45$3_0$139	= .
	.globl	C$end_game_state.c$45$3_0$139
;src/states/end_game_state.c:45: switch (end_currentLine)
	ld	a, (_end_currentLine+0)
	sub	a, #0x07
	jp	Z,_waitForFrame
	ld	a, (_end_currentLine+0)
	sub	a, #0x08
	jr	Z, 00104$
	ld	a, (_end_currentLine+0)
	sub	a, #0x09
	jr	Z, 00105$
	jp	_waitForFrame
	C$end_game_state.c$50$4_0$140	= .
	.globl	C$end_game_state.c$50$4_0$140
;src/states/end_game_state.c:50: case 8:
00104$:
	C$end_game_state.c$51$4_0$140	= .
	.globl	C$end_game_state.c$51$4_0$140
;src/states/end_game_state.c:51: background_fade_out();
	call	_background_fade_out
	C$end_game_state.c$52$4_0$140	= .
	.globl	C$end_game_state.c$52$4_0$140
;src/states/end_game_state.c:52: load_ending2_assets();
	call	_load_ending2_assets
	C$end_game_state.c$53$4_0$140	= .
	.globl	C$end_game_state.c$53$4_0$140
;src/states/end_game_state.c:53: break;
	jp	_waitForFrame
	C$end_game_state.c$54$4_0$140	= .
	.globl	C$end_game_state.c$54$4_0$140
;src/states/end_game_state.c:54: case 9:
00105$:
	C$end_game_state.c$55$4_0$140	= .
	.globl	C$end_game_state.c$55$4_0$140
;src/states/end_game_state.c:55: start_fadein_music();
	call	_start_fadein_music
	C$end_game_state.c$56$4_0$140	= .
	.globl	C$end_game_state.c$56$4_0$140
;src/states/end_game_state.c:56: background_fade_in(ENDING2_PALETTE,2);
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0c
	call	_background_fade_in
	C$end_game_state.c$60$2_0$137	= .
	.globl	C$end_game_state.c$60$2_0$137
;src/states/end_game_state.c:60: }
	C$end_game_state.c$62$2_0$137	= .
	.globl	C$end_game_state.c$62$2_0$137
;src/states/end_game_state.c:62: waitForFrame();
	jp	_waitForFrame
00131$:
	C$end_game_state.c$63$1_0$135	= .
	.globl	C$end_game_state.c$63$1_0$135
;src/states/end_game_state.c:63: }  else if(end_state_status == 2) {
	ld	a, (_end_state_status+0)
	sub	a, #0x02
	jr	NZ, 00128$
	C$end_game_state.c$64$2_0$141	= .
	.globl	C$end_game_state.c$64$2_0$141
;src/states/end_game_state.c:64: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$end_game_state.c$65$2_0$141	= .
	.globl	C$end_game_state.c$65$2_0$141
;src/states/end_game_state.c:65: while(frame_cnt<L_PERIOD) {
00110$:
	ld	de, #0x00b4
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00112$
	C$end_game_state.c$66$3_0$142	= .
	.globl	C$end_game_state.c$66$3_0$142
;src/states/end_game_state.c:66: waitForFrame();
	call	_waitForFrame
	jr	00110$
00112$:
	C$end_game_state.c$68$2_0$141	= .
	.globl	C$end_game_state.c$68$2_0$141
;src/states/end_game_state.c:68: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$end_game_state.c$69$2_0$141	= .
	.globl	C$end_game_state.c$69$2_0$141
;src/states/end_game_state.c:69: sprite_fade_out();
	call	_sprite_fade_out
	C$end_game_state.c$70$2_0$141	= .
	.globl	C$end_game_state.c$70$2_0$141
;src/states/end_game_state.c:70: while(frame_cnt<XL_PERIOD) {
00113$:
	ld	de, #0x00f0
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00115$
	C$end_game_state.c$71$3_0$143	= .
	.globl	C$end_game_state.c$71$3_0$143
;src/states/end_game_state.c:71: waitForFrame();
	call	_waitForFrame
	C$end_game_state.c$72$3_0$143	= .
	.globl	C$end_game_state.c$72$3_0$143
;src/states/end_game_state.c:72: scroll_ending();
	call	_scroll_ending
	jr	00113$
00115$:
	C$end_game_state.c$74$2_0$141	= .
	.globl	C$end_game_state.c$74$2_0$141
;src/states/end_game_state.c:74: end_state_status = 3;
	ld	hl, #_end_state_status
	ld	(hl), #0x03
	ret
00128$:
	C$end_game_state.c$75$1_0$135	= .
	.globl	C$end_game_state.c$75$1_0$135
;src/states/end_game_state.c:75: }  else if(end_state_status == 3) {
	ld	a, (_end_state_status+0)
	sub	a, #0x03
	jr	NZ, 00125$
	C$end_game_state.c$76$2_0$144	= .
	.globl	C$end_game_state.c$76$2_0$144
;src/states/end_game_state.c:76: ending_update_face();
	call	_ending_update_face
	C$end_game_state.c$77$2_0$144	= .
	.globl	C$end_game_state.c$77$2_0$144
;src/states/end_game_state.c:77: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$end_game_state.c$78$2_0$144	= .
	.globl	C$end_game_state.c$78$2_0$144
;src/states/end_game_state.c:78: while(frame_cnt<XXL_PERIOD) {
00116$:
	ld	de, #0x01e0
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00118$
	C$end_game_state.c$79$3_0$145	= .
	.globl	C$end_game_state.c$79$3_0$145
;src/states/end_game_state.c:79: waitForFrame();
	call	_waitForFrame
	jr	00116$
00118$:
	C$end_game_state.c$81$2_0$144	= .
	.globl	C$end_game_state.c$81$2_0$144
;src/states/end_game_state.c:81: start_fadeout_music();
	call	_start_fadeout_music
	C$end_game_state.c$82$2_0$144	= .
	.globl	C$end_game_state.c$82$2_0$144
;src/states/end_game_state.c:82: background_fade_out();
	call	_background_fade_out
	C$end_game_state.c$83$2_0$144	= .
	.globl	C$end_game_state.c$83$2_0$144
;src/states/end_game_state.c:83: end_state_status = 4;
	ld	hl, #_end_state_status
	ld	(hl), #0x04
	C$end_game_state.c$84$2_0$144	= .
	.globl	C$end_game_state.c$84$2_0$144
;src/states/end_game_state.c:84: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$end_game_state.c$85$2_0$144	= .
	.globl	C$end_game_state.c$85$2_0$144
;src/states/end_game_state.c:85: endingScrollFinish();
	call	_endingScrollFinish
	C$end_game_state.c$86$2_0$144	= .
	.globl	C$end_game_state.c$86$2_0$144
;src/states/end_game_state.c:86: while(frame_cnt<M_PERIOD) {
00119$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	ret	NC
	C$end_game_state.c$87$3_0$146	= .
	.globl	C$end_game_state.c$87$3_0$146
;src/states/end_game_state.c:87: waitForFrame();
	call	_waitForFrame
	jr	00119$
00125$:
	C$end_game_state.c$89$1_0$135	= .
	.globl	C$end_game_state.c$89$1_0$135
;src/states/end_game_state.c:89: } else if(end_state_status == 4) {
	ld	a, (_end_state_status+0)
	sub	a, #0x04
	ret	NZ
	C$end_game_state.c$90$2_0$147	= .
	.globl	C$end_game_state.c$90$2_0$147
;src/states/end_game_state.c:90: clear_cursor_sprites();
	call	_clear_cursor_sprites
	C$end_game_state.c$91$2_0$147	= .
	.globl	C$end_game_state.c$91$2_0$147
;src/states/end_game_state.c:91: transition_to_state(CREDITS_STATE);
	ld	a, #0x05
	C$end_game_state.c$93$1_0$135	= .
	.globl	C$end_game_state.c$93$1_0$135
;src/states/end_game_state.c:93: }
	C$end_game_state.c$93$1_0$135	= .
	.globl	C$end_game_state.c$93$1_0$135
	XG$end_game_state_update$0$0	= .
	.globl	XG$end_game_state_update$0$0
	jp	_transition_to_state
	G$end_game_state_stop$0$0	= .
	.globl	G$end_game_state_stop$0$0
	C$end_game_state.c$95$1_0$149	= .
	.globl	C$end_game_state.c$95$1_0$149
;src/states/end_game_state.c:95: void end_game_state_stop(void) {
;	---------------------------------
; Function end_game_state_stop
; ---------------------------------
_end_game_state_stop::
	C$end_game_state.c$96$1_0$149	= .
	.globl	C$end_game_state.c$96$1_0$149
;src/states/end_game_state.c:96: stop_music();
	call	_PSGStop
	C$end_game_state.c$97$1_0$149	= .
	.globl	C$end_game_state.c$97$1_0$149
;src/states/end_game_state.c:97: stop_fx();
	C$end_game_state.c$98$1_0$149	= .
	.globl	C$end_game_state.c$98$1_0$149
;src/states/end_game_state.c:98: }
	C$end_game_state.c$98$1_0$149	= .
	.globl	C$end_game_state.c$98$1_0$149
	XG$end_game_state_stop$0$0	= .
	.globl	XG$end_game_state_stop$0$0
	jp	_PSGSFXStop
	.area _CODE
	.area _INITIALIZER
Fend_game_state$__xinit_end_state_status$0_0$0 == .
__xinit__end_state_status:
	.db #0x00	; 0
Fend_game_state$__xinit_end_currentLine$0_0$0 == .
__xinit__end_currentLine:
	.db #0x00	; 0
	.area _CABS (ABS)
