;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module globalvariables
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _background_palette
	.globl _sprite_palette
	.globl _statemanager
	.globl _currentSaveSystem
	.globl _highscore_table
	.globl _highscore_table_backup
	.globl _active_points_list
	.globl _current_player
	.globl _number_human_players
	.globl _next_square
	.globl _current_square
	.globl _active_pieces
	.globl _max_row_index
	.globl _row_index
	.globl _piece_index
	.globl _suitable_columns
	.globl _board
	.globl _rotation_counter
	.globl _movement_counter
	.globl _current_rotation
	.globl _target_rotation
	.globl _target_row
	.globl _target_column
	.globl _endOfDrawingLevel
	.globl _decodingIndex
	.globl _newLevel
	.globl _resetPressed
	.globl _rotation2Action
	.globl _rotation1Action
	.globl _downAction
	.globl _rightAction
	.globl _leftAction
	.globl _level
	.globl _fallAcum
	.globl _fallSpeed
	.globl _generateBonus
	.globl _isHurryUp
	.globl _isPaletteRed
	.globl _piecesDestroyed
	.globl _piecesUpdated
	.globl _last_square_x
	.globl _bonusSamplePlay
	.globl _bonusScore
	.globl _highScoreReached
	.globl _score_p2
	.globl _score_p1
	.globl _game_status
	.globl _end_state
	.globl _preserve_context
	.globl _previous_state
	.globl _next_state
	.globl _current_state
	.globl _currentScrollColumn
	.globl _creditsEnd
	.globl _waveIndex
	.globl _cursory
	.globl _cursorx
	.globl _lineCnt
	.globl _scroll_x
	.globl _current_atenuation
	.globl _target_atenuation
	.globl _current_fx_bank
	.globl _current_music_bank
	.globl _current_resource_bank
	.globl _repeat_rotation
	.globl _repeat_movement
	.globl _button_input
	.globl _rotation_input
	.globl _movement_direction
	.globl _input_cnt_rotation
	.globl _input_cnt_direction
	.globl _level_timer
	.globl _frame_cnt
	.globl _rand_index
	.globl _pause
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
G$pause$0_0$0==.
_pause::
	.ds 1
G$rand_index$0_0$0==.
_rand_index::
	.ds 1
G$frame_cnt$0_0$0==.
_frame_cnt::
	.ds 2
G$level_timer$0_0$0==.
_level_timer::
	.ds 2
G$input_cnt_direction$0_0$0==.
_input_cnt_direction::
	.ds 2
G$input_cnt_rotation$0_0$0==.
_input_cnt_rotation::
	.ds 2
G$movement_direction$0_0$0==.
_movement_direction::
	.ds 2
G$rotation_input$0_0$0==.
_rotation_input::
	.ds 2
G$button_input$0_0$0==.
_button_input::
	.ds 2
G$repeat_movement$0_0$0==.
_repeat_movement::
	.ds 2
G$repeat_rotation$0_0$0==.
_repeat_rotation::
	.ds 2
G$current_resource_bank$0_0$0==.
_current_resource_bank::
	.ds 1
G$current_music_bank$0_0$0==.
_current_music_bank::
	.ds 1
G$current_fx_bank$0_0$0==.
_current_fx_bank::
	.ds 1
G$target_atenuation$0_0$0==.
_target_atenuation::
	.ds 1
G$current_atenuation$0_0$0==.
_current_atenuation::
	.ds 1
G$scroll_x$0_0$0==.
_scroll_x::
	.ds 12
G$lineCnt$0_0$0==.
_lineCnt::
	.ds 1
G$cursorx$0_0$0==.
_cursorx::
	.ds 1
G$cursory$0_0$0==.
_cursory::
	.ds 1
G$waveIndex$0_0$0==.
_waveIndex::
	.ds 1
G$creditsEnd$0_0$0==.
_creditsEnd::
	.ds 1
G$currentScrollColumn$0_0$0==.
_currentScrollColumn::
	.ds 1
G$current_state$0_0$0==.
_current_state::
	.ds 1
G$next_state$0_0$0==.
_next_state::
	.ds 1
G$previous_state$0_0$0==.
_previous_state::
	.ds 1
G$preserve_context$0_0$0==.
_preserve_context::
	.ds 1
G$end_state$0_0$0==.
_end_state::
	.ds 1
G$game_status$0_0$0==.
_game_status::
	.ds 1
G$score_p1$0_0$0==.
_score_p1::
	.ds 2
G$score_p2$0_0$0==.
_score_p2::
	.ds 2
G$highScoreReached$0_0$0==.
_highScoreReached::
	.ds 1
G$bonusScore$0_0$0==.
_bonusScore::
	.ds 1
G$bonusSamplePlay$0_0$0==.
_bonusSamplePlay::
	.ds 1
G$last_square_x$0_0$0==.
_last_square_x::
	.ds 1
G$piecesUpdated$0_0$0==.
_piecesUpdated::
	.ds 1
G$piecesDestroyed$0_0$0==.
_piecesDestroyed::
	.ds 1
G$isPaletteRed$0_0$0==.
_isPaletteRed::
	.ds 1
G$isHurryUp$0_0$0==.
_isHurryUp::
	.ds 1
G$generateBonus$0_0$0==.
_generateBonus::
	.ds 1
G$fallSpeed$0_0$0==.
_fallSpeed::
	.ds 1
G$fallAcum$0_0$0==.
_fallAcum::
	.ds 2
G$level$0_0$0==.
_level::
	.ds 1
G$leftAction$0_0$0==.
_leftAction::
	.ds 1
G$rightAction$0_0$0==.
_rightAction::
	.ds 1
G$downAction$0_0$0==.
_downAction::
	.ds 1
G$rotation1Action$0_0$0==.
_rotation1Action::
	.ds 1
G$rotation2Action$0_0$0==.
_rotation2Action::
	.ds 1
G$resetPressed$0_0$0==.
_resetPressed::
	.ds 1
G$newLevel$0_0$0==.
_newLevel::
	.ds 1
G$decodingIndex$0_0$0==.
_decodingIndex::
	.ds 1
G$endOfDrawingLevel$0_0$0==.
_endOfDrawingLevel::
	.ds 1
G$target_column$0_0$0==.
_target_column::
	.ds 1
G$target_row$0_0$0==.
_target_row::
	.ds 1
G$target_rotation$0_0$0==.
_target_rotation::
	.ds 1
G$current_rotation$0_0$0==.
_current_rotation::
	.ds 1
G$movement_counter$0_0$0==.
_movement_counter::
	.ds 1
G$rotation_counter$0_0$0==.
_rotation_counter::
	.ds 1
G$board$0_0$0==.
_board::
	.ds 220
G$suitable_columns$0_0$0==.
_suitable_columns::
	.ds 10
G$piece_index$0_0$0==.
_piece_index::
	.ds 1
G$row_index$0_0$0==.
_row_index::
	.ds 1
G$max_row_index$0_0$0==.
_max_row_index::
	.ds 1
G$active_pieces$0_0$0==.
_active_pieces::
	.ds 30
G$current_square$0_0$0==.
_current_square::
	.ds 6
G$next_square$0_0$0==.
_next_square::
	.ds 6
G$number_human_players$0_0$0==.
_number_human_players::
	.ds 1
G$current_player$0_0$0==.
_current_player::
	.ds 1
G$active_points_list$0_0$0==.
_active_points_list::
	.ds 2
G$highscore_table_backup$0_0$0==.
_highscore_table_backup::
	.ds 2
G$highscore_table$0_0$0==.
_highscore_table::
	.ds 52
G$currentSaveSystem$0_0$0==.
_currentSaveSystem::
	.ds 1
G$statemanager$0_0$0==.
_statemanager::
	.ds 66
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$sprite_palette$0_0$0==.
_sprite_palette::
	.ds 16
G$background_palette$0_0$0==.
_background_palette::
	.ds 16
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	.area _CODE
	.area _INITIALIZER
Fglobalvariables$__xinit_sprite_palette$0_0$0 == .
__xinit__sprite_palette:
	.db #0x00	; 0
	.db #0x3f	; 63
	.db #0x36	; 54	'6'
	.db #0x3e	; 62
	.db #0x3b	; 59
	.db #0x11	; 17
	.db #0x29	; 41
	.db #0x2a	; 42
	.db #0x00	; 0
	.db #0x3f	; 63
	.db #0x2a	; 42
	.db #0x00	; 0
	.db #0x3f	; 63
	.db #0x2a	; 42
	.db #0x0f	; 15
	.db #0x07	; 7
Fglobalvariables$__xinit_background_palette$0_0$0 == .
__xinit__background_palette:
	.db #0x38	; 56	'8'
	.db #0x00	; 0
	.db #0x38	; 56	'8'
	.db #0x38	; 56	'8'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.area _CABS (ABS)
