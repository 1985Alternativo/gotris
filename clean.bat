@echo off
SET CURRENTDIR="%cd%"
SET DOCKER_FLAGS=--rm -v %CURRENTDIR%:/home/sms-tk/host -w /home/sms-tk/host -t retcon85/toolchain-sms -c
docker run  %DOCKER_FLAGS% "make clean"
