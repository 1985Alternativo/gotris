;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module boardtest
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl ___SMS__SDSC_signature
	.globl ___SMS__SDSC_descr
	.globl ___SMS__SDSC_name
	.globl ___SMS__SDSC_author
	.globl ___SMS__SEGA_signature
	.globl _main
	.globl _test4
	.globl _test3
	.globl _test2
	.globl _test1
	.globl _check_disconnected
	.globl _compute_paths
	.globl _compute_path2
	.globl _printSuitables
	.globl _printHexadecimal
	.globl _printHexadecimalCharacter
	.globl _printBoard
	.globl _initializeBoardTest4
	.globl _initializeBoardTest2
	.globl _initializeBoardTest1
	.globl _waitForFrame
	.globl _clear_tilemap
	.globl _mark_as_disconnected
	.globl _neighbour_notprocessed
	.globl _neighbour_connected
	.globl _neighbour_emptyspace
	.globl _init_board
	.globl _decideNextMove
	.globl _SMS_getKeysPressed
	.globl _SMS_autoSetUpTextRenderer
	.globl _SMS_crt0_RST08
	.globl _SMS_init
	.globl _printf
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$initializeBoardTest1$0$0	= .
	.globl	G$initializeBoardTest1$0$0
	C$boardtest.c$11$0_0$137	= .
	.globl	C$boardtest.c$11$0_0$137
;src/boardtest.c:11: void initializeBoardTest1(void) {
;	---------------------------------
; Function initializeBoardTest1
; ---------------------------------
_initializeBoardTest1::
	C$boardtest.c$12$1_0$137	= .
	.globl	C$boardtest.c$12$1_0$137
;src/boardtest.c:12: init_board();
	call	_init_board
	C$boardtest.c$16$1_1$138	= .
	.globl	C$boardtest.c$16$1_1$138
;src/boardtest.c:16: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 190)
	ld	(hl), #0x01
	C$boardtest.c$18$1_1$138	= .
	.globl	C$boardtest.c$18$1_1$138
;src/boardtest.c:18: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 191)
	ld	(hl), #0x01
	C$boardtest.c$20$1_1$138	= .
	.globl	C$boardtest.c$20$1_1$138
;src/boardtest.c:20: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 192)
	ld	(hl), #0x01
	C$boardtest.c$23$1_1$138	= .
	.globl	C$boardtest.c$23$1_1$138
;src/boardtest.c:23: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 200)
	ld	(hl), #0x01
	C$boardtest.c$25$1_1$138	= .
	.globl	C$boardtest.c$25$1_1$138
;src/boardtest.c:25: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 201)
	ld	(hl), #0x01
	C$boardtest.c$27$1_1$138	= .
	.globl	C$boardtest.c$27$1_1$138
;src/boardtest.c:27: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 202)
	ld	(hl), #0x04
	C$boardtest.c$30$1_1$138	= .
	.globl	C$boardtest.c$30$1_1$138
;src/boardtest.c:30: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 210)
	ld	(hl), #0x01
	C$boardtest.c$32$1_1$138	= .
	.globl	C$boardtest.c$32$1_1$138
;src/boardtest.c:32: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 211)
	ld	(hl), #0x01
	C$boardtest.c$34$1_1$138	= .
	.globl	C$boardtest.c$34$1_1$138
;src/boardtest.c:34: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 212)
	ld	(hl), #0x01
	C$boardtest.c$35$1_1$137	= .
	.globl	C$boardtest.c$35$1_1$137
;src/boardtest.c:35: }
	C$boardtest.c$35$1_1$137	= .
	.globl	C$boardtest.c$35$1_1$137
	XG$initializeBoardTest1$0$0	= .
	.globl	XG$initializeBoardTest1$0$0
	ret
	G$initializeBoardTest2$0$0	= .
	.globl	G$initializeBoardTest2$0$0
	C$boardtest.c$37$1_1$140	= .
	.globl	C$boardtest.c$37$1_1$140
;src/boardtest.c:37: void initializeBoardTest2(void) {
;	---------------------------------
; Function initializeBoardTest2
; ---------------------------------
_initializeBoardTest2::
	C$boardtest.c$38$1_0$140	= .
	.globl	C$boardtest.c$38$1_0$140
;src/boardtest.c:38: init_board();
	call	_init_board
	C$boardtest.c$42$1_1$141	= .
	.globl	C$boardtest.c$42$1_1$141
;src/boardtest.c:42: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 190)
	ld	(hl), #0x01
	C$boardtest.c$44$1_1$141	= .
	.globl	C$boardtest.c$44$1_1$141
;src/boardtest.c:44: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 192)
	ld	(hl), #0x01
	C$boardtest.c$47$1_1$141	= .
	.globl	C$boardtest.c$47$1_1$141
;src/boardtest.c:47: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 200)
	ld	(hl), #0x01
	C$boardtest.c$49$1_1$141	= .
	.globl	C$boardtest.c$49$1_1$141
;src/boardtest.c:49: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 201)
	ld	(hl), #0x04
	C$boardtest.c$51$1_1$141	= .
	.globl	C$boardtest.c$51$1_1$141
;src/boardtest.c:51: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 202)
	ld	(hl), #0x01
	C$boardtest.c$54$1_1$141	= .
	.globl	C$boardtest.c$54$1_1$141
;src/boardtest.c:54: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 210)
	ld	(hl), #0x01
	C$boardtest.c$56$1_1$141	= .
	.globl	C$boardtest.c$56$1_1$141
;src/boardtest.c:56: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 211)
	ld	(hl), #0x01
	C$boardtest.c$58$1_1$141	= .
	.globl	C$boardtest.c$58$1_1$141
;src/boardtest.c:58: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 212)
	ld	(hl), #0x01
	C$boardtest.c$59$1_1$140	= .
	.globl	C$boardtest.c$59$1_1$140
;src/boardtest.c:59: }
	C$boardtest.c$59$1_1$140	= .
	.globl	C$boardtest.c$59$1_1$140
	XG$initializeBoardTest2$0$0	= .
	.globl	XG$initializeBoardTest2$0$0
	ret
	G$initializeBoardTest4$0$0	= .
	.globl	G$initializeBoardTest4$0$0
	C$boardtest.c$61$1_1$143	= .
	.globl	C$boardtest.c$61$1_1$143
;src/boardtest.c:61: void initializeBoardTest4(void) {
;	---------------------------------
; Function initializeBoardTest4
; ---------------------------------
_initializeBoardTest4::
	C$boardtest.c$62$1_0$143	= .
	.globl	C$boardtest.c$62$1_0$143
;src/boardtest.c:62: init_board();
	call	_init_board
	C$boardtest.c$66$1_1$144	= .
	.globl	C$boardtest.c$66$1_1$144
;src/boardtest.c:66: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 170)
	ld	(hl), #0x04
	C$boardtest.c$68$1_1$144	= .
	.globl	C$boardtest.c$68$1_1$144
;src/boardtest.c:68: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 171)
	ld	(hl), #0x04
	C$boardtest.c$70$1_1$144	= .
	.globl	C$boardtest.c$70$1_1$144
;src/boardtest.c:70: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 172)
	ld	(hl), #0x01
	C$boardtest.c$73$1_1$144	= .
	.globl	C$boardtest.c$73$1_1$144
;src/boardtest.c:73: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 180)
	ld	(hl), #0x01
	C$boardtest.c$75$1_1$144	= .
	.globl	C$boardtest.c$75$1_1$144
;src/boardtest.c:75: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 181)
	ld	(hl), #0x04
	C$boardtest.c$77$1_1$144	= .
	.globl	C$boardtest.c$77$1_1$144
;src/boardtest.c:77: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 182)
	ld	(hl), #0x01
	C$boardtest.c$79$1_1$144	= .
	.globl	C$boardtest.c$79$1_1$144
;src/boardtest.c:79: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 183)
	ld	(hl), #0x01
	C$boardtest.c$81$1_1$144	= .
	.globl	C$boardtest.c$81$1_1$144
;src/boardtest.c:81: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 184)
	ld	(hl), #0x04
	C$boardtest.c$83$1_1$144	= .
	.globl	C$boardtest.c$83$1_1$144
;src/boardtest.c:83: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 185)
	ld	(hl), #0x01
	C$boardtest.c$86$1_1$144	= .
	.globl	C$boardtest.c$86$1_1$144
;src/boardtest.c:86: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 190)
	ld	(hl), #0x01
	C$boardtest.c$88$1_1$144	= .
	.globl	C$boardtest.c$88$1_1$144
;src/boardtest.c:88: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 191)
	ld	(hl), #0x01
	C$boardtest.c$90$1_1$144	= .
	.globl	C$boardtest.c$90$1_1$144
;src/boardtest.c:90: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 192)
	ld	(hl), #0x04
	C$boardtest.c$92$1_1$144	= .
	.globl	C$boardtest.c$92$1_1$144
;src/boardtest.c:92: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 193)
	ld	(hl), #0x04
	C$boardtest.c$94$1_1$144	= .
	.globl	C$boardtest.c$94$1_1$144
;src/boardtest.c:94: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 194)
	ld	(hl), #0x04
	C$boardtest.c$96$1_1$144	= .
	.globl	C$boardtest.c$96$1_1$144
;src/boardtest.c:96: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 195)
	ld	(hl), #0x01
	C$boardtest.c$99$1_1$144	= .
	.globl	C$boardtest.c$99$1_1$144
;src/boardtest.c:99: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 200)
	ld	(hl), #0x01
	C$boardtest.c$101$1_1$144	= .
	.globl	C$boardtest.c$101$1_1$144
;src/boardtest.c:101: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 201)
	ld	(hl), #0x01
	C$boardtest.c$103$1_1$144	= .
	.globl	C$boardtest.c$103$1_1$144
;src/boardtest.c:103: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 202)
	ld	(hl), #0x01
	C$boardtest.c$105$1_1$144	= .
	.globl	C$boardtest.c$105$1_1$144
;src/boardtest.c:105: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 203)
	ld	(hl), #0x01
	C$boardtest.c$107$1_1$144	= .
	.globl	C$boardtest.c$107$1_1$144
;src/boardtest.c:107: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 204)
	ld	(hl), #0x04
	C$boardtest.c$109$1_1$144	= .
	.globl	C$boardtest.c$109$1_1$144
;src/boardtest.c:109: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 205)
	ld	(hl), #0x01
	C$boardtest.c$111$1_1$144	= .
	.globl	C$boardtest.c$111$1_1$144
;src/boardtest.c:111: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 206)
	ld	(hl), #0x04
	C$boardtest.c$113$1_1$144	= .
	.globl	C$boardtest.c$113$1_1$144
;src/boardtest.c:113: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 207)
	ld	(hl), #0x04
	C$boardtest.c$116$1_1$144	= .
	.globl	C$boardtest.c$116$1_1$144
;src/boardtest.c:116: board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
	ld	hl, #(_board + 210)
	ld	(hl), #0x04
	C$boardtest.c$118$1_1$144	= .
	.globl	C$boardtest.c$118$1_1$144
;src/boardtest.c:118: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 211)
	ld	(hl), #0x01
	C$boardtest.c$120$1_1$144	= .
	.globl	C$boardtest.c$120$1_1$144
;src/boardtest.c:120: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 212)
	ld	(hl), #0x01
	C$boardtest.c$122$1_1$144	= .
	.globl	C$boardtest.c$122$1_1$144
;src/boardtest.c:122: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 213)
	ld	(hl), #0x01
	C$boardtest.c$124$1_1$144	= .
	.globl	C$boardtest.c$124$1_1$144
;src/boardtest.c:124: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 214)
	ld	(hl), #0x01
	C$boardtest.c$126$1_1$144	= .
	.globl	C$boardtest.c$126$1_1$144
;src/boardtest.c:126: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 215)
	ld	(hl), #0x01
	C$boardtest.c$128$1_1$144	= .
	.globl	C$boardtest.c$128$1_1$144
;src/boardtest.c:128: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 216)
	ld	(hl), #0x01
	C$boardtest.c$130$1_1$144	= .
	.globl	C$boardtest.c$130$1_1$144
;src/boardtest.c:130: board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
	ld	hl, #(_board + 217)
	ld	(hl), #0x01
	C$boardtest.c$131$1_1$143	= .
	.globl	C$boardtest.c$131$1_1$143
;src/boardtest.c:131: }
	C$boardtest.c$131$1_1$143	= .
	.globl	C$boardtest.c$131$1_1$143
	XG$initializeBoardTest4$0$0	= .
	.globl	XG$initializeBoardTest4$0$0
	ret
	G$printBoard$0$0	= .
	.globl	G$printBoard$0$0
	C$boardtest.c$133$1_1$146	= .
	.globl	C$boardtest.c$133$1_1$146
;src/boardtest.c:133: void printBoard(void) {
;	---------------------------------
; Function printBoard
; ---------------------------------
_printBoard::
	C$boardtest.c$136$3_0$148	= .
	.globl	C$boardtest.c$136$3_0$148
;src/boardtest.c:136: while(i<BOARD_ROWS) {
	ld	c, #0x00
00104$:
	ld	a, c
	sub	a, #0x16
	ret	NC
	C$boardtest.c$137$2_0$147	= .
	.globl	C$boardtest.c$137$2_0$147
;src/boardtest.c:137: SMS_setNextTileatXY(0, i);
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	rst	#0x08
	pop	bc
	C$boardtest.c$139$1_0$146	= .
	.globl	C$boardtest.c$139$1_0$146
;src/boardtest.c:139: while(j<BOARD_COLUMNS) {
	ld	b, #0x00
00101$:
	ld	a, b
	sub	a, #0x0a
	jr	NC, 00103$
	C$boardtest.c$140$3_0$148	= .
	.globl	C$boardtest.c$140$3_0$148
;src/boardtest.c:140: printf("%d",board[(i << 3) + (i << 1) + j]);
	ld	e, c
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	add	hl, hl
	ex	de, hl
	add	hl, de
	ld	e, b
	ld	d, #0x00
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	e, (hl)
	ld	d, #0x00
	push	bc
	push	de
	ld	hl, #___str_0
	push	hl
	call	_printf
	pop	af
	pop	af
	pop	bc
	C$boardtest.c$141$3_0$148	= .
	.globl	C$boardtest.c$141$3_0$148
;src/boardtest.c:141: j++;
	inc	b
	jr	00101$
00103$:
	C$boardtest.c$143$2_0$147	= .
	.globl	C$boardtest.c$143$2_0$147
;src/boardtest.c:143: i++;
	inc	c
	C$boardtest.c$145$1_0$146	= .
	.globl	C$boardtest.c$145$1_0$146
;src/boardtest.c:145: }
	C$boardtest.c$145$1_0$146	= .
	.globl	C$boardtest.c$145$1_0$146
	XG$printBoard$0$0	= .
	.globl	XG$printBoard$0$0
	jr	00104$
Fboardtest$__str_0$0_0$0 == .
___str_0:
	.ascii "%d"
	.db 0x00
	G$printHexadecimalCharacter$0$0	= .
	.globl	G$printHexadecimalCharacter$0$0
	C$boardtest.c$146$1_0$150	= .
	.globl	C$boardtest.c$146$1_0$150
;src/boardtest.c:146: void printHexadecimalCharacter(u8 number) {
;	---------------------------------
; Function printHexadecimalCharacter
; ---------------------------------
_printHexadecimalCharacter::
	ld	c, a
	C$boardtest.c$147$1_0$150	= .
	.globl	C$boardtest.c$147$1_0$150
;src/boardtest.c:147: switch(number) {
	ld	a, #0x15
	sub	a, c
	jp	C, 00123$
	ld	b, #0x00
	ld	hl, #00132$
	add	hl, bc
	add	hl, bc
	add	hl, bc
	jp	(hl)
00132$:
	jp	00101$
	jp	00102$
	jp	00103$
	jp	00104$
	jp	00105$
	jp	00106$
	jp	00107$
	jp	00108$
	jp	00109$
	jp	00110$
	jp	00111$
	jp	00112$
	jp	00113$
	jp	00114$
	jp	00115$
	jp	00116$
	jp	00117$
	jp	00118$
	jp	00119$
	jp	00120$
	jp	00121$
	jp	00122$
	C$boardtest.c$148$2_0$151	= .
	.globl	C$boardtest.c$148$2_0$151
;src/boardtest.c:148: case 0:
00101$:
	C$boardtest.c$149$2_0$151	= .
	.globl	C$boardtest.c$149$2_0$151
;src/boardtest.c:149: printf("0");
	ld	hl, #___str_1
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$150$2_0$151	= .
	.globl	C$boardtest.c$150$2_0$151
;src/boardtest.c:150: break;
	ret
	C$boardtest.c$151$2_0$151	= .
	.globl	C$boardtest.c$151$2_0$151
;src/boardtest.c:151: case 1:
00102$:
	C$boardtest.c$152$2_0$151	= .
	.globl	C$boardtest.c$152$2_0$151
;src/boardtest.c:152: printf("1");
	ld	hl, #___str_2
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$153$2_0$151	= .
	.globl	C$boardtest.c$153$2_0$151
;src/boardtest.c:153: break;
	ret
	C$boardtest.c$154$2_0$151	= .
	.globl	C$boardtest.c$154$2_0$151
;src/boardtest.c:154: case 2:
00103$:
	C$boardtest.c$155$2_0$151	= .
	.globl	C$boardtest.c$155$2_0$151
;src/boardtest.c:155: printf("2");
	ld	hl, #___str_3
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$156$2_0$151	= .
	.globl	C$boardtest.c$156$2_0$151
;src/boardtest.c:156: break;
	ret
	C$boardtest.c$157$2_0$151	= .
	.globl	C$boardtest.c$157$2_0$151
;src/boardtest.c:157: case 3:
00104$:
	C$boardtest.c$158$2_0$151	= .
	.globl	C$boardtest.c$158$2_0$151
;src/boardtest.c:158: printf("3");
	ld	hl, #___str_4
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$159$2_0$151	= .
	.globl	C$boardtest.c$159$2_0$151
;src/boardtest.c:159: break;
	ret
	C$boardtest.c$160$2_0$151	= .
	.globl	C$boardtest.c$160$2_0$151
;src/boardtest.c:160: case 4:
00105$:
	C$boardtest.c$161$2_0$151	= .
	.globl	C$boardtest.c$161$2_0$151
;src/boardtest.c:161: printf("4");
	ld	hl, #___str_5
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$162$2_0$151	= .
	.globl	C$boardtest.c$162$2_0$151
;src/boardtest.c:162: break;
	ret
	C$boardtest.c$163$2_0$151	= .
	.globl	C$boardtest.c$163$2_0$151
;src/boardtest.c:163: case 5:
00106$:
	C$boardtest.c$164$2_0$151	= .
	.globl	C$boardtest.c$164$2_0$151
;src/boardtest.c:164: printf("5");
	ld	hl, #___str_6
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$165$2_0$151	= .
	.globl	C$boardtest.c$165$2_0$151
;src/boardtest.c:165: break;
	ret
	C$boardtest.c$166$2_0$151	= .
	.globl	C$boardtest.c$166$2_0$151
;src/boardtest.c:166: case 6:
00107$:
	C$boardtest.c$167$2_0$151	= .
	.globl	C$boardtest.c$167$2_0$151
;src/boardtest.c:167: printf("6");
	ld	hl, #___str_7
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$168$2_0$151	= .
	.globl	C$boardtest.c$168$2_0$151
;src/boardtest.c:168: break;
	ret
	C$boardtest.c$169$2_0$151	= .
	.globl	C$boardtest.c$169$2_0$151
;src/boardtest.c:169: case 7:
00108$:
	C$boardtest.c$170$2_0$151	= .
	.globl	C$boardtest.c$170$2_0$151
;src/boardtest.c:170: printf("7");
	ld	hl, #___str_8
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$171$2_0$151	= .
	.globl	C$boardtest.c$171$2_0$151
;src/boardtest.c:171: break;
	ret
	C$boardtest.c$172$2_0$151	= .
	.globl	C$boardtest.c$172$2_0$151
;src/boardtest.c:172: case 8:
00109$:
	C$boardtest.c$173$2_0$151	= .
	.globl	C$boardtest.c$173$2_0$151
;src/boardtest.c:173: printf("8");
	ld	hl, #___str_9
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$174$2_0$151	= .
	.globl	C$boardtest.c$174$2_0$151
;src/boardtest.c:174: break;
	ret
	C$boardtest.c$175$2_0$151	= .
	.globl	C$boardtest.c$175$2_0$151
;src/boardtest.c:175: case 9:
00110$:
	C$boardtest.c$176$2_0$151	= .
	.globl	C$boardtest.c$176$2_0$151
;src/boardtest.c:176: printf("9");
	ld	hl, #___str_10
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$177$2_0$151	= .
	.globl	C$boardtest.c$177$2_0$151
;src/boardtest.c:177: break;
	ret
	C$boardtest.c$178$2_0$151	= .
	.globl	C$boardtest.c$178$2_0$151
;src/boardtest.c:178: case 10:
00111$:
	C$boardtest.c$179$2_0$151	= .
	.globl	C$boardtest.c$179$2_0$151
;src/boardtest.c:179: printf("A");
	ld	hl, #___str_11
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$180$2_0$151	= .
	.globl	C$boardtest.c$180$2_0$151
;src/boardtest.c:180: break;
	ret
	C$boardtest.c$181$2_0$151	= .
	.globl	C$boardtest.c$181$2_0$151
;src/boardtest.c:181: case 11:
00112$:
	C$boardtest.c$182$2_0$151	= .
	.globl	C$boardtest.c$182$2_0$151
;src/boardtest.c:182: printf("B");
	ld	hl, #___str_12
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$183$2_0$151	= .
	.globl	C$boardtest.c$183$2_0$151
;src/boardtest.c:183: break;
	ret
	C$boardtest.c$184$2_0$151	= .
	.globl	C$boardtest.c$184$2_0$151
;src/boardtest.c:184: case 12:
00113$:
	C$boardtest.c$185$2_0$151	= .
	.globl	C$boardtest.c$185$2_0$151
;src/boardtest.c:185: printf("C");
	ld	hl, #___str_13
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$186$2_0$151	= .
	.globl	C$boardtest.c$186$2_0$151
;src/boardtest.c:186: break;
	ret
	C$boardtest.c$187$2_0$151	= .
	.globl	C$boardtest.c$187$2_0$151
;src/boardtest.c:187: case 13:
00114$:
	C$boardtest.c$188$2_0$151	= .
	.globl	C$boardtest.c$188$2_0$151
;src/boardtest.c:188: printf("D");
	ld	hl, #___str_14
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$189$2_0$151	= .
	.globl	C$boardtest.c$189$2_0$151
;src/boardtest.c:189: break;
	ret
	C$boardtest.c$190$2_0$151	= .
	.globl	C$boardtest.c$190$2_0$151
;src/boardtest.c:190: case 14:
00115$:
	C$boardtest.c$191$2_0$151	= .
	.globl	C$boardtest.c$191$2_0$151
;src/boardtest.c:191: printf("E");
	ld	hl, #___str_15
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$192$2_0$151	= .
	.globl	C$boardtest.c$192$2_0$151
;src/boardtest.c:192: break;
	ret
	C$boardtest.c$193$2_0$151	= .
	.globl	C$boardtest.c$193$2_0$151
;src/boardtest.c:193: case 15:
00116$:
	C$boardtest.c$194$2_0$151	= .
	.globl	C$boardtest.c$194$2_0$151
;src/boardtest.c:194: printf("F");
	ld	hl, #___str_16
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$195$2_0$151	= .
	.globl	C$boardtest.c$195$2_0$151
;src/boardtest.c:195: break;     
	ret
	C$boardtest.c$196$2_0$151	= .
	.globl	C$boardtest.c$196$2_0$151
;src/boardtest.c:196: case 16: //I'm really clever here, hehehe
00117$:
	C$boardtest.c$197$2_0$151	= .
	.globl	C$boardtest.c$197$2_0$151
;src/boardtest.c:197: printf("G");
	ld	hl, #___str_17
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$198$2_0$151	= .
	.globl	C$boardtest.c$198$2_0$151
;src/boardtest.c:198: break;
	ret
	C$boardtest.c$199$2_0$151	= .
	.globl	C$boardtest.c$199$2_0$151
;src/boardtest.c:199: case 17:
00118$:
	C$boardtest.c$200$2_0$151	= .
	.globl	C$boardtest.c$200$2_0$151
;src/boardtest.c:200: printf("H");
	ld	hl, #___str_18
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$201$2_0$151	= .
	.globl	C$boardtest.c$201$2_0$151
;src/boardtest.c:201: break;
	ret
	C$boardtest.c$202$2_0$151	= .
	.globl	C$boardtest.c$202$2_0$151
;src/boardtest.c:202: case 18:
00119$:
	C$boardtest.c$203$2_0$151	= .
	.globl	C$boardtest.c$203$2_0$151
;src/boardtest.c:203: printf("I");
	ld	hl, #___str_19
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$204$2_0$151	= .
	.globl	C$boardtest.c$204$2_0$151
;src/boardtest.c:204: break;
	ret
	C$boardtest.c$205$2_0$151	= .
	.globl	C$boardtest.c$205$2_0$151
;src/boardtest.c:205: case 19:
00120$:
	C$boardtest.c$206$2_0$151	= .
	.globl	C$boardtest.c$206$2_0$151
;src/boardtest.c:206: printf("J");
	ld	hl, #___str_20
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$207$2_0$151	= .
	.globl	C$boardtest.c$207$2_0$151
;src/boardtest.c:207: break;
	ret
	C$boardtest.c$208$2_0$151	= .
	.globl	C$boardtest.c$208$2_0$151
;src/boardtest.c:208: case 20:
00121$:
	C$boardtest.c$209$2_0$151	= .
	.globl	C$boardtest.c$209$2_0$151
;src/boardtest.c:209: printf("K");
	ld	hl, #___str_21
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$210$2_0$151	= .
	.globl	C$boardtest.c$210$2_0$151
;src/boardtest.c:210: break;
	ret
	C$boardtest.c$211$2_0$151	= .
	.globl	C$boardtest.c$211$2_0$151
;src/boardtest.c:211: case 21:
00122$:
	C$boardtest.c$212$2_0$151	= .
	.globl	C$boardtest.c$212$2_0$151
;src/boardtest.c:212: printf("L");
	ld	hl, #___str_22
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$213$2_0$151	= .
	.globl	C$boardtest.c$213$2_0$151
;src/boardtest.c:213: break;       
	ret
	C$boardtest.c$214$2_0$151	= .
	.globl	C$boardtest.c$214$2_0$151
;src/boardtest.c:214: default:
00123$:
	C$boardtest.c$215$2_0$151	= .
	.globl	C$boardtest.c$215$2_0$151
;src/boardtest.c:215: printf("x");
	ld	hl, #___str_23
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$216$1_0$150	= .
	.globl	C$boardtest.c$216$1_0$150
;src/boardtest.c:216: }
	C$boardtest.c$217$1_0$150	= .
	.globl	C$boardtest.c$217$1_0$150
;src/boardtest.c:217: }
	C$boardtest.c$217$1_0$150	= .
	.globl	C$boardtest.c$217$1_0$150
	XG$printHexadecimalCharacter$0$0	= .
	.globl	XG$printHexadecimalCharacter$0$0
	ret
Fboardtest$__str_1$0_0$0 == .
___str_1:
	.ascii "0"
	.db 0x00
Fboardtest$__str_2$0_0$0 == .
___str_2:
	.ascii "1"
	.db 0x00
Fboardtest$__str_3$0_0$0 == .
___str_3:
	.ascii "2"
	.db 0x00
Fboardtest$__str_4$0_0$0 == .
___str_4:
	.ascii "3"
	.db 0x00
Fboardtest$__str_5$0_0$0 == .
___str_5:
	.ascii "4"
	.db 0x00
Fboardtest$__str_6$0_0$0 == .
___str_6:
	.ascii "5"
	.db 0x00
Fboardtest$__str_7$0_0$0 == .
___str_7:
	.ascii "6"
	.db 0x00
Fboardtest$__str_8$0_0$0 == .
___str_8:
	.ascii "7"
	.db 0x00
Fboardtest$__str_9$0_0$0 == .
___str_9:
	.ascii "8"
	.db 0x00
Fboardtest$__str_10$0_0$0 == .
___str_10:
	.ascii "9"
	.db 0x00
Fboardtest$__str_11$0_0$0 == .
___str_11:
	.ascii "A"
	.db 0x00
Fboardtest$__str_12$0_0$0 == .
___str_12:
	.ascii "B"
	.db 0x00
Fboardtest$__str_13$0_0$0 == .
___str_13:
	.ascii "C"
	.db 0x00
Fboardtest$__str_14$0_0$0 == .
___str_14:
	.ascii "D"
	.db 0x00
Fboardtest$__str_15$0_0$0 == .
___str_15:
	.ascii "E"
	.db 0x00
Fboardtest$__str_16$0_0$0 == .
___str_16:
	.ascii "F"
	.db 0x00
Fboardtest$__str_17$0_0$0 == .
___str_17:
	.ascii "G"
	.db 0x00
Fboardtest$__str_18$0_0$0 == .
___str_18:
	.ascii "H"
	.db 0x00
Fboardtest$__str_19$0_0$0 == .
___str_19:
	.ascii "I"
	.db 0x00
Fboardtest$__str_20$0_0$0 == .
___str_20:
	.ascii "J"
	.db 0x00
Fboardtest$__str_21$0_0$0 == .
___str_21:
	.ascii "K"
	.db 0x00
Fboardtest$__str_22$0_0$0 == .
___str_22:
	.ascii "L"
	.db 0x00
Fboardtest$__str_23$0_0$0 == .
___str_23:
	.ascii "x"
	.db 0x00
	G$printHexadecimal$0$0	= .
	.globl	G$printHexadecimal$0$0
	C$boardtest.c$219$1_0$153	= .
	.globl	C$boardtest.c$219$1_0$153
;src/boardtest.c:219: void printHexadecimal(u8 number) {
;	---------------------------------
; Function printHexadecimal
; ---------------------------------
_printHexadecimal::
	C$boardtest.c$220$1_0$153	= .
	.globl	C$boardtest.c$220$1_0$153
;src/boardtest.c:220: u8 part = (number & 0xf0) >> 4;
	ld	c, a
	and	a, #0xf0
	ld	e, a
	ld	d, #0x00
	sra	d
	rr	e
	sra	d
	rr	e
	sra	d
	rr	e
	sra	d
	rr	e
	C$boardtest.c$221$1_0$153	= .
	.globl	C$boardtest.c$221$1_0$153
;src/boardtest.c:221: printHexadecimalCharacter(part);
	push	bc
	ld	a, e
	call	_printHexadecimalCharacter
	pop	bc
	C$boardtest.c$222$1_0$153	= .
	.globl	C$boardtest.c$222$1_0$153
;src/boardtest.c:222: part = number & 0x0f;
	ld	a, c
	and	a, #0x0f
	C$boardtest.c$223$1_0$153	= .
	.globl	C$boardtest.c$223$1_0$153
;src/boardtest.c:223: printHexadecimalCharacter(part);
	call	_printHexadecimalCharacter
	C$boardtest.c$224$1_0$153	= .
	.globl	C$boardtest.c$224$1_0$153
;src/boardtest.c:224: printf(" ");
	ld	hl, #___str_24
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$225$1_0$153	= .
	.globl	C$boardtest.c$225$1_0$153
;src/boardtest.c:225: }
	C$boardtest.c$225$1_0$153	= .
	.globl	C$boardtest.c$225$1_0$153
	XG$printHexadecimal$0$0	= .
	.globl	XG$printHexadecimal$0$0
	ret
Fboardtest$__str_24$0_0$0 == .
___str_24:
	.ascii " "
	.db 0x00
	G$printSuitables$0$0	= .
	.globl	G$printSuitables$0$0
	C$boardtest.c$227$1_0$155	= .
	.globl	C$boardtest.c$227$1_0$155
;src/boardtest.c:227: void printSuitables(void) {
;	---------------------------------
; Function printSuitables
; ---------------------------------
_printSuitables::
	C$boardtest.c$230$1_0$155	= .
	.globl	C$boardtest.c$230$1_0$155
;src/boardtest.c:230: SMS_setNextTileatXY(0, 23);
	ld	hl, #0x7dc0
	rst	#0x08
	C$boardtest.c$231$2_0$156	= .
	.globl	C$boardtest.c$231$2_0$156
;src/boardtest.c:231: while(j<BOARD_COLUMNS) {
	ld	c, #0x00
00101$:
	ld	a, c
	sub	a, #0x0a
	ret	NC
	C$boardtest.c$233$2_0$156	= .
	.globl	C$boardtest.c$233$2_0$156
;src/boardtest.c:233: printHexadecimal(suitable_columns[j]);
	ld	hl, #_suitable_columns
	ld	b, #0x00
	add	hl, bc
	ld	b, (hl)
	push	bc
	ld	a, b
	call	_printHexadecimal
	pop	bc
	C$boardtest.c$234$2_0$156	= .
	.globl	C$boardtest.c$234$2_0$156
;src/boardtest.c:234: j++;
	inc	c
	C$boardtest.c$236$1_0$155	= .
	.globl	C$boardtest.c$236$1_0$155
;src/boardtest.c:236: }
	C$boardtest.c$236$1_0$155	= .
	.globl	C$boardtest.c$236$1_0$155
	XG$printSuitables$0$0	= .
	.globl	XG$printSuitables$0$0
	jr	00101$
	G$compute_path2$0$0	= .
	.globl	G$compute_path2$0$0
	C$boardtest.c$287$1_0$158	= .
	.globl	C$boardtest.c$287$1_0$158
;src/boardtest.c:287: bool compute_path2(u8 x, u8 y) {
;	---------------------------------
; Function compute_path2
; ---------------------------------
_compute_path2::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-20
	add	iy, sp
	ld	sp, iy
	ld	-1 (ix), a
	C$boardtest.c$288$1_0$158	= .
	.globl	C$boardtest.c$288$1_0$158
;src/boardtest.c:288: u8 position = (y << 3) + (y << 1) + x;
	ld	-2 (ix), l
	ld	c, l
	ld	a, c
	add	a, a
	add	a, a
	add	a, a
	sla	c
	add	a, c
	ld	c, -1 (ix)
	add	a, c
	C$boardtest.c$290$1_0$158	= .
	.globl	C$boardtest.c$290$1_0$158
;src/boardtest.c:290: if(board[position] == NOT_PROCESSED_WHITE_CELL) {
	ld	-20 (ix), a
	add	a, #<(_board)
	ld	-19 (ix), a
	ld	a, #0x00
	adc	a, #>(_board)
	ld	-18 (ix), a
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	a, (hl)
	ld	-17 (ix), a
	C$boardtest.c$291$1_0$158	= .
	.globl	C$boardtest.c$291$1_0$158
;src/boardtest.c:291: if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
	ld	a, -2 (ix)
	sub	a, #0x15
	ld	a, #0x01
	jr	Z, 00329$
	xor	a, a
00329$:
	ld	-16 (ix), a
	ld	a, -1 (ix)
	sub	a, #0x09
	ld	a, #0x01
	jr	Z, 00331$
	xor	a, a
00331$:
	ld	-15 (ix), a
	C$boardtest.c$294$1_0$158	= .
	.globl	C$boardtest.c$294$1_0$158
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	add	a, #0x14
	ld	-3 (ix), a
	C$boardtest.c$309$1_0$158	= .
	.globl	C$boardtest.c$309$1_0$158
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	ld	a, c
	add	a, #0x0a
	C$boardtest.c$316$1_0$158	= .
	.globl	C$boardtest.c$316$1_0$158
;src/boardtest.c:316: if(is_free_board_position(position+9)) {
	ld	e, -20 (ix)
	ld	d, #0x00
	C$boardtest.c$317$1_0$158	= .
	.globl	C$boardtest.c$317$1_0$158
;src/boardtest.c:317: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	ld	b, c
	dec	b
	C$boardtest.c$325$1_0$158	= .
	.globl	C$boardtest.c$325$1_0$158
;src/boardtest.c:325: suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	inc	c
	C$boardtest.c$294$1_0$158	= .
	.globl	C$boardtest.c$294$1_0$158
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	push	af
	ld	a, -3 (ix)
	ld	-8 (ix), a
	pop	af
	C$boardtest.c$309$1_0$158	= .
	.globl	C$boardtest.c$309$1_0$158
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	ld	-7 (ix), a
	C$boardtest.c$316$1_0$158	= .
	.globl	C$boardtest.c$316$1_0$158
;src/boardtest.c:316: if(is_free_board_position(position+9)) {
	ld	a, e
	add	a, #0x09
	ld	-6 (ix), a
	ld	a, d
	adc	a, #0x00
	ld	-5 (ix), a
	C$boardtest.c$317$1_0$158	= .
	.globl	C$boardtest.c$317$1_0$158
;src/boardtest.c:317: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	ld	a, b
	ld	-14 (ix), a
	rlca
	sbc	a, a
	ld	-13 (ix), a
	C$boardtest.c$324$1_0$158	= .
	.globl	C$boardtest.c$324$1_0$158
;src/boardtest.c:324: if(is_free_board_position(position+11)) {
	ld	a, e
	add	a, #0x0b
	ld	-4 (ix), a
	ld	a, d
	adc	a, #0x00
	ld	-3 (ix), a
	C$boardtest.c$325$1_0$158	= .
	.globl	C$boardtest.c$325$1_0$158
;src/boardtest.c:325: suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	ld	a, c
	ld	-12 (ix), a
	rlca
	sbc	a, a
	ld	-11 (ix), a
	C$boardtest.c$294$1_0$158	= .
	.globl	C$boardtest.c$294$1_0$158
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	ld	c, -8 (ix)
	ld	b, #0x00
	C$boardtest.c$309$1_0$158	= .
	.globl	C$boardtest.c$309$1_0$158
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	ld	e, -7 (ix)
	ld	d, #0x00
	C$boardtest.c$316$1_0$158	= .
	.globl	C$boardtest.c$316$1_0$158
;src/boardtest.c:316: if(is_free_board_position(position+9)) {
	ld	a, -6 (ix)
	add	a, #<(_board)
	ld	-10 (ix), a
	ld	a, -5 (ix)
	adc	a, #>(_board)
	ld	-9 (ix), a
	C$boardtest.c$324$1_0$158	= .
	.globl	C$boardtest.c$324$1_0$158
;src/boardtest.c:324: if(is_free_board_position(position+11)) {
	ld	a, #<(_board)
	add	a, -4 (ix)
	ld	-8 (ix), a
	ld	a, #>(_board)
	adc	a, -3 (ix)
	ld	-7 (ix), a
	C$boardtest.c$294$1_0$158	= .
	.globl	C$boardtest.c$294$1_0$158
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	C$boardtest.c$309$1_0$158	= .
	.globl	C$boardtest.c$309$1_0$158
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	C$boardtest.c$294$1_0$158	= .
	.globl	C$boardtest.c$294$1_0$158
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	ld	a, c
	add	a, l
	ld	c, a
	ld	a, b
	adc	a, h
	ld	b, a
	C$boardtest.c$309$1_0$158	= .
	.globl	C$boardtest.c$309$1_0$158
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	add	hl, de
	C$boardtest.c$294$1_0$158	= .
	.globl	C$boardtest.c$294$1_0$158
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	ld	a, b
	sla	c
	adc	a, a
	C$boardtest.c$309$1_0$158	= .
	.globl	C$boardtest.c$309$1_0$158
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	add	hl, hl
	C$boardtest.c$294$1_0$158	= .
	.globl	C$boardtest.c$294$1_0$158
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	ld	-6 (ix), c
	or	a, #0x78
	ld	-5 (ix), a
	C$boardtest.c$309$1_0$158	= .
	.globl	C$boardtest.c$309$1_0$158
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	ld	-4 (ix), l
	ld	a, h
	or	a, #0x78
	ld	-3 (ix), a
	C$boardtest.c$290$1_0$158	= .
	.globl	C$boardtest.c$290$1_0$158
;src/boardtest.c:290: if(board[position] == NOT_PROCESSED_WHITE_CELL) {
	ld	a, -17 (ix)
	dec	a
	jp	NZ,00169$
	C$boardtest.c$291$2_0$159	= .
	.globl	C$boardtest.c$291$2_0$159
;src/boardtest.c:291: if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00126$
	ld	a, -16 (ix)
	or	a, a
	jr	NZ, 00126$
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00126$
	ld	a, -15 (ix)
	or	a, a
	jr	Z, 00127$
00126$:
	C$boardtest.c$292$3_0$160	= .
	.globl	C$boardtest.c$292$3_0$160
;src/boardtest.c:292: board[position] = CONNECTED_WHITE_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x03
	C$boardtest.c$294$3_0$160	= .
	.globl	C$boardtest.c$294$3_0$160
;src/boardtest.c:294: SMS_setNextTileatXY(x+20, y); 
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$295$3_0$160	= .
	.globl	C$boardtest.c$295$3_0$160
;src/boardtest.c:295: printf("4");
	ld	hl, #___str_25
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$296$3_0$160	= .
	.globl	C$boardtest.c$296$3_0$160
;src/boardtest.c:296: return true;
	ld	a, #0x01
	jp	00171$
00127$:
	C$boardtest.c$298$3_0$161	= .
	.globl	C$boardtest.c$298$3_0$161
;src/boardtest.c:298: if(neighbour_connected(position)) {
	ld	a, -20 (ix)
	call	_neighbour_connected
	bit	0,a
	jr	Z, 00124$
	C$boardtest.c$299$4_0$162	= .
	.globl	C$boardtest.c$299$4_0$162
;src/boardtest.c:299: board[position] = CONNECTED_WHITE_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x03
	C$boardtest.c$301$4_0$162	= .
	.globl	C$boardtest.c$301$4_0$162
;src/boardtest.c:301: SMS_setNextTileatXY(x+20, y); 
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$302$4_0$162	= .
	.globl	C$boardtest.c$302$4_0$162
;src/boardtest.c:302: printf("5");
	ld	hl, #___str_26
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$303$4_0$162	= .
	.globl	C$boardtest.c$303$4_0$162
;src/boardtest.c:303: return true;
	ld	a, #0x01
	jp	00171$
00124$:
	C$boardtest.c$305$4_0$163	= .
	.globl	C$boardtest.c$305$4_0$163
;src/boardtest.c:305: SMS_setNextTileatXY(x+20, y); 
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$306$4_0$163	= .
	.globl	C$boardtest.c$306$4_0$163
;src/boardtest.c:306: printf("6");
	ld	hl, #___str_27
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$307$4_0$163	= .
	.globl	C$boardtest.c$307$4_0$163
;src/boardtest.c:307: empty_directions = neighbour_emptyspace(position);
	ld	a, -20 (ix)
	call	_neighbour_emptyspace
	C$boardtest.c$308$4_0$163	= .
	.globl	C$boardtest.c$308$4_0$163
;src/boardtest.c:308: if(empty_directions) {
	ld	e, a
	or	a, a
	jp	Z, 00121$
	C$boardtest.c$309$5_0$164	= .
	.globl	C$boardtest.c$309$5_0$164
;src/boardtest.c:309: SMS_setNextTileatXY(10+x, y); 
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$312$5_0$164	= .
	.globl	C$boardtest.c$312$5_0$164
;src/boardtest.c:312: if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
	bit	0, e
	jr	Z, 00115$
	ld	a, e
	xor	a, #0x01
	jr	NZ, 00115$
	C$boardtest.c$313$6_0$165	= .
	.globl	C$boardtest.c$313$6_0$165
;src/boardtest.c:313: suitable_columns[x] = ((suitable_columns[x] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x]+ 1) &0xF) ;
	ld	bc, #_suitable_columns+0
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x21
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	C$boardtest.c$314$6_0$165	= .
	.globl	C$boardtest.c$314$6_0$165
;src/boardtest.c:314: printf("f"); 
	ld	hl, #___str_28
	push	hl
	call	_printf
	pop	af
	jp	00116$
00115$:
	C$boardtest.c$315$5_0$164	= .
	.globl	C$boardtest.c$315$5_0$164
;src/boardtest.c:315: } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {
	bit	1, e
	jr	Z, 00111$
	ld	a, e
	xor	a, #0x02
	jr	NZ, 00111$
	C$boardtest.c$316$6_0$166	= .
	.globl	C$boardtest.c$316$6_0$166
;src/boardtest.c:316: if(is_free_board_position(position+9)) {
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	ld	a, (hl)
	or	a, a
	jr	NZ, 00102$
	C$boardtest.c$317$7_0$167	= .
	.globl	C$boardtest.c$317$7_0$167
;src/boardtest.c:317: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -14 (ix)
	ld	h, -13 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x81
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	C$boardtest.c$318$7_0$167	= .
	.globl	C$boardtest.c$318$7_0$167
;src/boardtest.c:318: printf("g"); 
	ld	hl, #___str_29
	push	hl
	call	_printf
	pop	af
	jr	00116$
00102$:
	C$boardtest.c$320$7_0$168	= .
	.globl	C$boardtest.c$320$7_0$168
;src/boardtest.c:320: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x-1]+1)& 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -14 (ix)
	ld	h, -13 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x21
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	C$boardtest.c$321$7_0$168	= .
	.globl	C$boardtest.c$321$7_0$168
;src/boardtest.c:321: printf("h"); 
	ld	hl, #___str_30
	push	hl
	call	_printf
	pop	af
	jr	00116$
00111$:
	C$boardtest.c$323$5_0$164	= .
	.globl	C$boardtest.c$323$5_0$164
;src/boardtest.c:323: } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
	bit	2, e
	jr	Z, 00116$
	ld	a, e
	xor	a, #0x04
	jr	NZ, 00116$
	C$boardtest.c$324$6_0$169	= .
	.globl	C$boardtest.c$324$6_0$169
;src/boardtest.c:324: if(is_free_board_position(position+11)) {
	ld	l, -8 (ix)
	ld	h, -7 (ix)
	ld	a, (hl)
	or	a, a
	jr	NZ, 00105$
	C$boardtest.c$325$7_0$170	= .
	.globl	C$boardtest.c$325$7_0$170
;src/boardtest.c:325: suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -12 (ix)
	ld	h, -11 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x81
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	C$boardtest.c$326$7_0$170	= .
	.globl	C$boardtest.c$326$7_0$170
;src/boardtest.c:326: printf("i"); 
	ld	hl, #___str_31
	push	hl
	call	_printf
	pop	af
	jr	00116$
00105$:
	C$boardtest.c$328$7_0$171	= .
	.globl	C$boardtest.c$328$7_0$171
;src/boardtest.c:328: suitable_columns[x+1] = ((suitable_columns[x+1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -12 (ix)
	ld	h, -11 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x21
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	C$boardtest.c$329$7_0$171	= .
	.globl	C$boardtest.c$329$7_0$171
;src/boardtest.c:329: printf("j"); 
	ld	hl, #___str_32
	push	hl
	call	_printf
	pop	af
00116$:
	C$boardtest.c$332$5_0$164	= .
	.globl	C$boardtest.c$332$5_0$164
;src/boardtest.c:332: board[position] = CONNECTED_WHITE_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x03
	C$boardtest.c$333$5_0$164	= .
	.globl	C$boardtest.c$333$5_0$164
;src/boardtest.c:333: return true;
	ld	a, #0x01
	jp	00171$
00121$:
	C$boardtest.c$334$4_0$163	= .
	.globl	C$boardtest.c$334$4_0$163
;src/boardtest.c:334: } else if(!neighbour_notprocessed(position)) {
	ld	a, -20 (ix)
	call	_neighbour_notprocessed
	bit	0,a
	jp	NZ, 00170$
	C$boardtest.c$335$5_0$172	= .
	.globl	C$boardtest.c$335$5_0$172
;src/boardtest.c:335: board[position] = NOT_CONNECTED_WHITE_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x02
	C$boardtest.c$336$5_0$172	= .
	.globl	C$boardtest.c$336$5_0$172
;src/boardtest.c:336: return true;
	ld	a, #0x01
	jp	00171$
00169$:
	C$boardtest.c$340$1_0$158	= .
	.globl	C$boardtest.c$340$1_0$158
;src/boardtest.c:340: } else if(board[position] == NOT_PROCESSED_BLACK_CELL) {
	ld	a, -17 (ix)
	sub	a, #0x04
	jp	NZ,00166$
	C$boardtest.c$341$2_0$173	= .
	.globl	C$boardtest.c$341$2_0$173
;src/boardtest.c:341: if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00157$
	ld	a, -16 (ix)
	or	a, a
	jr	NZ, 00157$
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00157$
	ld	a, -15 (ix)
	or	a, a
	jr	Z, 00158$
00157$:
	C$boardtest.c$342$3_0$174	= .
	.globl	C$boardtest.c$342$3_0$174
;src/boardtest.c:342: board[position] = CONNECTED_BLACK_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x06
	C$boardtest.c$344$3_0$174	= .
	.globl	C$boardtest.c$344$3_0$174
;src/boardtest.c:344: SMS_setNextTileatXY(x+20, y); 
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$345$3_0$174	= .
	.globl	C$boardtest.c$345$3_0$174
;src/boardtest.c:345: printf("1");
	ld	hl, #___str_33
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$346$3_0$174	= .
	.globl	C$boardtest.c$346$3_0$174
;src/boardtest.c:346: return true;
	ld	a, #0x01
	jp	00171$
00158$:
	C$boardtest.c$348$3_0$175	= .
	.globl	C$boardtest.c$348$3_0$175
;src/boardtest.c:348: if(neighbour_connected(position)) {
	ld	a, -20 (ix)
	call	_neighbour_connected
	bit	0,a
	jr	Z, 00155$
	C$boardtest.c$349$4_0$176	= .
	.globl	C$boardtest.c$349$4_0$176
;src/boardtest.c:349: board[position] = CONNECTED_BLACK_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x06
	C$boardtest.c$351$4_0$176	= .
	.globl	C$boardtest.c$351$4_0$176
;src/boardtest.c:351: SMS_setNextTileatXY(x+20, y); 
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$352$4_0$176	= .
	.globl	C$boardtest.c$352$4_0$176
;src/boardtest.c:352: printf("2");
	ld	hl, #___str_34
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$353$4_0$176	= .
	.globl	C$boardtest.c$353$4_0$176
;src/boardtest.c:353: return true;
	ld	a, #0x01
	jp	00171$
00155$:
	C$boardtest.c$355$4_0$177	= .
	.globl	C$boardtest.c$355$4_0$177
;src/boardtest.c:355: SMS_setNextTileatXY(x+20, y); 
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$356$4_0$177	= .
	.globl	C$boardtest.c$356$4_0$177
;src/boardtest.c:356: printf("3");
	ld	hl, #___str_35
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$357$4_0$177	= .
	.globl	C$boardtest.c$357$4_0$177
;src/boardtest.c:357: empty_directions = neighbour_emptyspace(position);
	ld	a, -20 (ix)
	call	_neighbour_emptyspace
	C$boardtest.c$358$4_0$177	= .
	.globl	C$boardtest.c$358$4_0$177
;src/boardtest.c:358: if(empty_directions) {
	ld	-5 (ix), a
	or	a, a
	jp	Z, 00152$
	C$boardtest.c$360$5_0$178	= .
	.globl	C$boardtest.c$360$5_0$178
;src/boardtest.c:360: SMS_setNextTileatXY(10+x, y); 
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$361$5_0$178	= .
	.globl	C$boardtest.c$361$5_0$178
;src/boardtest.c:361: if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
	bit	0, -5 (ix)
	jr	Z, 00146$
	ld	a, -5 (ix)
	xor	a, #0x01
	jr	NZ, 00146$
	C$boardtest.c$362$6_0$179	= .
	.globl	C$boardtest.c$362$6_0$179
;src/boardtest.c:362: suitable_columns[x] = ((suitable_columns[x] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x] + 1) & 0xF); 
	ld	a, #<(_suitable_columns)
	add	a, -1 (ix)
	ld	-7 (ix), a
	ld	a, #>(_suitable_columns)
	adc	a, #0x00
	ld	-6 (ix), a
	ld	l, -7 (ix)
	ld	h, -6 (ix)
	ld	a, (hl)
	ld	-3 (ix), a
	or	a, #0x11
	ld	-4 (ix), a
	and	a, #0xf0
	ld	-5 (ix), a
	inc	-3 (ix)
	ld	a, -3 (ix)
	and	a, #0x0f
	ld	-3 (ix), a
	add	a, -5 (ix)
	ld	-3 (ix), a
	ld	l, -7 (ix)
	ld	h, -6 (ix)
	ld	a, -3 (ix)
	ld	(hl), a
	C$boardtest.c$363$6_0$179	= .
	.globl	C$boardtest.c$363$6_0$179
;src/boardtest.c:363: printf("a"); 
	ld	hl, #___str_36
	push	hl
	call	_printf
	pop	af
	jp	00147$
00146$:
	C$boardtest.c$364$5_0$178	= .
	.globl	C$boardtest.c$364$5_0$178
;src/boardtest.c:364: } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {                        
	bit	1, -5 (ix)
	jp	Z,00142$
	ld	a, -5 (ix)
	xor	a, #0x02
	jp	NZ,00142$
	C$boardtest.c$365$6_0$180	= .
	.globl	C$boardtest.c$365$6_0$180
;src/boardtest.c:365: if(is_free_board_position(position+9)) {
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	ld	a, (hl)
	or	a, a
	jr	NZ, 00133$
	C$boardtest.c$366$7_0$181	= .
	.globl	C$boardtest.c$366$7_0$181
;src/boardtest.c:366: suitable_columns[x-1] =((suitable_columns[x-1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	ld	a, #<(_suitable_columns)
	add	a, -14 (ix)
	ld	-4 (ix), a
	ld	a, #>(_suitable_columns)
	adc	a, -13 (ix)
	ld	-3 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, (hl)
	ld	-5 (ix), a
	or	a, #0x41
	ld	-6 (ix), a
	and	a, #0xf0
	ld	-7 (ix), a
	inc	-5 (ix)
	ld	a, -5 (ix)
	and	a, #0x0f
	ld	-5 (ix), a
	add	a, -7 (ix)
	ld	-5 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, -5 (ix)
	ld	(hl), a
	C$boardtest.c$367$7_0$181	= .
	.globl	C$boardtest.c$367$7_0$181
;src/boardtest.c:367: printf("b"); 
	ld	hl, #___str_37
	push	hl
	call	_printf
	pop	af
	jp	00147$
00133$:
	C$boardtest.c$369$7_0$182	= .
	.globl	C$boardtest.c$369$7_0$182
;src/boardtest.c:369: suitable_columns[x-1] = ((suitable_columns[x-1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	ld	a, #<(_suitable_columns)
	add	a, -14 (ix)
	ld	-4 (ix), a
	ld	a, #>(_suitable_columns)
	adc	a, -13 (ix)
	ld	-3 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, (hl)
	ld	-5 (ix), a
	or	a, #0x11
	ld	-6 (ix), a
	and	a, #0xf0
	ld	-7 (ix), a
	inc	-5 (ix)
	ld	a, -5 (ix)
	and	a, #0x0f
	ld	-5 (ix), a
	add	a, -7 (ix)
	ld	-5 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, -5 (ix)
	ld	(hl), a
	C$boardtest.c$370$7_0$182	= .
	.globl	C$boardtest.c$370$7_0$182
;src/boardtest.c:370: printf("c"); 
	ld	hl, #___str_38
	push	hl
	call	_printf
	pop	af
	jp	00147$
00142$:
	C$boardtest.c$372$5_0$178	= .
	.globl	C$boardtest.c$372$5_0$178
;src/boardtest.c:372: } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
	bit	2, -5 (ix)
	jp	Z,00147$
	ld	a, -5 (ix)
	xor	a, #0x04
	jp	NZ,00147$
	C$boardtest.c$373$6_0$183	= .
	.globl	C$boardtest.c$373$6_0$183
;src/boardtest.c:373: if(is_free_board_position(position+11)) {
	ld	l, -8 (ix)
	ld	h, -7 (ix)
	ld	a, (hl)
	or	a, a
	jr	NZ, 00136$
	C$boardtest.c$374$7_0$184	= .
	.globl	C$boardtest.c$374$7_0$184
;src/boardtest.c:374: suitable_columns[x+1] =((suitable_columns[x+1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] + 1) & 0xF);
	ld	a, -12 (ix)
	add	a, #<(_suitable_columns)
	ld	-4 (ix), a
	ld	a, -11 (ix)
	adc	a, #>(_suitable_columns)
	ld	-3 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, (hl)
	ld	-5 (ix), a
	or	a, #0x41
	ld	-6 (ix), a
	and	a, #0xf0
	ld	-6 (ix), a
	inc	-5 (ix)
	ld	a, -5 (ix)
	and	a, #0x0f
	ld	-5 (ix), a
	add	a, -6 (ix)
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	(hl), a
	C$boardtest.c$375$7_0$184	= .
	.globl	C$boardtest.c$375$7_0$184
;src/boardtest.c:375: printf("d"); 
	ld	hl, #___str_39
	push	hl
	call	_printf
	pop	af
	jr	00147$
00136$:
	C$boardtest.c$377$7_0$185	= .
	.globl	C$boardtest.c$377$7_0$185
;src/boardtest.c:377: suitable_columns[x+1] = ((suitable_columns[x+1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x+1] + 1) & 0xF);
	ld	a, -12 (ix)
	add	a, #<(_suitable_columns)
	ld	-4 (ix), a
	ld	a, -11 (ix)
	adc	a, #>(_suitable_columns)
	ld	-3 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, (hl)
	ld	-5 (ix), a
	or	a, #0x11
	ld	-6 (ix), a
	and	a, #0xf0
	ld	-6 (ix), a
	inc	-5 (ix)
	ld	a, -5 (ix)
	and	a, #0x0f
	ld	-5 (ix), a
	add	a, -6 (ix)
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	(hl), a
	C$boardtest.c$378$7_0$185	= .
	.globl	C$boardtest.c$378$7_0$185
;src/boardtest.c:378: printf("e"); 
	ld	hl, #___str_40
	push	hl
	call	_printf
	pop	af
00147$:
	C$boardtest.c$381$5_0$178	= .
	.globl	C$boardtest.c$381$5_0$178
;src/boardtest.c:381: board[position] = CONNECTED_BLACK_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x06
	C$boardtest.c$382$5_0$178	= .
	.globl	C$boardtest.c$382$5_0$178
;src/boardtest.c:382: return true;
	ld	a, #0x01
	jr	00171$
00152$:
	C$boardtest.c$383$4_0$177	= .
	.globl	C$boardtest.c$383$4_0$177
;src/boardtest.c:383: } else if(!neighbour_notprocessed(position)) {
	ld	a, -20 (ix)
	call	_neighbour_notprocessed
	bit	0,a
	jr	NZ, 00170$
	C$boardtest.c$384$5_0$186	= .
	.globl	C$boardtest.c$384$5_0$186
;src/boardtest.c:384: board[position] = NOT_CONNECTED_BLACK_CELL;
	ld	l, -19 (ix)
	ld	h, -18 (ix)
	ld	(hl), #0x05
	C$boardtest.c$385$5_0$186	= .
	.globl	C$boardtest.c$385$5_0$186
;src/boardtest.c:385: return true;
	ld	a, #0x01
	jr	00171$
00166$:
	C$boardtest.c$389$1_0$158	= .
	.globl	C$boardtest.c$389$1_0$158
;src/boardtest.c:389: } else if(board[position] == EMPTY_CELL) {
	ld	a, -17 (ix)
	or	a, a
	jr	NZ, 00170$
	C$boardtest.c$390$2_0$187	= .
	.globl	C$boardtest.c$390$2_0$187
;src/boardtest.c:390: SMS_setNextTileatXY(x+20, y); 
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$boardtest.c$391$2_0$187	= .
	.globl	C$boardtest.c$391$2_0$187
;src/boardtest.c:391: printf("0");
	ld	hl, #___str_41
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$392$2_0$187	= .
	.globl	C$boardtest.c$392$2_0$187
;src/boardtest.c:392: suitable_columns[x] = 0;
	ld	bc, #_suitable_columns+0
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	(hl), #0x00
00170$:
	C$boardtest.c$394$1_0$158	= .
	.globl	C$boardtest.c$394$1_0$158
;src/boardtest.c:394: return false;
	xor	a, a
00171$:
	C$boardtest.c$395$1_0$158	= .
	.globl	C$boardtest.c$395$1_0$158
;src/boardtest.c:395: }
	ld	sp, ix
	pop	ix
	C$boardtest.c$395$1_0$158	= .
	.globl	C$boardtest.c$395$1_0$158
	XG$compute_path2$0$0	= .
	.globl	XG$compute_path2$0$0
	ret
Fboardtest$__str_25$0_0$0 == .
___str_25:
	.ascii "4"
	.db 0x00
Fboardtest$__str_26$0_0$0 == .
___str_26:
	.ascii "5"
	.db 0x00
Fboardtest$__str_27$0_0$0 == .
___str_27:
	.ascii "6"
	.db 0x00
Fboardtest$__str_28$0_0$0 == .
___str_28:
	.ascii "f"
	.db 0x00
Fboardtest$__str_29$0_0$0 == .
___str_29:
	.ascii "g"
	.db 0x00
Fboardtest$__str_30$0_0$0 == .
___str_30:
	.ascii "h"
	.db 0x00
Fboardtest$__str_31$0_0$0 == .
___str_31:
	.ascii "i"
	.db 0x00
Fboardtest$__str_32$0_0$0 == .
___str_32:
	.ascii "j"
	.db 0x00
Fboardtest$__str_33$0_0$0 == .
___str_33:
	.ascii "1"
	.db 0x00
Fboardtest$__str_34$0_0$0 == .
___str_34:
	.ascii "2"
	.db 0x00
Fboardtest$__str_35$0_0$0 == .
___str_35:
	.ascii "3"
	.db 0x00
Fboardtest$__str_36$0_0$0 == .
___str_36:
	.ascii "a"
	.db 0x00
Fboardtest$__str_37$0_0$0 == .
___str_37:
	.ascii "b"
	.db 0x00
Fboardtest$__str_38$0_0$0 == .
___str_38:
	.ascii "c"
	.db 0x00
Fboardtest$__str_39$0_0$0 == .
___str_39:
	.ascii "d"
	.db 0x00
Fboardtest$__str_40$0_0$0 == .
___str_40:
	.ascii "e"
	.db 0x00
Fboardtest$__str_41$0_0$0 == .
___str_41:
	.ascii "0"
	.db 0x00
	G$compute_paths$0$0	= .
	.globl	G$compute_paths$0$0
	C$boardtest.c$397$1_0$189	= .
	.globl	C$boardtest.c$397$1_0$189
;src/boardtest.c:397: void compute_paths(void) {
;	---------------------------------
; Function compute_paths
; ---------------------------------
_compute_paths::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$boardtest.c$398$2_0$189	= .
	.globl	C$boardtest.c$398$2_0$189
;src/boardtest.c:398: bool someChangeHappened = true;
	ld	c, #0x01
	C$boardtest.c$399$5_2$195	= .
	.globl	C$boardtest.c$399$5_2$195
;src/boardtest.c:399: while(someChangeHappened) {
00111$:
	bit	0, c
	jr	Z, 00116$
	C$boardtest.c$400$2_0$190	= .
	.globl	C$boardtest.c$400$2_0$190
;src/boardtest.c:400: someChangeHappened = false;
	C$boardtest.c$402$3_0$191	= .
	.globl	C$boardtest.c$402$3_0$191
;src/boardtest.c:402: bool somePiece = true;
	ld	bc, #0x100
	C$boardtest.c$403$1_0$189	= .
	.globl	C$boardtest.c$403$1_0$189
;src/boardtest.c:403: while(row_index > 0 && somePiece) {
	ld	e, #0x15
00105$:
	ld	a, e
	or	a, a
	jr	Z, 00108$
	bit	0, b
	jr	Z, 00108$
	C$boardtest.c$404$3_1$192	= .
	.globl	C$boardtest.c$404$3_1$192
;src/boardtest.c:404: somePiece = false;
	ld	b, #0x00
	C$boardtest.c$405$3_2$193	= .
	.globl	C$boardtest.c$405$3_2$193
;src/boardtest.c:405: u8 position = (row_index << 3) + (row_index << 1);
	ld	d, e
	ld	a, d
	add	a, a
	add	a, a
	add	a, a
	sla	d
	add	a, d
	ld	-1 (ix), a
	C$boardtest.c$407$1_0$189	= .
	.globl	C$boardtest.c$407$1_0$189
;src/boardtest.c:407: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	d, #0x00
00114$:
	C$boardtest.c$408$5_2$195	= .
	.globl	C$boardtest.c$408$5_2$195
;src/boardtest.c:408: if(!is_free_board_position(position)) {
	ld	a, #<(_board)
	add	a, -1 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, #0x00
	ld	h, a
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
	C$boardtest.c$409$6_2$196	= .
	.globl	C$boardtest.c$409$6_2$196
;src/boardtest.c:409: someChangeHappened = someChangeHappened || compute_path2(i,row_index);
	bit	0, c
	jr	NZ, 00119$
	push	de
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	a, d
	call	_compute_path2
	ld	c, a
	pop	de
	bit	0, c
	ld	c, #0x00
	jr	Z, 00120$
00119$:
	ld	c, #0x01
00120$:
	C$boardtest.c$410$6_2$196	= .
	.globl	C$boardtest.c$410$6_2$196
;src/boardtest.c:410: somePiece = true;
	ld	b, #0x01
00102$:
	C$boardtest.c$412$5_2$195	= .
	.globl	C$boardtest.c$412$5_2$195
;src/boardtest.c:412: position++;
	inc	-1 (ix)
	C$boardtest.c$407$4_2$194	= .
	.globl	C$boardtest.c$407$4_2$194
;src/boardtest.c:407: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	d
	ld	a, d
	sub	a, #0x0a
	jr	C, 00114$
	C$boardtest.c$414$3_2$193	= .
	.globl	C$boardtest.c$414$3_2$193
;src/boardtest.c:414: row_index--;
	dec	e
	jr	00105$
	C$boardtest.c$416$2_1$191	= .
	.globl	C$boardtest.c$416$2_1$191
;src/boardtest.c:416: while(!(SMS_getKeysPressed() & PORT_A_KEY_1));
00108$:
	push	bc
	call	_SMS_getKeysPressed
	pop	bc
	bit	4, e
	jr	Z, 00108$
	jr	00111$
00116$:
	C$boardtest.c$418$1_0$189	= .
	.globl	C$boardtest.c$418$1_0$189
;src/boardtest.c:418: }
	inc	sp
	pop	ix
	C$boardtest.c$418$1_0$189	= .
	.globl	C$boardtest.c$418$1_0$189
	XG$compute_paths$0$0	= .
	.globl	XG$compute_paths$0$0
	ret
	G$check_disconnected$0$0	= .
	.globl	G$check_disconnected$0$0
	C$boardtest.c$420$1_0$198	= .
	.globl	C$boardtest.c$420$1_0$198
;src/boardtest.c:420: void check_disconnected(void) {
;	---------------------------------
; Function check_disconnected
; ---------------------------------
_check_disconnected::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-5
	add	hl, sp
	ld	sp, hl
	C$boardtest.c$422$2_0$198	= .
	.globl	C$boardtest.c$422$2_0$198
;src/boardtest.c:422: bool somePiece = true;
	ld	-5 (ix), #0x01
	C$boardtest.c$423$2_0$198	= .
	.globl	C$boardtest.c$423$2_0$198
;src/boardtest.c:423: bool somePieceDestroyed = false;
	ld	-4 (ix), #0x00
	C$boardtest.c$424$4_1$202	= .
	.globl	C$boardtest.c$424$4_1$202
;src/boardtest.c:424: while(row_index > 0 && somePiece) {
	ld	-3 (ix), #0x15
00105$:
	ld	a, -3 (ix)
	or	a, a
	jr	Z, 00110$
	bit	0, -5 (ix)
	jr	Z, 00110$
	C$boardtest.c$425$2_0$199	= .
	.globl	C$boardtest.c$425$2_0$199
;src/boardtest.c:425: somePiece = false;
	ld	-5 (ix), #0x00
	C$boardtest.c$426$2_1$200	= .
	.globl	C$boardtest.c$426$2_1$200
;src/boardtest.c:426: u8 position = (row_index << 3) + (row_index << 1);
	ld	a, -3 (ix)
	ld	-1 (ix), a
	add	a, a
	add	a, a
	add	a, a
	ld	-2 (ix), a
	sla	-1 (ix)
	ld	a, -1 (ix)
	add	a, -2 (ix)
	ld	-2 (ix), a
	C$boardtest.c$428$1_0$198	= .
	.globl	C$boardtest.c$428$1_0$198
;src/boardtest.c:428: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	-1 (ix), #0x00
00108$:
	C$boardtest.c$429$4_1$202	= .
	.globl	C$boardtest.c$429$4_1$202
;src/boardtest.c:429: if(!is_free_board_position(position)) {
	ld	a, #<(_board)
	add	a, -2 (ix)
	ld	c, a
	ld	a, #>(_board)
	adc	a, #0x00
	ld	b, a
	ld	a, (bc)
	or	a, a
	jr	Z, 00102$
	C$boardtest.c$430$5_1$203	= .
	.globl	C$boardtest.c$430$5_1$203
;src/boardtest.c:430: somePieceDestroyed = mark_as_disconnected(i, row_index, position) || somePieceDestroyed;
	ld	a, -2 (ix)
	push	af
	inc	sp
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_mark_as_disconnected
	ld	-5 (ix), a
	bit	0, -5 (ix)
	jr	NZ, 00113$
	bit	0, -4 (ix)
	jr	NZ, 00113$
	ld	-4 (ix), #0x00
	jr	00114$
00113$:
	ld	-4 (ix), #0x01
00114$:
	C$boardtest.c$431$5_1$203	= .
	.globl	C$boardtest.c$431$5_1$203
;src/boardtest.c:431: somePiece = true;
	ld	-5 (ix), #0x01
00102$:
	C$boardtest.c$433$4_1$202	= .
	.globl	C$boardtest.c$433$4_1$202
;src/boardtest.c:433: position++;
	inc	-2 (ix)
	C$boardtest.c$428$3_1$201	= .
	.globl	C$boardtest.c$428$3_1$201
;src/boardtest.c:428: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	-1 (ix)
	ld	a, -1 (ix)
	sub	a, #0x0a
	jr	C, 00108$
	C$boardtest.c$435$2_1$200	= .
	.globl	C$boardtest.c$435$2_1$200
;src/boardtest.c:435: row_index--;
	dec	-3 (ix)
	jr	00105$
00110$:
	C$boardtest.c$437$1_0$198	= .
	.globl	C$boardtest.c$437$1_0$198
;src/boardtest.c:437: }
	ld	sp, ix
	pop	ix
	C$boardtest.c$437$1_0$198	= .
	.globl	C$boardtest.c$437$1_0$198
	XG$check_disconnected$0$0	= .
	.globl	XG$check_disconnected$0$0
	ret
	G$test1$0$0	= .
	.globl	G$test1$0$0
	C$boardtest.c$439$1_0$205	= .
	.globl	C$boardtest.c$439$1_0$205
;src/boardtest.c:439: void test1(void) {
;	---------------------------------
; Function test1
; ---------------------------------
_test1::
	C$boardtest.c$440$1_0$205	= .
	.globl	C$boardtest.c$440$1_0$205
;src/boardtest.c:440: clear_tilemap();
	call	_clear_tilemap
	C$boardtest.c$441$1_0$205	= .
	.globl	C$boardtest.c$441$1_0$205
;src/boardtest.c:441: initializeBoardTest1();
	call	_initializeBoardTest1
	C$boardtest.c$442$1_0$205	= .
	.globl	C$boardtest.c$442$1_0$205
;src/boardtest.c:442: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$boardtest.c$444$1_0$205	= .
	.globl	C$boardtest.c$444$1_0$205
;src/boardtest.c:444: current_square.color00 = WHITE_PIECE;
	ld	hl, #(_current_square + 2)
	ld	(hl), #0x01
	C$boardtest.c$445$1_0$205	= .
	.globl	C$boardtest.c$445$1_0$205
;src/boardtest.c:445: current_square.color01 = WHITE_PIECE;
	ld	hl, #(_current_square + 3)
	ld	(hl), #0x01
	C$boardtest.c$446$1_0$205	= .
	.globl	C$boardtest.c$446$1_0$205
;src/boardtest.c:446: current_square.color10 = WHITE_PIECE;
	ld	hl, #(_current_square + 4)
	ld	(hl), #0x01
	C$boardtest.c$447$1_0$205	= .
	.globl	C$boardtest.c$447$1_0$205
;src/boardtest.c:447: current_square.color11 = WHITE_PIECE;
	ld	hl, #(_current_square + 5)
	ld	(hl), #0x01
	C$boardtest.c$448$1_0$205	= .
	.globl	C$boardtest.c$448$1_0$205
;src/boardtest.c:448: compute_paths();
	call	_compute_paths
	C$boardtest.c$449$1_0$205	= .
	.globl	C$boardtest.c$449$1_0$205
;src/boardtest.c:449: check_disconnected();
	call	_check_disconnected
	C$boardtest.c$450$1_0$205	= .
	.globl	C$boardtest.c$450$1_0$205
;src/boardtest.c:450: printBoard();
	call	_printBoard
	C$boardtest.c$451$1_0$205	= .
	.globl	C$boardtest.c$451$1_0$205
;src/boardtest.c:451: printSuitables();
	call	_printSuitables
	C$boardtest.c$452$1_0$205	= .
	.globl	C$boardtest.c$452$1_0$205
;src/boardtest.c:452: decideNextMove();
	call	_decideNextMove
	C$boardtest.c$453$1_0$205	= .
	.globl	C$boardtest.c$453$1_0$205
;src/boardtest.c:453: SMS_setNextTileatXY(21, 20);
	ld	hl, #0x7d2a
	rst	#0x08
	C$boardtest.c$454$1_0$205	= .
	.globl	C$boardtest.c$454$1_0$205
;src/boardtest.c:454: printf("Column: ");
	ld	hl, #___str_42
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$455$1_0$205	= .
	.globl	C$boardtest.c$455$1_0$205
;src/boardtest.c:455: printf("%d",target_column);
	ld	a, (_target_column+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_43
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$456$1_0$205	= .
	.globl	C$boardtest.c$456$1_0$205
;src/boardtest.c:456: SMS_setNextTileatXY(21, 21);
	ld	hl, #0x7d6a
	rst	#0x08
	C$boardtest.c$457$1_0$205	= .
	.globl	C$boardtest.c$457$1_0$205
;src/boardtest.c:457: printf("Rotation: ");
	ld	hl, #___str_44
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$458$1_0$205	= .
	.globl	C$boardtest.c$458$1_0$205
;src/boardtest.c:458: printf("%d",target_rotation);
	ld	a, (_target_rotation+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_43
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$459$1_0$205	= .
	.globl	C$boardtest.c$459$1_0$205
;src/boardtest.c:459: while(frame_cnt < 200) {
00101$:
	ld	de, #0x00c8
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	ret	NC
	C$boardtest.c$460$2_0$206	= .
	.globl	C$boardtest.c$460$2_0$206
;src/boardtest.c:460: waitForFrame();
	call	_waitForFrame
	C$boardtest.c$462$1_0$205	= .
	.globl	C$boardtest.c$462$1_0$205
;src/boardtest.c:462: }
	C$boardtest.c$462$1_0$205	= .
	.globl	C$boardtest.c$462$1_0$205
	XG$test1$0$0	= .
	.globl	XG$test1$0$0
	jr	00101$
Fboardtest$__str_42$0_0$0 == .
___str_42:
	.ascii "Column: "
	.db 0x00
Fboardtest$__str_43$0_0$0 == .
___str_43:
	.ascii "%d"
	.db 0x00
Fboardtest$__str_44$0_0$0 == .
___str_44:
	.ascii "Rotation: "
	.db 0x00
	G$test2$0$0	= .
	.globl	G$test2$0$0
	C$boardtest.c$464$1_0$208	= .
	.globl	C$boardtest.c$464$1_0$208
;src/boardtest.c:464: void test2(void) {
;	---------------------------------
; Function test2
; ---------------------------------
_test2::
	C$boardtest.c$465$1_0$208	= .
	.globl	C$boardtest.c$465$1_0$208
;src/boardtest.c:465: clear_tilemap();
	call	_clear_tilemap
	C$boardtest.c$466$1_0$208	= .
	.globl	C$boardtest.c$466$1_0$208
;src/boardtest.c:466: initializeBoardTest2();
	call	_initializeBoardTest2
	C$boardtest.c$467$1_0$208	= .
	.globl	C$boardtest.c$467$1_0$208
;src/boardtest.c:467: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$boardtest.c$469$1_0$208	= .
	.globl	C$boardtest.c$469$1_0$208
;src/boardtest.c:469: current_square.color00 = WHITE_PIECE;
	ld	hl, #(_current_square + 2)
	ld	(hl), #0x01
	C$boardtest.c$470$1_0$208	= .
	.globl	C$boardtest.c$470$1_0$208
;src/boardtest.c:470: current_square.color01 = WHITE_PIECE;
	ld	hl, #(_current_square + 3)
	ld	(hl), #0x01
	C$boardtest.c$471$1_0$208	= .
	.globl	C$boardtest.c$471$1_0$208
;src/boardtest.c:471: current_square.color10 = WHITE_PIECE;
	ld	hl, #(_current_square + 4)
	ld	(hl), #0x01
	C$boardtest.c$472$1_0$208	= .
	.globl	C$boardtest.c$472$1_0$208
;src/boardtest.c:472: current_square.color11 = WHITE_PIECE;
	ld	hl, #(_current_square + 5)
	ld	(hl), #0x01
	C$boardtest.c$473$1_0$208	= .
	.globl	C$boardtest.c$473$1_0$208
;src/boardtest.c:473: compute_paths();
	call	_compute_paths
	C$boardtest.c$474$1_0$208	= .
	.globl	C$boardtest.c$474$1_0$208
;src/boardtest.c:474: check_disconnected();
	call	_check_disconnected
	C$boardtest.c$475$1_0$208	= .
	.globl	C$boardtest.c$475$1_0$208
;src/boardtest.c:475: printBoard();
	call	_printBoard
	C$boardtest.c$476$1_0$208	= .
	.globl	C$boardtest.c$476$1_0$208
;src/boardtest.c:476: printSuitables();
	call	_printSuitables
	C$boardtest.c$477$1_0$208	= .
	.globl	C$boardtest.c$477$1_0$208
;src/boardtest.c:477: decideNextMove();
	call	_decideNextMove
	C$boardtest.c$478$1_0$208	= .
	.globl	C$boardtest.c$478$1_0$208
;src/boardtest.c:478: SMS_setNextTileatXY(21, 20);
	ld	hl, #0x7d2a
	rst	#0x08
	C$boardtest.c$479$1_0$208	= .
	.globl	C$boardtest.c$479$1_0$208
;src/boardtest.c:479: printf("Column: ");
	ld	hl, #___str_45
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$480$1_0$208	= .
	.globl	C$boardtest.c$480$1_0$208
;src/boardtest.c:480: printf("%d",target_column);
	ld	a, (_target_column+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_46
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$481$1_0$208	= .
	.globl	C$boardtest.c$481$1_0$208
;src/boardtest.c:481: SMS_setNextTileatXY(21, 21);
	ld	hl, #0x7d6a
	rst	#0x08
	C$boardtest.c$482$1_0$208	= .
	.globl	C$boardtest.c$482$1_0$208
;src/boardtest.c:482: printf("Rotation: ");
	ld	hl, #___str_47
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$483$1_0$208	= .
	.globl	C$boardtest.c$483$1_0$208
;src/boardtest.c:483: printf("%d",target_rotation);
	ld	a, (_target_rotation+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_46
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$484$1_0$208	= .
	.globl	C$boardtest.c$484$1_0$208
;src/boardtest.c:484: while(frame_cnt < 200) {
00101$:
	ld	de, #0x00c8
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	ret	NC
	C$boardtest.c$485$2_0$209	= .
	.globl	C$boardtest.c$485$2_0$209
;src/boardtest.c:485: waitForFrame();
	call	_waitForFrame
	C$boardtest.c$487$1_0$208	= .
	.globl	C$boardtest.c$487$1_0$208
;src/boardtest.c:487: }
	C$boardtest.c$487$1_0$208	= .
	.globl	C$boardtest.c$487$1_0$208
	XG$test2$0$0	= .
	.globl	XG$test2$0$0
	jr	00101$
Fboardtest$__str_45$0_0$0 == .
___str_45:
	.ascii "Column: "
	.db 0x00
Fboardtest$__str_46$0_0$0 == .
___str_46:
	.ascii "%d"
	.db 0x00
Fboardtest$__str_47$0_0$0 == .
___str_47:
	.ascii "Rotation: "
	.db 0x00
	G$test3$0$0	= .
	.globl	G$test3$0$0
	C$boardtest.c$489$1_0$211	= .
	.globl	C$boardtest.c$489$1_0$211
;src/boardtest.c:489: void test3(void) {
;	---------------------------------
; Function test3
; ---------------------------------
_test3::
	C$boardtest.c$490$1_0$211	= .
	.globl	C$boardtest.c$490$1_0$211
;src/boardtest.c:490: clear_tilemap();
	call	_clear_tilemap
	C$boardtest.c$491$1_0$211	= .
	.globl	C$boardtest.c$491$1_0$211
;src/boardtest.c:491: initializeBoardTest2();
	call	_initializeBoardTest2
	C$boardtest.c$492$1_0$211	= .
	.globl	C$boardtest.c$492$1_0$211
;src/boardtest.c:492: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$boardtest.c$494$1_0$211	= .
	.globl	C$boardtest.c$494$1_0$211
;src/boardtest.c:494: current_square.color00 = WHITE_PIECE;
	ld	hl, #(_current_square + 2)
	ld	(hl), #0x01
	C$boardtest.c$495$1_0$211	= .
	.globl	C$boardtest.c$495$1_0$211
;src/boardtest.c:495: current_square.color01 = BLACK_PIECE;
	ld	hl, #(_current_square + 3)
	ld	(hl), #0x04
	C$boardtest.c$496$1_0$211	= .
	.globl	C$boardtest.c$496$1_0$211
;src/boardtest.c:496: current_square.color10 = BLACK_PIECE;
	ld	hl, #(_current_square + 4)
	ld	(hl), #0x04
	C$boardtest.c$497$1_0$211	= .
	.globl	C$boardtest.c$497$1_0$211
;src/boardtest.c:497: current_square.color11 = BLACK_PIECE;
	ld	hl, #(_current_square + 5)
	ld	(hl), #0x04
	C$boardtest.c$498$1_0$211	= .
	.globl	C$boardtest.c$498$1_0$211
;src/boardtest.c:498: compute_paths();
	call	_compute_paths
	C$boardtest.c$499$1_0$211	= .
	.globl	C$boardtest.c$499$1_0$211
;src/boardtest.c:499: check_disconnected();
	call	_check_disconnected
	C$boardtest.c$500$1_0$211	= .
	.globl	C$boardtest.c$500$1_0$211
;src/boardtest.c:500: printBoard();
	call	_printBoard
	C$boardtest.c$501$1_0$211	= .
	.globl	C$boardtest.c$501$1_0$211
;src/boardtest.c:501: printSuitables();
	call	_printSuitables
	C$boardtest.c$502$1_0$211	= .
	.globl	C$boardtest.c$502$1_0$211
;src/boardtest.c:502: decideNextMove();
	call	_decideNextMove
	C$boardtest.c$503$1_0$211	= .
	.globl	C$boardtest.c$503$1_0$211
;src/boardtest.c:503: SMS_setNextTileatXY(21, 20);
	ld	hl, #0x7d2a
	rst	#0x08
	C$boardtest.c$504$1_0$211	= .
	.globl	C$boardtest.c$504$1_0$211
;src/boardtest.c:504: printf("Column: ");
	ld	hl, #___str_48
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$505$1_0$211	= .
	.globl	C$boardtest.c$505$1_0$211
;src/boardtest.c:505: printf("%d",target_column);
	ld	a, (_target_column+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_49
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$506$1_0$211	= .
	.globl	C$boardtest.c$506$1_0$211
;src/boardtest.c:506: SMS_setNextTileatXY(21, 21);
	ld	hl, #0x7d6a
	rst	#0x08
	C$boardtest.c$507$1_0$211	= .
	.globl	C$boardtest.c$507$1_0$211
;src/boardtest.c:507: printf("Rotation: ");
	ld	hl, #___str_50
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$508$1_0$211	= .
	.globl	C$boardtest.c$508$1_0$211
;src/boardtest.c:508: printf("%d",target_rotation);
	ld	a, (_target_rotation+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_49
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$509$1_0$211	= .
	.globl	C$boardtest.c$509$1_0$211
;src/boardtest.c:509: while(frame_cnt < 200) {
00101$:
	ld	de, #0x00c8
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	ret	NC
	C$boardtest.c$510$2_0$212	= .
	.globl	C$boardtest.c$510$2_0$212
;src/boardtest.c:510: waitForFrame();
	call	_waitForFrame
	C$boardtest.c$512$1_0$211	= .
	.globl	C$boardtest.c$512$1_0$211
;src/boardtest.c:512: }
	C$boardtest.c$512$1_0$211	= .
	.globl	C$boardtest.c$512$1_0$211
	XG$test3$0$0	= .
	.globl	XG$test3$0$0
	jr	00101$
Fboardtest$__str_48$0_0$0 == .
___str_48:
	.ascii "Column: "
	.db 0x00
Fboardtest$__str_49$0_0$0 == .
___str_49:
	.ascii "%d"
	.db 0x00
Fboardtest$__str_50$0_0$0 == .
___str_50:
	.ascii "Rotation: "
	.db 0x00
	G$test4$0$0	= .
	.globl	G$test4$0$0
	C$boardtest.c$514$1_0$214	= .
	.globl	C$boardtest.c$514$1_0$214
;src/boardtest.c:514: void test4(void) {
;	---------------------------------
; Function test4
; ---------------------------------
_test4::
	C$boardtest.c$515$1_0$214	= .
	.globl	C$boardtest.c$515$1_0$214
;src/boardtest.c:515: clear_tilemap();
	call	_clear_tilemap
	C$boardtest.c$516$1_0$214	= .
	.globl	C$boardtest.c$516$1_0$214
;src/boardtest.c:516: initializeBoardTest4();
	call	_initializeBoardTest4
	C$boardtest.c$517$1_0$214	= .
	.globl	C$boardtest.c$517$1_0$214
;src/boardtest.c:517: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$boardtest.c$518$1_0$214	= .
	.globl	C$boardtest.c$518$1_0$214
;src/boardtest.c:518: current_square.color00 = WHITE_PIECE;
	ld	hl, #(_current_square + 2)
	ld	(hl), #0x01
	C$boardtest.c$519$1_0$214	= .
	.globl	C$boardtest.c$519$1_0$214
;src/boardtest.c:519: current_square.color01 = WHITE_PIECE;
	ld	hl, #(_current_square + 3)
	ld	(hl), #0x01
	C$boardtest.c$520$1_0$214	= .
	.globl	C$boardtest.c$520$1_0$214
;src/boardtest.c:520: current_square.color10 = WHITE_PIECE;
	ld	hl, #(_current_square + 4)
	ld	(hl), #0x01
	C$boardtest.c$521$1_0$214	= .
	.globl	C$boardtest.c$521$1_0$214
;src/boardtest.c:521: current_square.color11 = BLACK_PIECE;
	ld	hl, #(_current_square + 5)
	ld	(hl), #0x04
	C$boardtest.c$522$1_0$214	= .
	.globl	C$boardtest.c$522$1_0$214
;src/boardtest.c:522: compute_paths();
	call	_compute_paths
	C$boardtest.c$523$1_0$214	= .
	.globl	C$boardtest.c$523$1_0$214
;src/boardtest.c:523: check_disconnected();
	call	_check_disconnected
	C$boardtest.c$524$1_0$214	= .
	.globl	C$boardtest.c$524$1_0$214
;src/boardtest.c:524: printBoard();
	call	_printBoard
	C$boardtest.c$525$1_0$214	= .
	.globl	C$boardtest.c$525$1_0$214
;src/boardtest.c:525: printSuitables();
	call	_printSuitables
	C$boardtest.c$526$1_0$214	= .
	.globl	C$boardtest.c$526$1_0$214
;src/boardtest.c:526: decideNextMove();
	call	_decideNextMove
	C$boardtest.c$527$1_0$214	= .
	.globl	C$boardtest.c$527$1_0$214
;src/boardtest.c:527: SMS_setNextTileatXY(0, 22);
	ld	hl, #0x7d80
	rst	#0x08
	C$boardtest.c$528$1_0$214	= .
	.globl	C$boardtest.c$528$1_0$214
;src/boardtest.c:528: printf("Column: ");
	ld	hl, #___str_51
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$529$1_0$214	= .
	.globl	C$boardtest.c$529$1_0$214
;src/boardtest.c:529: printf("%d",target_column);
	ld	a, (_target_column+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_52
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$530$1_0$214	= .
	.globl	C$boardtest.c$530$1_0$214
;src/boardtest.c:530: SMS_setNextTileatXY(11, 22);
	ld	hl, #0x7d96
	rst	#0x08
	C$boardtest.c$531$1_0$214	= .
	.globl	C$boardtest.c$531$1_0$214
;src/boardtest.c:531: printf("Rotation: ");
	ld	hl, #___str_53
	push	hl
	call	_printf
	pop	af
	C$boardtest.c$532$1_0$214	= .
	.globl	C$boardtest.c$532$1_0$214
;src/boardtest.c:532: printf("%d",target_rotation);
	ld	a, (_target_rotation+0)
	ld	c, a
	ld	b, #0x00
	push	bc
	ld	hl, #___str_52
	push	hl
	call	_printf
	pop	af
	pop	af
	C$boardtest.c$533$1_0$214	= .
	.globl	C$boardtest.c$533$1_0$214
;src/boardtest.c:533: while(frame_cnt < 200) {
00101$:
	ld	de, #0x00c8
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	ret	NC
	C$boardtest.c$534$2_0$215	= .
	.globl	C$boardtest.c$534$2_0$215
;src/boardtest.c:534: waitForFrame();
	call	_waitForFrame
	C$boardtest.c$536$1_0$214	= .
	.globl	C$boardtest.c$536$1_0$214
;src/boardtest.c:536: }
	C$boardtest.c$536$1_0$214	= .
	.globl	C$boardtest.c$536$1_0$214
	XG$test4$0$0	= .
	.globl	XG$test4$0$0
	jr	00101$
Fboardtest$__str_51$0_0$0 == .
___str_51:
	.ascii "Column: "
	.db 0x00
Fboardtest$__str_52$0_0$0 == .
___str_52:
	.ascii "%d"
	.db 0x00
Fboardtest$__str_53$0_0$0 == .
___str_53:
	.ascii "Rotation: "
	.db 0x00
	G$main$0$0	= .
	.globl	G$main$0$0
	C$boardtest.c$538$1_0$217	= .
	.globl	C$boardtest.c$538$1_0$217
;src/boardtest.c:538: void main(void) {
;	---------------------------------
; Function main
; ---------------------------------
_main::
	C$boardtest.c$539$1_0$217	= .
	.globl	C$boardtest.c$539$1_0$217
;src/boardtest.c:539: SMS_init();
	call	_SMS_init
	C$boardtest.c$540$1_0$217	= .
	.globl	C$boardtest.c$540$1_0$217
;src/boardtest.c:540: SMS_autoSetUpTextRenderer();
	call	_SMS_autoSetUpTextRenderer
	C$boardtest.c$544$1_0$217	= .
	.globl	C$boardtest.c$544$1_0$217
;src/boardtest.c:544: test4();
	C$boardtest.c$545$1_0$217	= .
	.globl	C$boardtest.c$545$1_0$217
;src/boardtest.c:545: }
	C$boardtest.c$545$1_0$217	= .
	.globl	C$boardtest.c$545$1_0$217
	XG$main$0$0	= .
	.globl	XG$main$0$0
	jp	_test4
	.area _CODE
Fboardtest$__str_54$0_0$0 == .
__str_54:
	.ascii "TuxedoGames"
	.db 0x00
Fboardtest$__str_55$0_0$0 == .
__str_55:
	.ascii "GOTRIS Board Test"
	.db 0x00
Fboardtest$__str_56$0_0$0 == .
__str_56:
	.ascii "Testing ROM Board Logic"
	.db 0x00
	.area _INITIALIZER
	.area _CABS (ABS)
	.org 0x7FF0
G$__SMS__SEGA_signature$0_0$0 == .
___SMS__SEGA_signature:
	.db #0x54	; 84	'T'
	.db #0x4d	; 77	'M'
	.db #0x52	; 82	'R'
	.db #0x20	; 32
	.db #0x53	; 83	'S'
	.db #0x45	; 69	'E'
	.db #0x47	; 71	'G'
	.db #0x41	; 65	'A'
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0x99	; 153
	.db #0x99	; 153
	.db #0x00	; 0
	.db #0x4c	; 76	'L'
	.org 0x7FD4
G$__SMS__SDSC_author$0_0$0 == .
___SMS__SDSC_author:
	.ascii "TuxedoGames"
	.db 0x00
	.org 0x7FC2
G$__SMS__SDSC_name$0_0$0 == .
___SMS__SDSC_name:
	.ascii "GOTRIS Board Test"
	.db 0x00
	.org 0x7FAA
G$__SMS__SDSC_descr$0_0$0 == .
___SMS__SDSC_descr:
	.ascii "Testing ROM Board Logic"
	.db 0x00
	.org 0x7FE0
G$__SMS__SDSC_signature$0_0$0 == .
___SMS__SDSC_signature:
	.db #0x53	; 83	'S'
	.db #0x44	; 68	'D'
	.db #0x53	; 83	'S'
	.db #0x43	; 67	'C'
	.db #0x01	; 1
	.db #0x03	; 3
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0xd4	; 212
	.db #0x7f	; 127
	.db #0xc2	; 194
	.db #0x7f	; 127
	.db #0xaa	; 170
	.db #0x7f	; 127
