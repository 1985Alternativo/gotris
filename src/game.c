#include <stdbool.h>
#include <SMSlib.h>
#include "engine/globaldefinitions.h"
#include "engine/gamelogic.h"
#include "engine/states.h"
#include "states/default_state.h"

extern bool resetPressed;

void main(void) {
    while(true) {
        resetPressed = false;
        init_game();
        init_states();
        while(!resetPressed) {
            update_state();
        }
    }
    
}
SMS_EMBED_SEGA_ROM_HEADER(9999, 0); // code 9999 hopefully free, here this means 'homebrew'
#ifdef PAL_MACHINE
SMS_EMBED_SDSC_HEADER_AUTO_DATE(1, 3, "TuxedoGames", "GOTRIS",
	"Tetris meets GO - PAL VERSION");
#else
SMS_EMBED_SDSC_HEADER_AUTO_DATE(1, 3, "TuxedoGames", "GOTRIS",
	"Tetris meets GO - NTSC VERSION");
#endif

