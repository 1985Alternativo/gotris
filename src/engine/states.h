//
// Created by JordiM on 31/12/2019.
//

#ifndef GOTRIS_STATES_H
#define GOTRIS_STATES_H

void push_state(State* state);
void pop_state(void);
State *top_state(void);
void init_states(void);
void clear_states(void);
void update_state(void);
void transition_to_state(s8 target_state);
void move_to_sub_state(s8 target_state);

#endif //GOTRIS_STATES_H