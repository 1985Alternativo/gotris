//
// Created by Jordi Montornes on 04/04/2017.
//

#ifndef GOTRIS_BOARD_H
#define GOTRIS_BOARD_H

#define BOARD_ROWS 22
#define BOARD_COLUMNS 10

#define EMPTY_CELL 0
#define NOT_PROCESSED_WHITE_CELL 1
#define NOT_CONNECTED_WHITE_CELL 2
#define CONNECTED_WHITE_CELL 3
#define NOT_PROCESSED_BLACK_CELL 4
#define NOT_CONNECTED_BLACK_CELL 5
#define CONNECTED_BLACK_CELL 6
#define BLOCKED_CELL 10
#define EXPLODING_CELL 11
#define BONUS_CELL_INVERSE 12
#define BONUS_CELL_DESTROY 13

#define NULLED_PIECE 0
#define WHITE_PIECE 1
#define BLACK_PIECE 4

#define INVERSE_BONUS 12
#define DESTROY_BONUS 13

#define EMPTY_UP 0x1
#define EMPTY_LEFT 0x2
#define EMPTY_RIGHT 0x4
#define EMPTY_DOWN 0x8

#define NO_SUITABLE 0x00
#define WHITE_SUITABLE 0x11 
#define BLACK_SUITABLE 0x21
#define WHITE_SUITABLE_UPPER_PART 0x41
#define BLACK_SUITABLE_UPPER_PART 0x81


#define is_free_board_position_for_square_creation() !((board[current_square.x] != EMPTY_CELL) || (board[current_square.x+1] != EMPTY_CELL) || (board[current_square.x+10] != EMPTY_CELL) || (board[current_square.x+11] != EMPTY_CELL))
#define is_free_board_down_position_for_square(position2check) !((board[position2check+20] != EMPTY_CELL) || (board[position2check+21] != EMPTY_CELL))
#define is_free_board_left_position_for_square(position2check) !((board[position2check-1] != EMPTY_CELL) || (board[position2check+9] != EMPTY_CELL))
#define is_free_board_right_position_for_square(position2check) !((board[position2check+2] != EMPTY_CELL) || (board[position2check+12] != EMPTY_CELL))
#define is_free_board_position_for_bonus_creation() (board[current_square.x] == EMPTY_CELL) 
#define is_free_board_down_position_for_bonus(position2check) (board[position2check+10] == EMPTY_CELL)
#define is_free_board_left_position_for_bonus(position2check) (board[position2check-1] == EMPTY_CELL) 
#define is_free_board_right_position_for_bonus(position2check) (board[position2check+1] == EMPTY_CELL)
#define is_free_board_next_position_for_piece(index) board[(active_pieces[index].y << 3) + (active_pieces[index].y << 1) + active_pieces[index].x+10] == EMPTY_CELL
#define is_free_board_next_position(x,y) (y < BOARD_ROWS-1) && (board[(y << 3) + (y << 1) + x+10] == EMPTY_CELL)
#define is_free_board_position(position2check) (board[position2check] == EMPTY_CELL)
#define isExploding(position2check) (board[position2check] == EXPLODING_CELL)

void init_board(void);
void clear_suitable_columns(void);
void rotate_square_piece(void);
void rotate_inv_square_piece(void);
void delete_individual_pieces_board(u8 index);
void delete_square_pieces_board(void);
void delete_bonus_board(void);
void create_individual_pieces_from_square(void);
void create_individual_pieces_from_board_position(u8 x, u8 y, u8 position);
void put_individual_pieces_board(u8 index);
u8 reverse_piece_board(u8 x, u8 y);
void destroy_piece_board(u8 x, u8 y);
void mark_position_for_change_in_board(u8 x, u8 y);
void mark_position_for_change_in_board_rec(u8 x, u8 y, u8 color);
void put_square_pieces_board(void);
void put_bonus_board(void);
bool compute_path(u8 x, u8 y);
u8 neighbour_emptyspace(u8 position);
bool neighbour_connected(u8 position);
bool neighbour_notprocessed(u8 position);
bool mark_as_disconnected(u8 x, u8 y, u8 position);
u8 destroy_disconnected(u8 x, u8 y, u8 position);
void clean_explosion(u8 x, u8 y, u8 position);
void clear_board_position(u8 x, u8 y);
void block_board_position(u8 x, u8 y);
void add_piece_board_position(u8 x, u8 y, u8 color);
u8 getCollisionRowFromColumn(u8 column);

#endif //GOTRIS_BOARD_H
