//
// Created by Jordi Montornes on 07/04/2017.
//

#ifndef GOTRIS_GAMELOGIC_H
#define GOTRIS_GAMELOGIC_H

void init_game(void);
void init_gamestatus(u8 startLevel);
void execute_game_logic(void);
void decide_next_square(void);
void update_player(void);
void update_level(void);
void get_game_actions(void);
void restore_board(void);
void end_demo(void);
#endif //GOTRIS_GAMELOGIC_H
