//
// Created by Jordi Montornes on 07/04/2017.
//
#include <stdbool.h>
#include <stdlib.h>
#include <SMSlib.h>
#include "globaldefinitions.h"
#include "gamelogic.h"
#include "board.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "input.h"
#include "cpu.h"
#include "../ram/globalvariables.h"
#include "../data/constantdata.h"
#include "../libs/montylib.h"
#include "../libs/saves.h"
#include <PSGlib.h>

void update_player(void) {

    if(number_human_players == 2) {
         if(current_player == PLAYER_1) { 
            current_player = PLAYER_2;
         } else {
            current_player = PLAYER_1;
         }
    } else if(number_human_players == 1) {
        if(current_player == PLAYER_1) { 
            current_player = PLAYER_CPU_1;
         } else {
            current_player = PLAYER_1;
         }
    } else {
        if(current_player == PLAYER_CPU_1) { 
            current_player = PLAYER_CPU_2;
         } else {
            current_player = PLAYER_CPU_1;
         }
    }   
}

void update_level(void) {
    if(level < newLevel) { 
        if(number_human_players == 1 && current_player == PLAYER_CPU_1) { 
            game_status = GAME_STATUS_LOOSING_GAME; 
            row_index = BOARD_ROWS - 1; 
            stop_music(); 
            stop_fx(); 
        } else { 
            level = newLevel; 
            game_status = GAME_STATUS_LEVEL_COMPLETE; 
            frame_cnt = 0;
            row_index = BOARD_ROWS - 1;
            if(number_human_players == 1 && current_player == PLAYER_1) {
                score_p2 = (level)<<LEVEL_DELIMITER;
                print_score_value(24,23,score_p2);
            } 
            //start_fadeout_music(); 
            play_song(CONGRATULATIONS1_OST);
        } 
        return;
    }
}

void init_game(void) {
    init_console();
    blackBackgroundPalette();
    blackSpritePalette();
    clear_tilemap();
    frame_cnt = 0;
    if(init_savesystem()) {
        draw_sram_present();
        while(frame_cnt < M_PERIOD) {
            waitForFrame();
        }
    } 
    blackBackgroundPalette();
    blackSpritePalette();
}

void init_gamestatus(u8 startLevel) {
    pause = false;
    game_status = GAME_STATUS_DRAWING_NEW_LEVEL;
    score_p1 = 0;
    score_p2 = 0;
    highScoreReached = false;
    bonusScore = false;
    bonusSamplePlay = false;
    piece_index = 0;
    row_index = BOARD_ROWS - 1;
    max_row_index = BOARD_ROWS - 1;

    frame_cnt = 0;
    level_timer = 0;
    row_index = BOARD_ROWS-1;
    endOfDrawingLevel = false;
    decodingIndex = 0;

    piecesUpdated = false;
    piecesDestroyed = false;
    isPaletteRed = false;
    isHurryUp = false;
    generateBonus = false;
    level = startLevel;
    newLevel = 0;
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    fallSpeed = levelfallSpeeds[level];
    fallAcum = 0;
    if(number_human_players > 0)
        current_player = PLAYER_1;
    else
        current_player = PLAYER_CPU_1;
    last_square_x = (get_rand() & 7) + 1;
    decide_next_square();
}


void restore_board(void) {
    u8 i;
    for(i=2;i<7;i++) {
        draw_board_position(i, 9,board[100+i]);
    }  
}

void decide_next_square(void) {
    u8 randnumber;
    randnumber = get_rand() & 1;
    if(randnumber) {
        next_square.color00 = WHITE_PIECE;   
    } else {
        next_square.color00 = BLACK_PIECE;
    }
    randnumber = get_rand() & 1;
    if(randnumber) {
        next_square.color01 = WHITE_PIECE;   
    } else {
        next_square.color01 = BLACK_PIECE;
    }
    randnumber = get_rand() & 1;
    if(randnumber) {
        next_square.color10 = WHITE_PIECE;   
    } else {
        next_square.color10 = BLACK_PIECE;
    }
    randnumber = get_rand() & 1;
    if(randnumber) {
        next_square.color11 = WHITE_PIECE;   
    } else {
        next_square.color11 = BLACK_PIECE;
    }
}

void generate_square_piece(void) {
    //current_square.x = last_square_x; //FIXME: see if uncomment it
    current_square.x = 4;
    current_square.y = 0;
    if(!is_free_board_position_for_square_creation()) {
        game_status = GAME_STATUS_LOOSING_GAME;
        row_index = BOARD_ROWS - 1;
        stop_music();
        stop_fx();
        return;
    }

    current_square.color00 = next_square.color00;
    current_square.color01 = next_square.color01;
    current_square.color10 = next_square.color10;
    current_square.color11 = next_square.color11;
    decide_next_square();
    draw_next_square(next_square);

    game_status = GAME_STATUS_SQUAREPIECEFALLING;
    frame_cnt = 0;
}

void fall_square_piece(void) {
     if(downAction) {
         fallAcum += 196;
     }
    
    if(rightAction && (current_square.x < BOARD_COLUMNS -2) && is_free_board_right_position_for_square((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
        delete_square_pieces_board();
        current_square.x++;
        put_square_pieces_board();
        play_fx(MOVE_FX);
    } 
    else if(leftAction && (current_square.x > 0 && is_free_board_left_position_for_square((current_square.y << 3) + (current_square.y << 1) + current_square.x))) {
        delete_square_pieces_board();
        current_square.x--;
        put_square_pieces_board();
        play_fx(MOVE_FX);
    }
    
    if(rotation1Action) {
        delete_square_pieces_board();
        rotate_square_piece();
        put_square_pieces_board();
    } else if(rotation2Action) {
        delete_square_pieces_board();
        rotate_inv_square_piece();
        put_square_pieces_board();
    }
    fallAcum = fallAcum + fallSpeed;
    if((fallAcum >> 8) > 1){
        fallAcum = 0;
        if(current_square.y < (BOARD_ROWS - 2) && is_free_board_down_position_for_square((current_square.y+1 << 3) + (current_square.y+1 << 1) + current_square.x)) {
            delete_square_pieces_board();
            current_square.y++;
            put_square_pieces_board();
        } else {
            last_square_x = current_square.x;
            create_individual_pieces_from_square();
            game_status = GAME_STATUS_PIECESFALLING;
            play_fx(FALL_FX);
            return;
        }
    }
}

void generate_bonus_piece(u8 bonusType) {
    
    //current_square.x = last_square_x; //FIXME: see if uncomment it
    current_square.x = 4;
    current_square.y = 0;
    if(bonusType == 0) {
        current_square.color00 = INVERSE_BONUS;
    } else if(bonusType == 1){
        current_square.color00 = DESTROY_BONUS;
    }
    if(!is_free_board_position_for_bonus_creation()) {
        game_status = GAME_STATUS_LOOSING_GAME;
        row_index = BOARD_ROWS - 1;
        stop_music();
        stop_fx();
        return;
    }
    game_status = GAME_STATUS_BONUSFALLING;
    frame_cnt = 0;
}

void fall_bonus_piece(void) {
    if((frame_cnt & 1) == 0) {
        if(downAction && current_square.y < (BOARD_ROWS - 1) && is_free_board_down_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
            delete_bonus_board();
            current_square.y++;
            put_bonus_board();
        }
    }

    if(current_square.color00 == DESTROY_BONUS) {
        if((frame_cnt & 3) == 0) { 
            if(isPaletteRed) {
                restore_fire_colors();
                isPaletteRed = false;
            } else {
                swap_fire_colors();
                isPaletteRed = true;
            }
        }
    }
    
    if(rightAction && (current_square.x < BOARD_COLUMNS - 1) && is_free_board_right_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
        delete_bonus_board();
        current_square.x++;
        put_bonus_board();
        play_fx(MOVE_FX);
    } 
    else if(leftAction && (current_square.x > 0 && is_free_board_left_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x))) {
        delete_bonus_board();
        current_square.x--;
        put_bonus_board();
        play_fx(MOVE_FX);
    }
    
    
    fallAcum = fallAcum + fallSpeed;
    if((fallAcum >> 8) > 1){
        fallAcum = 0;    
        if(current_square.y < (BOARD_ROWS - 1) && is_free_board_down_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
            delete_bonus_board();
            current_square.y++;
            put_bonus_board();
        } else {
            last_square_x = current_square.x; 
            if(current_square.color00 == INVERSE_BONUS) {
                if(current_square.y < (BOARD_ROWS -1)) {
                    game_status = GAME_STATUS_PIECESREVERSING;
                    delete_bonus_board();
                    row_index = current_square.y+1;
                } else {
                    delete_bonus_board();
                    game_status = GAME_STATUS_NOACTION;
                    update_level();
                    update_player();
                    clear_input();
                    row_index = BOARD_ROWS-1;
                    piecesUpdated = false;
                }
            } else if(current_square.color00 == DESTROY_BONUS){
                game_status = GAME_STATUS_PIECESDESTROYING;
                row_index = current_square.y;
            }
            frame_cnt = 0;
            play_fx(FALL_FX);
            return;
        }
    }
}


bool fall_pieces(void) {
    bool onepieceupdated = false;
    u8 i = 0;
    for(i = 0;i<piece_index;i++) {
        if(active_pieces[i].color != NULLED_PIECE) {
            //only works if downside pieces created before upside's
            if((active_pieces[i].y < (BOARD_ROWS - 1)) && is_free_board_next_position_for_piece(i)) { 
                delete_individual_pieces_board(i);
                active_pieces[i].y++;
                put_individual_pieces_board(i);
                onepieceupdated = true;
            } else {
                mark_position_for_change_in_board(active_pieces[i].x,active_pieces[i].y);
                active_pieces[i].color = NULLED_PIECE;
            }
        }
    }
    return onepieceupdated;
}

void reverse_pieces(void) {
    u8 original_color;
    if(!(frame_cnt & 3)) {
        original_color= reverse_piece_board(last_square_x, row_index);
        mark_position_for_change_in_board_rec(last_square_x, row_index, original_color);
        if(row_index < (BOARD_ROWS - 1)){
            row_index++;
            if(!(row_index & 3)) {
                play_fx(SELECT_FX);
            }
        } else {
            game_status = GAME_STATUS_CALCULATIONS;
            piecesUpdated = false;
        }
    }
}

void increment_score(u8 numPiecesDestroyed) {
    if(current_player == PLAYER_1 || (number_human_players == 0 && current_player == PLAYER_CPU_1)) {
        score_p1 = score_p1 + numPiecesDestroyed;
        if(bonusScore) {
            score_p1 = score_p1 + (numPiecesDestroyed<<1);
            bonusScore = false;
        } else {
            score_p1 = score_p1 + numPiecesDestroyed;
        }
        newLevel = score_p1 >> LEVEL_DELIMITER; 
        print_score_value(24,19,score_p1);
    } else {
        if(bonusScore) {
            score_p2 = score_p2 + (numPiecesDestroyed<<1);
            bonusScore = false;
        } else {
            score_p2 = score_p2 + numPiecesDestroyed;
        }
        newLevel = score_p2 >> LEVEL_DELIMITER;
        print_score_value(24,23,score_p2);
    }    
    
}

void add_100point_sprite(u8 x, u8 y) {
    list_node_t* current_element = active_points_list->head;
    point_sprite* current_point_sprite;
    u8 consecutive100Points = 0;
    u8 consecutive500Points = 0;
    while(current_element != NULL) { //FIXME: possible error aquí quan peten els punts
        current_point_sprite = (point_sprite*)(current_element->data);
        if(current_point_sprite->firstsprite == POINT_100_SPRITE_INDEX) {
           consecutive100Points++;
        } else if(current_point_sprite->firstsprite == POINT_500_SPRITE_INDEX) {
            consecutive100Points = 0;
            consecutive500Points++;
        }
        current_element = current_element->next;
    }
    if(consecutive100Points == 4) {
        deleteElementEndList(active_points_list);
        deleteElementEndList(active_points_list);
        deleteElementEndList(active_points_list);
        current_element = active_points_list->tail;
        current_point_sprite = (point_sprite*)(current_element->data);
        current_point_sprite->firstsprite = POINT_500_SPRITE_INDEX;
        consecutive500Points++;
        if(consecutive500Points == 4 && !bonusScore) {
            current_point_sprite = malloc(sizeof(point_sprite));
            current_point_sprite->x = (x<<3) + 80 + (active_points_list->numElements<<2);
            current_point_sprite->y = (y<<3) + 8 + (active_points_list->numElements<<2);
            current_point_sprite->firstsprite = BONUS_SPRITE_INDEX;
            addElementEndList(active_points_list, current_point_sprite);
            bonusScore = true;
            bonusSamplePlay = true;
        }
    } else {
        current_point_sprite = malloc(sizeof(point_sprite));
        current_point_sprite->x = (x<<3) + 80 + (active_points_list->numElements<<2);
        current_point_sprite->y = (y<<3) + 8 + (active_points_list->numElements<<2);
        current_point_sprite->firstsprite = POINT_100_SPRITE_INDEX;
        addElementEndList(active_points_list, current_point_sprite);
    }  
}

void destroy_pieces(void) {
    u8 position = ((row_index) << 3) + ((row_index) << 1) + last_square_x;
    u8 numPiecesDestroyed = 0;
    if(frame_cnt == XS_PERIOD) {
        destroy_piece_board(last_square_x, row_index);
        if(row_index < (BOARD_ROWS - 1)) {
           if(!is_free_board_position(position+10)) {
               numPiecesDestroyed++;
               add_100point_sprite(last_square_x, row_index + 1);
           }
           destroy_piece_board(last_square_x, row_index + 1);
        }
        if(row_index < (BOARD_ROWS - 2)) {
           if(!is_free_board_position(position+20)) {
               numPiecesDestroyed++;
               add_100point_sprite(last_square_x, row_index + 2);
           }
           destroy_piece_board(last_square_x, row_index + 2);
        }
        if(last_square_x > 0) {
            if(!is_free_board_position(position-1)) {
               numPiecesDestroyed++;
               add_100point_sprite(last_square_x - 1, row_index);
            }
            destroy_piece_board(last_square_x - 1, row_index);
        }
        if(last_square_x > 1) {
            if(!is_free_board_position(position-2)) {
               numPiecesDestroyed++;
               add_100point_sprite(last_square_x - 2, row_index);
            }
            destroy_piece_board(last_square_x - 2, row_index);
        }
        if(last_square_x  < (BOARD_COLUMNS - 1)) {
            if(!is_free_board_position(position+1)) {
               numPiecesDestroyed++;
               add_100point_sprite(last_square_x + 1, row_index);
            }
            destroy_piece_board(last_square_x + 1, row_index);
        }
        if(last_square_x  < (BOARD_COLUMNS - 2)) {
            if(!is_free_board_position(position+2)) {
               numPiecesDestroyed++;
               add_100point_sprite(last_square_x + 2, row_index);
            }
            destroy_piece_board(last_square_x + 2, row_index);
        }
        increment_score(numPiecesDestroyed);
        play_fx(DESTROY_FX);
    } else if(frame_cnt == XS_PERIOD + XXXS_PERIOD) {
        clean_explosion(last_square_x, row_index, position);
        if(row_index < (BOARD_ROWS - 1)) {
           clean_explosion(last_square_x, row_index + 1, position + 10); 
        }
        if(row_index < (BOARD_ROWS - 2)) {
           clean_explosion(last_square_x, row_index + 2, position + 20); 
        }
        if(last_square_x > 0) {
            clean_explosion(last_square_x - 1, row_index, position - 1);
        }
        if(last_square_x > 1) {
            clean_explosion(last_square_x - 2, row_index, position - 2);
        }
        if(last_square_x < (BOARD_COLUMNS - 1)) {
            clean_explosion(last_square_x + 1, row_index, position + 1);
        }
        if(last_square_x < (BOARD_COLUMNS - 2)) {
            clean_explosion(last_square_x + 2, row_index, position + 2);
        }
        game_status = GAME_STATUS_MAKE_PIECES_FALL;
        row_index = BOARD_ROWS-1;
        piecesUpdated = false;
        restore_fire_colors();
        frame_cnt = 0;
    } else {
        if((frame_cnt & 3) == 0) { 
            if(isPaletteRed) {
                restore_fire_colors();
                isPaletteRed = false;
            } else {
                swap_fire_colors();
                isPaletteRed = true;
            }
        }
    }
    
}

bool compute_paths(void) {
    bool somePiece = false;
    bool someChangeHappened = false;
    u8 position = (row_index << 3) + (row_index << 1);
    u8 i;
    for(i=0;i<BOARD_COLUMNS;i++) {
        if(!is_free_board_position(position)) {
            someChangeHappened = someChangeHappened || compute_path(i,row_index);
            somePiece = true;
        }
        position++;
       /*print_single_digit(11+i,22,suitable_columns[i]&0x0f);
        print_single_digit(11+i,23,(suitable_columns[i]&0xf0)>>4);*/
    }
    if(row_index > 0 && somePiece) {
        row_index--;
    } else {
        game_status = GAME_STATUS_CHECK_CALCULATIONS; 
    }
    return someChangeHappened;
}

bool compute_disconnected_pieces(void) {
    bool somePiece = false;
    bool somePieceDestroyed = false;
    u8 position = (row_index << 3) + (row_index << 1);
    u8 i;
    for(i=0;i<BOARD_COLUMNS;i++) {
        if(!is_free_board_position(position)) {
            somePieceDestroyed = mark_as_disconnected(i, row_index, position) || somePieceDestroyed;
            somePiece = true;
        }
        position++;
    }
    if(row_index > 0 && somePiece) {
        row_index--;
    } else {
        game_status = GAME_STATUS_ANIMATE_DISCONNECTED;
        frame_cnt = 0;
        connected_pieces_to_bw_palette();
        max_row_index = row_index;
        if(row_index < 6) {
            if(!isHurryUp) {
                play_song(HURRYUP_OST);
                isHurryUp = true;
            }
        } else {
            if(isHurryUp) {
                play_song(INGAME_OST);
                isHurryUp = false;
            }
        }
        row_index = BOARD_ROWS-1;
    }
    return somePieceDestroyed;
}

void destroy_disconnected_pieces(void) {
    bool somePieceInRow = false;
    u8 position = (row_index << 3) + (row_index << 1);
    u8 i,j;
    u8 numPiecesDestroyed = 0;
    u8 numPiecesDestroyedPosition = 0;
    for(i=0;i<BOARD_COLUMNS;i++) {
        if(!is_free_board_position(position)) {
            numPiecesDestroyedPosition = destroy_disconnected(i, row_index, position);
            for(j=0;j<numPiecesDestroyedPosition;j++) {
                add_100point_sprite(i,row_index);
            }
            numPiecesDestroyed = numPiecesDestroyed + numPiecesDestroyedPosition;
            somePieceInRow = true;
        }
        if(row_index < BOARD_ROWS - 2) {
            if(isExploding(position+20)) {
                clean_explosion(i, row_index+2,position+20);
                somePieceInRow = true; //TODO: TOO HACKY??
            }
        }
        position++;
    }
    increment_score(numPiecesDestroyed);

    if(row_index > 0 && somePieceInRow) {
        row_index--;
        if((frame_cnt & 3) == 0) { 
            if(isPaletteRed) {
                restore_fire_colors();
                isPaletteRed = false;
            } else {
                swap_fire_colors();
                isPaletteRed = true;
            }
        }
    } else {
        game_status = GAME_STATUS_MAKE_PIECES_FALL;
        row_index = BOARD_ROWS-1;
        piecesUpdated = false;
        restore_fire_colors();
    }
}

bool make_pieces_fall(void) {
    bool somePieceFalled = false;
    u8 position = (row_index << 3) + (row_index << 1);
    u8 i;
    for(i=0;i<BOARD_COLUMNS;i++) {
        if(!is_free_board_position(position) && is_free_board_next_position(i, row_index)) {
            create_individual_pieces_from_board_position(i, row_index, position);
            somePieceFalled = true;
        }
        position++;
    }
    if(row_index > max_row_index) { //FIXME check if it's > or >=
        row_index--;
    } else {
        game_status = GAME_STATUS_CHECK_PIECES_FALL;
        row_index = BOARD_ROWS-1;
    }
    return somePieceFalled;
}

void make_points_float(void) {
    list_node_t* current_element;
    list_node_t* element_to_delete;
    point_sprite* current_point_sprite;

    current_element = active_points_list->head;
    while(current_element != NULL) {
        current_point_sprite = (point_sprite*)current_element->data;
        if(current_point_sprite->y > 10 && current_point_sprite->y < 250) {
            if(current_point_sprite->firstsprite == POINT_100_SPRITE_INDEX)
                current_point_sprite->y = (current_point_sprite->y - 2);
            else if(current_point_sprite->firstsprite == POINT_500_SPRITE_INDEX) {
                current_point_sprite->y = (current_point_sprite->y - 1);
            } else if(current_point_sprite->firstsprite == BONUS_SPRITE_INDEX) {
                 current_point_sprite->y = (current_point_sprite->y - 3);
            }
            current_element = current_element->next;
        } else {
            element_to_delete = current_element;
            current_element = current_element->next;
            deleteElement_Unsafe(active_points_list, element_to_delete);
        }
    }
    draw_point_sprites();
}

void end_demo(void) {
    clear_point_sprites();
    clear_cursor_sprites();
    start_fadeout_music();
}

void clear_line(void) {
    u8 i;
    for(i=0;i<BOARD_COLUMNS;i++) {
        clear_board_position(i, row_index);
    }
}

void block_line(void) {
    u8 i;
    for(i=0;i<BOARD_COLUMNS;i++) {
        block_board_position(i, row_index);
    }
}

void draw_line(void) {
    u8 i,j;
    u8 nibble, number, color, position;
    u8* currentlevel;

    SMS_mapROMBank(CONSTANT_DATA_BANK);
    currentlevel = levels[level];
    i = 0;
    position = (row_index << 3) + (row_index << 1);
    while(1) {
        if(currentlevel[decodingIndex] == 0x22) {
            decodingIndex++;
            return;
        }
        if(currentlevel[decodingIndex] == 0xEE) {
            endOfDrawingLevel = true;
            return;
        }
        nibble = ((currentlevel[decodingIndex]) & 0xF0) >> 4; //upper nibble
        number = (nibble & 0xC) >> 2;
        color = (nibble & 0x3)+1;
        if(color == 2) color = 0;
        for(j=0;j<number;j++) {
            draw_board_position(i, row_index,color);
            board[position] = color;
            position++;
            i++;
        }
        nibble = (currentlevel[decodingIndex]) & 0x0F; //bottom nibble
        number = (nibble & 0xC) >> 2;
        color = (nibble & 0x3)+1;
        if(color == 2) color = 0;
        for(j=0;j<number;j++) {
            draw_board_position(i, row_index,color);
            board[position] = color;
            position++;
            i++;
        }
        decodingIndex++;
    }
}

void execute_game_logic(void) {
    if(game_status == GAME_STATUS_NOACTION) {
        u8 randValue = get_rand() & 3;
        if(last_square_x == BOARD_COLUMNS - 1) {
            last_square_x = BOARD_COLUMNS - 2;
        }
        //if(isHurryUp && randValue <= 1) {
        if(generateBonus && randValue <= 1) {
            generate_bonus_piece(randValue);
        } else {
            generate_square_piece();
        }
        generateBonus = false;
        if(current_player == PLAYER_CPU_1 || current_player == PLAYER_CPU_2) {
            decideNextMove();
            clear_suitable_columns();
            /*print_unsigned_char(22,10, target_column);
            print_unsigned_char(22,11, target_rotation);*/
        } 
        
    } else if(game_status == GAME_STATUS_SQUAREPIECEFALLING) {
        fall_square_piece();
        draw_cursor_sprites((current_square.x<<3)+88,(current_square.y<<3) - 2);
    } else if(game_status == GAME_STATUS_BONUSFALLING) {
        fall_bonus_piece();
        draw_cursor_sprites((current_square.x<<3)+80,(current_square.y<<3) - 2);
    } else if(game_status == GAME_STATUS_PIECESFALLING) {
        clear_cursor_sprites();
        piecesUpdated = fall_pieces();
        if(!piecesUpdated) {
            game_status = GAME_STATUS_CALCULATIONS;
            piece_index = 0;
            row_index = BOARD_ROWS-1;
            piecesUpdated = false;
        }
    } else if(game_status == GAME_STATUS_PIECESREVERSING) {
        reverse_pieces();
    } else if(game_status == GAME_STATUS_PIECESDESTROYING) {
        destroy_pieces();
    } else if(game_status == GAME_STATUS_CALCULATIONS) {
        piecesUpdated = compute_paths() || piecesUpdated;
    } else if(game_status == GAME_STATUS_CHECK_CALCULATIONS) {
        if(!piecesUpdated) {
            game_status = GAME_STATUS_COMPUTE_DISCONNECTED;
            row_index = BOARD_ROWS-1;
            piecesUpdated = false;
        } else {
            game_status = GAME_STATUS_CALCULATIONS;
            row_index = BOARD_ROWS-1;
            piecesUpdated = false;
        }
    } else if(game_status == GAME_STATUS_COMPUTE_DISCONNECTED) {
        piecesUpdated = compute_disconnected_pieces() || piecesUpdated;
    } else if(game_status == GAME_STATUS_ANIMATE_DISCONNECTED) {
        connected_pieces_to_normal_palette();
        if(!piecesUpdated) {
            game_status = GAME_STATUS_DESTROY_DISCONNECTED;
            isPaletteRed = false;
        } else {
            if(frame_cnt > S_PERIOD) {
                game_status = GAME_STATUS_DESTROY_DISCONNECTED;
                not_connected_pieces_to_normal_palette();
                isPaletteRed = false;
                play_fx(DESTROY_FX);
            } else {
                if((frame_cnt & 7) == 0) {
                    if(!isPaletteRed) {
                        not_connected_pieces_to_red_palette();
                        isPaletteRed = true;
                    } else {
                        not_connected_pieces_to_normal_palette();
                        isPaletteRed = false;
                    }
                } 
                
            }
        }
    } else if(game_status == GAME_STATUS_DESTROY_DISCONNECTED) {
        if(!piecesUpdated) {
            game_status = GAME_STATUS_NOACTION;
            if(bonusSamplePlay) {
                play_sample(BONUS_SAMPLE);
                bonusSamplePlay = false;
            }
            update_level();
            update_player();
            clear_input();
            row_index = BOARD_ROWS-1;
            piecesUpdated = false;
        } else {
            generateBonus = true;
            destroy_disconnected_pieces();  
        }
    } else if(game_status == GAME_STATUS_MAKE_PIECES_FALL) {
        piecesUpdated = make_pieces_fall() || piecesUpdated;
        make_points_float();
    }  else if(game_status == GAME_STATUS_CHECK_PIECES_FALL) {
        if(!piecesUpdated) {
            game_status = GAME_STATUS_CALCULATIONS;
            row_index = BOARD_ROWS-1;
            clear_point_sprites();
            piecesUpdated = false;
        } else {
            game_status = GAME_STATUS_PIECESFALLING_2;
            play_fx(FALL_FX);
            row_index = BOARD_ROWS-1;
            piecesUpdated = false;
        }
    } else if(game_status == GAME_STATUS_PIECESFALLING_2) {
        piecesUpdated = fall_pieces();
        make_points_float();
        if(!piecesUpdated) {
            game_status = GAME_STATUS_MAKE_PIECES_FALL;
            piece_index = 0;
            row_index = BOARD_ROWS-1;
            piecesUpdated = false;
        }
    } else if (game_status == GAME_STATUS_LEVEL_COMPLETE) {
        if(frame_cnt > M_PERIOD) {
            game_status = GAME_STATUS_ERASING_OLD_LEVEL;
        }
    } else if(game_status == GAME_STATUS_ERASING_OLD_LEVEL) {
        if(row_index > 0) {
            clear_line();
            row_index--;
        } else {
            clear_line();
            if(level < NUMBER_LEVELS) { 
            //if(level < 2) { //FIX ME
                game_status = GAME_STATUS_DRAWING_NEW_LEVEL;
                frame_cnt = 0;
                row_index = BOARD_ROWS-1;
                endOfDrawingLevel = false;
                decodingIndex = 0;
            } else {
                game_status = GAME_STATUS_ENDGAME;
                frame_cnt = 0;
            }
            
        }
    } else if(game_status == GAME_STATUS_DRAWING_NEW_LEVEL) {
        if(row_index > 0 && !endOfDrawingLevel) {
            draw_line();
            row_index--;
        } else {
            SMS_mapROMBank(CONSTANT_DATA_BANK);
            fallSpeed = levelfallSpeeds[level];
            background_to_level_palette(level);
            print_level_number(2,23,level+1);
            print_string(2,19,"LVL]GOAL^");
            print_score_value(2,21,(level+1)<<LEVEL_DELIMITER);
            game_status = GAME_STATUS_NOACTION;
            frame_cnt = 0;
            level_timer = 0;
            clear_input();
            row_index = BOARD_ROWS-1;
            piecesUpdated = false;
            play_song(INGAME_OST);
            //start_fadein_music();
        }
    } else if(game_status == GAME_STATUS_LOOSING_GAME) {
        if(row_index > 0) {
            block_line();
            row_index--;
        } else {
            block_line();
            game_status = GAME_STATUS_GAMEOVER;
            frame_cnt = 0;
        }
    } else if(game_status == GAME_STATUS_GAMEOVER) {
        if(frame_cnt == XS_PERIOD) {
            print_string(11,10,"GAME^");
            print_string(17,10,"OVER^");
            play_sample(GAMEOVER1_SAMPLE);
            play_sample(GAMEOVER2_SAMPLE);
            play_song(GAMEOVER_OST);
            start_fadein_music();
        }
        if(frame_cnt > XL_PERIOD) {
            if((score_p1 > highscore_table.table[9].score) || (score_p2 > highscore_table.table[9].score)) {
                highScoreReached = true;
            } 
            game_status = GAME_STATUS_RESET;
            stop_music();
            stop_fx();
        }
    } else if(game_status == GAME_STATUS_ENDGAME) {
        if(frame_cnt == XS_PERIOD) {        
            draw_congratulations();
            play_sample(CONGRATULATIONS1_SAMPLE);
            play_sample(CONGRATULATIONS2_SAMPLE);
            play_song(CONGRATULATIONS2_OST);
        }
        if(frame_cnt > XXL_PERIOD) {
            stop_music();
            stop_fx();
            if((score_p1 > highscore_table.table[9].score) || (score_p2 > highscore_table.table[9].score)) {
                highScoreReached = true;
            } 
            game_status = GAME_STATUS_ENDGAME2;
        }
    }
    level_timer++;
    if(level_timer == XXXXL_PERIOD) {
        fallSpeed += 32;
    }
}

void get_game_actions(void) {
    u16 direction;
    u16 rotation;
    leftAction = false;
    rightAction = false;
    downAction = false;
    rotation1Action = false;
    rotation2Action = false;
    if(current_player == PLAYER_CPU_1 || current_player == PLAYER_CPU_2) {
        direction = decideDirection(current_square.x, current_square.y+1);
        rotation = decideRotation(current_square.y+1); 
    } else {
        update_input();
        direction = getDirection();
        rotation = getRotation();
    }
    if(direction & DOWN_ACTION) {
        downAction = true;
    } else if(direction & LEFT_ACTION) {
        leftAction = true;
    } else if(direction & RIGHT_ACTION) {
        rightAction = true;
    }
    if(rotation & ROTATION_ONE) {
        rotation1Action = true;
    } else if(rotation & ROTATION_TWO) {
        rotation2Action = true;
    }
}
