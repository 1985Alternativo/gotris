
#ifndef GOTRIS_PALETTES_H_H
#define GOTRIS_PALETTES_H_H

#define getRedFromRGB(color)        (color&11)
#define getGreenFromRGB(color)      ((color&1100)>>2)
#define getBlueFromRGB(color)      ((color&110000)>>4)

#define WHITE_FFFFFF 0x3F
#define BLACK_000000 0x00
#define YELLOW_FFFF00 0x0F
#define ORANGE_FF5500 0x07
#define GREY_AAAAAA  0x2A

void setBGPalette(u8 *palette, u8 numcolors);
void setSpritePalette(u8 *palette,  u8 numcolors);
u8* getBGPalette(void);
u8* getSpritePalette(void);
void setBGPaletteColor(u8 position, u8 value);
void setSpritePaletteColor(u8 position, u8 value);

//void createBWPaletteVersion(u8 *original_palette, u8 *bw_palette, u8 numcolors);
void createSepiaPaletteVersion(const u8 *original_palette, u8 *bw_palette, u8 numcolors);
void createBluePaletteVersion(const u8 *original_palette, u8 *bw_palette, u8 numcolors);

void fadeToTargetBGPalette(const u8 *target_palette, u8 numcolors, u8 framesperstep);
void fadeToTargetSpritePalette(const u8 *target_palette, u8 numcolors, u8 framesperstep);
#endif //GOTRIS_PALETTES_H
