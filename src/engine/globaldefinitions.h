#ifndef GOTRIS_GLOBALDEFINITIONS_H_H
#define GOTRIS_GLOBALDEFINITIONS_H_H

#define MAX_PIECES_ACTIVE 10
#define MAX_POINTS_ACTIVE 8
#define MAX_100POINTS_ACTIVE 5
#define NUMBER_LEVELS 14

#define PLAYER_1 0
#define PLAYER_2 1
#define PLAYER_CPU_1 2
#define PLAYER_CPU_2 3

#define NEUTRAL_ACTION 0x0000
#define DOWN_ACTION  0x0002
#define LEFT_ACTION  0x0004
#define RIGHT_ACTION 0x0008
#define ROTATION_ONE 0x0010
#define ROTATION_TWO 0x0020 

#define POINT_100_SPRITE_INDEX 14
#define POINT_500_SPRITE_INDEX 15
#define POINT_00_SPRITE_INDEX 16
#define PLAYER_1_SPRITE_INDEX 17
#define PLAYER_P_SPRITE_INDEX 18
#define PLAYER_2_SPRITE_INDEX 19
#define PLAYER_CP_SPRITE_INDEX 20
#define PLAYER_PU_SPRITE_INDEX 21
#define BONUS_SPRITE_INDEX 22

#define LEVEL_DELIMITER 6


#define NUMBER_INTRO_SENTENCES 13
#define NUMBER_ENDGAME_SENTENCES 9

#define CONSTANT_DATA_BANK 2

#define FLASH_SAVING_ADDRESS 0x7C000 
#define FLASH_SAVING_BANK FLASH_SAVING_ADDRESS >> 14 

//timing times
#ifdef PAL_MACHINE
//PAL
#define XXXS_PERIOD 6 //PAL
#define XXS_PERIOD 10 //PAL
#define XS_PERIOD 20 //PAL
#define S_PERIOD 50 //PAL
#define M_PERIOD 100 //PAL
#define L_PERIOD 150 //PAL
#define XL_PERIOD 200 //PAL
#define XXL_PERIOD 400 //PAL
#define XXXL_PERIOD 2000 //PAL
#define XXXXL_PERIOD 3000 //PAL
#else
//NTSC
#define XXXS_PERIOD 5 //NTSC
#define XXS_PERIOD 12 //NTSC
#define XS_PERIOD 24 //NTSC
#define S_PERIOD 60 //NTSC
#define M_PERIOD 120 //NTSC
#define L_PERIOD 180 //NTSC
#define XL_PERIOD 240 //NTSC
#define XXL_PERIOD 480 //NTSC
#define XXXL_PERIOD 2400 //NTSC
#define XXXXL_PERIOD 3600 //NTSC
#endif

//Compatibilty types
typedef unsigned char u8;
typedef unsigned int u16;
typedef unsigned long u32;

typedef signed char s8;
typedef signed int s16;
typedef unsigned long s32;

//enums
enum GAME_STATUS {
    GAME_STATUS_NOACTION = 0
    , GAME_STATUS_SQUAREPIECEFALLING
    , GAME_STATUS_BONUSFALLING
    , GAME_STATUS_PIECESFALLING 
    , GAME_STATUS_PIECESREVERSING
    , GAME_STATUS_PIECESDESTROYING 
    , GAME_STATUS_CALCULATIONS 
    , GAME_STATUS_CHECK_CALCULATIONS 
    , GAME_STATUS_COMPUTE_DISCONNECTED
    , GAME_STATUS_ANIMATE_DISCONNECTED
    , GAME_STATUS_DESTROY_DISCONNECTED
    , GAME_STATUS_MAKE_PIECES_FALL 
    , GAME_STATUS_CHECK_PIECES_FALL 
    , GAME_STATUS_PIECESFALLING_2 
    , GAME_STATUS_LEVEL_COMPLETE
    , GAME_STATUS_ERASING_OLD_LEVEL 
    , GAME_STATUS_DRAWING_NEW_LEVEL 
    , GAME_STATUS_LOOSING_GAME 
    , GAME_STATUS_GAMEOVER 
    , GAME_STATUS_ENDGAME
    , GAME_STATUS_ENDGAME2 
    , GAME_STATUS_RESET 
};

enum GAME_STATES {
      DEFAULT_STATE = 0
    , LOGO_STATE 
    , INTRO_STATE 
    , TITLE_STATE 
    , SCORE_STATE 
    , CREDITS_STATE 
    , GAME_STATE 
    , DEMO_STATE 
    , ENTER_SCORE_STATE 
    , END_GAME_STATE
    , NUM_STATES
};

enum OST {
    LOGO_OST = 0
    ,TITLE_OST
    ,INTRO_OST
    ,SCORE_OST 
    ,CREDITS_OST
    ,START_OST 
    ,INGAME_OST
    ,HURRYUP_OST
    ,GAMEOVER_OST
    ,CONGRATULATIONS1_OST
    ,CONGRATULATIONS2_OST
    ,NUM_OST
};

enum FX {
    START_FX = 0
    ,SELECT_FX
    ,MOVE_FX
    ,FALL_FX
    ,DESTROY_FX
    ,NUM_FX
};

enum SAMPLES {
    THUNDER_SAMPLE = 0,
    GOTRIS_SAMPLE,
    GETREADY_SAMPLE,
    BONUS_SAMPLE,
    GAMEOVER1_SAMPLE,
    GAMEOVER2_SAMPLE,
    CONGRATULATIONS1_SAMPLE,
    CONGRATULATIONS2_SAMPLE,
    TUXEDO_SAMPLE,
    NUM_SAMPLES
};

enum ASSETS {
    LOGO_ASSETS=0
    ,DEVKIT_ASSETS
    ,SIMPLE_FONT_ASSET
    ,INTRO1_ASSETS
    ,INTRO2_ASSETS
    ,INTRO3_ASSETS
    ,TITLE_ASSETS
    ,CREDITS_ASSETS
    ,ENDING1_ASSETS
    ,NUM_ASSETS
};

enum PALETTES {
    FONT_PALETTE=0
    ,LOGO_PALETTE
    ,DEVKIT_PALETTE
    ,INTRO1_PALETTE
    ,INTRO2_PALETTE
    ,INTRO3_PALETTE
    ,TITLE_FIRST_PALETTE
    ,TITLE_PALETTE
    ,WHITE_PALETTE
    ,CREDITS_PALETTE
    ,PIECES_PALETTE
    ,ENDING1_PALETTE
    ,ENDING2_PALETTE
    ,NUM_PALETTES
};

enum SAVE_SYSTEMS {
    NO_SAVE=0,
    SRAM_SAVE,
    FLASH_SAVE
};

typedef struct {
    u8 x;
    u8 y;
    u8 color;
} piece;

typedef struct {
    u8 x;
    u8 y;
    u8 color00;
    u8 color01;
    u8 color10;
    u8 color11;
} square;

typedef struct {
    u8 name[3];
    u16 score;
} highscore;

typedef struct {
    u8 marker1;
    u8 marker2;
    highscore table[10];
} highscore_struct;

typedef struct {
    u8 x;
    u8 y;
    u8 firstsprite;
} point_sprite;

typedef struct list_node {
    void * data;
    struct list_node * next;
    struct list_node * prev;
} list_node_t;

typedef struct linked_list {
    list_node_t* head;
    list_node_t* tail;
    u8 numElements;
    int typeSize;
} linked_list_t;

typedef void (*pointerFunc) (void);

typedef struct {
    pointerFunc start;
    pointerFunc update;
    pointerFunc stop;
} State;

typedef struct {
    State** state_stack;
    s8 current_state_index;
    u8 state_stack_capacity;
    s8 current_target_state;
    bool preserve_context;
    State state_array[NUM_STATES];
} Statemanager;

#endif //GOTRIS_GLOBALDEFINITIONS_H_H