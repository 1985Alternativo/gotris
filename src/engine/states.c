#include <stdbool.h>
#include <stdlib.h>
#include "globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "states.h"
#include "../states/default_state.h"
#include "../states/logo_state.h"
#include "../states/intro_state.h"
#include "../states/title_state.h"
#include "../states/score_state.h"
#include "../states/credits_state.h"
#include "../states/game_state.h"
#include "../states/demo_state.h"
#include "../states/enter_score_state.h"
#include "../states/end_game_state.h"



void push_state(State* state) {
    if(statemanager.current_state_index + 1 < statemanager.state_stack_capacity) {
        statemanager.current_state_index++;
        statemanager.state_stack[statemanager.current_state_index] = state;
        statemanager.current_target_state = -1;
        statemanager.preserve_context = false;
        if(state->start != NULL) {
            state->start();
        }
    }
}

void pop_state(void) {
    State *topState;
    if(statemanager.current_state_index == -1)
        return;
    topState = top_state();
    if(topState->stop != NULL) {
        topState->stop();
        statemanager.state_stack[statemanager.current_state_index] = NULL;
        statemanager.current_state_index--;
    }
}

State *top_state(void) {
    return statemanager.state_stack[statemanager.current_state_index];
}

void init_states(void) {
    statemanager.current_state_index = -1;
    statemanager.state_stack_capacity = 10;
    statemanager.state_stack = malloc(statemanager.state_stack_capacity * sizeof(State*));
    statemanager.current_target_state = -1;
    statemanager.state_array[DEFAULT_STATE].start = default_state_start;
    statemanager.state_array[DEFAULT_STATE].update = default_state_update;
    statemanager.state_array[DEFAULT_STATE].stop = default_state_stop;
    statemanager.state_array[LOGO_STATE].start = logo_state_start;
    statemanager.state_array[LOGO_STATE].update = logo_state_update;
    statemanager.state_array[LOGO_STATE].stop = logo_state_stop;
    statemanager.state_array[INTRO_STATE].start = intro_state_start;
    statemanager.state_array[INTRO_STATE].update = intro_state_update;
    statemanager.state_array[INTRO_STATE].stop = intro_state_stop;
    statemanager.state_array[TITLE_STATE].start = title_state_start;
    statemanager.state_array[TITLE_STATE].update = title_state_update;
    statemanager.state_array[TITLE_STATE].stop = title_state_stop;
    statemanager.state_array[SCORE_STATE].start = score_state_start;
    statemanager.state_array[SCORE_STATE].update = score_state_update;
    statemanager.state_array[SCORE_STATE].stop = score_state_stop;
    statemanager.state_array[CREDITS_STATE].start = credits_state_start;
    statemanager.state_array[CREDITS_STATE].update = credits_state_update;
    statemanager.state_array[CREDITS_STATE].stop = credits_state_stop;
    statemanager.state_array[GAME_STATE].start = game_state_start;
    statemanager.state_array[GAME_STATE].update = game_state_update;
    statemanager.state_array[GAME_STATE].stop = game_state_stop;
    statemanager.state_array[DEMO_STATE].start = demo_state_start;
    statemanager.state_array[DEMO_STATE].update = demo_state_update;
    statemanager.state_array[DEMO_STATE].stop = demo_state_stop;
    statemanager.state_array[ENTER_SCORE_STATE].start = enter_score_state_start;
    statemanager.state_array[ENTER_SCORE_STATE].update = enter_score_state_update;
    statemanager.state_array[ENTER_SCORE_STATE].stop = enter_score_state_stop;
    statemanager.state_array[END_GAME_STATE].start = end_game_state_start;
    statemanager.state_array[END_GAME_STATE].update = end_game_state_update;
    statemanager.state_array[END_GAME_STATE].stop = end_game_state_stop;
    push_state(statemanager.state_array);
}

void clear_states(void) {
    do {
        pop_state();
    } while(statemanager.current_state_index > -1);
    free(statemanager.state_stack);
}

void update_state(void) {
    State *topState;
    if(statemanager.current_state_index != -1) {
        if(statemanager.current_target_state != -1) {
            if(!statemanager.preserve_context) {
                pop_state();
            }
            push_state(statemanager.state_array + statemanager.current_target_state);
        } else {
            topState = top_state();
            if(topState->update != NULL) {
                topState->update();
            }
        }
    } else {
        if(statemanager.current_target_state != -1) {
            push_state(statemanager.state_array + statemanager.current_target_state);
        }
    }
}

void transition_to_state(s8 target_state) {
    statemanager.current_target_state = target_state;
    statemanager.preserve_context = false;
}

void move_to_sub_state(s8 target_state) {
    statemanager.current_target_state = target_state;
    statemanager.preserve_context = true;
}