#include <stdbool.h>
#include <SMSlib.h>
#include "globaldefinitions.h"
#include "input.h"
#include "../ram/globalvariables.h"


void clear_input(void) {
    movement_direction = 0; 
    rotation_input = 0;
    button_input = 0;
    repeat_movement = 0;
    repeat_rotation = 0; 
}

void update_input(void) {
    u16 keys_pressed = SMS_getKeysPressed();
    u16 keys_held = SMS_getKeysHeld(); 
    u16 keys_released = SMS_getKeysReleased();
    u16 KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_ONE, KEY_TWO;

    if(current_player == PLAYER_1) {
        KEY_DOWN = KEY_DOWN_P1;
        KEY_LEFT = KEY_LEFT_P1;
        KEY_RIGHT = KEY_RIGHT_P1;
        KEY_ONE = KEY_ONE_P1;
        KEY_TWO = KEY_TWO_P1;
    } else {
        KEY_DOWN = KEY_DOWN_P2;
        KEY_LEFT = KEY_LEFT_P2;
        KEY_RIGHT = KEY_RIGHT_P2;
        KEY_ONE = KEY_ONE_P2;
        KEY_TWO = KEY_TWO_P2;
    }
    
    if(keys_pressed != 0) {
        button_input = keys_pressed;
        if(movement_direction == 0) {
            if(keys_pressed & KEY_DOWN) {
                movement_direction = DOWN_ACTION;
                input_cnt_direction =  0;
                repeat_movement = 0;
            } else if((keys_pressed & KEY_LEFT) && !(keys_pressed & KEY_RIGHT)) {
                movement_direction = LEFT_ACTION;
                input_cnt_direction =  0;
                repeat_movement = 0;
            } else if((keys_pressed & KEY_RIGHT) && !(keys_pressed & KEY_LEFT)) {
                movement_direction = RIGHT_ACTION;
                input_cnt_direction =  0;
                repeat_movement = 0;
            }
        } else {
            if(keys_pressed & KEY_DOWN) {
                movement_direction = DOWN_ACTION;
                repeat_movement = 0;
                input_cnt_direction = 0;
            } else if(movement_direction & KEY_LEFT) {
                if((keys_pressed & KEY_RIGHT) && !(keys_pressed & KEY_LEFT)) {
                    movement_direction = 0;
                    repeat_movement = 0;
                    input_cnt_direction = 0;
                }
            } else if(movement_direction & KEY_RIGHT) {
                if((keys_pressed & KEY_LEFT) && !(keys_pressed & KEY_RIGHT)) {
                    movement_direction = 0;
                    repeat_movement = 0;
                    input_cnt_direction = 0;
                }
            }
        }
        if(rotation_input == 0) {
            if((keys_pressed & KEY_ONE) && !(keys_pressed & KEY_TWO)) {
                rotation_input = ROTATION_ONE;
                repeat_rotation = 0;
                input_cnt_rotation =  0;
            } else if((keys_pressed & KEY_TWO) && !(keys_pressed & KEY_ONE)) {
                rotation_input = ROTATION_TWO;
                repeat_rotation = 0;
                input_cnt_rotation =  0;
            }
        } else {
            if(rotation_input & KEY_ONE) {
                if((keys_pressed & KEY_TWO) && !(keys_pressed & KEY_ONE)) {
                    rotation_input = 0;
                    repeat_rotation = 0;
                    input_cnt_rotation = 0;
                }
            } else if(rotation_input & KEY_TWO) {
                if((keys_pressed & KEY_ONE) && !(keys_pressed & KEY_TWO)) {
                    rotation_input = 0;
                    repeat_rotation = 0;
                    input_cnt_rotation = 0; 
                } 
            }
        }
        if(keys_pressed & RESET_KEY) {
            resetPressed = true;
        }
    }
    if(keys_held != 0) {
        if(keys_held & KEY_DOWN) {
            movement_direction = DOWN_ACTION;
            repeat_movement = 0;
            input_cnt_direction = 0;
        } else if(movement_direction & LEFT_ACTION) {
            if((keys_held & KEY_RIGHT) && !(keys_held & KEY_LEFT)) {
                movement_direction = 0;
                repeat_movement = 0;
                input_cnt_direction = 0;
            }
        } else if(movement_direction & RIGHT_ACTION) {
            if((keys_held & KEY_LEFT) && !(keys_held & KEY_RIGHT)) {
                movement_direction = 0;
                repeat_movement = 0;
                input_cnt_direction = 0;
            } 
        }
         
        if(rotation_input & ROTATION_ONE) {
            if((keys_held & KEY_TWO) && !(keys_held & KEY_ONE)) {
                rotation_input = 0;
                repeat_rotation = 0;
                input_cnt_rotation = 0;
            }
        } else if(rotation_input & ROTATION_TWO) {
            if((keys_held & KEY_ONE) && !(keys_held & KEY_TWO)) {
                rotation_input = 0;
                repeat_rotation = 0;
                input_cnt_rotation = 0; 
            }
        }
    }
    if(keys_released != 0) {
        if(keys_released & KEY_DOWN) {
            if(movement_direction & DOWN_ACTION) {
                movement_direction = 0;
                repeat_movement = 0;
                input_cnt_direction = 0;
            }
        }
        if(keys_released & KEY_LEFT) {
            if(movement_direction & LEFT_ACTION) {
                movement_direction = 0;
                repeat_movement = 0;
                input_cnt_rotation = 0;
            }
        }
        if(keys_released & KEY_RIGHT) {
            if(movement_direction & RIGHT_ACTION) {
                movement_direction = 0;
                repeat_movement = 0;
                input_cnt_rotation = 0;
            }
        }
        if(keys_released & KEY_ONE) {
            if(rotation_input & ROTATION_ONE) {
                rotation_input = 0;
                repeat_rotation = 0;
                input_cnt_rotation = 0;
            }
        }
        if(keys_released & KEY_TWO) {
            if(rotation_input & ROTATION_TWO) {
                rotation_input = 0;
                repeat_rotation = 0;
                input_cnt_rotation = 0;
            }
        }
    }
    
    input_cnt_direction++;
    input_cnt_rotation++;
}

u16 getDirection(void) {
    if(repeat_movement == 0){
        if(movement_direction != NEUTRAL) 
            repeat_movement = 1;
        return movement_direction;
    } else if(repeat_movement == 1){
        if(input_cnt_direction > FIRST_REPEAT_KEY_DELAY) {
            input_cnt_direction = 0;
            repeat_movement++;
            return movement_direction;
        } else {
            return NEUTRAL;
        }
    } else {
        if(input_cnt_direction > REPEAT_KEY_DELAY) {
            input_cnt_direction = 0;
            repeat_movement++;
            return movement_direction;
        } else {
            return NEUTRAL;
        }
    }
}

u16 getRotation(void) {
     if(repeat_rotation == 0){
        if(rotation_input != NEUTRAL)
            repeat_rotation = 1;
        return rotation_input;
    } else if(repeat_rotation == 1){
        if(input_cnt_rotation > FIRST_REPEAT_KEY_DELAY) {
            input_cnt_rotation = 0;
            return rotation_input;
        } else {
            return NEUTRAL;
        }
    } else {
        if(input_cnt_rotation > REPEAT_KEY_DELAY) {
            input_cnt_rotation = 0;
            return rotation_input;
        } else {
            return NEUTRAL;
        }
    }
}

u16 getButtonsPressed(void) {
    return button_input;
}