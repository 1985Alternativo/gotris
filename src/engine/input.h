//
// Created by Jordi Montornes on 04/04/2017.
//

#ifndef GOTRIS_INPUT_H_H
#define GOTRIS_INPUT_H_H

#define FIRST_REPEAT_KEY_DELAY 20
#define REPEAT_KEY_DELAY 10 
#define NEUTRAL 0x0000
#define KEY_DOWN_P1  0x0002
#define KEY_LEFT_P1  0x0004
#define KEY_RIGHT_P1 0x0008
#define KEY_ONE_P1   0x0010
#define KEY_TWO_P1   0x0020 
#define KEY_DOWN_P2  0x0080
#define KEY_LEFT_P2  0x0100
#define KEY_RIGHT_P2 0x0200
#define KEY_ONE_P2   0x0400
#define KEY_TWO_P2   0x0800 

void clear_input(void);
void update_input(void);
u16 getDirection(void);
u16 getRotation(void);
u16 getButtonsPressed(void);

#endif //GOTRIS_INPUT_H_H
