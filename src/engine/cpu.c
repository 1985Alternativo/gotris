#include <stdbool.h>
#include <stdlib.h>
#include "globaldefinitions.h"
#include "board.h"
#include "../ram/globalvariables.h"
#include "cpu.h"

#include "../libs/montylib.h"


void getTargetBottomPart(u8 color, u8 column) {
    if(current_square.color10 == color) {
        target_column = column;
    } else if(current_square.color11 == color) {
        if(column) {
            target_column = column-1;
        } else {
            target_column = column;
            target_rotation = 4;
        }
    } else if(current_square.color00 == color) {
        target_column = column;
        target_rotation = 2;
    } else if(current_square.color01 == color) {
        if(column) {
            target_column = column-1;
            target_rotation = 1;
        } else {
            target_column = column;
            target_rotation = 4;
        }
    }
}

void getTargetUpperPart(u8 color, u8 column) {
    if(current_square.color10 == color) {
        target_column = column;
        target_rotation = 4;
    } else if(current_square.color11 == color) {
        if(column) {
            target_column = column-1;
            target_rotation = 2;
        } else {
            target_column = column;
            target_rotation = 5;
        }
    } else if(current_square.color00 == color) {
        target_column = column;
    } else if(current_square.color01 == color) {
        if(column) {
            target_column = column-1;
        } else {
            target_column = column;
            target_rotation = 2;
        }
    }
}

void decideNextMove(void) {
    u8 i, tmpValue, tmpColor, maxWhite, maxBlack;
    u8 maxSuitableColumn, maxSuitableColor, maxSuitableElements;

    current_rotation = 3;
    target_rotation = 3;
    target_column = 0;
    tmpColor = WHITE_PIECE;
    if((current_square.color00 != BONUS_CELL_INVERSE) && (current_square.color00 != BONUS_CELL_DESTROY)) {
        maxSuitableColumn = get_rand() & 7;
        maxSuitableColor = WHITE_SUITABLE;
        maxSuitableElements = 0;
        maxWhite = 0;
        maxBlack = 0;
        for(i=0;i<BOARD_COLUMNS;i++) {
            if(suitable_columns[i] != NO_SUITABLE) {
                maxBlack = (suitable_columns[i]&0x3);//las two bits
                maxWhite = (suitable_columns[i]&0xC)>>2;//3rd and 4th bit
                if(maxBlack > maxWhite) { 
                    tmpValue = maxBlack;
                    tmpColor = BLACK_PIECE;
                } else {
                    tmpValue = maxWhite;
                    tmpColor = WHITE_PIECE;
                }
                if(tmpValue > maxSuitableElements) {
                    maxSuitableColumn = i;
                    maxSuitableColor = (suitable_columns[i]&0xf0); 
                    maxSuitableElements = tmpValue;
                }
            }
        }
        i=maxSuitableColumn;

        if(tmpColor == BLACK_PIECE) {
            if(maxSuitableColor & WHITE_SUITABLE) {
            getTargetBottomPart(WHITE_PIECE,i);                    
            } else if(maxSuitableColor & WHITE_SUITABLE_UPPER_PART) {
                getTargetUpperPart(WHITE_PIECE,i);
            }
        } else {
            if(maxSuitableColor & BLACK_SUITABLE) {
                getTargetBottomPart(BLACK_PIECE,i);
            } else if(maxSuitableColor & BLACK_SUITABLE_UPPER_PART) {
                getTargetUpperPart(BLACK_PIECE,i);
            }
        }



        target_row = getCollisionRowFromColumn(target_column);
        if(i==BOARD_COLUMNS || (target_row < 8 && current_square.color00 != BONUS_CELL_INVERSE) ){
            target_column = get_rand() & 7;
            target_rotation = get_rand() & 7; //value from 0-7
            if(!target_rotation) target_rotation++; //value from 1-7
            target_rotation--; //value from 0-6
            target_row = getCollisionRowFromColumn(target_column);
        } 
    } else {
        if(current_square.color00 == BONUS_CELL_INVERSE) {
            target_row = 10;
        } else {
            target_row = 0;
        }
        
        for(i=0;i<BOARD_COLUMNS;i++) {
            tmpValue = getCollisionRowFromColumn(i);
            if((tmpValue < target_row && current_square.color00 == BONUS_CELL_INVERSE) || (tmpValue > target_row && tmpValue < BOARD_ROWS && current_square.color00 == BONUS_CELL_DESTROY)) {
                target_column = i;
                target_row = getCollisionRowFromColumn(target_column);
            }           
        }
    } 
    
    movement_counter = target_row;
    rotation_counter = target_row;
}


u16 decideDirection(u8 current_column, u8 current_row) {
    s8 column_delta, row_delta;

    if(!movement_counter) {
        row_delta = target_row - current_row;
        column_delta = target_column - current_column;
        if(row_delta > 0) {
            movement_counter = row_delta;
        } else {
            movement_counter = 1;
        }
        if(column_delta > 0) {
            return RIGHT_ACTION;
        } else if(column_delta < 0) {
            return LEFT_ACTION;
        } else {
            movement_counter = 0;
            return DOWN_ACTION;
        }
    } else {
        movement_counter--;
        return NEUTRAL_ACTION;
    }
    
}

u16 decideRotation(u8 current_row) {
    s8 row_delta, rotation_delta;
    if(!rotation_counter) {
        rotation_delta = target_rotation - current_rotation;
        row_delta = target_row - current_row;
        if(row_delta > 0) {
            rotation_counter = row_delta;
        } else {
            rotation_counter = 1;
        }
        if(rotation_delta > 0) {
            current_rotation++;
            return ROTATION_ONE;
        } else if(rotation_delta < 0) {
            current_rotation--;
            return ROTATION_TWO;
        }
        return NEUTRAL_ACTION;
   } else {
        rotation_counter--;
        return NEUTRAL_ACTION;
   }
    
}