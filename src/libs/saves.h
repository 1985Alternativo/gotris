//
// Created by Jordi Montornes on 13/06/2023.
//
#ifndef GOTRIS_SAVES_H_H
#define GOTRIS_SAVES_H_H

bool init_savesystem(void);
void saveChanges(void);

#endif //GOTRIS_SAVES_H_H
