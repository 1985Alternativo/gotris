//
// Created by Jordi Montornes on 04/04/2017.
//
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "../engine/globaldefinitions.h"
#include "../data/constantdata.h"
#include "../ram/globalvariables.h"
#include "montylib.h"
#include <SMSlib.h>
#include <PSGlib.h>

void init_console(void) {
    SMS_init();
    SMS_setSpriteMode(SPRITEMODE_NORMAL);
    SMS_useFirstHalfTilesforSprites(true);
    SMS_VDPturnOnFeature(VDPFEATURE_LEFTCOLBLANK);
    SMS_displayOn();
    pause = false;
    rand_index = 0;
    current_music_bank = 0;
    current_fx_bank = 0;
    current_resource_bank = 0;
}

void clear_tilemap(void) {
    u8 i,j;
    SMS_setNextTileatXY(0,0);
    for(j=0;j<SCREEN_ROWS;j++) {
        for(i=0;i<SCREEN_COLUMNS;i++) {
            SMS_setTile(0);
        }
    }
}




void waitForFrame(void) {
    SMS_mapROMBank(current_music_bank);
    if(PSGGetStatus() == PSG_PLAYING){
        if(target_atenuation < current_atenuation) {
            current_atenuation = current_atenuation - 4;
            PSGSetMusicVolumeAttenuation(current_atenuation>>4);
        } else if(target_atenuation > current_atenuation) {
            current_atenuation = current_atenuation + 4;
            PSGSetMusicVolumeAttenuation(current_atenuation>>4);
        }
        PSGFrame();
    }
    SMS_mapROMBank(current_fx_bank);
    if(PSGSFXGetStatus() == PSG_PLAYING){
        PSGSFXFrame();
    } 
    SMS_mapROMBank(current_resource_bank); 
    waitForFrameNoMusic();
}

void waitForFrameNoMusic(void) {
    
    SMS_waitForVBlank();
    SMS_copySpritestoSAT();
    frame_cnt++;
}


u8 get_highscore_position(u16 score) {
    s8 sramIndex = 0;
    if( highscore_table.marker1!= 69 || highscore_table.marker2 != 148) {
        return 69;
    } else {
        for(sramIndex=9;sramIndex>=0;sramIndex--) {
            if(highscore_table.table[sramIndex].score > score) {
                SMS_disableSRAM();
                return sramIndex + 1;
            }
        }
        return 0;
    }
}

void insert_highscore_position(u8 scorePosition, u16 score) {
    u8 sramIndex = 0;
    
    for(sramIndex=8;sramIndex>=scorePosition;sramIndex--) {
        highscore_table.table[sramIndex+1].name[0] = highscore_table.table[sramIndex].name[0];
        highscore_table.table[sramIndex+1].name[1] = highscore_table.table[sramIndex].name[1];
        highscore_table.table[sramIndex+1].name[2] = highscore_table.table[sramIndex].name[2];
        highscore_table.table[sramIndex+1].score = highscore_table.table[sramIndex].score;
    }
    highscore_table.table[scorePosition].name[0] = 0;
    highscore_table.table[scorePosition].name[1] = 0;
    highscore_table.table[scorePosition].name[2] = 0;
    highscore_table.table[scorePosition].score = score;
    
}

u8 get_highscore_position_char(u8 scorePosition, u8 charPosition) {
    u8 charValue;   
    charValue = highscore_table.table[scorePosition].name[charPosition];
    return charValue;
}

void update_highscore_position(u8 scorePosition, u8 charPosition, u8 charValue) {  
    highscore_table.table[scorePosition].name[charPosition] = charValue;
}

u8 get_rand(void) {
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    return randLUT[rand_index++]; 
}

void setTileMapToHighPriority(u8 x, u8 y) {
    u8 value[1] = {0x10}; //FIXME: It only works by chance...
    SMS_VRAMmemcpy(XYtoADDR((x),(y))+1,value,1);
}

linked_list_t* createList(int typeSize) {
    linked_list_t* newList = malloc(sizeof(linked_list_t));
    newList->head = NULL;
    newList->tail = NULL;
    newList->numElements = 0;
    newList->typeSize = typeSize;
    return newList;
}
void addElementBeginingList(linked_list_t* list, void * element) {
    list_node_t* new_head = NULL;
    new_head = (list_node_t *) malloc(sizeof(list_node_t));
    if(new_head == NULL) {
        return;
    }
    new_head->data = malloc(list->typeSize);
    memcpy(new_head->data, element, list->typeSize);
    new_head->next = list->head;
    new_head->prev = NULL;
    if(list->head != NULL)
        list->head->prev = new_head;
    if(list->tail == NULL)
        list->tail = new_head;
    list->head = new_head;
    list->numElements++;
}

void addElementEndList(linked_list_t* list, void * element) {
    list_node_t* new_tail = NULL;
    new_tail = (list_node_t *) malloc(sizeof(list_node_t));
    if(new_tail == NULL) {
        return;
    }
    new_tail->data = malloc(list->typeSize);
    memcpy(new_tail->data, element, list->typeSize);
    new_tail->prev = list->tail;
    new_tail->next = NULL;
    if(list->tail != NULL) 
        list->tail->next = new_tail;
    if(list->head == NULL)
        list->head = new_tail;
    list->tail = new_tail;
    list->numElements++;
}


void deleteElementBeginingList(linked_list_t* list) {
    if(list->numElements == 0)
        return;
    list_node_t* current_element = list->head;
    if(current_element->next != NULL) {
        current_element->next->prev = NULL;
        list->head = current_element->next;
    } else {
        list->head = NULL;
        list->tail = NULL;
    }
    free(current_element->data);
    free(current_element);
    list->numElements--;
}

void deleteElementEndList(linked_list_t* list) {
    if(list->numElements == 0)
        return;
    list_node_t* current_element = list->tail;
    if(current_element->prev != NULL) {
        current_element->prev->next = NULL;
        list->tail = current_element->prev;
    } else {
        list->head = NULL;
        list->tail = NULL;
    }
    free(current_element->data);
    free(current_element);
    list->numElements--;    
}

void deleteElementPosList(linked_list_t* list, u8 pos) {
    u8 currentPos = 0;
    if(list->numElements == 0)
        return;
    if(pos==0) {
        deleteElementBeginingList(list);
        return;
    }
    if(pos == (list->numElements-1)) {
        deleteElementEndList(list);
        return;
    }
    list_node_t* current_element = list->head;
    while(current_element->next != NULL) {
        current_element = current_element->next;
        currentPos++;
        if(currentPos == pos) 
            break;
        
    }
    if(currentPos!=pos)
        return;
    if(current_element->next != NULL) {
        if(current_element->prev != NULL) {
            current_element->next->prev = current_element->prev;
        } 
    } 
    if(current_element->prev != NULL) {
        if(current_element->next != NULL) {
            current_element->prev->next = current_element->next;
        }
    }
    free(current_element->data);
    free(current_element);
    list->numElements--;
}

void deleteElement_Unsafe(linked_list_t* list, list_node_t* element_to_delete) {
    if(element_to_delete->next == NULL && element_to_delete->prev == NULL) {
        list->head = NULL;
        list->tail = NULL;
    } else {
        if(element_to_delete->next != NULL) {
            if(element_to_delete->prev != NULL) {
                element_to_delete->next->prev = element_to_delete->prev;
            } else { //first element
                element_to_delete->next->prev = NULL;
                list->head = element_to_delete->next;
            }
        } 
        
        if(element_to_delete->prev != NULL) {
            if(element_to_delete->next != NULL) {
                element_to_delete->prev->next = element_to_delete->next;
            } else { //last element
                element_to_delete->prev->next = NULL;
                list->tail = element_to_delete->prev;
            }
        }
    }
    free(element_to_delete->data);
    free(element_to_delete);
    list->numElements--;
}

void destroyList(linked_list_t* list) {
    while(list->numElements) {
        deleteElementBeginingList(list);
    }
    free(list);
}

