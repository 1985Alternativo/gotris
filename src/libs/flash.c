#include <stdint.h>
#include <string.h>

typedef void (*ids_read_fn)(uint8_t*) __sdcccall(1);
typedef void (*sect_erase_fn)(uint8_t bank, uint16_t offset) __sdcccall(1);
typedef void (*program_fn)(uint8_t bank, uint16_t offset, uint8_t value) __sdcccall(1);

// void ids_read(uint8_t *ids);
// Parameters:
// - hl: *ids
static const uint8_t IDS_READ[] = {
				// _ids_read::
	0xF3,			//	di
	0xEB,                   //	ex de, hl		; de <- *ids
	0x21, 0x55, 0x55,       //	ld hl, #0x5555		; unlock
	0x36, 0xAA,             //	ld (hl), #0xaa
	0x21, 0xAA, 0x2A,       //	ld hl, #0x2aaa
	0x36, 0x55,             //	ld (hl), #0x55
	0x21, 0x55, 0x55,       //	ld hl, #0x5555		; Software ID entry
	0x36, 0x90,             //	ld (hl), #0x90
	0x3A, 0x00, 0x00,       //	ld a, (#0x0000)		; Read manufacturer ID
	0x12,                   //	ld (de), a
	0x13,                   //	inc de
	0x3A, 0x01, 0x00,       //	ld a, (#0x0001)		; Read device ID
	0x12,                   //	ld (de), a
	0x36, 0xF0,             //	ld (hl), #0xf0		; Reset
	0xFB,                   //	ei
	0xC9                    //	ret
};

#define IDS_READ_LEN sizeof(IDS_READ)

// void sector_erase(uint8_t bank, uint16_t offset);
// Parameters:
// - a: bank
// - de: offset
static const uint8_t SECTOR_ERASE[] = {
				// _sector_erase::
	0xF3,			//	di
	0x21, 0xFF, 0xFF,       //	ld	hl, #0xffff	; c <- original_bank
	0x4E,                   //	ld	c, (hl)
	0x77,                   //	ld	(hl), a		; slot2_reg <- bank
	0x21, 0x55, 0x55,       //	ld	hl, #0x5555	; Erase unlock
	0x36, 0xAA,             //	ld	(hl), #0xaa
	0x21, 0xAA, 0x2A,       //	ld	hl, #0x2aaa
	0x36, 0x55,             //	ld	(hl), #0x55
	0x21, 0x55, 0x55,       //	ld	hl, #0x5555
	0x36, 0x80,             //	ld	(hl), #0x80
	0x36, 0xAA,             //	ld	(hl), #0xaa
	0x21, 0xAA, 0x2A,       //	ld	hl, #0x2aaa
	0x36, 0x55,             //	ld	(hl), #0x55
	0x6B,                   //	ld	l, e		; (bank:offset) <- 0x30
	0x62,                   //	ld	h, d
	0x36, 0x30,             //	ld	(hl), #0x30
				// data_poll:
	0x7E,                   //	ld	a, (hl)		; Poll for erase completion
	0xE6, 0x80,             //	and	a, #0x80
	0x28, 0xFB,             //	jr  Z, data_poll
	0x21, 0xFF, 0xFF,       //	ld	hl, #0xffff	; Restore original_bank
	0x71,                   //	ld	(hl), c
	0xFB,                   //	ei
	0xC9                    //	ret
};

#define SECTOR_ERASE_LEN sizeof(SECTOR_ERASE)

// program(uint8_t bank, uint16_t offset, uint8_t value_to_write);
// Parameters:
// - a: bank
// - de: offset
// - stack: value_to_write
static const uint8_t PROGRAM[] = {
				// _program::
	0xF3,			// 	di
	0x21, 0xFF, 0xFF,       // 	ld hl, #0xffff		; c <- original_bank
	0x4E,                   // 	ld c, (hl)
	0x77,                   // 	ld (hl), a		; slot2_bank <- bank
	0xFD, 0x21, 0x02, 0x00, // 	ld iy, #2		; a <- value_to_write
	0xFD, 0x39,             // 	add iy, sp
	0xFD, 0x7E, 0x00,       // 	ld a, 0(iy)
	0x21, 0x55, 0x55,       // 	ld hl, #0x5555		; Program unlock
	0x36, 0xAA,             // 	ld (hl), #0xaa
	0x21, 0xAA, 0x2A,       // 	ld hl, #0x2aaa
	0x36, 0x55,             // 	ld (hl), #0x55
	0x21, 0x55, 0x55,       // 	ld hl, #0x5555
	0x36, 0xA0,             // 	ld (hl), #0xa0
	0x6B,                   // 	ld l, e			; (bank:offset) <- value_to_write
	0x62,                   // 	ld h, d
	0x77,                   // 	ld (hl), a		; a <- value_to_write & 0x80
	0x47,                   // 	ld b, a
				// data_poll:
	0x7E,			// 	ld a, (hl)
	0xB8,                   // 	cp b
	0x20, 0xFC,             // 	jr NZ, data_poll
	0x21, 0xFF, 0xFF,       // 	ld hl, #0xffff		; Restore original_bank
	0x71,                   // 	ld (hl), c
	0xFB, 			// 	ei
	0xE1,			//	pop hl			; hl <- return address
	0x33,			//	inc sp			; Clean stack
	0xE9 			// 	jp (hl) 		; Return to caller
};

#define PROGRAM_LEN sizeof(PROGRAM)

void flash_ids_get(uint8_t *ids)
{
	uint8_t code[sizeof(IDS_READ)];
	memcpy(code, IDS_READ, sizeof(IDS_READ));

	((ids_read_fn)code)(ids);
}

void flash_sector_erase(uint32_t addr)
{
	uint8_t code[SECTOR_ERASE_LEN];
	const uint8_t bank = addr >> 14;
	// Force offset into slot 2 to use bank
	const uint16_t offset = 0x8000 | (addr & 0x3FFF);

	memcpy(code, SECTOR_ERASE, SECTOR_ERASE_LEN);
	((sect_erase_fn)code)(bank, offset);
}

void flash_program_byte(uint32_t addr, uint8_t value)
{
	uint8_t code[PROGRAM_LEN];
	const uint8_t bank = addr >> 14;
	// Force offset into slot 2 to use bank
	const uint16_t offset = 0x8000 | (addr & 0x3FFF);

	memcpy(code, PROGRAM, PROGRAM_LEN);
	((program_fn)code)(bank, offset, value);
}

void flash_program(uint32_t addr, const uint8_t *data, int16_t len)
{
	uint8_t code[PROGRAM_LEN];

	const uint8_t bank = addr >> 14;
	uint16_t offset = 0x8000 | (addr & 0x3FFF);
	memcpy(code, PROGRAM, PROGRAM_LEN);

	while (len--) {
		((program_fn)code)(bank, offset, *data);
		offset++;
		data++;
	}
}
