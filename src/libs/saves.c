#include <stdbool.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../data/constantdata.h"
#include "../assets2banks.h"
#include "flash.h"
#include <string.h>
#include <SMSlib.h>
#include "saves.h"


bool init_savesystem(void) {
    bool saveToInitialize = false;
    bool failureToSave = false;
    currentSaveSystem = NO_SAVE;
    //Check if flash
    u8 ids[2];
	flash_ids_get(ids);
    if(ids[0]==0xBF && ids[1]==0xB7) {
        //flash present
        currentSaveSystem = FLASH_SAVE;       
        SMS_mapROMBank(FLASH_SAVING_BANK);
        highscore_table_backup = (highscore_struct*)(0x8000 | (FLASH_SAVING_ADDRESS & 0x3FFF));
    } else {
        currentSaveSystem = SRAM_SAVE; //tentative
        highscore_table_backup = (highscore_struct*)(&SMS_SRAM);
        SMS_enableSRAM();
    }
    //Here, we have mapped either sram or an empty flash sector
    //-----------
    //We copy from the highscore_table_backup object in the saving bank to RAM
    memcpy(&highscore_table, highscore_table_backup, sizeof(highscore_table)); 
    if( highscore_table.marker1== 69 && highscore_table.marker2 == 148) {
        //If the two markers have the right value, no need to initialize.
        saveToInitialize = false;
    } else {
        //The two markers have the wrong value, we need to initialize.
        highscore_table.marker1 = 69;
        highscore_table.marker2 = 148;
        if(currentSaveSystem == SRAM_SAVE) {
            //if SRAM, we copy to the highscore_table_backup object in the SRAM BANK
            memcpy(highscore_table_backup, &highscore_table, sizeof(highscore_table));
        } else {
            // if flash,  we copy to the highscore_table_backup object in the FLASH SAVE BANK
            flash_sector_erase(FLASH_SAVING_ADDRESS);
            flash_program(FLASH_SAVING_ADDRESS, (char*)&highscore_table, sizeof(highscore_table));
        }
        //We copy back from the highscore_table_backup object to RAM
        memcpy(&highscore_table, highscore_table_backup, sizeof(highscore_table));
        //we check if the save was correct
        if(highscore_table.marker1 == 69 && highscore_table.marker2 == 148) {
            saveToInitialize = true;
            //JMS
            highscore_table.table[0].name[0] = 9;
            highscore_table.table[0].name[1] = 12;
            highscore_table.table[0].name[2] = 18;
            highscore_table.table[0].score = 1000;
            // AMC
            highscore_table.table[1].name[0] = 0;
            highscore_table.table[1].name[1] = 12;
            highscore_table.table[1].name[2] = 2;
            highscore_table.table[1].score = 900;
            // ORM
            highscore_table.table[2].name[0] = 14;
            highscore_table.table[2].name[1] = 17;
            highscore_table.table[2].name[2] = 12;
            highscore_table.table[2].score = 800;
            // JBC
            highscore_table.table[3].name[0] = 9;
            highscore_table.table[3].name[1] = 1;
            highscore_table.table[3].name[2] = 2;
            highscore_table.table[3].score = 700;
            // HCB
            highscore_table.table[4].name[0] = 17;
            highscore_table.table[4].name[1] = 2;
            highscore_table.table[4].name[2] = 1;
            highscore_table.table[4].score = 600;
            // AMS
            highscore_table.table[5].name[0] = 0;
            highscore_table.table[5].name[1] = 12;
            highscore_table.table[5].name[2] = 18;
            highscore_table.table[5].score = 500;
            // MSM
            highscore_table.table[6].name[0] = 12;
            highscore_table.table[6].name[1] = 18;
            highscore_table.table[6].name[2] = 12;
            highscore_table.table[6].score = 400;
            // DSM
            highscore_table.table[7].name[0] = 3;
            highscore_table.table[7].name[1] = 18;
            highscore_table.table[7].name[2] = 12;
            highscore_table.table[7].score = 300;
            // EIP
            highscore_table.table[8].name[0] = 4;
            highscore_table.table[8].name[1] = 8;
            highscore_table.table[8].name[2] = 15;
            highscore_table.table[8].score = 200;
            // JVS
            highscore_table.table[9].name[0] = 9;
            highscore_table.table[9].name[1] = 21;
            highscore_table.table[9].name[2] = 18;
            highscore_table.table[9].score = 100;
        } else { 
            //Save didn't work, we assume no saving
            failureToSave = true;
        }   
    }
    if(!failureToSave) {
        if(currentSaveSystem == SRAM_SAVE) {
            //copy from RAM to SRAM
            memcpy(highscore_table_backup, &highscore_table, sizeof(highscore_table));
        } else {
            //copy from RAM to FLASH sector
            flash_sector_erase(FLASH_SAVING_ADDRESS);
            flash_program(FLASH_SAVING_ADDRESS, (char*)&highscore_table, sizeof(highscore_table));
        }
        //copy back to RAM
        memcpy(&highscore_table, highscore_table_backup, sizeof(highscore_table));
    }
    if(currentSaveSystem == SRAM_SAVE) {     
        SMS_disableSRAM();
    } else {
        SMS_mapROMBank(CONSTANT_DATA_BANK);
    }
    if(failureToSave) {
        currentSaveSystem = NO_SAVE;
    }
    return saveToInitialize;
}


void saveChanges(void) {
   switch (currentSaveSystem)
   {
    case NO_SAVE:
        break;
    case SRAM_SAVE:
        SMS_enableSRAM();
        memcpy(highscore_table_backup, &highscore_table, sizeof(highscore_table));
        SMS_disableSRAM();
        break;
    case FLASH_SAVE:
        SMS_mapROMBank(FLASH_SAVING_BANK);
        flash_sector_erase(FLASH_SAVING_ADDRESS);
        flash_program(FLASH_SAVING_ADDRESS, (char*)&highscore_table, sizeof(highscore_table));
        SMS_mapROMBank(CONSTANT_DATA_BANK);
        break;   
    default:
        break;
   }
}