//
// Created by JordiM on 31/12/2019.
//

#ifndef GOTRIS_DEFAULT_STATE_H
#define GOTRIS_DEFAULT_STATE_H

 void default_state_start(void);

 void default_state_update(void);

 void default_state_stop(void);

#endif //GOTRIS_DEFAULT_STATE_H
