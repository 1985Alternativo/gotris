//
// Created by JordiM on 31/12/2019.
//

#ifndef GOTRIS_LOGO_STATE_H
#define GOTRIS_LOGO_STATE_H

 void logo_state_start(void);

 void logo_state_update(void);

 void logo_state_stop(void);

#endif //GOTRIS_LOGO_STATE_H
