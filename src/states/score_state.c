#include "score_state.h"
#include <stdbool.h>
#include <SMSlib.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/states.h"
#include "../engine/gamelogic.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include <PSGlib.h>


u8 score_state_status;

 void score_state_start(void) {
    frame_cnt = 0;
    score_state_status = 0;
    clear_scroll_title_screen();
    load_highscore_assets();
}

 void score_state_update(void) {
    if(score_state_status == 0) {
        start_fadein_music();
        highscore_fade_in();
        score_state_status = 1;
    } else if(score_state_status == 1) {
        draw_highscore_table();
        play_song(SCORE_OST);
        score_state_status = 2;
    } else if(score_state_status == 2) {
        update_input();
        if(getButtonsPressed() & KEY_ONE_P1 || getButtonsPressed() & KEY_ONE_P2 || frame_cnt > XXL_PERIOD) {
            score_state_status = 3;
        }
        waitForFrame();
    } else if(score_state_status == 3) {
        start_fadeout_music();
        general_fade_out();
        score_state_status = 4;    
    } else if(score_state_status == 4) {
        transition_to_state(TITLE_STATE);
    }
}

 void score_state_stop(void) {
    clear_input();
    stop_music();
}