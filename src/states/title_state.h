//
// Created by JordiM on 31/12/2019.
//

#ifndef GOTRIS_TITLE_STATE_H
#define GOTRIS_TITLE_STATE_H

 void title_state_start(void);

 void title_state_update(void);

 void title_state_stop(void);

#endif //GOTRIS_TITLE_STATE_H
