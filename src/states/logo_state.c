#include "logo_state.h"
#include <stdbool.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/states.h"
#include "../libs/montylib.h"
#include <SMSlib.h>
#include <PSGlib.h>

u8 logo_state_status = 0;

 void logo_state_start(void) {
    load_tileset(LOGO_ASSETS,0);
    load_tilemap(LOGO_ASSETS);
    frame_cnt = 0;
    logo_state_status = 0;
}

 void logo_state_update(void) {
    if(logo_state_status == 0) {
        turn_black();
        background_fade_in(LOGO_PALETTE,2);
        full_volume();
        play_sample(TUXEDO_SAMPLE);
        logo_state_status = 1;
    } else if(logo_state_status == 1) {
        while(frame_cnt < M_PERIOD) {
            waitForFrame();
        }
        logo_state_status = 2;
    } else if(logo_state_status == 2){
        background_fade_out();
        logo_state_status = 3;
    }  else if(logo_state_status == 3) {
        load_tileset(DEVKIT_ASSETS,0);
        load_tilemap(DEVKIT_ASSETS);
        logo_state_status = 4;
    } else if(logo_state_status == 4) {
        background_fade_in(DEVKIT_PALETTE,2);
        frame_cnt = 0;
        logo_state_status = 5;
    } else if(logo_state_status == 5) {
        play_song(LOGO_OST);
        while(frame_cnt < M_PERIOD) {
            waitForFrame();
        }
        logo_state_status = 6;
    }  else if(logo_state_status == 6){
        background_fade_out();
        logo_state_status = 7;
    } else if(logo_state_status == 7) {
        transition_to_state(INTRO_STATE);
    }
}

 void logo_state_stop(void) {
    stop_music();
    blackBackgroundPalette();
}