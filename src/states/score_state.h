//
// Created by JordiM on 02/01/2020.
//

#ifndef GOTRIS_SCORE_STATE_H
#define GOTRIS_SCORE_STATE_H

 void score_state_start(void);

 void score_state_update(void);

 void score_state_stop(void);

#endif //GOTRIS_SCORE_STATE_H
