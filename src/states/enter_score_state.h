//
// Created by JordiM on 08/01/2020.
//

#ifndef GOTRIS_ENTER_SCORE_STATE_H
#define GOTRIS_ENTER_SCORE_STATE_H

void enter_score_state_start(void);

void enter_score_state_update(void);

void enter_score_state_stop(void);

#endif //GOTRIS_ENTER_SCORE_STATE_H
