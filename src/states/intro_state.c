#include "intro_state.h"
#include <stdbool.h>
#include <SMSlib.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/states.h"
#include "../engine/gamelogic.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include <PSGlib.h>


u8 intro_state_status;
u8 currentLine = 0;

 void intro_state_start(void) {
    frame_cnt = 0;
    intro_state_status = 0;
    currentLine = 0;
    clear_tilemap();
    load_tileset(SIMPLE_FONT_ASSET,DIGITS_BASE_ADDRESS);
    load_tileset(INTRO1_ASSETS,BACKGROUND_BASE_ADRESS);
    load_tilemap(INTRO1_ASSETS);

}

 void intro_state_update(void) {
    if(intro_state_status == 0) {
        play_song(INTRO_OST);
        start_fadein_music();
        background_fade_in_to_sepia(INTRO1_PALETTE,2);
        sprite_fade_in(FONT_PALETTE,1);
        intro_state_status = 1;
    } else if(intro_state_status == 1) {
        create_dialog_box();
        intro_state_status = 2;
    } else if(intro_state_status == 2) {
        update_input();
        if(getButtonsPressed() & KEY_ONE_P1 || getButtonsPressed() & KEY_ONE_P2 || (currentLine>>1) > NUMBER_INTRO_SENTENCES+5) {
            intro_state_status = 3;
            frame_cnt = 0;
        }
        if((frame_cnt & 63) == 0) {
            update_dialog_box(currentLine>>1, currentLine&1,0);
            currentLine++;
            switch (currentLine)
            {
                case 4:
                    background_fade_in(INTRO1_PALETTE,3);
                    break;
                case 12:
                    background_fade_out();
                    load_tileset_batches(INTRO2_ASSETS, BACKGROUND_BASE_ADRESS);
                    load_tilemap(INTRO2_ASSETS);
                    break;
                case 13:
                    background_fade_in(INTRO2_PALETTE,2);
                    break;
                case 20:
                    background_fade_out();
                    load_tileset_batches(INTRO3_ASSETS, BACKGROUND_BASE_ADRESS);
                    load_tilemap(INTRO3_ASSETS);
                    break;
                case 21:
                    background_fade_in(INTRO3_PALETTE,2);;
                    break;
                case 26:
                    background_fade_in_to_sepia(INTRO3_PALETTE,3);
                    break;
                default:
                    break;
            }
        }
        waitForFrame();
    } else if(intro_state_status == 3) {
        start_fadeout_music();
        general_fade_out();
        intro_state_status = 4;    
    } else if(intro_state_status == 4) {
       transition_to_state(TITLE_STATE);
    } 
}

 void intro_state_stop(void) {
    clear_input();
    stop_music();
}