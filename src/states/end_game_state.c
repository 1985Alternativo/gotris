#include "end_game_state.h"
#include <stdbool.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/states.h"
#include "../engine/gamelogic.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include <PSGlib.h>

u8 end_state_status = 0;
u8 end_currentLine = 0;

void end_game_state_start(void) {
    frame_cnt = 0;
    end_state_status = 0;
    end_currentLine = 0;
    clear_tilemap();
    load_ending1_assets();
    load_tileset(SIMPLE_FONT_ASSET,DIGITS_BASE_ADDRESS);
    load_tileset(ENDING1_ASSETS,BACKGROUND_BASE_ADRESS);
    load_tilemap(ENDING1_ASSETS);
}

void end_game_state_update(void) {
    if(end_state_status == 0) {
        play_song(INTRO_OST);
        ending_sprites();
        start_fadein_music();
        background_fade_in(ENDING1_PALETTE,2);
        sprite_fade_in(PIECES_PALETTE,1);
        create_dialog_box();
        end_state_status = 1;
    } else if(end_state_status == 1){
        if((end_currentLine>>1) > NUMBER_ENDGAME_SENTENCES) {
            end_state_status = 2;
            scroll_x[0] = 255;
            currentScrollColumn = scroll_x[0] >> 3;
        }
        if((frame_cnt & 63) == 0) {
            update_dialog_box(end_currentLine>>1, end_currentLine&1,1);
            end_currentLine++;
            switch (end_currentLine)
            {
                case 7:
                    //start_fadeout_music();
                    break;
                case 8:
                    background_fade_out();
                    load_ending2_assets();
                    break;
                case 9:
                    start_fadein_music();
                    background_fade_in(ENDING2_PALETTE,2);
                    break;
                default:
                    break;
            }
        }
        waitForFrame();
    }  else if(end_state_status == 2) {
        frame_cnt = 0;
        while(frame_cnt<L_PERIOD) {
            waitForFrame();
        }
        frame_cnt = 0;
        sprite_fade_out();
        while(frame_cnt<XL_PERIOD) {
            waitForFrame();
            scroll_ending();
        }
        end_state_status = 3;
    }  else if(end_state_status == 3) {
        ending_update_face();
        frame_cnt = 0;
        while(frame_cnt<XXL_PERIOD) {
            waitForFrame();
        }
        start_fadeout_music();
        background_fade_out();
        end_state_status = 4;
        frame_cnt = 0;
        endingScrollFinish();
        while(frame_cnt<M_PERIOD) {
            waitForFrame();
        }
    } else if(end_state_status == 4) {
        clear_cursor_sprites();
        transition_to_state(CREDITS_STATE);
    } 
}

void end_game_state_stop(void) {
    stop_music();
    stop_fx();
}