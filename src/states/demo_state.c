#include "demo_state.h"
#include <stdbool.h>
#include <SMSlib.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/board.h"
#include "../engine/gamelogic.h"
#include "../engine/states.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include <PSGlib.h>

u8 demo_state_status = 0;
u16 demo_duration;
u8 startLevel = 0;

 void demo_state_start(void) {
    number_human_players = 0;
    load_game_assets();
    draw_game_background();
    startLevel = get_rand()&7;
    init_gamestatus(startLevel); 
    init_board();
    frame_cnt = 0;
    demo_state_status = 0;
    demo_duration = 0;
}

 void demo_state_update(void) {
     if(demo_state_status == 0) {
        start_fadein_music();
        game_fade_in(startLevel);
        play_song(INGAME_OST);
        demo_state_status = 1;
    } else if(demo_state_status == 1) {
        frame_cnt = 0;
        while(frame_cnt < XS_PERIOD) {
            waitForFrame();
        }
        demo_state_status = 2;
    } else if(demo_state_status == 2) {
        draw_game_screen();
        print_level_number(2,23,level+1);
        print_string(2,19,"LVL]GOAL^");
        print_score_value(2,21,(level+1)<<LEVEL_DELIMITER);
        print_score_value(24,19,score_p1);
        print_score_value(24,23,score_p2);
        initialize_game_lists();
        demo_state_status = 3;
    }  else if (demo_state_status == 3) {
        if (game_status!=GAME_STATUS_RESET && demo_duration < XXL_PERIOD) { 
            execute_game_logic();
            get_game_actions();
            update_input();
            if(getButtonsPressed() & KEY_ONE_P1 || getButtonsPressed() & KEY_ONE_P2) {
                end_demo();   
                general_fade_out();
                blackBackgroundPalette();
                blackSpritePalette();
                frame_cnt = 0;
                while(frame_cnt < M_PERIOD) {
                    waitForFrame();
                }
                clear_game_lists();
                clear_input();
                demo_state_status = 5;
            }
            waitForFrame();
            demo_duration++;
        } else {
            end_demo();   
            general_fade_out();
            blackBackgroundPalette();
            blackSpritePalette();
            frame_cnt = 0;
            while(frame_cnt < M_PERIOD) {
                waitForFrame();
            }
            clear_game_lists();
            clear_input();
            demo_state_status = 4;
        }
    } else if(demo_state_status == 4) {
        transition_to_state(SCORE_STATE);
    } else if(demo_state_status == 5) {
        transition_to_state(TITLE_STATE);
    }
}

 void demo_state_stop(void) {
    clear_input();
}