//
// Created by JordiM on 29/12/2020.
//

#ifndef GOTRIS_INTRO_STATE_H
#define GOTRIS_INTRO_STATE_H

 void intro_state_start(void);

 void intro_state_update(void);

 void intro_state_stop(void);

#endif //GOTRIS_INTRO_STATE_H
