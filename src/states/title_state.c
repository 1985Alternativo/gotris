#include "title_state.h"
#include <stdbool.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/states.h"
#include "../engine/gamelogic.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include <SMSlib.h>
#include <PSGlib.h>

u8 title_state_status = 0;

 void title_state_start(void) {
    load_tileset(TITLE_ASSETS, BACKGROUND_BASE_ADRESS);
    load_tilemap(TITLE_ASSETS);
    change_title_priority();
    init_scroll_title_screen();
    frame_cnt = 0;
    title_state_status = 0;
    clear_input();
}

 void title_state_update(void) {
   if(title_state_status == 0) {
       background_fade_in(TITLE_FIRST_PALETTE,2);
       title_state_status = 1;
   } else if(title_state_status == 1) {
        background_fade_in(TITLE_PALETTE,3);
        sprite_fade_in(FONT_PALETTE,2);
        background_fade_in(WHITE_PALETTE,0);
        frame_cnt = 0;
        title_state_status = 2;
    } else if(title_state_status == 2) {
        if(frame_cnt > XS_PERIOD) {
            background_fade_in(TITLE_PALETTE,3);
            play_sample(THUNDER_SAMPLE);
            play_sample(GOTRIS_SAMPLE);
            enable_scroll_title_screen();    
            start_fadein_music();
            play_song(TITLE_OST);
            frame_cnt = 0;
            title_state_status = 3;
        }
        waitForFrame();
    } else if(title_state_status == 3) {
        if (!(getButtonsPressed() & KEY_ONE_P1) && !(getButtonsPressed() & KEY_ONE_P2)) {
            if(frame_cnt > XXL_PERIOD) {
                clear_push_start();
                start_fadeout_music();
                general_fade_out();
                title_state_status = 4;
            }
            update_input();
            waitForFrame();
            scroll_title_screen();
            if(frame_cnt > M_PERIOD) {
                write_push_start(frame_cnt);
            }
        } else {
            play_fx(START_FX);
            if(getButtonsPressed() & KEY_ONE_P2) {
                number_human_players = 2;
            } else {
                number_human_players = 1;
            }
            clear_input();
            clear_push_start();
            rand_index = frame_cnt;
            //rand_index = 0; //Uncoment that for deterministic gameplay
            title_state_status = 5;
            start_fadeout_music();
            general_fade_out();
        }
   } else if(title_state_status == 4) {
       transition_to_state(DEMO_STATE);
   } else if(title_state_status == 5) {
       transition_to_state(GAME_STATE);
   }
}

 void title_state_stop(void) {
    clear_scroll_title_screen();
    stop_music();
    stop_fx();
}