//
// Created by JordiM on 16/04/2020.
//

#ifndef GOTRIS_CREDITS_STATE_H
#define GOTRIS_CREDITS_STATE_H

void credits_state_start(void);

void credits_state_update(void);

void credits_state_stop(void);

#endif //GOTRIS_CREDITS_STATE_H
