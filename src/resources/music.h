#ifndef GOTRIS_MUSIC_H_H
#define GOTRIS_MUSIC_H_H

void start_fadein_music(void);
void start_fadeout_music(void);
void full_volume(void);
void play_song(u8 songIndex);
void play_fx(u8 fxIndex);
void play_sample(u8 sampleIndex);
void enable_music_handler(void);
void disable_music_handler(void);
#define resume_music() PSGResume();
#define stop_music() PSGStop()
#define stop_fx() PSGSFXStop()
#endif //GOTRIS_MUSIC_H