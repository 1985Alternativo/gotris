//
// Created by JordiM on 13/04/2017.
//

#ifndef GOTRIS_RESOURCES_H
#define GOTRIS_RESOURCES_H

#define DIGITS_BASE_ADDRESS 24
#define CHARS_BASE_ADDRESS DIGITS_BASE_ADDRESS + 10
#define BACKGROUND_BASE_ADRESS CHARS_BASE_ADDRESS + 29

#define DIALOG_BOX_FIRST_ROW 15
#define DIALOG_BOX_LAST_ROW 22
#define DIALOG_BOX_FIRST_COLUMN 2
#define DIALOG_BOX_LAST_COLUMN 29

#define DIALOG_BOX_NUMBER_LINES 4

#define TILES_PER_FRAME 10
#define BYTES_PER_FRAME 320

#define blackBackgroundPalette() SMS_zeroBGPalette()
#define blackSpritePalette() SMS_zeroSpritePalette()

void load_tileset(u8 asset_num, u8 base_address);
void load_tileset_batches(u8 asset_num, u8 base_address);
void load_tilemap(u8 asset_num);

void load_title_assets(void);
void load_highscore_assets(void);
void load_game_assets(void);


void turn_black(void);

void background_fade_out(void);
void sprite_fade_out(void);
void general_fade_out(void);

void background_fade_in(u8 palette_num, u8 framesperstep);
void background_fade_in_to_sepia(u8 palette_num, u8 framesperstep);
void sprite_fade_in(u8 palette_num, u8 framesperstep);

void draw_sram_present(void);

void change_title_priority(void);

void init_scroll_title_screen(void);
void enable_scroll_title_screen(void);
void scroll_title_screen(void);
void titleScrollHandler(void);
void write_push_start(u16 counter);
void clear_push_start(void);
void clear_scroll_title_screen(void);

void draw_highscore_row(u8 row);
void draw_highscore_table(void);

void highscore_fade_in(void);

void init_credits(void);
void write_credits(u16 counter);
void rasterize_credits(void);
void creditsScrollHandler1(void);
void creditsScrollHandler2(void);
void disable_credits_effects(void);

void load_ending1_assets(void);
void load_ending2_assets(void);

void ending_sprites(void);
void scroll_ending(void);
void endingScrollFinish(void);
void ending_update_face(void);

void game_fade_in(u8 startLevel);

void initialize_game_lists(void);
void clear_game_lists(void);

void game_half_palette(void);
void game_full_palette(void);
void draw_game_background(void);
void draw_game_screen(void);
void draw_congratulations(void);

void draw_point_sprites(void);
void clear_point_sprites(void); 

void draw_cursor_sprites(u8 x, u8 y);
void clear_cursor_sprites(void);

#define draw_board_position(x, y, tile) SMS_setTileatXY(x+11,y+1,tile|TILE_USE_SPRITE_PALETTE)
#define draw_next_square(square) SMS_setTileatXY(28,13,square.color00|TILE_USE_SPRITE_PALETTE);SMS_setTileatXY(29,13,square.color01|TILE_USE_SPRITE_PALETTE);SMS_setTileatXY(28,14,square.color10|TILE_USE_SPRITE_PALETTE);SMS_setTileatXY(29,14,square.color11|TILE_USE_SPRITE_PALETTE); 
//void draw_board_position(u8 x, u8 y, u8 tile);

void connected_pieces_to_bw_palette(void);
void connected_pieces_to_normal_palette(void);
void not_connected_pieces_to_red_palette(void);
void not_connected_pieces_to_normal_palette(void);
void swap_fire_colors(void);
void restore_fire_colors(void);
void background_to_level_palette(u8 level);



void create_dialog_box(void);
void update_dialog_box(u8 currentLastLine, u8 upDown, u8 textNumber);
void load_tiles_in_batches(const u8 *data, u16 position, u16 size);
//void print_single_digit(u8 x, u8 y, u8 number);
//void print_unsigned_char(u8 x, u8 y, u8 number);
//void print_signed_char(u8 x, u8 y, s8 number);
//void print_unsigned_int(u8 x, u8 y, u16 number);
void print_score_value(u8 x, u8 y, u16 number);
void print_level_number(u8 x, u8 y, u8 level);
void print_string(u8 x, u8 y, u8* string);
#endif //GOTRIS_RESOURCES_H
