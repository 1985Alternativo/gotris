//
// Created by JordiM on 13/04/2017.
//
#include <stdbool.h>
#include <string.h>
#include <SMSlib.h>
#include "../engine/globaldefinitions.h"
#include "../resources/resources.h"
#include "../engine/palettes.h"
#include "../ram/globalvariables.h"
#include "../data/constantdata.h"
#include "../libs/montylib.h"
#include "../assets2banks.h"


const char* tiles[] = {
    logotiles_psgcompr,
    devkitsmstiles_psgcompr,
    simplefont_psgcompr,
    intro1tiles_psgcompr,
    intro2tiles_bin,
    intro3tiles_bin,
    titlescreentiles_psgcompr,
    creditsscreentiles_psgcompr,
    ending1tiles_psgcompr,
};

const u16 assets_size[] = {
    logotiles_psgcompr_size,
    devkitsmstiles_psgcompr_size,
    simplefont_psgcompr_size,
    intro1tiles_psgcompr_size,
    intro2tiles_bin_size,
    intro3tiles_bin_size,
    titlescreentiles_psgcompr_size,
    creditsscreentiles_psgcompr_size,
    ending1tiles_psgcompr_size
};


const char* tilemaps[] = {
    logotilemap_stmcompr,
    devkitsmstilemap_stmcompr,
    logotilemap_stmcompr,
    intro1tilemap_stmcompr,
    intro2tilemap_stmcompr,
    intro3tilemap_stmcompr,
    titlescreentilemap_stmcompr,
    creditsscreentilemap_stmcompr,
    ending1tilemap_stmcompr
};

const u8 assets_bank[] = {
    logotiles_psgcompr_bank,
    devkitsmstiles_psgcompr_bank,
    simplefont_psgcompr_bank,
    intro1tiles_psgcompr_bank,
    intro2tiles_bin_bank,
    intro3tiles_bin_bank,
    titlescreentiles_psgcompr_bank,
    creditsscreentiles_psgcompr_bank,
    ending1tiles_psgcompr_bank
};

const u8* palettes[] = {
    default_pieces_palette,
    logopalette_bin,
    devkitsmspalette_bin,
    intro1palette_bin,
    intro2palette_bin,
    intro3palette_bin,
    title_first_palette,
    titlescreenpalette_bin,
    white_palette,
    creditsscreenpalette_bin,
    default_pieces_palette,
    ending1palette_bin,
    ending2palette_bin
};

const u8 palettes_size[] = {
    16,
    logopalette_bin_size,
    devkitsmspalette_bin_size,
    intro1palette_bin_size,
    intro2palette_bin_size,
    intro3palette_bin_size,
    titlescreenpalette_bin_size,
    titlescreenpalette_bin_size,
    16,
    creditsscreenpalette_bin_size,
    16,
    ending1palette_bin_size,
    ending2palette_bin_size
};

const u8 palettes_bank[] = {
    CONSTANT_DATA_BANK,
    logopalette_bin_bank,
    devkitsmspalette_bin_bank,
    intro1palette_bin_bank,
    intro2palette_bin_bank,
    intro3palette_bin_bank,
    CONSTANT_DATA_BANK,
    titlescreenpalette_bin_bank,
    CONSTANT_DATA_BANK,
    creditsscreenpalette_bin_bank,
    CONSTANT_DATA_BANK,
    ending1palette_bin_bank,
    ending2palette_bin_bank
};

void turn_black(void) {
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    setBGPalette(black_palette, 16);
    setSpritePalette(black_palette, 16);
}


void background_fade_out(void) {
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    fadeToTargetBGPalette(black_palette, 16,2);
}

void sprite_fade_out(void) {
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    fadeToTargetSpritePalette(black_palette, 16,2);
}

void general_fade_out(void) {
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    fadeToTargetBGPalette(black_palette, 16,2);
    fadeToTargetSpritePalette(black_palette, 16,2);
}

void background_fade_in_to_sepia(u8 palette_num, u8 framesperstep) {
    u8 bw_palette[16];
    current_resource_bank = palettes_bank[palette_num];
    SMS_mapROMBank(palettes_bank[palette_num]);
    createSepiaPaletteVersion(palettes[palette_num], bw_palette, palettes_size[palette_num]);
    fadeToTargetBGPalette(bw_palette,  palettes_size[palette_num], framesperstep);
}

void background_fade_in(u8 palette_num, u8 framesperstep) {
    current_resource_bank = palettes_bank[palette_num];
    SMS_mapROMBank(palettes_bank[palette_num]);
    fadeToTargetBGPalette(palettes[palette_num], palettes_size[palette_num],framesperstep);
}

void sprite_fade_in(u8 palette_num, u8 framesperstep) {
    current_resource_bank = palettes_bank[palette_num];
    SMS_mapROMBank(palettes_bank[palette_num]);
    fadeToTargetSpritePalette(palettes[palette_num], palettes_size[palette_num],framesperstep);
}

void load_tileset(u8 asset_num, u8 base_address) {
    SMS_mapROMBank(assets_bank[asset_num]);
    current_resource_bank = assets_bank[asset_num];
    SMS_loadPSGaidencompressedTiles(tiles[asset_num], base_address);
}

void load_tileset_batches(u8 asset_num, u8 base_address) {
    SMS_mapROMBank(assets_bank[asset_num]);
    current_resource_bank = assets_bank[asset_num];
    load_tiles_in_batches(tiles[asset_num],base_address,assets_size[asset_num]);
}

void load_tilemap(u8 asset_num) {
    SMS_mapROMBank(assets_bank[asset_num]);
    current_resource_bank = assets_bank[asset_num];
    SMS_loadSTMcompressedTileMap(0, 0, tilemaps[asset_num]);
}

void draw_sram_present(void) {
    SMS_mapROMBank(simplefont_psgcompr_bank);
    current_resource_bank = simplefont_psgcompr_bank;
    SMS_loadPSGaidencompressedTiles(simplefont_psgcompr,DIGITS_BASE_ADDRESS);
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    setSpritePalette(default_pieces_palette,16);
    if(currentSaveSystem == SRAM_SAVE) {
        print_string(10,10,"SRAM]PRESENT^");
    } else if (currentSaveSystem == FLASH_SAVE) {
        print_string(10,10,"FLASH]PRESENT^");
    }
}

void change_title_priority(void) {
    u8 i,j;
    for(j=0;j<4;j++) {
        for(i=0;i<32;i++) {
            setTileMapToHighPriority(i,j);
        }
    }
}

void game_fade_in(u8 startLevel) {
    SMS_mapROMBank(backgroundpalette_bin_bank);
    current_resource_bank = backgroundpalette_bin_bank;
    background_to_level_palette(startLevel);
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    current_resource_bank = CONSTANT_DATA_BANK;
    fadeToTargetSpritePalette(default_pieces_palette, 16,2);
}

void init_scroll_title_screen(void) {
    scroll_x[0] = 0xFFFF;
    scroll_x[1] = 0xFFFF;
    scroll_x[2] = 0xFFFF;
    scroll_x[3] = 0xFFFF;
    scroll_x[4] = 0xFFFF;
    scroll_x[5] = 0xFFFF;
    lineCnt = 0;
    SMS_setBGScrollX(0);
    SMS_setBGScrollY(0);    
}

void enable_scroll_title_screen(void) {
    //SMS_VDPturnOnFeature(VDPFEATURE_LEFTCOLBLANK);
    SMS_setLineInterruptHandler(&titleScrollHandler);
    SMS_setLineCounter(31);    /* we're updating every 32 scanlines... */
    SMS_enableLineInterrupt();
}

void scroll_title_screen(void) {
    scroll_x[0] = scroll_x[0] - 128; //0.5<<8
    scroll_x[1] = scroll_x[1] - 160; //0.5<<8 + (0.125<<8)
    scroll_x[2] = scroll_x[2] - 160;
    scroll_x[3] = scroll_x[3] - 160;
    scroll_x[4] = scroll_x[4] - 160;
    //scroll_x[5] = scroll_x[5] - 1;

    lineCnt = 0;
}

void titleScrollHandler(void) {
  SMS_setBGScrollX((scroll_x[lineCnt++])>>8);
}

void write_push_start(u16 counter) {
    SMS_initSprites();
    if(counter & 0x8) {
        SMS_addSprite(80,150, CHARS_BASE_ADDRESS+'P'-'A');
        SMS_addSprite(90,150, CHARS_BASE_ADDRESS+'U'-'A');
        SMS_addSprite(100,150, CHARS_BASE_ADDRESS+'S'-'A');
        SMS_addSprite(110,150, CHARS_BASE_ADDRESS+'H'-'A');

        SMS_addSprite(130,150, DIGITS_BASE_ADDRESS+1);
        SMS_addSprite(150,150, CHARS_BASE_ADDRESS+'O'-'A');
        SMS_addSprite(160,150, CHARS_BASE_ADDRESS+'R'-'A');
        SMS_addSprite(180,150, DIGITS_BASE_ADDRESS+2);
    }
    
}

void clear_push_start(void) {
    SMS_initSprites();
}

void clear_scroll_title_screen(void) {
    SMS_disableLineInterrupt();
    scroll_x[0] = 0xFFFF;
    scroll_x[1] = 0xFFFF;
    scroll_x[2] = 0xFFFF;
    scroll_x[3] = 0xFFFF;
    scroll_x[4] = 0xFFFF;
    scroll_x[5] = 0xFFFF;
    SMS_setBGScrollX(scroll_x[0]>>8);
    //SMS_VDPturnOffFeature(VDPFEATURE_LEFTCOLBLANK);
}

void load_highscore_assets(void) {
    SMS_mapROMBank(simplefont_psgcompr_bank);
    SMS_loadPSGaidencompressedTiles(simplefont_psgcompr,DIGITS_BASE_ADDRESS);
    SMS_mapROMBank(scorescreentiles_psgcompr_bank);
    current_resource_bank = scorescreentiles_psgcompr_bank;
    SMS_loadPSGaidencompressedTiles(scorescreentiles_psgcompr, BACKGROUND_BASE_ADRESS);
    SMS_loadSTMcompressedTileMap(0, 0, scorescreentilemap_stmcompr);
    SMS_setBGScrollX(0);
}

void highscore_fade_in(void) {
    SMS_mapROMBank(scorescreenpalette_bin_bank);
    current_resource_bank = scorescreenpalette_bin_bank;
    fadeToTargetBGPalette(scorescreenpalette_bin, scorescreenpalette_bin_size,2);
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    current_resource_bank = CONSTANT_DATA_BANK;
    fadeToTargetSpritePalette(default_pieces_palette, 16,2);
}

void draw_highscore_row(u8 row) {
    SMS_setTileatXY(3,12+row,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+highscore_table.table[row].name[0]);
    SMS_setTileatXY(4,12+row,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+highscore_table.table[row].name[1]);
    SMS_setTileatXY(5,12+row,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+highscore_table.table[row].name[2]);
    print_score_value(7,12+row,highscore_table.table[row].score);
}

void draw_highscore_table(void) {
    u8 i;
    for(i=0;i<10;i++) {
        draw_highscore_row(i);
    }
}

void init_credits(void) {
    cursorx = 1;
    cursory = 2;
    waveIndex = 0;
    creditsEnd = false;
    SMS_setBGScrollX(0);
    //SMS_VDPturnOnFeature(VDPFEATURE_LEFTCOLBLANK);
    SMS_setLineInterruptHandler(&creditsScrollHandler1);
    SMS_setLineCounter(1);    /* we're updating every scanline... */
    SMS_enableLineInterrupt();
    lineCnt = 0;
}

void write_credits(u16 counter) {
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    u8 currentCharacter = credits[counter>>4] -'A';
    if((counter & 15) == 0) {
        if(creditsEnd)
            return;
        if(currentCharacter == 26) {
            cursory++;
            cursorx = cursory+1;
        } else if(currentCharacter == 28) {
            creditsEnd = true;
        } else if(currentCharacter == 29) {
            cursorx++;
        } else {
            SMS_setTileatXY(cursorx, cursory, TILE_USE_SPRITE_PALETTE|(CHARS_BASE_ADDRESS + currentCharacter));
            SMS_setTileatXY(cursorx++, 23 - cursory, TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|(CHARS_BASE_ADDRESS + currentCharacter));
        }
    }
}

void rasterize_credits(void) {
    SMS_disableLineInterrupt();
    lineCnt = 0;
    waveIndex = (waveIndex + 1) & 0x1F;
    //SMS_setBGPaletteColor(4,0x31);
    SMS_mapROMBank(creditsscreenpalette_bin_bank);
    SMS_loadBGPalette(creditsscreenpalette_bin);
    SMS_setBGScrollX(0);
    SMS_setLineInterruptHandler(&creditsScrollHandler1);
    SMS_enableLineInterrupt();
}

void creditsScrollHandler1(void) {
    if(lineCnt == 48) {        
        lineCnt = 0;
        SMS_disableLineInterrupt();
        SMS_mapROMBank(CONSTANT_DATA_BANK);
        SMS_loadBGPalette(credits_secondary_palette);
        SMS_setLineInterruptHandler(&creditsScrollHandler2);
        SMS_enableLineInterrupt();
    }
    lineCnt++;
}

void creditsScrollHandler2(void) { //should it be covered with previous change of bank
    SMS_setBGScrollX(waveLut[(lineCnt + waveIndex)&0x1F]);
    lineCnt++;
}

void disable_credits_effects(void) {
     SMS_disableLineInterrupt();
     //SMS_VDPturnOffFeature(VDPFEATURE_LEFTCOLBLANK);
}

void load_ending1_assets(void) {
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_loadTiles(black_tile,0,32);
    SMS_loadTiles(black_tile_sprite,2,32);
}

void load_ending2_assets(void) {
     SMS_mapROMBank(ending2tiles_bin_bank);
    current_resource_bank = ending2tiles_bin_bank;
    load_tiles_in_batches(ending2tiles_bin,BACKGROUND_BASE_ADRESS,ending2tiles_bin_size);
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_loadTiles(black_tile_sprite,2,32);
    SMS_loadTileMap(0, 0, ending2tilemap,896);
}

void ending_sprites(void) {
    u8 i;
    //SMS_VDPturnOnFeature(VDPFEATURE_LEFTCOLBLANK);
    SMS_initSprites();
    for(i=1;i<112;i=i+8) {
        SMS_addSprite(1, i, 2);
        SMS_addSprite(248, i, 2);
    }
}

void scroll_ending(void) {
    u8 column2Update = 0;
    if(scroll_x[0]>144) scroll_x[0]--;
    SMS_setBGScrollX(scroll_x[0]);
    if(scroll_x[0]&7 == 7) {
        currentScrollColumn = scroll_x[0]>>3;
        column2Update = 31-currentScrollColumn;
        SMS_loadTileMapArea(column2Update,0,ending2tilemapcolumns+column2Update*14,1,14); 
    //31 -> 0
    //30 -> 1
    //29 -> 2
    }
}

void endingScrollFinish(void) {
    SMS_setBGScrollX(0);
}

void ending_update_face(void) {
    SMS_loadTiles(ending_face_row1,78,96);
    SMS_loadTiles(ending_face_row2,102,96);
    SMS_loadTiles(ending_face_row3,123,96);
    SMS_loadTiles(ending_face_row4,146,96);
}

void load_game_assets(void) {
    SMS_mapROMBank(backgroundpalette_bin_bank);
    current_resource_bank = backgroundpalette_bin_bank;
    SMS_loadPSGaidencompressedTiles(piecestiles_psgcompr, 0);
    SMS_loadPSGaidencompressedTiles(simplefont_psgcompr,DIGITS_BASE_ADDRESS);
    SMS_loadPSGaidencompressedTiles(backgroundtiles_psgcompr, BACKGROUND_BASE_ADRESS);
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    current_resource_bank = CONSTANT_DATA_BANK;
    setSpritePalette(default_pieces_palette, 16);
    
}

void game_half_palette(void) {
    SMS_loadBGPaletteHalfBrightness(getBGPalette());
    SMS_loadSpritePaletteHalfBrightness(getSpritePalette());
}

void game_full_palette(void) {
    SMS_loadBGPalette(getBGPalette());
    SMS_loadSpritePalette(getSpritePalette());
}

void draw_game_background(void) {
    //SMS_VDPturnOffFeature(VDPFEATURE_LEFTCOLBLANK);
    SMS_mapROMBank(backgroundtilemap_stmcompr_bank);
    current_resource_bank = backgroundtilemap_stmcompr_bank;
    SMS_loadSTMcompressedTileMap(0, 0, backgroundtilemap_stmcompr); 
}

void initialize_game_lists(void) {
    active_points_list = createList(sizeof(point_sprite));
}

void clear_game_lists(void) {
    destroyList(active_points_list);
}

void draw_game_screen(void) {
    u8 i,j;

    SMS_setTileatXY(10,0,TILE_USE_SPRITE_PALETTE|7);
    for(i=11;i < 21;i++) {
        SMS_setTileatXY(i,0,TILE_USE_SPRITE_PALETTE|8);
    }
    SMS_setTileatXY(10,0,TILE_USE_SPRITE_PALETTE|7);
    SMS_setTileatXY(21,0,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|7);
    for(j=1;j<23;j++) {
        SMS_setTileatXY(10,j,TILE_USE_SPRITE_PALETTE|9);
        for(i=11;i < 21;i++) {
            SMS_setTileatXY(i,j,TILE_USE_SPRITE_PALETTE|0);
        }
        SMS_setTileatXY(21,j,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|9);
    }
    SMS_setTileatXY(10,23,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|7);
    SMS_setTileatXY(21,23,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|TILE_FLIPPED_Y|7);
    for(i=11;i < 21;i++) {
        SMS_setTileatXY(i,23,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|8);
    }
    //score
    
    if(number_human_players == 2) {
        print_string(23,17,"PLAYER]A^");
        print_string(23,21,"PLAYER]B^");
    } else if(number_human_players == 1) {
        print_string(23,17,"PLAYER^");
        print_string(23,21,"CPU^");
    } else if(number_human_players == 0) {
        print_string(23,17,"CPU]A^");
        print_string(23,21,"CPU]B^");
    }
    
    //next square
    SMS_setTileatXY(27,12,TILE_USE_SPRITE_PALETTE|7);
    SMS_setTileatXY(28,12,TILE_USE_SPRITE_PALETTE|8);
    SMS_setTileatXY(29,12,TILE_USE_SPRITE_PALETTE|8);
    SMS_setTileatXY(30,12,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|7);
    SMS_setTileatXY(27,13,TILE_USE_SPRITE_PALETTE|9);
    SMS_setTileatXY(30,13,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|9);
    SMS_setTileatXY(27,14,TILE_USE_SPRITE_PALETTE|9);
    SMS_setTileatXY(30,14,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|9);
    SMS_setTileatXY(27,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|7);
    SMS_setTileatXY(28,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|8);
    SMS_setTileatXY(29,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|8);
    SMS_setTileatXY(30,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|TILE_FLIPPED_X|7);
    print_string(23,15,"NEXT^");
}

void draw_congratulations(void) {
    print_string(8,10,"CONGRATULATIONS[^");
}

void draw_point_sprites(void) {
    SMS_initSprites();

    list_node_t* current_element = active_points_list->head;
    point_sprite* current_point_sprite;
    u8 dx = 8;
    if(bonusSamplePlay) {
        //SMS_setSpriteMode(SPRITEMODE_ZOOMED); //TODO Restore
        //dx = 16;
    }
    while(current_element != NULL) {
        current_point_sprite = (point_sprite*)(current_element->data);
         if(current_point_sprite->firstsprite != 0) {
            SMS_addSprite(current_point_sprite->x,current_point_sprite->y, current_point_sprite->firstsprite);
            if(current_point_sprite->firstsprite == POINT_100_SPRITE_INDEX || current_point_sprite->firstsprite == POINT_500_SPRITE_INDEX) {
                SMS_addSprite(current_point_sprite->x+dx,current_point_sprite->y, POINT_00_SPRITE_INDEX);
            } else if(current_point_sprite->firstsprite == BONUS_SPRITE_INDEX) {
                SMS_addSprite(current_point_sprite->x+dx,current_point_sprite->y, BONUS_SPRITE_INDEX+1);
            }
        }
        current_element = current_element->next;
    }
}

void clear_point_sprites(void) {   
    while(active_points_list->numElements) {
        deleteElementEndList(active_points_list);
    }
    SMS_setSpriteMode(SPRITEMODE_NORMAL);
    SMS_initSprites();
}

void draw_cursor_sprites(u8 x, u8 y) {
    u8 sprite1,sprite2;
    if(current_player == PLAYER_1) {
        sprite1 = PLAYER_1_SPRITE_INDEX;
        sprite2 = PLAYER_P_SPRITE_INDEX;
    } else if(current_player == PLAYER_2) {
        sprite1 = PLAYER_2_SPRITE_INDEX;
        sprite2 = PLAYER_P_SPRITE_INDEX;
    } else {
        sprite1 = PLAYER_CP_SPRITE_INDEX;
        sprite2 = PLAYER_PU_SPRITE_INDEX;
    }
    SMS_initSprites();
   
    SMS_addSprite(x,y, sprite1);
    SMS_addSprite(x+8,y, sprite2);
}

void clear_cursor_sprites(void) {   
    SMS_initSprites();
}

void connected_pieces_to_bw_palette(void) {
    SMS_setNextSpriteColoratIndex(13);
    SMS_setColor(WHITE_FFFFFF);
    setSpritePaletteColor(13,WHITE_FFFFFF);
}

void connected_pieces_to_normal_palette(void) {
    SMS_setNextSpriteColoratIndex(13);
    SMS_setColor(GREY_AAAAAA);
    setSpritePaletteColor(13,GREY_AAAAAA);
}

void not_connected_pieces_to_red_palette(void) {
    SMS_setNextSpriteColoratIndex(8);
    SMS_setColor(0x03);
    SMS_setColor(0x03);
    SMS_setColor(0x03);
    setSpritePaletteColor(8,0x03);
    setSpritePaletteColor(9,0x03);
    setSpritePaletteColor(10,0x03);
    setSpritePaletteColor(8,0x03);
    setSpritePaletteColor(9,0x03);
    setSpritePaletteColor(10,0x03);
}

void not_connected_pieces_to_normal_palette(void) {
    SMS_setNextSpriteColoratIndex(8);
    SMS_setColor(BLACK_000000);
    SMS_setColor(WHITE_FFFFFF);
    SMS_setColor(GREY_AAAAAA);
    setSpritePaletteColor(8,BLACK_000000);
    setSpritePaletteColor(9,WHITE_FFFFFF);
    setSpritePaletteColor(10,GREY_AAAAAA);
}

void swap_fire_colors(void) {
    SMS_setNextSpriteColoratIndex(14);
    SMS_setColor(ORANGE_FF5500);
    SMS_setColor(YELLOW_FFFF00);
    setSpritePaletteColor(14,ORANGE_FF5500);
    setSpritePaletteColor(15,YELLOW_FFFF00);
}

void restore_fire_colors(void) {
    SMS_setNextSpriteColoratIndex(14);
    SMS_setColor(YELLOW_FFFF00);
    SMS_setColor(ORANGE_FF5500);
    setSpritePaletteColor(14,YELLOW_FFFF00);
    setSpritePaletteColor(15,ORANGE_FF5500);
}

void background_to_level_palette(u8 level) {
    u8 base_color = level <<4;
    current_resource_bank = CONSTANT_DATA_BANK;
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    fadeToTargetBGPalette(&(background_palette_array[base_color]), 16,1); //TODO check correctness
}




void create_dialog_box(void) {
    //default: column 2 to column 29, rows 15 to 22
    for(int j=DIALOG_BOX_FIRST_ROW;j<=DIALOG_BOX_LAST_ROW;j++) {
        SMS_setNextTileatXY(DIALOG_BOX_FIRST_COLUMN,j);
        for(int i=DIALOG_BOX_FIRST_COLUMN;i<=DIALOG_BOX_LAST_COLUMN;i++) {            
            SMS_setTile(0);
        }
    }    
}

void update_dialog_box(u8 currentLastLine, u8 upDown, u8 textNumber) {
    //currentLastLine == 0, nada a la vista
    u8 writingLine = 0;
    u8 dialogLine = 0;
    while(writingLine < DIALOG_BOX_NUMBER_LINES) {
        
        if(writingLine < DIALOG_BOX_NUMBER_LINES - currentLastLine) {
           print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
           print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
        } else {
            dialogLine = writingLine - DIALOG_BOX_NUMBER_LINES + currentLastLine;
            current_resource_bank = CONSTANT_DATA_BANK;
            SMS_mapROMBank(CONSTANT_DATA_BANK);
            if(dialogLine < long_texts_size[textNumber]) {
                if(upDown == 1) { //UP    
                    print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, (u8 *)long_texts[textNumber][dialogLine]);
                    print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
                } else { //DOWN
                    print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
                    print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, (u8 *)long_texts[textNumber][dialogLine]);
                } 
            } else {
                print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
                print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
            }
                     
        }        
        writingLine++;
    }
}

void load_tiles_in_batches(const u8 *data, u16 position, u16 size) {
    u16 currentByte = 0;
    while(size>BYTES_PER_FRAME) {
        SMS_loadTiles(data+currentByte, position,BYTES_PER_FRAME);
        currentByte = currentByte + BYTES_PER_FRAME;
        size = size-BYTES_PER_FRAME;
        position = position + TILES_PER_FRAME;        
        waitForFrame();
    }
    SMS_loadTiles(data+currentByte, position,size);
    waitForFrame();
}

/*void print_single_digit(u8 x, u8 y, u8 number) {
    SMS_setNextTileatXY(x,y);
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + number);
}

void print_unsigned_char(u8 x, u8 y, u8 number) {
    u8 tmpHundreds, tmpTens, tmpUnits;
    SMS_setNextTileatXY(x,y);
    tmpHundreds = number / 100;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpHundreds);
    tmpTens = (number - (tmpHundreds * 100)) / 10;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTens);
    tmpUnits = (number - (tmpHundreds * 100) - (tmpTens * 10));
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpUnits);
}

 void print_signed_char(u8 x, u8 y, s8 number) {
    u8 tmpHundreds, tmpTens, tmpUnits;
    SMS_setNextTileatXY(x,y);
    if(number < 0) {
        SMS_setTile(TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS + ('N'-'A') ); 
        number = number * -1;
    } else {
        SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS);
    }
    tmpHundreds = number / 100;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpHundreds);
    tmpTens = (number - (tmpHundreds * 100)) / 10;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTens);
    tmpUnits = (number - (tmpHundreds * 100) - (tmpTens * 10));
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpUnits);
}*/

/*void print_unsigned_int(u8 x, u8 y, u16 number) {
    u8 tmpTenThousands, tmpThousands, tmpHundreds, tmpTens, tmpUnits;
    SMS_setNextTileatXY(x,y);
    tmpTenThousands = number / 10000;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTenThousands);
    tmpThousands = (number - (tmpTenThousands * 10000)) / 1000;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpThousands);
    tmpHundreds = (number - (tmpTenThousands * 10000) - (tmpThousands*1000)) / 100;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpHundreds);
    tmpTens = (number - (tmpTenThousands * 10000) - (tmpThousands*1000) - (tmpHundreds * 100)) / 10;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTens);
    tmpUnits = (number - (tmpTenThousands * 10000) - (tmpThousands*1000) - (tmpHundreds * 100) - (tmpTens * 10));
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpUnits);
}*/

void print_score_value(u8 x, u8 y, u16 number) {
    u8 tmpTenThousands, tmpThousands, tmpHundreds, tmpTens, tmpUnits;
    SMS_setNextTileatXY(x,y);
    tmpTenThousands = number / 10000;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTenThousands);
    tmpThousands = (number - (tmpTenThousands * 10000)) / 1000;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpThousands);
    tmpHundreds = (number - (tmpTenThousands * 10000) - (tmpThousands*1000)) / 100;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpHundreds);
    tmpTens = (number - (tmpTenThousands * 10000) - (tmpThousands*1000) - (tmpHundreds * 100)) / 10;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTens);
    tmpUnits = (number - (tmpTenThousands * 10000) - (tmpThousands*1000) - (tmpHundreds * 100) - (tmpTens * 10));
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpUnits);
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS);
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS);
}

void print_level_number(u8 x, u8 y, u8 level) {
    u8 tmpTens, tmpUnits;
    print_string(x,y,"LEVEL^");
    SMS_setNextTileatXY(x+6,y);
    tmpTens = level / 10;
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTens);
    tmpUnits = (level - (tmpTens * 10));
    SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpUnits);
}

void print_string(u8 x, u8 y, u8* string) {
    u8 i = 0;
    u8 caracter = string[i]-'A';
    //[ ! exclamation mark
    //\ ? interrogation mark
    //] Space
    //^ end of line
    while(caracter!=29) { //^ character
        SMS_setTileatXY(x+i,y,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+caracter);
        i++;
        caracter = string[i]-'A';
    }
}