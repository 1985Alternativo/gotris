#include <stdbool.h>
#include <stdlib.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../data/constantdata.h"
#include "music.h"
#include <SMSlib.h>
#include <PSGlib.h>
#include "../libs/fxsample.h"
#include "../assets2banks.h"


const char* musics[] = {
    logo1985music_psg,
    titlescreen_psg,
    intro_psg,
    highscore_psg,
    credits_psg,
    startjingle_psg, 
    ingame_psg,
    hurryup_psg,
    gameover_psg,
    fanfa1_psg,
    fanfa2_psg //congratulations.psg
};

const u8 music_bank[] = {
    logo1985music_psg_bank,
    titlescreen_psg_bank,
    intro_psg_bank,
    highscore_psg_bank,
    credits_psg_bank,
    startjingle_psg_bank,
    ingame_psg_bank,
    hurryup_psg_bank,
    gameover_psg_bank,
    fanfa1_psg_bank,
    fanfa2_psg_bank //congratulations.psg bank
};

const char* fxs[] = {
    start_psg,
    select_psg,
    move_psg,
    fall_psg,
    destroy_psg
};

const u8 fx_bank[] = {
    start_psg_bank,
    select_psg_bank,
    move_psg_bank,
    fall_psg_bank,
    destroy_psg_bank
};

const char* samples[] = {
    thunder_bin, 
    gotris_bin, 
    getready_bin,
    bonus_bin,
    gameover1_bin,
    gameover2_bin,
    congratulations1_bin,
    congratulations2_bin,
    tuxedo_bin
};

const u8 sample_bank[] = {
    thunder_bin_bank,
    gotris_bin_bank, 
    getready_bin_bank,
    bonus_bin_bank,
    gameover1_bin_bank,
    gameover2_bin_bank,
    congratulations1_bin_bank,
    congratulations2_bin_bank,
    tuxedo_bin_bank
};


void start_fadein_music(void) {
    target_atenuation = 0;
    current_atenuation = 252;
}

void start_fadeout_music(void) {
    target_atenuation = 252;
    current_atenuation = 0;
}

void full_volume(void) {
    target_atenuation = 0;
    current_atenuation = 0;
}

void play_song(u8 songIndex) {
    SMS_mapROMBank(music_bank[songIndex]);
    current_music_bank = music_bank[songIndex];
    PSGPlay(musics[songIndex]);
}

void play_fx(u8 fxIndex) {
    SMS_mapROMBank(fx_bank[fxIndex]);
    current_fx_bank = fx_bank[fxIndex];
    PSGSFXPlay(fxs[fxIndex], SFX_CHANNEL2);
}

void play_sample(u8 sampleIndex) {
    SMS_mapROMBank(CONSTANT_DATA_BANK);
    PSGStop();
    PSGSFXStop();
    initPSG(psgInit);
    
    SMS_mapROMBank(sample_bank[sampleIndex]);
    PlaySample(samples[sampleIndex]);
    SMS_mapROMBank(current_music_bank);
    PSGResume();
}

void enable_music_handler(void) {
    SMS_setLineInterruptHandler(&PSGFrame);
    SMS_setLineCounter(190); 
    SMS_enableLineInterrupt();
}

void disable_music_handler(void) {
   SMS_disableLineInterrupt();
}
