//
// Created by Jordi Montornes on 14/04/2021.
//

#ifndef GOTRIS_GLOBALVARIABLES_H_H
#define GOTRIS_GLOBALVARIABLES_H_H


//montylib
extern bool pause;
extern u8 rand_index;
extern u16 frame_cnt;
extern u16 level_timer;
extern u16 input_cnt_direction;
extern u16 input_cnt_rotation;
extern u16 movement_direction;
extern u16 rotation_input;
extern u16 button_input;
extern int repeat_movement;
extern int repeat_rotation;
extern u8 current_resource_bank;
extern u8 current_music_bank;
extern u8 current_fx_bank;
extern u8 target_atenuation;
extern u8 current_atenuation;

//title screen
extern u16 scroll_x[6];
extern u8 lineCnt;

//credits screen
extern u8 cursorx;
extern u8 cursory;
extern u8 waveIndex;
extern bool creditsEnd;

//ending screen
extern u8 currentScrollColumn; 

//state machine
extern u8 current_state;
extern u8 next_state;
extern u8 previous_state;
extern bool preserve_context;
extern bool end_state;


//game logic
extern u8 game_status;
extern u16 score_p1;
extern u16 score_p2;
extern bool highScoreReached;
extern bool bonusScore;
extern bool bonusSamplePlay;
extern u8 last_square_x;
extern bool piecesUpdated;
extern bool piecesDestroyed;
extern bool isPaletteRed;
extern bool isHurryUp;
extern bool generateBonus;
extern u8 fallSpeed;
extern u16 fallAcum;
extern u8 level;
extern bool leftAction;
extern bool rightAction;
extern bool downAction;
extern bool rotation1Action;
extern bool rotation2Action;
extern bool resetPressed;
extern u8 newLevel;
extern u8 decodingIndex;
extern bool endOfDrawingLevel;

//cpu
extern u8 target_column;
extern u8 target_row;
extern u8 target_rotation;
extern u8 current_rotation;
extern u8 movement_counter;
extern u8 rotation_counter;

//board
//10 tokens wide (5 squares), 22 token high  (11 squares)
extern u8 board[220];
extern u8 suitable_columns[10];
extern u8 piece_index;
extern u8 row_index;
extern u8 max_row_index;

extern u8 sprite_palette[];
extern u8 background_palette[];

extern piece active_pieces[];
extern square current_square;
extern square next_square;
extern u8 number_human_players;
extern u8 current_player;

extern linked_list_t* active_points_list;

extern volatile highscore_struct *highscore_table_backup;
extern highscore_struct highscore_table;
extern u8 currentSaveSystem;
//states
extern Statemanager statemanager;
#endif //GOTRIS_GLOBALVARIABLES_H_H