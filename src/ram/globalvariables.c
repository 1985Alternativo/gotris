#include <stdbool.h>
#include <SMSlib.h>
#include "../engine/globaldefinitions.h"
#include "globalvariables.h"


//montylib
bool pause;
u8 rand_index;
u16 frame_cnt;
u16 level_timer;
u16 input_cnt_direction;
u16 input_cnt_rotation;
u16 movement_direction;
u16 rotation_input;
u16 button_input;
int repeat_movement;
int repeat_rotation;
u8 current_resource_bank;
u8 current_music_bank;
u8 current_fx_bank;
u8 target_atenuation;
u8 current_atenuation;

//title screen
u16 scroll_x[6];
u8 lineCnt;

//credits screen
u8 cursorx;
u8 cursory;
u8 waveIndex;
bool creditsEnd;

//ending screen
u8 currentScrollColumn; 

//state machine
u8 current_state;
u8 next_state;
u8 previous_state;
bool preserve_context;
bool end_state;


//game logic
u8 game_status;
u16 score_p1;
u16 score_p2;
bool highScoreReached;
bool bonusScore;
bool bonusSamplePlay;
u8 last_square_x;
bool piecesUpdated;
bool piecesDestroyed;
bool isPaletteRed;
bool isHurryUp;
bool generateBonus;
u8 fallSpeed;
u16 fallAcum;
u8 level;
bool leftAction;
bool rightAction;
bool downAction;
bool rotation1Action;
bool rotation2Action;
bool resetPressed;
u8 newLevel;
u8 decodingIndex;
bool endOfDrawingLevel;

//cpu
u8 target_column;
u8 target_row;
u8 target_rotation;
u8 current_rotation;
u8 movement_counter;
u8 rotation_counter;

//board
//10 tokens wide (5 squares), 22 token high  (11 squares)
u8 board[220];
u8 suitable_columns[10];
u8 piece_index;
u8 row_index;
u8 max_row_index;

u8 sprite_palette[] = {
    0x00,0x3F,0x36,0x3E,0x3B,0x11,0x29,0x2A,0x00,0x3F,0x2A,0x00,0x3F,0x2A,0x0f,0x07
};
u8 background_palette[] = {
    0x38,0x00,0x38,0x38,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

piece active_pieces[MAX_PIECES_ACTIVE];
square current_square;
square next_square;
u8 number_human_players;
u8 current_player;

linked_list_t* active_points_list;


volatile highscore_struct *highscore_table_backup;
highscore_struct highscore_table;
u8 currentSaveSystem;
Statemanager statemanager;