#include <stdbool.h>
#include <stdio.h>
#include <SMSlib.h>
#include "engine/globaldefinitions.h"
#include "ram/globalvariables.h"
#include "engine/cpu.h"
#include "engine/board.h"
#include "libs/montylib.h"


void initializeBoardTest1(void) {
    init_board();
    u8 i,j;
    i=19;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    i=20;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    i=21;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
}

void initializeBoardTest2(void) {
    init_board();
    u8 i,j;
    i=19;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    i=20;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    i=21;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
}

void initializeBoardTest4(void) {
    init_board();
    u8 i,j;
    i=17;
    j=0;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    i=18;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=3;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=4;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=5;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    i=19;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=3;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=4;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=5;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    i=20;
    j=0;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=3;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=4;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=5;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=6;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=7;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    i=21;
    j=0;
    board[(i << 3) + (i << 1) + j] = BLACK_PIECE;
    j=1;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=2;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=3;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=4;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=5;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=6;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
    j=7;
    board[(i << 3) + (i << 1) + j] = WHITE_PIECE;
}

void printBoard(void) {
    u8 i,j;
    i=0;
    while(i<BOARD_ROWS) {
        SMS_setNextTileatXY(0, i);
        j = 0;
        while(j<BOARD_COLUMNS) {
            printf("%d",board[(i << 3) + (i << 1) + j]);
            j++;
        }
        i++;
    }
}
void printHexadecimalCharacter(u8 number) {
    switch(number) {
        case 0:
            printf("0");
            break;
        case 1:
            printf("1");
            break;
        case 2:
            printf("2");
            break;
        case 3:
            printf("3");
            break;
        case 4:
            printf("4");
            break;
        case 5:
            printf("5");
            break;
        case 6:
            printf("6");
            break;
        case 7:
            printf("7");
            break;
        case 8:
            printf("8");
            break;
        case 9:
            printf("9");
            break;
        case 10:
            printf("A");
            break;
        case 11:
            printf("B");
            break;
        case 12:
            printf("C");
            break;
        case 13:
            printf("D");
            break;
        case 14:
            printf("E");
            break;
        case 15:
            printf("F");
            break;     
        case 16: //I'm really clever here, hehehe
            printf("G");
            break;
        case 17:
            printf("H");
            break;
        case 18:
            printf("I");
            break;
        case 19:
            printf("J");
            break;
        case 20:
            printf("K");
            break;
        case 21:
            printf("L");
            break;       
        default:
            printf("x");
    }
}

void printHexadecimal(u8 number) {
    u8 part = (number & 0xf0) >> 4;
    printHexadecimalCharacter(part);
    part = number & 0x0f;
    printHexadecimalCharacter(part);
    printf(" ");
}

void printSuitables(void) {
    u8 j;
    j = 0;
    SMS_setNextTileatXY(0, 23);
    while(j<BOARD_COLUMNS) {
        //printf("%d ",suitable_columns[j]);
        printHexadecimal(suitable_columns[j]);
        j++;
    }
}

/*void decideNextMove2(void) {
    u8 i, color;
    u8 maxSuitableColumn, maxSuitableColor, maxSuitableElements;

    current_rotation = 3;
    target_rotation = 3;
    target_column = 0;
    
    maxSuitableColumn = get_rand() & 7;
    maxSuitableColor = WHITE_SUITABLE;
    maxSuitableElements = 0;
    for(i=0;i<BOARD_COLUMNS;i++) {
        if(suitable_columns[i] != NO_SUITABLE) {
            if((suitable_columns[i]&0x0f) > maxSuitableElements) {
                maxSuitableColumn = i;
                maxSuitableColor = (suitable_columns[i]&0xf0) + 1; //per a la comparació d'abaix
                maxSuitableElements = (suitable_columns[i]&0x0f);
            }
        }
    }
    i=maxSuitableColumn;
    if(maxSuitableColor & WHITE_SUITABLE) {
        color = WHITE_PIECE;
        getTargetBottomPart();                    
    } else if(maxSuitableColor & WHITE_SUITABLE_UPPER_PART) {
        color = WHITE_PIECE;
        getTargetUpperPart();
    } else if(maxSuitableColor & BLACK_SUITABLE) {
        color = BLACK_PIECE;
        getTargetBottomPart();
    } else if(maxSuitableColor & BLACK_SUITABLE_UPPER_PART) {
        color = BLACK_PIECE;
        getTargetUpperPart();
    }

    target_row = getCollisionRowFromColumn(target_column);
    if(i==BOARD_COLUMNS || (target_row < 8 && current_square.color00 != BONUS_CELL_INVERSE) ){
        target_column = get_rand() & 7;
        target_rotation = get_rand() & 7; //value from 0-7
        if(!target_rotation) target_rotation++; //value from 1-7
        target_rotation--; //value from 0-6
        target_row = getCollisionRowFromColumn(target_column);
    } 
    
    
    movement_counter = target_row;
    rotation_counter = target_row;
}
*/
bool compute_path2(u8 x, u8 y) {
    u8 position = (y << 3) + (y << 1) + x;
    u8 empty_directions;
    if(board[position] == NOT_PROCESSED_WHITE_CELL) {
        if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
            board[position] = CONNECTED_WHITE_CELL;
            //suitable_columns[x] = 0;
            SMS_setNextTileatXY(x+20, y); 
            printf("4");
            return true;
        } else {
            if(neighbour_connected(position)) {
                board[position] = CONNECTED_WHITE_CELL;
                //suitable_columns[x] = 0;
                SMS_setNextTileatXY(x+20, y); 
                printf("5");
                return true;
            } else {
                SMS_setNextTileatXY(x+20, y); 
                printf("6");
                empty_directions = neighbour_emptyspace(position);
                if(empty_directions) {
                    SMS_setNextTileatXY(10+x, y); 
                    //Emplenar array de posibilitats
                    //IMPROVE: Use counter separatedely for White and Black: 00 (white) 00 (black)
                    if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
                        suitable_columns[x] = ((suitable_columns[x] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x]+ 1) &0xF) ;
                        printf("f"); 
                    } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {
                        if(is_free_board_position(position+9)) {
                            suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
                            printf("g"); 
                        } else {
                            suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x-1]+1)& 0xF);
                            printf("h"); 
                        }
                    } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
                        if(is_free_board_position(position+11)) {
                            suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
                            printf("i"); 
                        } else {
                            suitable_columns[x+1] = ((suitable_columns[x+1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
                            printf("j"); 
                        }
                    }
                    board[position] = CONNECTED_WHITE_CELL;
                    return true;
                } else if(!neighbour_notprocessed(position)) {
                    board[position] = NOT_CONNECTED_WHITE_CELL;
                    return true;
                }
            } 
        }
    } else if(board[position] == NOT_PROCESSED_BLACK_CELL) {
        if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
            board[position] = CONNECTED_BLACK_CELL;
            //suitable_columns[x] = 0;
            SMS_setNextTileatXY(x+20, y); 
            printf("1");
            return true;
        } else {
            if(neighbour_connected(position)) {
                board[position] = CONNECTED_BLACK_CELL;
                //suitable_columns[x] = 0;
                SMS_setNextTileatXY(x+20, y); 
                printf("2");
                return true;
            } else {
                SMS_setNextTileatXY(x+20, y); 
                printf("3");
                empty_directions = neighbour_emptyspace(position);
                if(empty_directions) {
                    //Emplenar array de posibilitats
                    SMS_setNextTileatXY(10+x, y); 
                    if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
                        suitable_columns[x] = ((suitable_columns[x] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x] + 1) & 0xF); 
                        printf("a"); 
                    } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {                        
                        if(is_free_board_position(position+9)) {
                            suitable_columns[x-1] =((suitable_columns[x-1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
                            printf("b"); 
                        } else {
                            suitable_columns[x-1] = ((suitable_columns[x-1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
                            printf("c"); 
                        }                        
                    } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
                        if(is_free_board_position(position+11)) {
                            suitable_columns[x+1] =((suitable_columns[x+1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] + 1) & 0xF);
                            printf("d"); 
                        } else {
                            suitable_columns[x+1] = ((suitable_columns[x+1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x+1] + 1) & 0xF);
                            printf("e"); 
                        }
                    }
                    board[position] = CONNECTED_BLACK_CELL;
                    return true;
                } else if(!neighbour_notprocessed(position)) {
                    board[position] = NOT_CONNECTED_BLACK_CELL;
                    return true;
                }
            } 
        }
    } else if(board[position] == EMPTY_CELL) {
        SMS_setNextTileatXY(x+20, y); 
        printf("0");
        suitable_columns[x] = 0;
    }
    return false;
}

void compute_paths(void) {
    bool someChangeHappened = true;
    while(someChangeHappened) {
        someChangeHappened = false;
        u8 row_index = BOARD_ROWS-1;
        bool somePiece = true;
        while(row_index > 0 && somePiece) {
            somePiece = false;
            u8 position = (row_index << 3) + (row_index << 1);
            u8 i;
            for(i=0;i<BOARD_COLUMNS;i++) {
                if(!is_free_board_position(position)) {
                    someChangeHappened = someChangeHappened || compute_path2(i,row_index);
                    somePiece = true;
                }
                position++;
            }
            row_index--;
        }
        while(!(SMS_getKeysPressed() & PORT_A_KEY_1));
    }
}

void check_disconnected(void) {
    u8 row_index = BOARD_ROWS-1;
    bool somePiece = true;
    bool somePieceDestroyed = false;
    while(row_index > 0 && somePiece) {
        somePiece = false;
        u8 position = (row_index << 3) + (row_index << 1);
        u8 i;
        for(i=0;i<BOARD_COLUMNS;i++) {
            if(!is_free_board_position(position)) {
                somePieceDestroyed = mark_as_disconnected(i, row_index, position) || somePieceDestroyed;
                somePiece = true;
            }
            position++;
        }
        row_index--;
    }
}

void test1(void) {
    clear_tilemap();
    initializeBoardTest1();
    frame_cnt = 0;

    current_square.color00 = WHITE_PIECE;
    current_square.color01 = WHITE_PIECE;
    current_square.color10 = WHITE_PIECE;
    current_square.color11 = WHITE_PIECE;
    compute_paths();
    check_disconnected();
    printBoard();
    printSuitables();
    decideNextMove();
    SMS_setNextTileatXY(21, 20);
    printf("Column: ");
    printf("%d",target_column);
    SMS_setNextTileatXY(21, 21);
    printf("Rotation: ");
    printf("%d",target_rotation);
    while(frame_cnt < 200) {
        waitForFrame();
    }
}

void test2(void) {
    clear_tilemap();
    initializeBoardTest2();
    frame_cnt = 0;
    
    current_square.color00 = WHITE_PIECE;
    current_square.color01 = WHITE_PIECE;
    current_square.color10 = WHITE_PIECE;
    current_square.color11 = WHITE_PIECE;
    compute_paths();
    check_disconnected();
    printBoard();
    printSuitables();
    decideNextMove();
    SMS_setNextTileatXY(21, 20);
    printf("Column: ");
    printf("%d",target_column);
    SMS_setNextTileatXY(21, 21);
    printf("Rotation: ");
    printf("%d",target_rotation);
    while(frame_cnt < 200) {
        waitForFrame();
    }
}

void test3(void) {
    clear_tilemap();
    initializeBoardTest2();
    frame_cnt = 0;
    
    current_square.color00 = WHITE_PIECE;
    current_square.color01 = BLACK_PIECE;
    current_square.color10 = BLACK_PIECE;
    current_square.color11 = BLACK_PIECE;
    compute_paths();
    check_disconnected();
    printBoard();
    printSuitables();
    decideNextMove();
    SMS_setNextTileatXY(21, 20);
    printf("Column: ");
    printf("%d",target_column);
    SMS_setNextTileatXY(21, 21);
    printf("Rotation: ");
    printf("%d",target_rotation);
    while(frame_cnt < 200) {
        waitForFrame();
    }
}

void test4(void) {
    clear_tilemap();
    initializeBoardTest4();
    frame_cnt = 0;
    current_square.color00 = WHITE_PIECE;
    current_square.color01 = WHITE_PIECE;
    current_square.color10 = WHITE_PIECE;
    current_square.color11 = BLACK_PIECE;
    compute_paths();
    check_disconnected();
    printBoard();
    printSuitables();
    decideNextMove();
    SMS_setNextTileatXY(0, 22);
    printf("Column: ");
    printf("%d",target_column);
    SMS_setNextTileatXY(11, 22);
    printf("Rotation: ");
    printf("%d",target_rotation);
    while(frame_cnt < 200) {
        waitForFrame();
    }
}

void main(void) {
    SMS_init();
    SMS_autoSetUpTextRenderer();
    /*test1();
    test2();
    test3();*/
    test4();
}

SMS_EMBED_SEGA_ROM_HEADER(9999, 0); // code 9999 hopefully free, here this means 'homebrew'
SMS_EMBED_SDSC_HEADER_AUTO_DATE(1, 3, "TuxedoGames", "GOTRIS Board Test",
	"Testing ROM Board Logic");
