extern const unsigned char	ending1tiles_psgcompr[2964];
#define				ending1tiles_psgcompr_size 2964
#define				ending1tiles_psgcompr_bank 3
extern const unsigned char	ending1palette_bin[16];
#define				ending1palette_bin_size 16
#define				ending1palette_bin_bank 3
extern const unsigned char	ending1tilemap_stmcompr[230];
#define				ending1tilemap_stmcompr_size 230
#define				ending1tilemap_stmcompr_bank 3
extern const unsigned char	ending2tiles_bin[11008];
#define				ending2tiles_bin_size 11008
#define				ending2tiles_bin_bank 3
extern const unsigned char	ending2palette_bin[16];
#define				ending2palette_bin_size 16
#define				ending2palette_bin_bank 3
extern const unsigned char	devkitsmstiles_psgcompr[1877];
#define				devkitsmstiles_psgcompr_size 1877
#define				devkitsmstiles_psgcompr_bank 3
extern const unsigned char	devkitsmstilemap_stmcompr[155];
#define				devkitsmstilemap_stmcompr_size 155
#define				devkitsmstilemap_stmcompr_bank 3
extern const unsigned char	select_psg[32];
#define				select_psg_size 32
#define				select_psg_bank 3
extern const unsigned char	devkitsmspalette_bin[6];
#define				devkitsmspalette_bin_size 6
#define				devkitsmspalette_bin_bank 3
extern const unsigned char	thunder_bin[13018];
#define				thunder_bin_size 13018
#define				thunder_bin_bank 4
extern const unsigned char	fanfa1_psg[395];
#define				fanfa1_psg_size 395
#define				fanfa1_psg_bank 4
extern const unsigned char	fanfa2_psg[382];
#define				fanfa2_psg_size 382
#define				fanfa2_psg_bank 4
extern const unsigned char	gotris_bin[12048];
#define				gotris_bin_size 12048
#define				gotris_bin_bank 5
extern const unsigned char	tuxedo_bin[11500];
#define				tuxedo_bin_size 11500
#define				tuxedo_bin_bank 6
extern const unsigned char	congratulations1_bin[11476];
#define				congratulations1_bin_size 11476
#define				congratulations1_bin_bank 7
extern const unsigned char	backgroundpalette_bin[12];
#define				backgroundpalette_bin_size 12
#define				backgroundpalette_bin_bank 8
extern const unsigned char	backgroundtilemap_stmcompr[236];
#define				backgroundtilemap_stmcompr_size 236
#define				backgroundtilemap_stmcompr_bank 8
extern const unsigned char	backgroundtiles_psgcompr[3987];
#define				backgroundtiles_psgcompr_size 3987
#define				backgroundtiles_psgcompr_bank 8
extern const unsigned char	piecespalette_bin[16];
#define				piecespalette_bin_size 16
#define				piecespalette_bin_bank 8
extern const unsigned char	piecestiles_psgcompr[419];
#define				piecestiles_psgcompr_size 419
#define				piecestiles_psgcompr_bank 8
extern const unsigned char	simplefont_psgcompr[570];
#define				simplefont_psgcompr_size 570
#define				simplefont_psgcompr_bank 8
extern const unsigned char	startjingle_psg[542];
#define				startjingle_psg_size 542
#define				startjingle_psg_bank 8
extern const unsigned char	destroy_psg[31];
#define				destroy_psg_size 31
#define				destroy_psg_bank 8
extern const unsigned char	ingame_psg[4097];
#define				ingame_psg_size 4097
#define				ingame_psg_bank 8
extern const unsigned char	hurryup_psg[640];
#define				hurryup_psg_size 640
#define				hurryup_psg_bank 8
extern const unsigned char	fall_psg[41];
#define				fall_psg_size 41
#define				fall_psg_bank 8
extern const unsigned char	move_psg[32];
#define				move_psg_size 32
#define				move_psg_bank 8
extern const unsigned char	hit_psg[58];
#define				hit_psg_size 58
#define				hit_psg_bank 8
extern const unsigned char	gameover_psg[443];
#define				gameover_psg_size 443
#define				gameover_psg_bank 8
extern const unsigned char	intro1tiles_psgcompr[7569];
#define				intro1tiles_psgcompr_size 7569
#define				intro1tiles_psgcompr_bank 9
extern const unsigned char	intro1palette_bin[16];
#define				intro1palette_bin_size 16
#define				intro1palette_bin_bank 9
extern const unsigned char	intro1tilemap_stmcompr[71];
#define				intro1tilemap_stmcompr_size 71
#define				intro1tilemap_stmcompr_bank 9
extern const unsigned char	intro_psg[3449];
#define				intro_psg_size 3449
#define				intro_psg_bank 9
extern const unsigned char	gameover2_bin[10773];
#define				gameover2_bin_size 10773
#define				gameover2_bin_bank 10
extern const unsigned char	logotiles_psgcompr[1613];
#define				logotiles_psgcompr_size 1613
#define				logotiles_psgcompr_bank 11
extern const unsigned char	logotilemap_stmcompr[97];
#define				logotilemap_stmcompr_size 97
#define				logotilemap_stmcompr_bank 11
extern const unsigned char	logopalette_bin[13];
#define				logopalette_bin_size 13
#define				logopalette_bin_bank 11
extern const unsigned char	logo1985music_psg[201];
#define				logo1985music_psg_size 201
#define				logo1985music_psg_bank 11
extern const unsigned char	titlescreentiles_psgcompr[7292];
#define				titlescreentiles_psgcompr_size 7292
#define				titlescreentiles_psgcompr_bank 11
extern const unsigned char	titlescreenpalette_bin[16];
#define				titlescreenpalette_bin_size 16
#define				titlescreenpalette_bin_bank 11
extern const unsigned char	titlescreentilemap_stmcompr[493];
#define				titlescreentilemap_stmcompr_size 493
#define				titlescreentilemap_stmcompr_bank 11
extern const unsigned char	titlescreen_psg[441];
#define				titlescreen_psg_size 441
#define				titlescreen_psg_bank 11
extern const unsigned char	start_psg[36];
#define				start_psg_size 36
#define				start_psg_bank 11
extern const unsigned char	intro2tiles_bin[9856];
#define				intro2tiles_bin_size 9856
#define				intro2tiles_bin_bank 12
extern const unsigned char	intro2palette_bin[16];
#define				intro2palette_bin_size 16
#define				intro2palette_bin_bank 12
extern const unsigned char	intro2tilemap_stmcompr[191];
#define				intro2tilemap_stmcompr_size 191
#define				intro2tilemap_stmcompr_bank 12
extern const unsigned char	intro3tiles_bin[9888];
#define				intro3tiles_bin_size 9888
#define				intro3tiles_bin_bank 13
extern const unsigned char	intro3palette_bin[14];
#define				intro3palette_bin_size 14
#define				intro3palette_bin_bank 13
extern const unsigned char	intro3tilemap_stmcompr[119];
#define				intro3tilemap_stmcompr_size 119
#define				intro3tilemap_stmcompr_bank 13
extern const unsigned char	getready_bin[9497];
#define				getready_bin_size 9497
#define				getready_bin_bank 14
extern const unsigned char	scorescreentiles_psgcompr[5074];
#define				scorescreentiles_psgcompr_size 5074
#define				scorescreentiles_psgcompr_bank 14
extern const unsigned char	scorescreenpalette_bin[14];
#define				scorescreenpalette_bin_size 14
#define				scorescreenpalette_bin_bank 14
extern const unsigned char	scorescreentilemap_stmcompr[393];
#define				scorescreentilemap_stmcompr_size 393
#define				scorescreentilemap_stmcompr_bank 14
extern const unsigned char	highscore_psg[947];
#define				highscore_psg_size 947
#define				highscore_psg_bank 14
extern const unsigned char	bonus_bin[8890];
#define				bonus_bin_size 8890
#define				bonus_bin_bank 15
extern const unsigned char	gameover1_bin[7359];
#define				gameover1_bin_size 7359
#define				gameover1_bin_bank 15
extern const unsigned char	congratulations2_bin[8624];
#define				congratulations2_bin_size 8624
#define				congratulations2_bin_bank 16
extern const unsigned char	creditsscreentiles_psgcompr[4754];
#define				creditsscreentiles_psgcompr_size 4754
#define				creditsscreentiles_psgcompr_bank 16
extern const unsigned char	creditsscreenpalette_bin[15];
#define				creditsscreenpalette_bin_size 15
#define				creditsscreenpalette_bin_bank 16
extern const unsigned char	creditsscreentilemap_stmcompr[212];
#define				creditsscreentilemap_stmcompr_size 212
#define				creditsscreentilemap_stmcompr_bank 16
extern const unsigned char	credits_psg[2265];
#define				credits_psg_size 2265
#define				credits_psg_bank 16
