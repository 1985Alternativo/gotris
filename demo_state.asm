;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module demo_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _get_rand
	.globl _waitForFrame
	.globl _getButtonsPressed
	.globl _update_input
	.globl _clear_input
	.globl _transition_to_state
	.globl _end_demo
	.globl _get_game_actions
	.globl _execute_game_logic
	.globl _init_gamestatus
	.globl _init_board
	.globl _play_song
	.globl _start_fadein_music
	.globl _print_string
	.globl _print_level_number
	.globl _print_score_value
	.globl _draw_game_screen
	.globl _draw_game_background
	.globl _clear_game_lists
	.globl _initialize_game_lists
	.globl _game_fade_in
	.globl _general_fade_out
	.globl _load_game_assets
	.globl _SMS_zeroSpritePalette
	.globl _SMS_zeroBGPalette
	.globl _startLevel
	.globl _demo_state_status
	.globl _demo_duration
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _demo_state_start
	.globl _demo_state_update
	.globl _demo_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
G$demo_duration$0_0$0==.
_demo_duration::
	.ds 2
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$demo_state_status$0_0$0==.
_demo_state_status::
	.ds 1
G$startLevel$0_0$0==.
_startLevel::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$demo_state_start$0$0	= .
	.globl	G$demo_state_start$0$0
	C$demo_state.c$19$0_0$236	= .
	.globl	C$demo_state.c$19$0_0$236
;src/states/demo_state.c:19: void demo_state_start(void) {
;	---------------------------------
; Function demo_state_start
; ---------------------------------
_demo_state_start::
	C$demo_state.c$20$1_0$236	= .
	.globl	C$demo_state.c$20$1_0$236
;src/states/demo_state.c:20: number_human_players = 0;
	ld	hl, #_number_human_players
	ld	(hl), #0x00
	C$demo_state.c$21$1_0$236	= .
	.globl	C$demo_state.c$21$1_0$236
;src/states/demo_state.c:21: load_game_assets();
	call	_load_game_assets
	C$demo_state.c$22$1_0$236	= .
	.globl	C$demo_state.c$22$1_0$236
;src/states/demo_state.c:22: draw_game_background();
	call	_draw_game_background
	C$demo_state.c$23$1_0$236	= .
	.globl	C$demo_state.c$23$1_0$236
;src/states/demo_state.c:23: startLevel = get_rand()&7;
	call	_get_rand
	and	a, #0x07
	C$demo_state.c$24$1_0$236	= .
	.globl	C$demo_state.c$24$1_0$236
;src/states/demo_state.c:24: init_gamestatus(startLevel); 
	ld	(_startLevel+0), a
	call	_init_gamestatus
	C$demo_state.c$25$1_0$236	= .
	.globl	C$demo_state.c$25$1_0$236
;src/states/demo_state.c:25: init_board();
	call	_init_board
	C$demo_state.c$26$1_0$236	= .
	.globl	C$demo_state.c$26$1_0$236
;src/states/demo_state.c:26: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$demo_state.c$27$1_0$236	= .
	.globl	C$demo_state.c$27$1_0$236
;src/states/demo_state.c:27: demo_state_status = 0;
	ld	iy, #_demo_state_status
	ld	0 (iy), #0x00
	C$demo_state.c$28$1_0$236	= .
	.globl	C$demo_state.c$28$1_0$236
;src/states/demo_state.c:28: demo_duration = 0;
	ld	(_demo_duration), hl
	C$demo_state.c$29$1_0$236	= .
	.globl	C$demo_state.c$29$1_0$236
;src/states/demo_state.c:29: }
	C$demo_state.c$29$1_0$236	= .
	.globl	C$demo_state.c$29$1_0$236
	XG$demo_state_start$0$0	= .
	.globl	XG$demo_state_start$0$0
	ret
	G$demo_state_update$0$0	= .
	.globl	G$demo_state_update$0$0
	C$demo_state.c$31$1_0$238	= .
	.globl	C$demo_state.c$31$1_0$238
;src/states/demo_state.c:31: void demo_state_update(void) {
;	---------------------------------
; Function demo_state_update
; ---------------------------------
_demo_state_update::
	C$demo_state.c$32$1_0$238	= .
	.globl	C$demo_state.c$32$1_0$238
;src/states/demo_state.c:32: if(demo_state_status == 0) {
	ld	a, (_demo_state_status+0)
	or	a, a
	jr	NZ, 00132$
	C$demo_state.c$33$2_0$239	= .
	.globl	C$demo_state.c$33$2_0$239
;src/states/demo_state.c:33: start_fadein_music();
	call	_start_fadein_music
	C$demo_state.c$34$2_0$239	= .
	.globl	C$demo_state.c$34$2_0$239
;src/states/demo_state.c:34: game_fade_in(startLevel);
	ld	a, (_startLevel+0)
	call	_game_fade_in
	C$demo_state.c$35$2_0$239	= .
	.globl	C$demo_state.c$35$2_0$239
;src/states/demo_state.c:35: play_song(INGAME_OST);
	ld	a, #0x06
	call	_play_song
	C$demo_state.c$36$2_0$239	= .
	.globl	C$demo_state.c$36$2_0$239
;src/states/demo_state.c:36: demo_state_status = 1;
	ld	hl, #_demo_state_status
	ld	(hl), #0x01
	ret
00132$:
	C$demo_state.c$37$1_0$238	= .
	.globl	C$demo_state.c$37$1_0$238
;src/states/demo_state.c:37: } else if(demo_state_status == 1) {
	ld	a, (_demo_state_status+0)
	dec	a
	jr	NZ, 00129$
	C$demo_state.c$38$2_0$240	= .
	.globl	C$demo_state.c$38$2_0$240
;src/states/demo_state.c:38: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$demo_state.c$39$2_0$240	= .
	.globl	C$demo_state.c$39$2_0$240
;src/states/demo_state.c:39: while(frame_cnt < XS_PERIOD) {
00101$:
	ld	de, #0x0018
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00103$
	C$demo_state.c$40$3_0$241	= .
	.globl	C$demo_state.c$40$3_0$241
;src/states/demo_state.c:40: waitForFrame();
	call	_waitForFrame
	jr	00101$
00103$:
	C$demo_state.c$42$2_0$240	= .
	.globl	C$demo_state.c$42$2_0$240
;src/states/demo_state.c:42: demo_state_status = 2;
	ld	hl, #_demo_state_status
	ld	(hl), #0x02
	ret
00129$:
	C$demo_state.c$43$1_0$238	= .
	.globl	C$demo_state.c$43$1_0$238
;src/states/demo_state.c:43: } else if(demo_state_status == 2) {
	ld	a, (_demo_state_status+0)
	sub	a, #0x02
	jr	NZ, 00126$
	C$demo_state.c$44$2_0$242	= .
	.globl	C$demo_state.c$44$2_0$242
;src/states/demo_state.c:44: draw_game_screen();
	call	_draw_game_screen
	C$demo_state.c$45$2_0$242	= .
	.globl	C$demo_state.c$45$2_0$242
;src/states/demo_state.c:45: print_level_number(2,23,level+1);
	ld	a, (_level+0)
	inc	a
	push	af
	inc	sp
	ld	l, #0x17
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_level_number
	C$demo_state.c$46$2_0$242	= .
	.globl	C$demo_state.c$46$2_0$242
;src/states/demo_state.c:46: print_string(2,19,"LVL]GOAL^");
	ld	hl, #___str_0
	push	hl
	ld	l, #0x13
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	C$demo_state.c$47$2_0$242	= .
	.globl	C$demo_state.c$47$2_0$242
;src/states/demo_state.c:47: print_score_value(2,21,(level+1)<<LEVEL_DELIMITER);
	ld	a, (_level+0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	push	hl
	ld	l, #0x15
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_score_value
	C$demo_state.c$48$2_0$242	= .
	.globl	C$demo_state.c$48$2_0$242
;src/states/demo_state.c:48: print_score_value(24,19,score_p1);
	ld	hl, (_score_p1)
	push	hl
	ld	l, #0x13
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x18
	call	_print_score_value
	C$demo_state.c$49$2_0$242	= .
	.globl	C$demo_state.c$49$2_0$242
;src/states/demo_state.c:49: print_score_value(24,23,score_p2);
	ld	hl, (_score_p2)
	push	hl
	ld	l, #0x17
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x18
	call	_print_score_value
	C$demo_state.c$50$2_0$242	= .
	.globl	C$demo_state.c$50$2_0$242
;src/states/demo_state.c:50: initialize_game_lists();
	call	_initialize_game_lists
	C$demo_state.c$51$2_0$242	= .
	.globl	C$demo_state.c$51$2_0$242
;src/states/demo_state.c:51: demo_state_status = 3;
	ld	hl, #_demo_state_status
	ld	(hl), #0x03
	ret
00126$:
	C$demo_state.c$52$1_0$238	= .
	.globl	C$demo_state.c$52$1_0$238
;src/states/demo_state.c:52: }  else if (demo_state_status == 3) {
	ld	a, (_demo_state_status+0)
	sub	a, #0x03
	jp	NZ,00123$
	C$demo_state.c$53$2_0$243	= .
	.globl	C$demo_state.c$53$2_0$243
;src/states/demo_state.c:53: if (game_status!=GAME_STATUS_RESET && demo_duration < XXL_PERIOD) { 
	ld	a, (_game_status+0)
	sub	a, #0x15
	jr	Z, 00114$
	ld	de, #0x01e0
	ld	hl, (_demo_duration)
	cp	a, a
	sbc	hl, de
	jr	NC, 00114$
	C$demo_state.c$54$3_0$244	= .
	.globl	C$demo_state.c$54$3_0$244
;src/states/demo_state.c:54: execute_game_logic();
	call	_execute_game_logic
	C$demo_state.c$55$3_0$244	= .
	.globl	C$demo_state.c$55$3_0$244
;src/states/demo_state.c:55: get_game_actions();
	call	_get_game_actions
	C$demo_state.c$56$3_0$244	= .
	.globl	C$demo_state.c$56$3_0$244
;src/states/demo_state.c:56: update_input();
	call	_update_input
	C$demo_state.c$57$3_0$244	= .
	.globl	C$demo_state.c$57$3_0$244
;src/states/demo_state.c:57: if(getButtonsPressed() & KEY_ONE_P1 || getButtonsPressed() & KEY_ONE_P2) {
	call	_getButtonsPressed
	bit	4, e
	jr	NZ, 00107$
	call	_getButtonsPressed
	bit	2, d
	jr	Z, 00108$
00107$:
	C$demo_state.c$58$4_0$245	= .
	.globl	C$demo_state.c$58$4_0$245
;src/states/demo_state.c:58: end_demo();   
	call	_end_demo
	C$demo_state.c$59$4_0$245	= .
	.globl	C$demo_state.c$59$4_0$245
;src/states/demo_state.c:59: general_fade_out();
	call	_general_fade_out
	C$demo_state.c$60$4_0$245	= .
	.globl	C$demo_state.c$60$4_0$245
;src/states/demo_state.c:60: blackBackgroundPalette();
	call	_SMS_zeroBGPalette
	C$demo_state.c$61$4_0$245	= .
	.globl	C$demo_state.c$61$4_0$245
;src/states/demo_state.c:61: blackSpritePalette();
	call	_SMS_zeroSpritePalette
	C$demo_state.c$62$4_0$245	= .
	.globl	C$demo_state.c$62$4_0$245
;src/states/demo_state.c:62: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$demo_state.c$63$4_0$245	= .
	.globl	C$demo_state.c$63$4_0$245
;src/states/demo_state.c:63: while(frame_cnt < M_PERIOD) {
00104$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00106$
	C$demo_state.c$64$5_0$246	= .
	.globl	C$demo_state.c$64$5_0$246
;src/states/demo_state.c:64: waitForFrame();
	call	_waitForFrame
	jr	00104$
00106$:
	C$demo_state.c$66$4_0$245	= .
	.globl	C$demo_state.c$66$4_0$245
;src/states/demo_state.c:66: clear_game_lists();
	call	_clear_game_lists
	C$demo_state.c$67$4_0$245	= .
	.globl	C$demo_state.c$67$4_0$245
;src/states/demo_state.c:67: clear_input();
	call	_clear_input
	C$demo_state.c$68$4_0$245	= .
	.globl	C$demo_state.c$68$4_0$245
;src/states/demo_state.c:68: demo_state_status = 5;
	ld	hl, #_demo_state_status
	ld	(hl), #0x05
00108$:
	C$demo_state.c$70$3_0$244	= .
	.globl	C$demo_state.c$70$3_0$244
;src/states/demo_state.c:70: waitForFrame();
	call	_waitForFrame
	C$demo_state.c$71$3_0$244	= .
	.globl	C$demo_state.c$71$3_0$244
;src/states/demo_state.c:71: demo_duration++;
	ld	hl, (_demo_duration)
	inc	hl
	ld	(_demo_duration), hl
	ret
00114$:
	C$demo_state.c$73$3_0$247	= .
	.globl	C$demo_state.c$73$3_0$247
;src/states/demo_state.c:73: end_demo();   
	call	_end_demo
	C$demo_state.c$74$3_0$247	= .
	.globl	C$demo_state.c$74$3_0$247
;src/states/demo_state.c:74: general_fade_out();
	call	_general_fade_out
	C$demo_state.c$75$3_0$247	= .
	.globl	C$demo_state.c$75$3_0$247
;src/states/demo_state.c:75: blackBackgroundPalette();
	call	_SMS_zeroBGPalette
	C$demo_state.c$76$3_0$247	= .
	.globl	C$demo_state.c$76$3_0$247
;src/states/demo_state.c:76: blackSpritePalette();
	call	_SMS_zeroSpritePalette
	C$demo_state.c$77$3_0$247	= .
	.globl	C$demo_state.c$77$3_0$247
;src/states/demo_state.c:77: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$demo_state.c$78$3_0$247	= .
	.globl	C$demo_state.c$78$3_0$247
;src/states/demo_state.c:78: while(frame_cnt < M_PERIOD) {
00110$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00112$
	C$demo_state.c$79$4_0$248	= .
	.globl	C$demo_state.c$79$4_0$248
;src/states/demo_state.c:79: waitForFrame();
	call	_waitForFrame
	jr	00110$
00112$:
	C$demo_state.c$81$3_0$247	= .
	.globl	C$demo_state.c$81$3_0$247
;src/states/demo_state.c:81: clear_game_lists();
	call	_clear_game_lists
	C$demo_state.c$82$3_0$247	= .
	.globl	C$demo_state.c$82$3_0$247
;src/states/demo_state.c:82: clear_input();
	call	_clear_input
	C$demo_state.c$83$3_0$247	= .
	.globl	C$demo_state.c$83$3_0$247
;src/states/demo_state.c:83: demo_state_status = 4;
	ld	hl, #_demo_state_status
	ld	(hl), #0x04
	ret
00123$:
	C$demo_state.c$85$1_0$238	= .
	.globl	C$demo_state.c$85$1_0$238
;src/states/demo_state.c:85: } else if(demo_state_status == 4) {
	ld	a, (_demo_state_status+0)
	sub	a, #0x04
	jr	NZ, 00120$
	C$demo_state.c$86$2_0$249	= .
	.globl	C$demo_state.c$86$2_0$249
;src/states/demo_state.c:86: transition_to_state(SCORE_STATE);
	ld	a, #0x04
	jp	_transition_to_state
00120$:
	C$demo_state.c$87$1_0$238	= .
	.globl	C$demo_state.c$87$1_0$238
;src/states/demo_state.c:87: } else if(demo_state_status == 5) {
	ld	a, (_demo_state_status+0)
	sub	a, #0x05
	ret	NZ
	C$demo_state.c$88$2_0$250	= .
	.globl	C$demo_state.c$88$2_0$250
;src/states/demo_state.c:88: transition_to_state(TITLE_STATE);
	ld	a, #0x03
	C$demo_state.c$90$1_0$238	= .
	.globl	C$demo_state.c$90$1_0$238
;src/states/demo_state.c:90: }
	C$demo_state.c$90$1_0$238	= .
	.globl	C$demo_state.c$90$1_0$238
	XG$demo_state_update$0$0	= .
	.globl	XG$demo_state_update$0$0
	jp	_transition_to_state
Fdemo_state$__str_0$0_0$0 == .
___str_0:
	.ascii "LVL]GOAL^"
	.db 0x00
	G$demo_state_stop$0$0	= .
	.globl	G$demo_state_stop$0$0
	C$demo_state.c$92$1_0$252	= .
	.globl	C$demo_state.c$92$1_0$252
;src/states/demo_state.c:92: void demo_state_stop(void) {
;	---------------------------------
; Function demo_state_stop
; ---------------------------------
_demo_state_stop::
	C$demo_state.c$93$1_0$252	= .
	.globl	C$demo_state.c$93$1_0$252
;src/states/demo_state.c:93: clear_input();
	C$demo_state.c$94$1_0$252	= .
	.globl	C$demo_state.c$94$1_0$252
;src/states/demo_state.c:94: }
	C$demo_state.c$94$1_0$252	= .
	.globl	C$demo_state.c$94$1_0$252
	XG$demo_state_stop$0$0	= .
	.globl	XG$demo_state_stop$0$0
	jp	_clear_input
	.area _CODE
	.area _INITIALIZER
Fdemo_state$__xinit_demo_state_status$0_0$0 == .
__xinit__demo_state_status:
	.db #0x00	; 0
Fdemo_state$__xinit_startLevel$0_0$0 == .
__xinit__startLevel:
	.db #0x00	; 0
	.area _CABS (ABS)
