;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module intro_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGStop
	.globl _waitForFrame
	.globl _clear_tilemap
	.globl _getButtonsPressed
	.globl _update_input
	.globl _clear_input
	.globl _transition_to_state
	.globl _play_song
	.globl _start_fadeout_music
	.globl _start_fadein_music
	.globl _update_dialog_box
	.globl _create_dialog_box
	.globl _sprite_fade_in
	.globl _background_fade_in_to_sepia
	.globl _background_fade_in
	.globl _general_fade_out
	.globl _background_fade_out
	.globl _load_tilemap
	.globl _load_tileset_batches
	.globl _load_tileset
	.globl _currentLine
	.globl _intro_state_status
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _intro_state_start
	.globl _intro_state_update
	.globl _intro_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
G$intro_state_status$0_0$0==.
_intro_state_status::
	.ds 1
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$currentLine$0_0$0==.
_currentLine::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$intro_state_start$0$0	= .
	.globl	G$intro_state_start$0$0
	C$intro_state.c$18$0_0$209	= .
	.globl	C$intro_state.c$18$0_0$209
;src/states/intro_state.c:18: void intro_state_start(void) {
;	---------------------------------
; Function intro_state_start
; ---------------------------------
_intro_state_start::
	C$intro_state.c$19$1_0$209	= .
	.globl	C$intro_state.c$19$1_0$209
;src/states/intro_state.c:19: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$intro_state.c$20$1_0$209	= .
	.globl	C$intro_state.c$20$1_0$209
;src/states/intro_state.c:20: intro_state_status = 0;
	ld	hl, #_intro_state_status
	ld	(hl), #0x00
	C$intro_state.c$21$1_0$209	= .
	.globl	C$intro_state.c$21$1_0$209
;src/states/intro_state.c:21: currentLine = 0;
	ld	hl, #_currentLine
	ld	(hl), #0x00
	C$intro_state.c$22$1_0$209	= .
	.globl	C$intro_state.c$22$1_0$209
;src/states/intro_state.c:22: clear_tilemap();
	call	_clear_tilemap
	C$intro_state.c$23$1_0$209	= .
	.globl	C$intro_state.c$23$1_0$209
;src/states/intro_state.c:23: load_tileset(SIMPLE_FONT_ASSET,DIGITS_BASE_ADDRESS);
	ld	l, #0x18
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_load_tileset
	C$intro_state.c$24$1_0$209	= .
	.globl	C$intro_state.c$24$1_0$209
;src/states/intro_state.c:24: load_tileset(INTRO1_ASSETS,BACKGROUND_BASE_ADRESS);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x03
	call	_load_tileset
	C$intro_state.c$25$1_0$209	= .
	.globl	C$intro_state.c$25$1_0$209
;src/states/intro_state.c:25: load_tilemap(INTRO1_ASSETS);
	ld	a, #0x03
	C$intro_state.c$27$1_0$209	= .
	.globl	C$intro_state.c$27$1_0$209
;src/states/intro_state.c:27: }
	C$intro_state.c$27$1_0$209	= .
	.globl	C$intro_state.c$27$1_0$209
	XG$intro_state_start$0$0	= .
	.globl	XG$intro_state_start$0$0
	jp	_load_tilemap
	G$intro_state_update$0$0	= .
	.globl	G$intro_state_update$0$0
	C$intro_state.c$29$1_0$211	= .
	.globl	C$intro_state.c$29$1_0$211
;src/states/intro_state.c:29: void intro_state_update(void) {
;	---------------------------------
; Function intro_state_update
; ---------------------------------
_intro_state_update::
	C$intro_state.c$30$1_0$211	= .
	.globl	C$intro_state.c$30$1_0$211
;src/states/intro_state.c:30: if(intro_state_status == 0) {
	ld	a, (_intro_state_status+0)
	or	a, a
	jr	NZ, 00127$
	C$intro_state.c$31$2_0$212	= .
	.globl	C$intro_state.c$31$2_0$212
;src/states/intro_state.c:31: play_song(INTRO_OST);
	ld	a, #0x02
	call	_play_song
	C$intro_state.c$32$2_0$212	= .
	.globl	C$intro_state.c$32$2_0$212
;src/states/intro_state.c:32: start_fadein_music();
	call	_start_fadein_music
	C$intro_state.c$33$2_0$212	= .
	.globl	C$intro_state.c$33$2_0$212
;src/states/intro_state.c:33: background_fade_in_to_sepia(INTRO1_PALETTE,2);
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x03
	call	_background_fade_in_to_sepia
	C$intro_state.c$34$2_0$212	= .
	.globl	C$intro_state.c$34$2_0$212
;src/states/intro_state.c:34: sprite_fade_in(FONT_PALETTE,1);
	ld	l, #0x01
;	spillPairReg hl
;	spillPairReg hl
	xor	a, a
	call	_sprite_fade_in
	C$intro_state.c$35$2_0$212	= .
	.globl	C$intro_state.c$35$2_0$212
;src/states/intro_state.c:35: intro_state_status = 1;
	ld	hl, #_intro_state_status
	ld	(hl), #0x01
	ret
00127$:
	C$intro_state.c$36$1_0$211	= .
	.globl	C$intro_state.c$36$1_0$211
;src/states/intro_state.c:36: } else if(intro_state_status == 1) {
	ld	a, (_intro_state_status+0)
	dec	a
	jr	NZ, 00124$
	C$intro_state.c$37$2_0$213	= .
	.globl	C$intro_state.c$37$2_0$213
;src/states/intro_state.c:37: create_dialog_box();
	call	_create_dialog_box
	C$intro_state.c$38$2_0$213	= .
	.globl	C$intro_state.c$38$2_0$213
;src/states/intro_state.c:38: intro_state_status = 2;
	ld	hl, #_intro_state_status
	ld	(hl), #0x02
	ret
00124$:
	C$intro_state.c$39$1_0$211	= .
	.globl	C$intro_state.c$39$1_0$211
;src/states/intro_state.c:39: } else if(intro_state_status == 2) {
	ld	a, (_intro_state_status+0)
	sub	a, #0x02
	jp	NZ,00121$
	C$intro_state.c$40$2_0$214	= .
	.globl	C$intro_state.c$40$2_0$214
;src/states/intro_state.c:40: update_input();
	call	_update_input
	C$intro_state.c$41$2_0$214	= .
	.globl	C$intro_state.c$41$2_0$214
;src/states/intro_state.c:41: if(getButtonsPressed() & KEY_ONE_P1 || getButtonsPressed() & KEY_ONE_P2 || (currentLine>>1) > NUMBER_INTRO_SENTENCES+5) {
	call	_getButtonsPressed
	bit	4, e
	jr	NZ, 00101$
	call	_getButtonsPressed
	bit	2, d
	jr	NZ, 00101$
	ld	iy, #_currentLine
	ld	c, 0 (iy)
	srl	c
	ld	a, #0x12
	sub	a, c
	jr	NC, 00102$
00101$:
	C$intro_state.c$42$3_0$215	= .
	.globl	C$intro_state.c$42$3_0$215
;src/states/intro_state.c:42: intro_state_status = 3;
	ld	iy, #_intro_state_status
	ld	0 (iy), #0x03
	C$intro_state.c$43$3_0$215	= .
	.globl	C$intro_state.c$43$3_0$215
;src/states/intro_state.c:43: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
00102$:
	C$intro_state.c$45$2_0$214	= .
	.globl	C$intro_state.c$45$2_0$214
;src/states/intro_state.c:45: if((frame_cnt & 63) == 0) {
	ld	a, (_frame_cnt+0)
	and	a, #0x3f
	jp	NZ,_waitForFrame
	C$intro_state.c$46$3_0$216	= .
	.globl	C$intro_state.c$46$3_0$216
;src/states/intro_state.c:46: update_dialog_box(currentLine>>1, currentLine&1,0);
	ld	a, (_currentLine+0)
	and	a, #0x01
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (#_currentLine + 0)
	ld	c, a
	srl	c
	xor	a, a
	push	af
	inc	sp
	ld	a, c
	call	_update_dialog_box
	C$intro_state.c$47$3_0$216	= .
	.globl	C$intro_state.c$47$3_0$216
;src/states/intro_state.c:47: currentLine++;
	ld	iy, #_currentLine
	inc	0 (iy)
	C$intro_state.c$48$3_0$216	= .
	.globl	C$intro_state.c$48$3_0$216
;src/states/intro_state.c:48: switch (currentLine)
	ld	a,(_currentLine+0)
	cp	a,#0x04
	jr	Z, 00105$
	cp	a,#0x0c
	jr	Z, 00106$
	cp	a,#0x0d
	jr	Z, 00107$
	cp	a,#0x14
	jr	Z, 00108$
	cp	a,#0x15
	jr	Z, 00109$
	sub	a, #0x1a
	jr	Z, 00110$
	jp	_waitForFrame
	C$intro_state.c$50$4_0$217	= .
	.globl	C$intro_state.c$50$4_0$217
;src/states/intro_state.c:50: case 4:
00105$:
	C$intro_state.c$51$4_0$217	= .
	.globl	C$intro_state.c$51$4_0$217
;src/states/intro_state.c:51: background_fade_in(INTRO1_PALETTE,3);
;	spillPairReg hl
;	spillPairReg hl
	ld	a,#0x03
	ld	l,a
	call	_background_fade_in
	C$intro_state.c$52$4_0$217	= .
	.globl	C$intro_state.c$52$4_0$217
;src/states/intro_state.c:52: break;
	jp	_waitForFrame
	C$intro_state.c$53$4_0$217	= .
	.globl	C$intro_state.c$53$4_0$217
;src/states/intro_state.c:53: case 12:
00106$:
	C$intro_state.c$54$4_0$217	= .
	.globl	C$intro_state.c$54$4_0$217
;src/states/intro_state.c:54: background_fade_out();
	call	_background_fade_out
	C$intro_state.c$55$4_0$217	= .
	.globl	C$intro_state.c$55$4_0$217
;src/states/intro_state.c:55: load_tileset_batches(INTRO2_ASSETS, BACKGROUND_BASE_ADRESS);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x04
	call	_load_tileset_batches
	C$intro_state.c$56$4_0$217	= .
	.globl	C$intro_state.c$56$4_0$217
;src/states/intro_state.c:56: load_tilemap(INTRO2_ASSETS);
	ld	a, #0x04
	call	_load_tilemap
	C$intro_state.c$57$4_0$217	= .
	.globl	C$intro_state.c$57$4_0$217
;src/states/intro_state.c:57: break;
	jp	_waitForFrame
	C$intro_state.c$58$4_0$217	= .
	.globl	C$intro_state.c$58$4_0$217
;src/states/intro_state.c:58: case 13:
00107$:
	C$intro_state.c$59$4_0$217	= .
	.globl	C$intro_state.c$59$4_0$217
;src/states/intro_state.c:59: background_fade_in(INTRO2_PALETTE,2);
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x04
	call	_background_fade_in
	C$intro_state.c$60$4_0$217	= .
	.globl	C$intro_state.c$60$4_0$217
;src/states/intro_state.c:60: break;
	jp	_waitForFrame
	C$intro_state.c$61$4_0$217	= .
	.globl	C$intro_state.c$61$4_0$217
;src/states/intro_state.c:61: case 20:
00108$:
	C$intro_state.c$62$4_0$217	= .
	.globl	C$intro_state.c$62$4_0$217
;src/states/intro_state.c:62: background_fade_out();
	call	_background_fade_out
	C$intro_state.c$63$4_0$217	= .
	.globl	C$intro_state.c$63$4_0$217
;src/states/intro_state.c:63: load_tileset_batches(INTRO3_ASSETS, BACKGROUND_BASE_ADRESS);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x05
	call	_load_tileset_batches
	C$intro_state.c$64$4_0$217	= .
	.globl	C$intro_state.c$64$4_0$217
;src/states/intro_state.c:64: load_tilemap(INTRO3_ASSETS);
	ld	a, #0x05
	call	_load_tilemap
	C$intro_state.c$65$4_0$217	= .
	.globl	C$intro_state.c$65$4_0$217
;src/states/intro_state.c:65: break;
	jp	_waitForFrame
	C$intro_state.c$66$4_0$217	= .
	.globl	C$intro_state.c$66$4_0$217
;src/states/intro_state.c:66: case 21:
00109$:
	C$intro_state.c$67$4_0$217	= .
	.globl	C$intro_state.c$67$4_0$217
;src/states/intro_state.c:67: background_fade_in(INTRO3_PALETTE,2);;
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x05
	call	_background_fade_in
	C$intro_state.c$68$4_0$217	= .
	.globl	C$intro_state.c$68$4_0$217
;src/states/intro_state.c:68: break;
	jp	_waitForFrame
	C$intro_state.c$69$4_0$217	= .
	.globl	C$intro_state.c$69$4_0$217
;src/states/intro_state.c:69: case 26:
00110$:
	C$intro_state.c$70$4_0$217	= .
	.globl	C$intro_state.c$70$4_0$217
;src/states/intro_state.c:70: background_fade_in_to_sepia(INTRO3_PALETTE,3);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x05
	call	_background_fade_in_to_sepia
	C$intro_state.c$74$2_0$214	= .
	.globl	C$intro_state.c$74$2_0$214
;src/states/intro_state.c:74: }
	C$intro_state.c$76$2_0$214	= .
	.globl	C$intro_state.c$76$2_0$214
;src/states/intro_state.c:76: waitForFrame();
	jp	_waitForFrame
00121$:
	C$intro_state.c$77$1_0$211	= .
	.globl	C$intro_state.c$77$1_0$211
;src/states/intro_state.c:77: } else if(intro_state_status == 3) {
	ld	a, (_intro_state_status+0)
	sub	a, #0x03
	jr	NZ, 00118$
	C$intro_state.c$78$2_0$218	= .
	.globl	C$intro_state.c$78$2_0$218
;src/states/intro_state.c:78: start_fadeout_music();
	call	_start_fadeout_music
	C$intro_state.c$79$2_0$218	= .
	.globl	C$intro_state.c$79$2_0$218
;src/states/intro_state.c:79: general_fade_out();
	call	_general_fade_out
	C$intro_state.c$80$2_0$218	= .
	.globl	C$intro_state.c$80$2_0$218
;src/states/intro_state.c:80: intro_state_status = 4;    
	ld	hl, #_intro_state_status
	ld	(hl), #0x04
	ret
00118$:
	C$intro_state.c$81$1_0$211	= .
	.globl	C$intro_state.c$81$1_0$211
;src/states/intro_state.c:81: } else if(intro_state_status == 4) {
	ld	a, (_intro_state_status+0)
	sub	a, #0x04
	ret	NZ
	C$intro_state.c$82$2_0$219	= .
	.globl	C$intro_state.c$82$2_0$219
;src/states/intro_state.c:82: transition_to_state(TITLE_STATE);
	ld	a, #0x03
	C$intro_state.c$84$1_0$211	= .
	.globl	C$intro_state.c$84$1_0$211
;src/states/intro_state.c:84: }
	C$intro_state.c$84$1_0$211	= .
	.globl	C$intro_state.c$84$1_0$211
	XG$intro_state_update$0$0	= .
	.globl	XG$intro_state_update$0$0
	jp	_transition_to_state
	G$intro_state_stop$0$0	= .
	.globl	G$intro_state_stop$0$0
	C$intro_state.c$86$1_0$221	= .
	.globl	C$intro_state.c$86$1_0$221
;src/states/intro_state.c:86: void intro_state_stop(void) {
;	---------------------------------
; Function intro_state_stop
; ---------------------------------
_intro_state_stop::
	C$intro_state.c$87$1_0$221	= .
	.globl	C$intro_state.c$87$1_0$221
;src/states/intro_state.c:87: clear_input();
	call	_clear_input
	C$intro_state.c$88$1_0$221	= .
	.globl	C$intro_state.c$88$1_0$221
;src/states/intro_state.c:88: stop_music();
	C$intro_state.c$89$1_0$221	= .
	.globl	C$intro_state.c$89$1_0$221
;src/states/intro_state.c:89: }
	C$intro_state.c$89$1_0$221	= .
	.globl	C$intro_state.c$89$1_0$221
	XG$intro_state_stop$0$0	= .
	.globl	XG$intro_state_stop$0$0
	jp	_PSGStop
	.area _CODE
	.area _INITIALIZER
Fintro_state$__xinit_currentLine$0_0$0 == .
__xinit__currentLine:
	.db #0x00	; 0
	.area _CABS (ABS)
