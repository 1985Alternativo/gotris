;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.3.0 #14184 (Linux)
;--------------------------------------------------------
	.module music
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PlaySample
	.globl _initPSG
	.globl _PSGFrame
	.globl _PSGSFXStop
	.globl _PSGSFXPlay
	.globl _PSGResume
	.globl _PSGStop
	.globl _PSGPlay
	.globl _SMS_setLineCounter
	.globl _SMS_setLineInterruptHandler
	.globl _SMS_VDPturnOffFeature
	.globl _SMS_VDPturnOnFeature
	.globl _samples
	.globl _fxs
	.globl _musics
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _sample_bank
	.globl _fx_bank
	.globl _music_bank
	.globl _start_fadein_music
	.globl _start_fadeout_music
	.globl _full_volume
	.globl _play_song
	.globl _play_fx
	.globl _play_sample
	.globl _enable_music_handler
	.globl _disable_music_handler
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$musics$0_0$0==.
_musics::
	.ds 22
G$fxs$0_0$0==.
_fxs::
	.ds 10
G$samples$0_0$0==.
_samples::
	.ds 18
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$start_fadein_music$0$0	= .
	.globl	G$start_fadein_music$0$0
	C$music.c$82$0_0$138	= .
	.globl	C$music.c$82$0_0$138
;src/resources/music.c:82: void start_fadein_music(void) {
;	---------------------------------
; Function start_fadein_music
; ---------------------------------
_start_fadein_music::
	C$music.c$83$1_0$138	= .
	.globl	C$music.c$83$1_0$138
;src/resources/music.c:83: target_atenuation = 0;
	ld	hl, #_target_atenuation
	ld	(hl), #0x00
	C$music.c$84$1_0$138	= .
	.globl	C$music.c$84$1_0$138
;src/resources/music.c:84: current_atenuation = 252;
	ld	hl, #_current_atenuation
	ld	(hl), #0xfc
	C$music.c$85$1_0$138	= .
	.globl	C$music.c$85$1_0$138
;src/resources/music.c:85: }
	C$music.c$85$1_0$138	= .
	.globl	C$music.c$85$1_0$138
	XG$start_fadein_music$0$0	= .
	.globl	XG$start_fadein_music$0$0
	ret
G$music_bank$0_0$0 == .
_music_bank:
	.db #0x0b	; 11
	.db #0x0b	; 11
	.db #0x09	; 9
	.db #0x0e	; 14
	.db #0x10	; 16
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x04	; 4
	.db #0x04	; 4
G$fx_bank$0_0$0 == .
_fx_bank:
	.db #0x0b	; 11
	.db #0x03	; 3
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
G$sample_bank$0_0$0 == .
_sample_bank:
	.db #0x04	; 4
	.db #0x05	; 5
	.db #0x0e	; 14
	.db #0x0f	; 15
	.db #0x0f	; 15
	.db #0x0a	; 10
	.db #0x07	; 7
	.db #0x10	; 16
	.db #0x06	; 6
	G$start_fadeout_music$0$0	= .
	.globl	G$start_fadeout_music$0$0
	C$music.c$87$1_0$140	= .
	.globl	C$music.c$87$1_0$140
;src/resources/music.c:87: void start_fadeout_music(void) {
;	---------------------------------
; Function start_fadeout_music
; ---------------------------------
_start_fadeout_music::
	C$music.c$88$1_0$140	= .
	.globl	C$music.c$88$1_0$140
;src/resources/music.c:88: target_atenuation = 252;
	ld	hl, #_target_atenuation
	ld	(hl), #0xfc
	C$music.c$89$1_0$140	= .
	.globl	C$music.c$89$1_0$140
;src/resources/music.c:89: current_atenuation = 0;
	ld	hl, #_current_atenuation
	ld	(hl), #0x00
	C$music.c$90$1_0$140	= .
	.globl	C$music.c$90$1_0$140
;src/resources/music.c:90: }
	C$music.c$90$1_0$140	= .
	.globl	C$music.c$90$1_0$140
	XG$start_fadeout_music$0$0	= .
	.globl	XG$start_fadeout_music$0$0
	ret
	G$full_volume$0$0	= .
	.globl	G$full_volume$0$0
	C$music.c$92$1_0$142	= .
	.globl	C$music.c$92$1_0$142
;src/resources/music.c:92: void full_volume(void) {
;	---------------------------------
; Function full_volume
; ---------------------------------
_full_volume::
	C$music.c$93$1_0$142	= .
	.globl	C$music.c$93$1_0$142
;src/resources/music.c:93: target_atenuation = 0;
	ld	hl, #_target_atenuation
	ld	(hl), #0x00
	C$music.c$94$1_0$142	= .
	.globl	C$music.c$94$1_0$142
;src/resources/music.c:94: current_atenuation = 0;
	ld	hl, #_current_atenuation
	ld	(hl), #0x00
	C$music.c$95$1_0$142	= .
	.globl	C$music.c$95$1_0$142
;src/resources/music.c:95: }
	C$music.c$95$1_0$142	= .
	.globl	C$music.c$95$1_0$142
	XG$full_volume$0$0	= .
	.globl	XG$full_volume$0$0
	ret
	G$play_song$0$0	= .
	.globl	G$play_song$0$0
	C$music.c$97$1_0$144	= .
	.globl	C$music.c$97$1_0$144
;src/resources/music.c:97: void play_song(u8 songIndex) {
;	---------------------------------
; Function play_song
; ---------------------------------
_play_song::
	ld	e, a
	C$music.c$98$1_0$144	= .
	.globl	C$music.c$98$1_0$144
;src/resources/music.c:98: SMS_mapROMBank(music_bank[songIndex]);
	ld	hl, #_music_bank
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$music.c$99$1_0$144	= .
	.globl	C$music.c$99$1_0$144
;src/resources/music.c:99: current_music_bank = music_bank[songIndex];
	ld	(_current_music_bank+0), a
	C$music.c$100$1_0$144	= .
	.globl	C$music.c$100$1_0$144
;src/resources/music.c:100: PSGPlay(musics[songIndex]);
	ld	bc, #_musics+0
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, bc
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	ex	de, hl
	C$music.c$101$1_0$144	= .
	.globl	C$music.c$101$1_0$144
;src/resources/music.c:101: }
	C$music.c$101$1_0$144	= .
	.globl	C$music.c$101$1_0$144
	XG$play_song$0$0	= .
	.globl	XG$play_song$0$0
	jp	_PSGPlay
	G$play_fx$0$0	= .
	.globl	G$play_fx$0$0
	C$music.c$103$1_0$146	= .
	.globl	C$music.c$103$1_0$146
;src/resources/music.c:103: void play_fx(u8 fxIndex) {
;	---------------------------------
; Function play_fx
; ---------------------------------
_play_fx::
	ld	e, a
	C$music.c$104$1_0$146	= .
	.globl	C$music.c$104$1_0$146
;src/resources/music.c:104: SMS_mapROMBank(fx_bank[fxIndex]);
	ld	hl, #_fx_bank
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$music.c$105$1_0$146	= .
	.globl	C$music.c$105$1_0$146
;src/resources/music.c:105: current_fx_bank = fx_bank[fxIndex];
	ld	(_current_fx_bank+0), a
	C$music.c$106$1_0$146	= .
	.globl	C$music.c$106$1_0$146
;src/resources/music.c:106: PSGSFXPlay(fxs[fxIndex], SFX_CHANNEL2);
	ld	bc, #_fxs+0
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, bc
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	ld	a, #0x01
	push	af
	inc	sp
	ex	de, hl
	call	_PSGSFXPlay
	C$music.c$107$1_0$146	= .
	.globl	C$music.c$107$1_0$146
;src/resources/music.c:107: }
	C$music.c$107$1_0$146	= .
	.globl	C$music.c$107$1_0$146
	XG$play_fx$0$0	= .
	.globl	XG$play_fx$0$0
	ret
	G$play_sample$0$0	= .
	.globl	G$play_sample$0$0
	C$music.c$109$1_0$148	= .
	.globl	C$music.c$109$1_0$148
;src/resources/music.c:109: void play_sample(u8 sampleIndex) {
;	---------------------------------
; Function play_sample
; ---------------------------------
_play_sample::
	ld	e, a
	C$music.c$110$1_0$148	= .
	.globl	C$music.c$110$1_0$148
;src/resources/music.c:110: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$music.c$111$1_0$148	= .
	.globl	C$music.c$111$1_0$148
;src/resources/music.c:111: PSGStop();
	push	de
	call	_PSGStop
	call	_PSGSFXStop
	ld	hl, #_psgInit
	call	_initPSG
	pop	de
	C$music.c$115$1_0$148	= .
	.globl	C$music.c$115$1_0$148
;src/resources/music.c:115: SMS_mapROMBank(sample_bank[sampleIndex]);
	ld	hl, #_sample_bank
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$music.c$116$1_0$148	= .
	.globl	C$music.c$116$1_0$148
;src/resources/music.c:116: PlaySample(samples[sampleIndex]);
	ld	bc, #_samples+0
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, bc
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	ex	de, hl
	call	_PlaySample
	C$music.c$117$1_0$148	= .
	.globl	C$music.c$117$1_0$148
;src/resources/music.c:117: SMS_mapROMBank(current_music_bank);
	ld	a, (_current_music_bank+0)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$music.c$118$1_0$148	= .
	.globl	C$music.c$118$1_0$148
;src/resources/music.c:118: PSGResume();
	C$music.c$119$1_0$148	= .
	.globl	C$music.c$119$1_0$148
;src/resources/music.c:119: }
	C$music.c$119$1_0$148	= .
	.globl	C$music.c$119$1_0$148
	XG$play_sample$0$0	= .
	.globl	XG$play_sample$0$0
	jp	_PSGResume
	G$enable_music_handler$0$0	= .
	.globl	G$enable_music_handler$0$0
	C$music.c$121$1_0$150	= .
	.globl	C$music.c$121$1_0$150
;src/resources/music.c:121: void enable_music_handler(void) {
;	---------------------------------
; Function enable_music_handler
; ---------------------------------
_enable_music_handler::
	C$music.c$122$1_0$150	= .
	.globl	C$music.c$122$1_0$150
;src/resources/music.c:122: SMS_setLineInterruptHandler(&PSGFrame);
	ld	hl, #_PSGFrame
	call	_SMS_setLineInterruptHandler
	C$music.c$123$1_0$150	= .
	.globl	C$music.c$123$1_0$150
;src/resources/music.c:123: SMS_setLineCounter(190); 
	ld	l, #0xbe
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setLineCounter
	C$music.c$124$1_0$150	= .
	.globl	C$music.c$124$1_0$150
;src/resources/music.c:124: SMS_enableLineInterrupt();
	ld	hl, #0x0010
	C$music.c$125$1_0$150	= .
	.globl	C$music.c$125$1_0$150
;src/resources/music.c:125: }
	C$music.c$125$1_0$150	= .
	.globl	C$music.c$125$1_0$150
	XG$enable_music_handler$0$0	= .
	.globl	XG$enable_music_handler$0$0
	jp	_SMS_VDPturnOnFeature
	G$disable_music_handler$0$0	= .
	.globl	G$disable_music_handler$0$0
	C$music.c$127$1_0$152	= .
	.globl	C$music.c$127$1_0$152
;src/resources/music.c:127: void disable_music_handler(void) {
;	---------------------------------
; Function disable_music_handler
; ---------------------------------
_disable_music_handler::
	C$music.c$128$1_0$152	= .
	.globl	C$music.c$128$1_0$152
;src/resources/music.c:128: SMS_disableLineInterrupt();
	ld	hl, #0x0010
	C$music.c$129$1_0$152	= .
	.globl	C$music.c$129$1_0$152
;src/resources/music.c:129: }
	C$music.c$129$1_0$152	= .
	.globl	C$music.c$129$1_0$152
	XG$disable_music_handler$0$0	= .
	.globl	XG$disable_music_handler$0$0
	jp	_SMS_VDPturnOffFeature
	.area _CODE
	.area _INITIALIZER
Fmusic$__xinit_musics$0_0$0 == .
__xinit__musics:
	.dw _logo1985music_psg
	.dw _titlescreen_psg
	.dw _intro_psg
	.dw _highscore_psg
	.dw _credits_psg
	.dw _startjingle_psg
	.dw _ingame_psg
	.dw _hurryup_psg
	.dw _gameover_psg
	.dw _fanfa1_psg
	.dw _fanfa2_psg
Fmusic$__xinit_fxs$0_0$0 == .
__xinit__fxs:
	.dw _start_psg
	.dw _select_psg
	.dw _move_psg
	.dw _fall_psg
	.dw _destroy_psg
Fmusic$__xinit_samples$0_0$0 == .
__xinit__samples:
	.dw _thunder_bin
	.dw _gotris_bin
	.dw _getready_bin
	.dw _bonus_bin
	.dw _gameover1_bin
	.dw _gameover2_bin
	.dw _congratulations1_bin
	.dw _congratulations2_bin
	.dw _tuxedo_bin
	.area _CABS (ABS)
