echo Copiar musicas NTSC
cp ./music-source/ntsc/credits.psg ./assets
cp ./music-source/ntsc/gameover.psg ./assets
cp ./music-source/ntsc/highscore.psg ./assets
cp ./music-source/ntsc/hurryup.psg ./assets
cp ./music-source/ntsc/ingame.psg ./assets
cp ./music-source/ntsc/intro.psg ./assets
cp ./music-source/ntsc/startjingle.psg ./assets
cp ./music-source/ntsc/titlescreen.psg ./assets
cp ./music-source/ntsc/fanfa1.psg ./assets
cp ./music-source/ntsc/fanfa2.psg ./assets
cp ./music-source/ntsc/move.psg ./assets
cp ./music-source/ntsc/select.psg ./assets
cp ./music-source/ntsc/start.psg ./assets
assets2banks assets --firstbank=3 --compile --singleheader
cp ./assets2banks.h ./src
SYSTEM="NTSC_MACHINE"
docker run  --rm -v $PWD:/home/sms-tk/host -w /home/sms-tk/host -t retcon85/toolchain-sms -c "make build"
java -jar Emulicious.jar gotris.sms