@echo off
set PATH=%SDCC_SMS_PATH%\sdcc\bin;%PATH%
echo Reexportar graficos
.\tools\bmp2tile\bmp2tile.exe .\gfx\logo.png -mirror -removedupes -savetiles .\assets\logotiles.psgcompr -savepalette .\assets\logopalette.bin -savetilemap .\assets\logotilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\devkitsms.png -mirror -removedupes -savetiles .\assets\devkitsmstiles.psgcompr -savepalette .\assets\devkitsmspalette.bin -savetilemap .\assets\devkitsmstilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\pieces.png -noremovedupes -savetiles .\assets\piecestiles.psgcompr -savepalette .\assets\piecespalette.bin -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\background.png -mirror -removedupes -savetiles .\assets\backgroundtiles.psgcompr -savepalette .\assets\backgroundpalette.bin -tileoffset 63 -savetilemap .\assets\backgroundtilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\simplefont.png -removedupes -savetiles .\assets\simplefont.psgcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\titlescreen.png -mirror -removedupes -savetiles .\assets\titlescreentiles.psgcompr -savepalette .\assets\titlescreenpalette.bin -tileoffset 63 -savetilemap .\assets\titlescreentilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\scorescreen.png -mirror -removedupes -savetiles .\assets\scorescreentiles.psgcompr -savepalette .\assets\scorescreenpalette.bin -tileoffset 63 -savetilemap .\assets\scorescreentilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\credits.png -mirror -removedupes -savetiles .\assets\creditsscreentiles.psgcompr -savepalette .\assets\creditsscreenpalette.bin -tileoffset 63 -savetilemap .\assets\creditsscreentilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\gotrisintro1.png -mirror -removedupes -savetiles .\assets\intro1tiles.psgcompr -savepalette .\assets\intro1palette.bin -tileoffset 63 -savetilemap .\assets\intro1tilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\gotrisintro2.png -mirror -removedupes -savetiles .\assets\intro2tiles.bin -savepalette .\assets\intro2palette.bin -tileoffset 63 -savetilemap .\assets\intro2tilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\gotrisintro3.png -mirror -removedupes -savetiles .\assets\intro3tiles.bin -savepalette .\assets\intro3palette.bin -tileoffset 63 -savetilemap .\assets\intro3tilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\gotrisending1.png -mirror -removedupes -savetiles .\assets\ending1tiles.psgcompr -savepalette .\assets\ending1palette.bin -tileoffset 63 -savetilemap .\assets\ending1tilemap.stmcompr -exit
.\tools\bmp2tile\bmp2tile.exe .\gfx\gotrisending2.png -mirror -removedupes -savetiles .\assets\ending2tiles.bin -savepalette .\assets\ending2palette.bin -exit
echo Convertir ficheros de graficos a C y compilarlos
assets2banks assets --firstbank=3 --compile --singleheader
copy .\assets2banks.h .\src\
