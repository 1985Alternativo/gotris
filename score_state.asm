;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module score_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGStop
	.globl _waitForFrame
	.globl _getButtonsPressed
	.globl _update_input
	.globl _clear_input
	.globl _transition_to_state
	.globl _play_song
	.globl _start_fadeout_music
	.globl _start_fadein_music
	.globl _highscore_fade_in
	.globl _draw_highscore_table
	.globl _clear_scroll_title_screen
	.globl _general_fade_out
	.globl _load_highscore_assets
	.globl _score_state_status
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _score_state_start
	.globl _score_state_update
	.globl _score_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
G$score_state_status$0_0$0==.
_score_state_status::
	.ds 1
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$score_state_start$0$0	= .
	.globl	G$score_state_start$0$0
	C$score_state.c$17$0_0$209	= .
	.globl	C$score_state.c$17$0_0$209
;src/states/score_state.c:17: void score_state_start(void) {
;	---------------------------------
; Function score_state_start
; ---------------------------------
_score_state_start::
	C$score_state.c$18$1_0$209	= .
	.globl	C$score_state.c$18$1_0$209
;src/states/score_state.c:18: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$score_state.c$19$1_0$209	= .
	.globl	C$score_state.c$19$1_0$209
;src/states/score_state.c:19: score_state_status = 0;
	ld	hl, #_score_state_status
	ld	(hl), #0x00
	C$score_state.c$20$1_0$209	= .
	.globl	C$score_state.c$20$1_0$209
;src/states/score_state.c:20: clear_scroll_title_screen();
	call	_clear_scroll_title_screen
	C$score_state.c$21$1_0$209	= .
	.globl	C$score_state.c$21$1_0$209
;src/states/score_state.c:21: load_highscore_assets();
	C$score_state.c$22$1_0$209	= .
	.globl	C$score_state.c$22$1_0$209
;src/states/score_state.c:22: }
	C$score_state.c$22$1_0$209	= .
	.globl	C$score_state.c$22$1_0$209
	XG$score_state_start$0$0	= .
	.globl	XG$score_state_start$0$0
	jp	_load_highscore_assets
	G$score_state_update$0$0	= .
	.globl	G$score_state_update$0$0
	C$score_state.c$24$1_0$211	= .
	.globl	C$score_state.c$24$1_0$211
;src/states/score_state.c:24: void score_state_update(void) {
;	---------------------------------
; Function score_state_update
; ---------------------------------
_score_state_update::
	C$score_state.c$25$1_0$211	= .
	.globl	C$score_state.c$25$1_0$211
;src/states/score_state.c:25: if(score_state_status == 0) {
	ld	a, (_score_state_status+0)
	or	a, a
	jr	NZ, 00117$
	C$score_state.c$26$2_0$212	= .
	.globl	C$score_state.c$26$2_0$212
;src/states/score_state.c:26: start_fadein_music();
	call	_start_fadein_music
	C$score_state.c$27$2_0$212	= .
	.globl	C$score_state.c$27$2_0$212
;src/states/score_state.c:27: highscore_fade_in();
	call	_highscore_fade_in
	C$score_state.c$28$2_0$212	= .
	.globl	C$score_state.c$28$2_0$212
;src/states/score_state.c:28: score_state_status = 1;
	ld	hl, #_score_state_status
	ld	(hl), #0x01
	ret
00117$:
	C$score_state.c$29$1_0$211	= .
	.globl	C$score_state.c$29$1_0$211
;src/states/score_state.c:29: } else if(score_state_status == 1) {
	ld	a, (_score_state_status+0)
	dec	a
	jr	NZ, 00114$
	C$score_state.c$30$2_0$213	= .
	.globl	C$score_state.c$30$2_0$213
;src/states/score_state.c:30: draw_highscore_table();
	call	_draw_highscore_table
	C$score_state.c$31$2_0$213	= .
	.globl	C$score_state.c$31$2_0$213
;src/states/score_state.c:31: play_song(SCORE_OST);
	ld	a, #0x03
	call	_play_song
	C$score_state.c$32$2_0$213	= .
	.globl	C$score_state.c$32$2_0$213
;src/states/score_state.c:32: score_state_status = 2;
	ld	hl, #_score_state_status
	ld	(hl), #0x02
	ret
00114$:
	C$score_state.c$33$1_0$211	= .
	.globl	C$score_state.c$33$1_0$211
;src/states/score_state.c:33: } else if(score_state_status == 2) {
	ld	a, (_score_state_status+0)
	sub	a, #0x02
	jr	NZ, 00111$
	C$score_state.c$34$2_0$214	= .
	.globl	C$score_state.c$34$2_0$214
;src/states/score_state.c:34: update_input();
	call	_update_input
	C$score_state.c$35$2_0$214	= .
	.globl	C$score_state.c$35$2_0$214
;src/states/score_state.c:35: if(getButtonsPressed() & KEY_ONE_P1 || getButtonsPressed() & KEY_ONE_P2 || frame_cnt > XXL_PERIOD) {
	call	_getButtonsPressed
	bit	4, e
	jr	NZ, 00101$
	call	_getButtonsPressed
	bit	2, d
	jr	NZ, 00101$
	ld	a, #0xe0
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x01
	sbc	a, 1 (iy)
	jp	NC,_waitForFrame
00101$:
	C$score_state.c$36$3_0$215	= .
	.globl	C$score_state.c$36$3_0$215
;src/states/score_state.c:36: score_state_status = 3;
	ld	iy, #_score_state_status
	ld	0 (iy), #0x03
	C$score_state.c$38$2_0$214	= .
	.globl	C$score_state.c$38$2_0$214
;src/states/score_state.c:38: waitForFrame();
	jp	_waitForFrame
00111$:
	C$score_state.c$39$1_0$211	= .
	.globl	C$score_state.c$39$1_0$211
;src/states/score_state.c:39: } else if(score_state_status == 3) {
	ld	a, (_score_state_status+0)
	sub	a, #0x03
	jr	NZ, 00108$
	C$score_state.c$40$2_0$216	= .
	.globl	C$score_state.c$40$2_0$216
;src/states/score_state.c:40: start_fadeout_music();
	call	_start_fadeout_music
	C$score_state.c$41$2_0$216	= .
	.globl	C$score_state.c$41$2_0$216
;src/states/score_state.c:41: general_fade_out();
	call	_general_fade_out
	C$score_state.c$42$2_0$216	= .
	.globl	C$score_state.c$42$2_0$216
;src/states/score_state.c:42: score_state_status = 4;    
	ld	hl, #_score_state_status
	ld	(hl), #0x04
	ret
00108$:
	C$score_state.c$43$1_0$211	= .
	.globl	C$score_state.c$43$1_0$211
;src/states/score_state.c:43: } else if(score_state_status == 4) {
	ld	a, (_score_state_status+0)
	sub	a, #0x04
	ret	NZ
	C$score_state.c$44$2_0$217	= .
	.globl	C$score_state.c$44$2_0$217
;src/states/score_state.c:44: transition_to_state(TITLE_STATE);
	ld	a, #0x03
	C$score_state.c$46$1_0$211	= .
	.globl	C$score_state.c$46$1_0$211
;src/states/score_state.c:46: }
	C$score_state.c$46$1_0$211	= .
	.globl	C$score_state.c$46$1_0$211
	XG$score_state_update$0$0	= .
	.globl	XG$score_state_update$0$0
	jp	_transition_to_state
	G$score_state_stop$0$0	= .
	.globl	G$score_state_stop$0$0
	C$score_state.c$48$1_0$219	= .
	.globl	C$score_state.c$48$1_0$219
;src/states/score_state.c:48: void score_state_stop(void) {
;	---------------------------------
; Function score_state_stop
; ---------------------------------
_score_state_stop::
	C$score_state.c$49$1_0$219	= .
	.globl	C$score_state.c$49$1_0$219
;src/states/score_state.c:49: clear_input();
	call	_clear_input
	C$score_state.c$50$1_0$219	= .
	.globl	C$score_state.c$50$1_0$219
;src/states/score_state.c:50: stop_music();
	C$score_state.c$51$1_0$219	= .
	.globl	C$score_state.c$51$1_0$219
;src/states/score_state.c:51: }
	C$score_state.c$51$1_0$219	= .
	.globl	C$score_state.c$51$1_0$219
	XG$score_state_stop$0$0	= .
	.globl	XG$score_state_stop$0$0
	jp	_PSGStop
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
