@echo off
set PATH=%SDCC_SMS_PATH%\sdcc\bin;%PATH%
echo Copiar musicas PAL
copy .\music-source\pal\credits.psg .\assets
copy .\music-source\pal\gameover.psg .\assets
copy .\music-source\pal\highscore.psg .\assets
copy .\music-source\pal\hurryup.psg .\assets
copy .\music-source\pal\ingame.psg .\assets
copy .\music-source\pal\intro.psg .\assets
copy .\music-source\pal\startjingle.psg .\assets
copy .\music-source\pal\titlescreen.psg .\assets
copy .\music-source\pal\fanfa1.psg .\assets
copy .\music-source\pal\fanfa2.psg .\assets
copy .\music-source\pal\move.psg .\assets
copy .\music-source\pal\select.psg .\assets
copy .\music-source\pal\start.psg .\assets
assets2banks assets --firstbank=3 --compile --singleheader
copy .\assets2banks.h .\src
set SYSTEM=PAL_MACHINE
SET CURRENTDIR="%cd%"
SET DOCKER_FLAGS=--rm -v %CURRENTDIR%:/home/sms-tk/host -w /home/sms-tk/host -t retcon85/toolchain-sms -c
docker run  %DOCKER_FLAGS% "make build SYSTEM=PAL_MACHINE"
copy gotris.sms gotris-pal.sms
java -jar %EMULICIOUS_PATH%\Emulicious.jar gotris-pal.sms