;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module credits_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGStop
	.globl _waitForFrame
	.globl _clear_input
	.globl _transition_to_state
	.globl _play_song
	.globl _start_fadeout_music
	.globl _start_fadein_music
	.globl _disable_credits_effects
	.globl _rasterize_credits
	.globl _write_credits
	.globl _init_credits
	.globl _sprite_fade_in
	.globl _background_fade_in
	.globl _general_fade_out
	.globl _load_tilemap
	.globl _load_tileset
	.globl _credits_state_status
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _credits_state_start
	.globl _credits_state_update
	.globl _credits_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
G$credits_state_status$0_0$0==.
_credits_state_status::
	.ds 1
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$credits_state_start$0$0	= .
	.globl	G$credits_state_start$0$0
	C$credits_state.c$16$0_0$209	= .
	.globl	C$credits_state.c$16$0_0$209
;src/states/credits_state.c:16: void credits_state_start(void) {
;	---------------------------------
; Function credits_state_start
; ---------------------------------
_credits_state_start::
	C$credits_state.c$17$1_0$209	= .
	.globl	C$credits_state.c$17$1_0$209
;src/states/credits_state.c:17: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$credits_state.c$18$1_0$209	= .
	.globl	C$credits_state.c$18$1_0$209
;src/states/credits_state.c:18: credits_state_status = 0;
	ld	hl, #_credits_state_status
	ld	(hl), #0x00
	C$credits_state.c$19$1_0$209	= .
	.globl	C$credits_state.c$19$1_0$209
;src/states/credits_state.c:19: load_tileset(SIMPLE_FONT_ASSET,DIGITS_BASE_ADDRESS);
	ld	l, #0x18
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_load_tileset
	C$credits_state.c$20$1_0$209	= .
	.globl	C$credits_state.c$20$1_0$209
;src/states/credits_state.c:20: load_tileset(CREDITS_ASSETS,BACKGROUND_BASE_ADRESS);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x07
	call	_load_tileset
	C$credits_state.c$21$1_0$209	= .
	.globl	C$credits_state.c$21$1_0$209
;src/states/credits_state.c:21: load_tilemap(CREDITS_ASSETS);
	ld	a, #0x07
	C$credits_state.c$22$1_0$209	= .
	.globl	C$credits_state.c$22$1_0$209
;src/states/credits_state.c:22: }
	C$credits_state.c$22$1_0$209	= .
	.globl	C$credits_state.c$22$1_0$209
	XG$credits_state_start$0$0	= .
	.globl	XG$credits_state_start$0$0
	jp	_load_tilemap
	G$credits_state_update$0$0	= .
	.globl	G$credits_state_update$0$0
	C$credits_state.c$24$1_0$211	= .
	.globl	C$credits_state.c$24$1_0$211
;src/states/credits_state.c:24: void credits_state_update(void) {
;	---------------------------------
; Function credits_state_update
; ---------------------------------
_credits_state_update::
	C$credits_state.c$25$1_0$211	= .
	.globl	C$credits_state.c$25$1_0$211
;src/states/credits_state.c:25: if(credits_state_status == 0) {
	ld	a, (_credits_state_status+0)
	or	a, a
	jr	NZ, 00118$
	C$credits_state.c$26$2_0$212	= .
	.globl	C$credits_state.c$26$2_0$212
;src/states/credits_state.c:26: start_fadein_music();
	call	_start_fadein_music
	C$credits_state.c$27$2_0$212	= .
	.globl	C$credits_state.c$27$2_0$212
;src/states/credits_state.c:27: play_song(CREDITS_OST);
	ld	a, #0x04
	call	_play_song
	C$credits_state.c$28$2_0$212	= .
	.globl	C$credits_state.c$28$2_0$212
;src/states/credits_state.c:28: background_fade_in(CREDITS_PALETTE,1);
	ld	l, #0x01
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x09
	call	_background_fade_in
	C$credits_state.c$29$2_0$212	= .
	.globl	C$credits_state.c$29$2_0$212
;src/states/credits_state.c:29: sprite_fade_in(PIECES_PALETTE,1);
	ld	l, #0x01
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0a
	call	_sprite_fade_in
	C$credits_state.c$30$2_0$212	= .
	.globl	C$credits_state.c$30$2_0$212
;src/states/credits_state.c:30: credits_state_status = 1;
	ld	hl, #_credits_state_status
	ld	(hl), #0x01
	ret
00118$:
	C$credits_state.c$31$1_0$211	= .
	.globl	C$credits_state.c$31$1_0$211
;src/states/credits_state.c:31: } else if(credits_state_status == 1) {
	ld	a, (_credits_state_status+0)
	dec	a
	jr	NZ, 00115$
	C$credits_state.c$33$2_0$213	= .
	.globl	C$credits_state.c$33$2_0$213
;src/states/credits_state.c:33: init_credits(); 
	call	_init_credits
	C$credits_state.c$34$2_0$213	= .
	.globl	C$credits_state.c$34$2_0$213
;src/states/credits_state.c:34: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$credits_state.c$35$2_0$213	= .
	.globl	C$credits_state.c$35$2_0$213
;src/states/credits_state.c:35: credits_state_status = 2;
	ld	hl, #_credits_state_status
	ld	(hl), #0x02
	ret
00115$:
	C$credits_state.c$36$1_0$211	= .
	.globl	C$credits_state.c$36$1_0$211
;src/states/credits_state.c:36: } else if(credits_state_status == 2) {
	ld	a, (_credits_state_status+0)
	sub	a, #0x02
	jr	NZ, 00112$
	C$credits_state.c$37$2_0$214	= .
	.globl	C$credits_state.c$37$2_0$214
;src/states/credits_state.c:37: if(frame_cnt > XXXL_PERIOD) {
	ld	a, #0x60
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x09
	sbc	a, 1 (iy)
	jr	NC, 00102$
	C$credits_state.c$38$3_0$215	= .
	.globl	C$credits_state.c$38$3_0$215
;src/states/credits_state.c:38: credits_state_status = 3;
	ld	hl, #_credits_state_status
	ld	(hl), #0x03
00102$:
	C$credits_state.c$40$2_0$214	= .
	.globl	C$credits_state.c$40$2_0$214
;src/states/credits_state.c:40: write_credits(frame_cnt);
	ld	hl, (_frame_cnt)
	call	_write_credits
	C$credits_state.c$41$2_0$214	= .
	.globl	C$credits_state.c$41$2_0$214
;src/states/credits_state.c:41: waitForFrame();
	call	_waitForFrame
	C$credits_state.c$42$2_0$214	= .
	.globl	C$credits_state.c$42$2_0$214
;src/states/credits_state.c:42: rasterize_credits();
	jp	_rasterize_credits
00112$:
	C$credits_state.c$43$1_0$211	= .
	.globl	C$credits_state.c$43$1_0$211
;src/states/credits_state.c:43: } else if(credits_state_status == 3) {
	ld	a, (_credits_state_status+0)
	sub	a, #0x03
	jr	NZ, 00109$
	C$credits_state.c$44$2_0$216	= .
	.globl	C$credits_state.c$44$2_0$216
;src/states/credits_state.c:44: disable_credits_effects();
	call	_disable_credits_effects
	C$credits_state.c$45$2_0$216	= .
	.globl	C$credits_state.c$45$2_0$216
;src/states/credits_state.c:45: start_fadeout_music();
	call	_start_fadeout_music
	C$credits_state.c$46$2_0$216	= .
	.globl	C$credits_state.c$46$2_0$216
;src/states/credits_state.c:46: general_fade_out();
	call	_general_fade_out
	C$credits_state.c$47$2_0$216	= .
	.globl	C$credits_state.c$47$2_0$216
;src/states/credits_state.c:47: credits_state_status = 4;    
	ld	hl, #_credits_state_status
	ld	(hl), #0x04
	ret
00109$:
	C$credits_state.c$48$1_0$211	= .
	.globl	C$credits_state.c$48$1_0$211
;src/states/credits_state.c:48: } else if(credits_state_status == 4) {
	ld	a, (_credits_state_status+0)
	sub	a, #0x04
	ret	NZ
	C$credits_state.c$49$2_0$217	= .
	.globl	C$credits_state.c$49$2_0$217
;src/states/credits_state.c:49: if(highScoreReached) {
	ld	iy, #_highScoreReached
	bit	0, 0 (iy)
	jr	Z, 00104$
	C$credits_state.c$50$3_0$218	= .
	.globl	C$credits_state.c$50$3_0$218
;src/states/credits_state.c:50: transition_to_state(ENTER_SCORE_STATE);
	ld	a, #0x08
	jp	_transition_to_state
00104$:
	C$credits_state.c$52$3_0$219	= .
	.globl	C$credits_state.c$52$3_0$219
;src/states/credits_state.c:52: transition_to_state(TITLE_STATE);
	ld	a, #0x03
	C$credits_state.c$55$1_0$211	= .
	.globl	C$credits_state.c$55$1_0$211
;src/states/credits_state.c:55: }
	C$credits_state.c$55$1_0$211	= .
	.globl	C$credits_state.c$55$1_0$211
	XG$credits_state_update$0$0	= .
	.globl	XG$credits_state_update$0$0
	jp	_transition_to_state
	G$credits_state_stop$0$0	= .
	.globl	G$credits_state_stop$0$0
	C$credits_state.c$57$1_0$221	= .
	.globl	C$credits_state.c$57$1_0$221
;src/states/credits_state.c:57: void credits_state_stop(void) {
;	---------------------------------
; Function credits_state_stop
; ---------------------------------
_credits_state_stop::
	C$credits_state.c$58$1_0$221	= .
	.globl	C$credits_state.c$58$1_0$221
;src/states/credits_state.c:58: clear_input();
	call	_clear_input
	C$credits_state.c$59$1_0$221	= .
	.globl	C$credits_state.c$59$1_0$221
;src/states/credits_state.c:59: stop_music();
	C$credits_state.c$60$1_0$221	= .
	.globl	C$credits_state.c$60$1_0$221
;src/states/credits_state.c:60: }
	C$credits_state.c$60$1_0$221	= .
	.globl	C$credits_state.c$60$1_0$221
	XG$credits_state_stop$0$0	= .
	.globl	XG$credits_state_stop$0$0
	jp	_PSGStop
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
