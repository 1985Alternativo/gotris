;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module board
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _SMS_crt0_RST18
	.globl _SMS_crt0_RST08
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _init_board
	.globl _clear_suitable_columns
	.globl _delete_square_pieces_board
	.globl _delete_bonus_board
	.globl _create_individual_pieces_from_square
	.globl _create_individual_pieces_from_board_position
	.globl _rotate_square_piece
	.globl _rotate_inv_square_piece
	.globl _delete_individual_pieces_board
	.globl _put_individual_pieces_board
	.globl _reverse_piece_board
	.globl _destroy_piece_board
	.globl _mark_position_for_change_in_board
	.globl _mark_position_for_change_in_board_rec
	.globl _put_square_pieces_board
	.globl _put_bonus_board
	.globl _compute_path
	.globl _neighbour_emptyspace
	.globl _neighbour_connected
	.globl _neighbour_notprocessed
	.globl _mark_as_disconnected
	.globl _destroy_disconnected
	.globl _clean_explosion
	.globl _clear_board_position
	.globl _block_board_position
	.globl _add_piece_board_position
	.globl _getCollisionRowFromColumn
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$init_board$0$0	= .
	.globl	G$init_board$0$0
	C$board.c$11$0_0$168	= .
	.globl	C$board.c$11$0_0$168
;src/engine/board.c:11: void init_board(void) {
;	---------------------------------
; Function init_board
; ---------------------------------
_init_board::
	C$board.c$14$3_0$170	= .
	.globl	C$board.c$14$3_0$170
;src/engine/board.c:14: while(i<BOARD_ROWS) {
	ld	c, #0x00
00104$:
	ld	a, c
	sub	a, #0x16
	jr	NC, 00106$
	C$board.c$16$1_0$168	= .
	.globl	C$board.c$16$1_0$168
;src/engine/board.c:16: while(j<BOARD_COLUMNS) {
	ld	b, #0x00
00101$:
	ld	a, b
	sub	a, #0x0a
	jr	NC, 00103$
	C$board.c$17$3_0$170	= .
	.globl	C$board.c$17$3_0$170
;src/engine/board.c:17: board[(i << 3) + (i << 1) + j] = EMPTY_CELL;
	ld	e, c
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	add	hl, hl
	ex	de, hl
	add	hl, de
	ex	de, hl
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	(hl), #0x00
	C$board.c$18$3_0$170	= .
	.globl	C$board.c$18$3_0$170
;src/engine/board.c:18: j++;
	inc	b
	jr	00101$
00103$:
	C$board.c$20$2_0$169	= .
	.globl	C$board.c$20$2_0$169
;src/engine/board.c:20: i++;
	inc	c
	jr	00104$
00106$:
	C$board.c$23$2_0$171	= .
	.globl	C$board.c$23$2_0$171
;src/engine/board.c:23: while(j<BOARD_COLUMNS) {
	ld	c, #0x00
00107$:
	ld	a, c
	sub	a, #0x0a
	ret	NC
	C$board.c$24$2_0$171	= .
	.globl	C$board.c$24$2_0$171
;src/engine/board.c:24: suitable_columns[j] = NO_SUITABLE;
	ld	hl, #_suitable_columns
	ld	b, #0x00
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$25$2_0$171	= .
	.globl	C$board.c$25$2_0$171
;src/engine/board.c:25: j++;
	inc	c
	C$board.c$27$1_0$168	= .
	.globl	C$board.c$27$1_0$168
;src/engine/board.c:27: }
	C$board.c$27$1_0$168	= .
	.globl	C$board.c$27$1_0$168
	XG$init_board$0$0	= .
	.globl	XG$init_board$0$0
	jr	00107$
	G$clear_suitable_columns$0$0	= .
	.globl	G$clear_suitable_columns$0$0
	C$board.c$29$1_0$173	= .
	.globl	C$board.c$29$1_0$173
;src/engine/board.c:29: void clear_suitable_columns(void) {
;	---------------------------------
; Function clear_suitable_columns
; ---------------------------------
_clear_suitable_columns::
	C$board.c$32$2_0$174	= .
	.globl	C$board.c$32$2_0$174
;src/engine/board.c:32: while(i<BOARD_COLUMNS) {
	ld	c, #0x00
00101$:
	ld	a, c
	sub	a, #0x0a
	ret	NC
	C$board.c$33$2_0$174	= .
	.globl	C$board.c$33$2_0$174
;src/engine/board.c:33: suitable_columns[i] = NO_SUITABLE;
	ld	hl, #_suitable_columns
	ld	b, #0x00
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$34$2_0$174	= .
	.globl	C$board.c$34$2_0$174
;src/engine/board.c:34: i++;
	inc	c
	C$board.c$36$1_0$173	= .
	.globl	C$board.c$36$1_0$173
;src/engine/board.c:36: }
	C$board.c$36$1_0$173	= .
	.globl	C$board.c$36$1_0$173
	XG$clear_suitable_columns$0$0	= .
	.globl	XG$clear_suitable_columns$0$0
	jr	00101$
	G$delete_square_pieces_board$0$0	= .
	.globl	G$delete_square_pieces_board$0$0
	C$board.c$38$1_0$176	= .
	.globl	C$board.c$38$1_0$176
;src/engine/board.c:38: void delete_square_pieces_board(void) {
;	---------------------------------
; Function delete_square_pieces_board
; ---------------------------------
_delete_square_pieces_board::
	C$board.c$39$1_0$176	= .
	.globl	C$board.c$39$1_0$176
;src/engine/board.c:39: u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
	ld	a, (#(_current_square + 1) + 0)
	ld	c, a
	add	a, a
	add	a, a
	add	a, a
	sla	c
	add	a, c
	ld	hl, #_current_square
	ld	c, (hl)
	add	a, c
	ld	e, a
	C$board.c$40$1_0$176	= .
	.globl	C$board.c$40$1_0$176
;src/engine/board.c:40: board[position] = EMPTY_CELL;
	ld	bc, #_board+0
	ld	l, e
	ld	h, #0x00
	add	hl, bc
	C$board.c$41$1_0$176	= .
	.globl	C$board.c$41$1_0$176
;src/engine/board.c:41: board[position+1] = EMPTY_CELL;
	ld	(hl), #0x00
	ld	d, (hl)
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$42$1_0$176	= .
	.globl	C$board.c$42$1_0$176
;src/engine/board.c:42: board[position+10] = EMPTY_CELL;
	ld	hl, #0x000a
	add	hl, de
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$43$1_0$176	= .
	.globl	C$board.c$43$1_0$176
;src/engine/board.c:43: board[position+11] = EMPTY_CELL;
	ld	hl, #0x000b
	add	hl, de
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$44$2_0$177	= .
	.globl	C$board.c$44$2_0$177
;src/engine/board.c:44: draw_board_position(current_square.x, current_square.y, EMPTY_CELL);
	ld	a, (#(_current_square + 1) + 0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0b
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	rst	#0x18
	C$board.c$45$2_0$178	= .
	.globl	C$board.c$45$2_0$178
;src/engine/board.c:45: draw_board_position(current_square.x+1, current_square.y, EMPTY_CELL);
	ld	a, (#(_current_square + 1) + 0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	rst	#0x18
	C$board.c$46$2_0$179	= .
	.globl	C$board.c$46$2_0$179
;src/engine/board.c:46: draw_board_position(current_square.x, current_square.y+1, EMPTY_CELL);
	ld	a, (#(_current_square + 1) + 0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0b
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	rst	#0x18
	C$board.c$47$2_0$180	= .
	.globl	C$board.c$47$2_0$180
;src/engine/board.c:47: draw_board_position(current_square.x+1, current_square.y+1, EMPTY_CELL);
	ld	a, (#(_current_square + 1) + 0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	C$board.c$48$1_0$176	= .
	.globl	C$board.c$48$1_0$176
;src/engine/board.c:48: }
	C$board.c$48$1_0$176	= .
	.globl	C$board.c$48$1_0$176
	XG$delete_square_pieces_board$0$0	= .
	.globl	XG$delete_square_pieces_board$0$0
	jp	_SMS_crt0_RST18
	G$delete_bonus_board$0$0	= .
	.globl	G$delete_bonus_board$0$0
	C$board.c$50$1_0$182	= .
	.globl	C$board.c$50$1_0$182
;src/engine/board.c:50: void delete_bonus_board(void) {
;	---------------------------------
; Function delete_bonus_board
; ---------------------------------
_delete_bonus_board::
	C$board.c$51$1_0$182	= .
	.globl	C$board.c$51$1_0$182
;src/engine/board.c:51: u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
	ld	bc, #_current_square+0
	ld	a, (#(_current_square + 1) + 0)
	ld	e, a
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	ld	e, a
	ld	a, (bc)
	add	a, e
	ld	e, a
	C$board.c$52$1_0$182	= .
	.globl	C$board.c$52$1_0$182
;src/engine/board.c:52: board[position] = EMPTY_CELL;
	ld	hl, #_board+0
	ld	d, #0x00
	add	hl, de
	ld	(hl), #0x00
	C$board.c$53$2_0$183	= .
	.globl	C$board.c$53$2_0$183
;src/engine/board.c:53: draw_board_position(current_square.x, current_square.y, EMPTY_CELL);
	ld	a, (#(_current_square + 1) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, (bc)
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	C$board.c$54$1_0$182	= .
	.globl	C$board.c$54$1_0$182
;src/engine/board.c:54: }
	C$board.c$54$1_0$182	= .
	.globl	C$board.c$54$1_0$182
	XG$delete_bonus_board$0$0	= .
	.globl	XG$delete_bonus_board$0$0
	jp	_SMS_crt0_RST18
	G$create_individual_pieces_from_square$0$0	= .
	.globl	G$create_individual_pieces_from_square$0$0
	C$board.c$57$1_0$185	= .
	.globl	C$board.c$57$1_0$185
;src/engine/board.c:57: void create_individual_pieces_from_square(void) {
;	---------------------------------
; Function create_individual_pieces_from_square
; ---------------------------------
_create_individual_pieces_from_square::
	C$board.c$59$1_0$185	= .
	.globl	C$board.c$59$1_0$185
;src/engine/board.c:59: active_pieces[piece_index].x = current_square.x;
	ld	bc, #_active_pieces+0
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	ld	a, (#_current_square + 0)
	ld	(de), a
	C$board.c$60$1_0$185	= .
	.globl	C$board.c$60$1_0$185
;src/engine/board.c:60: active_pieces[piece_index].y = current_square.y+1;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	ld	a, (#(_current_square + 1) + 0)
	inc	a
	ld	(de), a
	C$board.c$61$1_0$185	= .
	.globl	C$board.c$61$1_0$185
;src/engine/board.c:61: active_pieces[piece_index].color = current_square.color10;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	inc	de
	ld	a, (#_current_square + 4)
	ld	(de), a
	C$board.c$62$1_0$185	= .
	.globl	C$board.c$62$1_0$185
;src/engine/board.c:62: piece_index++;
	ld	iy, #_piece_index
	inc	0 (iy)
	C$board.c$63$1_0$185	= .
	.globl	C$board.c$63$1_0$185
;src/engine/board.c:63: active_pieces[piece_index].x = current_square.x+1;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	ld	a, (#_current_square + 0)
	inc	a
	ld	(de), a
	C$board.c$64$1_0$185	= .
	.globl	C$board.c$64$1_0$185
;src/engine/board.c:64: active_pieces[piece_index].y = current_square.y+1;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	ld	a, (#(_current_square + 1) + 0)
	inc	a
	ld	(de), a
	C$board.c$65$1_0$185	= .
	.globl	C$board.c$65$1_0$185
;src/engine/board.c:65: active_pieces[piece_index].color = current_square.color11;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	inc	de
	ld	a, (#_current_square + 5)
	ld	(de), a
	C$board.c$66$1_0$185	= .
	.globl	C$board.c$66$1_0$185
;src/engine/board.c:66: piece_index++;
	inc	0 (iy)
	C$board.c$67$1_0$185	= .
	.globl	C$board.c$67$1_0$185
;src/engine/board.c:67: active_pieces[piece_index].x = current_square.x;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	ld	a, (#_current_square + 0)
	ld	(de), a
	C$board.c$68$1_0$185	= .
	.globl	C$board.c$68$1_0$185
;src/engine/board.c:68: active_pieces[piece_index].y = current_square.y;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	ld	a, (#(_current_square + 1) + 0)
	ld	(de), a
	C$board.c$69$1_0$185	= .
	.globl	C$board.c$69$1_0$185
;src/engine/board.c:69: active_pieces[piece_index].color = current_square.color00;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	inc	de
	ld	a, (#_current_square + 2)
	ld	(de), a
	C$board.c$70$1_0$185	= .
	.globl	C$board.c$70$1_0$185
;src/engine/board.c:70: piece_index++;
	inc	0 (iy)
	C$board.c$71$1_0$185	= .
	.globl	C$board.c$71$1_0$185
;src/engine/board.c:71: active_pieces[piece_index].x = current_square.x+1;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	ld	a, (#_current_square + 0)
	inc	a
	ld	(de), a
	C$board.c$72$1_0$185	= .
	.globl	C$board.c$72$1_0$185
;src/engine/board.c:72: active_pieces[piece_index].y = current_square.y;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	ld	a, (#(_current_square + 1) + 0)
	ld	(de), a
	C$board.c$73$1_0$185	= .
	.globl	C$board.c$73$1_0$185
;src/engine/board.c:73: active_pieces[piece_index].color = current_square.color01;
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	inc	de
	inc	de
	ld	a, (#_current_square + 3)
	ld	(de), a
	C$board.c$74$1_0$185	= .
	.globl	C$board.c$74$1_0$185
;src/engine/board.c:74: piece_index++;
	inc	0 (iy)
	C$board.c$75$1_0$185	= .
	.globl	C$board.c$75$1_0$185
;src/engine/board.c:75: }
	C$board.c$75$1_0$185	= .
	.globl	C$board.c$75$1_0$185
	XG$create_individual_pieces_from_square$0$0	= .
	.globl	XG$create_individual_pieces_from_square$0$0
	ret
	G$create_individual_pieces_from_board_position$0$0	= .
	.globl	G$create_individual_pieces_from_board_position$0$0
	C$board.c$77$1_0$187	= .
	.globl	C$board.c$77$1_0$187
;src/engine/board.c:77: void create_individual_pieces_from_board_position(u8 x, u8 y, u8 position2check) {
;	---------------------------------
; Function create_individual_pieces_from_board_position
; ---------------------------------
_create_individual_pieces_from_board_position::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	b, a
	ld	c, l
	C$board.c$78$1_0$187	= .
	.globl	C$board.c$78$1_0$187
;src/engine/board.c:78: active_pieces[piece_index].x = x;
	ld	de, #_active_pieces+0
	push	de
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	pop	de
	add	hl, de
	ld	(hl), b
	C$board.c$79$1_0$187	= .
	.globl	C$board.c$79$1_0$187
;src/engine/board.c:79: active_pieces[piece_index].y = y;
	push	de
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	pop	de
	add	hl, de
	inc	hl
	ld	(hl), c
	C$board.c$80$1_0$187	= .
	.globl	C$board.c$80$1_0$187
;src/engine/board.c:80: if(board[position2check] == CONNECTED_WHITE_CELL || board[position2check] == NOT_PROCESSED_WHITE_CELL) {
	ld	bc, #_board+0
	ld	l, 4 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	c, (hl)
	C$board.c$78$1_0$187	= .
	.globl	C$board.c$78$1_0$187
;src/engine/board.c:78: active_pieces[piece_index].x = x;
	push	de
	ld	de, (_piece_index)
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	pop	de
	C$board.c$81$1_0$187	= .
	.globl	C$board.c$81$1_0$187
;src/engine/board.c:81: active_pieces[piece_index].color = WHITE_PIECE;
	add	hl, de
	inc	hl
	inc	hl
	C$board.c$80$1_0$187	= .
	.globl	C$board.c$80$1_0$187
;src/engine/board.c:80: if(board[position2check] == CONNECTED_WHITE_CELL || board[position2check] == NOT_PROCESSED_WHITE_CELL) {
	ld	a, c
	sub	a, #0x03
	jr	Z, 00104$
	ld	a, c
	dec	a
	jr	NZ, 00105$
00104$:
	C$board.c$81$2_0$188	= .
	.globl	C$board.c$81$2_0$188
;src/engine/board.c:81: active_pieces[piece_index].color = WHITE_PIECE;
	ld	(hl), #0x01
	jr	00106$
00105$:
	C$board.c$82$1_0$187	= .
	.globl	C$board.c$82$1_0$187
;src/engine/board.c:82: } else if(board[position2check] == CONNECTED_BLACK_CELL || board[position2check] == NOT_PROCESSED_BLACK_CELL) {
	ld	a, c
	sub	a, #0x06
	jr	Z, 00101$
	ld	a, c
	sub	a, #0x04
	jr	NZ, 00106$
00101$:
	C$board.c$83$2_0$189	= .
	.globl	C$board.c$83$2_0$189
;src/engine/board.c:83: active_pieces[piece_index].color = BLACK_PIECE;
	ld	(hl), #0x04
00106$:
	C$board.c$85$1_0$187	= .
	.globl	C$board.c$85$1_0$187
;src/engine/board.c:85: piece_index++;
	ld	iy, #_piece_index
	inc	0 (iy)
	C$board.c$86$1_0$187	= .
	.globl	C$board.c$86$1_0$187
;src/engine/board.c:86: }
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$rotate_square_piece$0$0	= .
	.globl	G$rotate_square_piece$0$0
	C$board.c$88$1_0$191	= .
	.globl	C$board.c$88$1_0$191
;src/engine/board.c:88: void rotate_square_piece(void) {
;	---------------------------------
; Function rotate_square_piece
; ---------------------------------
_rotate_square_piece::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$board.c$90$1_0$191	= .
	.globl	C$board.c$90$1_0$191
;src/engine/board.c:90: backupColor00 =  current_square.color00;
	ld	hl, #(_current_square + 2)
	ld	c, (hl)
	C$board.c$91$1_0$191	= .
	.globl	C$board.c$91$1_0$191
;src/engine/board.c:91: backupColor01 =  current_square.color01;
	ld	hl, #(_current_square + 3)
	ld	e, (hl)
	C$board.c$92$1_0$191	= .
	.globl	C$board.c$92$1_0$191
;src/engine/board.c:92: backupColor10 =  current_square.color10;
	ld	a, (#(_current_square + 4) + 0)
	ld	-1 (ix), a
	C$board.c$93$1_0$191	= .
	.globl	C$board.c$93$1_0$191
;src/engine/board.c:93: backupColor11 =  current_square.color11;
	ld	a, (#(_current_square + 5) + 0)
	C$board.c$94$1_0$191	= .
	.globl	C$board.c$94$1_0$191
;src/engine/board.c:94: current_square.color00 = backupColor01;
	ld	hl, #(_current_square + 2)
	ld	(hl), e
	C$board.c$95$1_0$191	= .
	.globl	C$board.c$95$1_0$191
;src/engine/board.c:95: current_square.color01 = backupColor11;
	ld	(#(_current_square + 3)),a
	C$board.c$96$1_0$191	= .
	.globl	C$board.c$96$1_0$191
;src/engine/board.c:96: current_square.color10 = backupColor00;
	ld	hl, #(_current_square + 4)
	ld	(hl), c
	C$board.c$97$1_0$191	= .
	.globl	C$board.c$97$1_0$191
;src/engine/board.c:97: current_square.color11 = backupColor10;
	ld	hl, #(_current_square + 5)
	ld	a, -1 (ix)
	ld	(hl), a
	C$board.c$98$1_0$191	= .
	.globl	C$board.c$98$1_0$191
;src/engine/board.c:98: }
	inc	sp
	pop	ix
	C$board.c$98$1_0$191	= .
	.globl	C$board.c$98$1_0$191
	XG$rotate_square_piece$0$0	= .
	.globl	XG$rotate_square_piece$0$0
	ret
	G$rotate_inv_square_piece$0$0	= .
	.globl	G$rotate_inv_square_piece$0$0
	C$board.c$100$1_0$193	= .
	.globl	C$board.c$100$1_0$193
;src/engine/board.c:100: void rotate_inv_square_piece(void) {
;	---------------------------------
; Function rotate_inv_square_piece
; ---------------------------------
_rotate_inv_square_piece::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$board.c$102$1_0$193	= .
	.globl	C$board.c$102$1_0$193
;src/engine/board.c:102: backupColor00 =  current_square.color00;
	ld	hl, #(_current_square + 2)
	ld	c, (hl)
	C$board.c$103$1_0$193	= .
	.globl	C$board.c$103$1_0$193
;src/engine/board.c:103: backupColor01 =  current_square.color01;
	ld	a, (#(_current_square + 3) + 0)
	ld	-1 (ix), a
	C$board.c$104$1_0$193	= .
	.globl	C$board.c$104$1_0$193
;src/engine/board.c:104: backupColor10 =  current_square.color10;
	ld	hl, #(_current_square + 4)
	ld	e, (hl)
	C$board.c$105$1_0$193	= .
	.globl	C$board.c$105$1_0$193
;src/engine/board.c:105: backupColor11 =  current_square.color11;
	ld	a, (#(_current_square + 5) + 0)
	C$board.c$106$1_0$193	= .
	.globl	C$board.c$106$1_0$193
;src/engine/board.c:106: current_square.color00 = backupColor10;
	ld	hl, #(_current_square + 2)
	ld	(hl), e
	C$board.c$107$1_0$193	= .
	.globl	C$board.c$107$1_0$193
;src/engine/board.c:107: current_square.color01 = backupColor00;
	ld	hl, #(_current_square + 3)
	ld	(hl), c
	C$board.c$108$1_0$193	= .
	.globl	C$board.c$108$1_0$193
;src/engine/board.c:108: current_square.color10 = backupColor11;
	ld	(#(_current_square + 4)),a
	C$board.c$109$1_0$193	= .
	.globl	C$board.c$109$1_0$193
;src/engine/board.c:109: current_square.color11 = backupColor01;
	ld	hl, #(_current_square + 5)
	ld	a, -1 (ix)
	ld	(hl), a
	C$board.c$110$1_0$193	= .
	.globl	C$board.c$110$1_0$193
;src/engine/board.c:110: }
	inc	sp
	pop	ix
	C$board.c$110$1_0$193	= .
	.globl	C$board.c$110$1_0$193
	XG$rotate_inv_square_piece$0$0	= .
	.globl	XG$rotate_inv_square_piece$0$0
	ret
	G$delete_individual_pieces_board$0$0	= .
	.globl	G$delete_individual_pieces_board$0$0
	C$board.c$112$1_0$195	= .
	.globl	C$board.c$112$1_0$195
;src/engine/board.c:112: void delete_individual_pieces_board(u8 index) {     
;	---------------------------------
; Function delete_individual_pieces_board
; ---------------------------------
_delete_individual_pieces_board::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	e, a
	C$board.c$113$1_0$195	= .
	.globl	C$board.c$113$1_0$195
;src/engine/board.c:113: u8 position = (active_pieces[index].y << 3) + (active_pieces[index].y << 1) + active_pieces[index].x;
	ld	bc, #_active_pieces+0
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	ld	c, e
	ld	b, d
	inc	bc
	ld	a, (bc)
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
	add	a, a
	add	a, a
	add	a, a
	sla	l
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (de)
	add	a, l
	ld	-1 (ix), a
	C$board.c$114$1_0$195	= .
	.globl	C$board.c$114$1_0$195
;src/engine/board.c:114: board[position] = EMPTY_CELL;
	ld	a, #<(_board)
	add	a, -1 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, #0x00
	ld	h, a
	ld	(hl), #0x00
	C$board.c$115$2_0$196	= .
	.globl	C$board.c$115$2_0$196
;src/engine/board.c:115: draw_board_position(active_pieces[index].x, active_pieces[index].y, EMPTY_CELL);
	ld	a, (bc)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, (de)
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	rst	#0x18
	C$board.c$116$1_0$195	= .
	.globl	C$board.c$116$1_0$195
;src/engine/board.c:116: }
	inc	sp
	pop	ix
	C$board.c$116$1_0$195	= .
	.globl	C$board.c$116$1_0$195
	XG$delete_individual_pieces_board$0$0	= .
	.globl	XG$delete_individual_pieces_board$0$0
	ret
	G$put_individual_pieces_board$0$0	= .
	.globl	G$put_individual_pieces_board$0$0
	C$board.c$118$1_0$198	= .
	.globl	C$board.c$118$1_0$198
;src/engine/board.c:118: void put_individual_pieces_board(u8 index) {
;	---------------------------------
; Function put_individual_pieces_board
; ---------------------------------
_put_individual_pieces_board::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	e, a
	C$board.c$119$1_0$198	= .
	.globl	C$board.c$119$1_0$198
;src/engine/board.c:119: u8 position = (active_pieces[index].y << 3) + (active_pieces[index].y << 1) + active_pieces[index].x;
	ld	bc, #_active_pieces+0
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, bc
	ex	de, hl
	ld	c, e
	ld	b, d
	inc	bc
	ld	a, (bc)
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
	add	a, a
	add	a, a
	add	a, a
	sla	l
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (de)
	add	a, l
	ld	-1 (ix), a
	C$board.c$120$1_0$198	= .
	.globl	C$board.c$120$1_0$198
;src/engine/board.c:120: board[position] = active_pieces[index].color;
	ld	hl, #_board+0
	ld	a, l
	add	a, -1 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	jr	NC, 00106$
	inc	h
00106$:
	push	de
	pop	iy
	ld	a, 2 (iy)
	inc	iy
	inc	iy
	ld	(hl), a
	C$board.c$121$2_0$199	= .
	.globl	C$board.c$121$2_0$199
;src/engine/board.c:121: draw_board_position(active_pieces[index].x, active_pieces[index].y, active_pieces[index].color);
	ld	a, (bc)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, (de)
	add	a, #0x0b
	ld	b, #0x00
	ld	c, a
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	push	iy
	rst	#0x08
	pop	iy
	ld	l, 0 (iy)
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$board.c$122$1_0$198	= .
	.globl	C$board.c$122$1_0$198
;src/engine/board.c:122: }
	inc	sp
	pop	ix
	C$board.c$122$1_0$198	= .
	.globl	C$board.c$122$1_0$198
	XG$put_individual_pieces_board$0$0	= .
	.globl	XG$put_individual_pieces_board$0$0
	ret
	G$reverse_piece_board$0$0	= .
	.globl	G$reverse_piece_board$0$0
	C$board.c$124$1_0$201	= .
	.globl	C$board.c$124$1_0$201
;src/engine/board.c:124: u8 reverse_piece_board(u8 x, u8 y) {
;	---------------------------------
; Function reverse_piece_board
; ---------------------------------
_reverse_piece_board::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	ld	c, a
	C$board.c$125$1_0$201	= .
	.globl	C$board.c$125$1_0$201
;src/engine/board.c:125: u8 position = (y << 3) + (y << 1) + x;
	ld	-1 (ix), l
	ld	e, l
	ld	a, e
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	add	a, c
	ld	e, a
	C$board.c$126$1_0$201	= .
	.globl	C$board.c$126$1_0$201
;src/engine/board.c:126: u8 original_color = NOT_PROCESSED_WHITE_CELL;
	ld	-2 (ix), #0x01
	C$board.c$127$1_0$201	= .
	.globl	C$board.c$127$1_0$201
;src/engine/board.c:127: if(board[position] == NOT_PROCESSED_WHITE_CELL) {
	ld	a, #<(_board)
	add	a, e
	ld	e, a
	ld	a, #>(_board)
	adc	a, #0x00
	ld	d, a
	ld	a, (de)
	cp	a, #0x01
	jr	NZ, 00116$
	C$board.c$128$2_0$202	= .
	.globl	C$board.c$128$2_0$202
;src/engine/board.c:128: board[position] = NOT_PROCESSED_BLACK_CELL;
	ld	a, #0x04
	ld	(de), a
	jr	00118$
00116$:
	C$board.c$129$1_0$201	= .
	.globl	C$board.c$129$1_0$201
;src/engine/board.c:129: } else if(board[position] == NOT_CONNECTED_WHITE_CELL) {
	cp	a, #0x02
	jr	NZ, 00113$
	C$board.c$130$2_0$203	= .
	.globl	C$board.c$130$2_0$203
;src/engine/board.c:130: board[position] = NOT_PROCESSED_BLACK_CELL;
	ld	a, #0x04
	ld	(de), a
	jr	00118$
00113$:
	C$board.c$131$1_0$201	= .
	.globl	C$board.c$131$1_0$201
;src/engine/board.c:131: } else if(board[position] == CONNECTED_WHITE_CELL) {
	cp	a, #0x03
	jr	NZ, 00110$
	C$board.c$132$2_0$204	= .
	.globl	C$board.c$132$2_0$204
;src/engine/board.c:132: board[position] = NOT_PROCESSED_BLACK_CELL;
	ld	a, #0x04
	ld	(de), a
	jr	00118$
00110$:
	C$board.c$133$1_0$201	= .
	.globl	C$board.c$133$1_0$201
;src/engine/board.c:133: } else if(board[position] == NOT_PROCESSED_BLACK_CELL) {
	cp	a, #0x04
	jr	NZ, 00107$
	C$board.c$134$2_0$205	= .
	.globl	C$board.c$134$2_0$205
;src/engine/board.c:134: original_color = NOT_PROCESSED_BLACK_CELL;
	ld	-2 (ix), #0x04
	C$board.c$135$2_0$205	= .
	.globl	C$board.c$135$2_0$205
;src/engine/board.c:135: board[position] = NOT_PROCESSED_WHITE_CELL;
	ld	a, #0x01
	ld	(de), a
	jr	00118$
00107$:
	C$board.c$136$1_0$201	= .
	.globl	C$board.c$136$1_0$201
;src/engine/board.c:136: } else if(board[position] == NOT_CONNECTED_BLACK_CELL) {
	cp	a, #0x05
	jr	NZ, 00104$
	C$board.c$137$2_0$206	= .
	.globl	C$board.c$137$2_0$206
;src/engine/board.c:137: original_color = NOT_PROCESSED_BLACK_CELL;
	ld	-2 (ix), #0x04
	C$board.c$138$2_0$206	= .
	.globl	C$board.c$138$2_0$206
;src/engine/board.c:138: board[position] = NOT_PROCESSED_WHITE_CELL;
	ld	a, #0x01
	ld	(de), a
	jr	00118$
00104$:
	C$board.c$139$1_0$201	= .
	.globl	C$board.c$139$1_0$201
;src/engine/board.c:139: } else if(board[position] == CONNECTED_BLACK_CELL) {
	sub	a, #0x06
	jr	NZ, 00118$
	C$board.c$140$2_0$207	= .
	.globl	C$board.c$140$2_0$207
;src/engine/board.c:140: original_color = NOT_PROCESSED_BLACK_CELL;
	ld	-2 (ix), #0x04
	C$board.c$141$2_0$207	= .
	.globl	C$board.c$141$2_0$207
;src/engine/board.c:141: board[position] = NOT_PROCESSED_WHITE_CELL;
	ld	a, #0x01
	ld	(de), a
	C$board.c$143$1_0$201	= .
	.globl	C$board.c$143$1_0$201
;src/engine/board.c:143: draw_board_position(x,y,board[position]);
00118$:
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x0b
	ld	b, #0x00
	ld	c, a
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, (de)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$board.c$144$1_0$201	= .
	.globl	C$board.c$144$1_0$201
;src/engine/board.c:144: return original_color;
	ld	a, -2 (ix)
	C$board.c$145$1_0$201	= .
	.globl	C$board.c$145$1_0$201
;src/engine/board.c:145: }
	ld	sp, ix
	pop	ix
	C$board.c$145$1_0$201	= .
	.globl	C$board.c$145$1_0$201
	XG$reverse_piece_board$0$0	= .
	.globl	XG$reverse_piece_board$0$0
	ret
	G$destroy_piece_board$0$0	= .
	.globl	G$destroy_piece_board$0$0
	C$board.c$147$1_0$210	= .
	.globl	C$board.c$147$1_0$210
;src/engine/board.c:147: void destroy_piece_board(u8 x, u8 y) {
;	---------------------------------
; Function destroy_piece_board
; ---------------------------------
_destroy_piece_board::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, a
	C$board.c$148$1_0$210	= .
	.globl	C$board.c$148$1_0$210
;src/engine/board.c:148: u8 position = (y << 3) + (y << 1) + x;
	ld	-1 (ix), l
	ld	e, l
	ld	a, e
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	add	a, c
	ld	e, a
	C$board.c$149$1_0$210	= .
	.globl	C$board.c$149$1_0$210
;src/engine/board.c:149: board[position] = EXPLODING_CELL;
	ld	hl, #_board+0
	ld	d, #0x00
	add	hl, de
	ld	(hl), #0x0b
	C$board.c$150$2_0$211	= .
	.globl	C$board.c$150$2_0$211
;src/engine/board.c:150: draw_board_position(x,y, EXPLODING_CELL);
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$151$1_0$210	= .
	.globl	C$board.c$151$1_0$210
;src/engine/board.c:151: }
	inc	sp
	pop	ix
	C$board.c$151$1_0$210	= .
	.globl	C$board.c$151$1_0$210
	XG$destroy_piece_board$0$0	= .
	.globl	XG$destroy_piece_board$0$0
	ret
	G$mark_position_for_change_in_board$0$0	= .
	.globl	G$mark_position_for_change_in_board$0$0
	C$board.c$153$1_0$213	= .
	.globl	C$board.c$153$1_0$213
;src/engine/board.c:153: void mark_position_for_change_in_board(u8 x, u8 y) {
;	---------------------------------
; Function mark_position_for_change_in_board
; ---------------------------------
_mark_position_for_change_in_board::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-18
	add	iy, sp
	ld	sp, iy
	ld	-1 (ix), a
	C$board.c$155$1_0$213	= .
	.globl	C$board.c$155$1_0$213
;src/engine/board.c:155: u8 position = (y << 3) + (y << 1) + x;
	ld	-2 (ix), l
	ld	a, l
	ld	-17 (ix), a
	add	a, a
	add	a, a
	add	a, a
	ld	-4 (ix), a
	ld	a, -17 (ix)
	add	a, a
	ld	-3 (ix), a
	add	a, -4 (ix)
	ld	-4 (ix), a
	ld	a, -1 (ix)
	ld	-3 (ix), a
	ld	a, -4 (ix)
	add	a, -3 (ix)
	C$board.c$156$1_0$213	= .
	.globl	C$board.c$156$1_0$213
;src/engine/board.c:156: if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
	ld	-4 (ix), a
	ld	-16 (ix), a
	ld	-15 (ix), #0x00
	C$board.c$158$1_0$213	= .
	.globl	C$board.c$158$1_0$213
;src/engine/board.c:158: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -2 (ix)
	ld	-14 (ix), a
	ld	-13 (ix), #0x00
	ld	a, -3 (ix)
	add	a, #0x0a
	ld	-6 (ix), a
	C$board.c$159$1_0$213	= .
	.globl	C$board.c$159$1_0$213
;src/engine/board.c:159: mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -3 (ix)
	dec	a
	ld	-12 (ix), a
	C$board.c$156$1_0$213	= .
	.globl	C$board.c$156$1_0$213
;src/engine/board.c:156: if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
	ld	a, -16 (ix)
	add	a, #0xff
	ld	-11 (ix), a
	ld	a, -15 (ix)
	adc	a, #0xff
	ld	-10 (ix), a
	C$board.c$158$1_0$213	= .
	.globl	C$board.c$158$1_0$213
;src/engine/board.c:158: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -14 (ix)
	add	a, #0x01
	ld	-5 (ix), a
	ld	a, -13 (ix)
	adc	a, #0x00
	ld	-4 (ix), a
	ld	a, -6 (ix)
	ld	-7 (ix), a
	ld	-6 (ix), #0x00
	ld	b, #0x05
00199$:
	sla	-5 (ix)
	rl	-4 (ix)
	djnz	00199$
	ld	a, -7 (ix)
	ld	-9 (ix), a
	ld	a, -6 (ix)
	ld	-8 (ix), a
	ld	a, -9 (ix)
	add	a, -5 (ix)
	ld	-7 (ix), a
	ld	a, -8 (ix)
	adc	a, -4 (ix)
	ld	-6 (ix), a
	sla	-7 (ix)
	rl	-6 (ix)
	ld	a, -7 (ix)
	ld	-9 (ix), a
	ld	a, -6 (ix)
	or	a, #0x78
	ld	-8 (ix), a
	C$board.c$156$1_0$213	= .
	.globl	C$board.c$156$1_0$213
;src/engine/board.c:156: if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00105$
	ld	a, #<(_board)
	add	a, -11 (ix)
	ld	-7 (ix), a
	ld	a, #>(_board)
	adc	a, -10 (ix)
	ld	-6 (ix), a
	ld	l, -7 (ix)
	ld	h, -6 (ix)
	ld	a, (hl)
	sub	a, #0x03
	jr	NZ, 00105$
	C$board.c$157$2_0$214	= .
	.globl	C$board.c$157$2_0$214
;src/engine/board.c:157: board[position-1] = NOT_PROCESSED_WHITE_CELL;
	ld	l, -7 (ix)
	ld	h, -6 (ix)
	ld	(hl), #0x01
	C$board.c$158$3_0$215	= .
	.globl	C$board.c$158$3_0$215
;src/engine/board.c:158: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	l, -9 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -8 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0801
	rst	#0x18
	C$board.c$159$2_0$214	= .
	.globl	C$board.c$159$2_0$214
;src/engine/board.c:159: mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, #0x01
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -12 (ix)
	call	_mark_position_for_change_in_board_rec
00105$:
	C$board.c$161$1_0$213	= .
	.globl	C$board.c$161$1_0$213
;src/engine/board.c:161: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
	ld	a, -1 (ix)
	sub	a, #0x09
	ld	a, #0x00
	rla
	ld	-7 (ix), a
	C$board.c$163$1_0$213	= .
	.globl	C$board.c$163$1_0$213
;src/engine/board.c:163: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -3 (ix)
	add	a, #0x0c
	C$board.c$164$1_0$213	= .
	.globl	C$board.c$164$1_0$213
;src/engine/board.c:164: mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	e, -3 (ix)
	inc	e
	C$board.c$161$1_0$213	= .
	.globl	C$board.c$161$1_0$213
;src/engine/board.c:161: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
	pop	hl
	pop	bc
	push	bc
	push	hl
	inc	bc
	C$board.c$163$1_0$213	= .
	.globl	C$board.c$163$1_0$213
;src/engine/board.c:163: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, l
	add	a, -5 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, h
	adc	a, -4 (ix)
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	a, h
	ld	-6 (ix), l
	or	a, #0x78
	ld	-5 (ix), a
	C$board.c$161$1_0$213	= .
	.globl	C$board.c$161$1_0$213
;src/engine/board.c:161: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
	ld	a, -7 (ix)
	or	a, a
	jr	Z, 00111$
	ld	hl, #_board+0
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x03
	jr	NZ, 00111$
	C$board.c$162$2_0$216	= .
	.globl	C$board.c$162$2_0$216
;src/engine/board.c:162: board[position+1] = NOT_PROCESSED_WHITE_CELL;
	ld	(hl), #0x01
	C$board.c$163$3_0$217	= .
	.globl	C$board.c$163$3_0$217
;src/engine/board.c:163: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	push	bc
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	pop	bc
	ld	hl, #0x0801
	rst	#0x18
	C$board.c$164$2_0$216	= .
	.globl	C$board.c$164$2_0$216
;src/engine/board.c:164: mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_WHITE_CELL);
	push	bc
	push	de
	ld	a, #0x01
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, e
	call	_mark_position_for_change_in_board_rec
	pop	de
	pop	bc
00111$:
	C$board.c$166$1_0$213	= .
	.globl	C$board.c$166$1_0$213
;src/engine/board.c:166: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
	ld	a, -2 (ix)
	sub	a, #0x15
	ld	a, #0x00
	rla
	ld	-4 (ix), a
	C$board.c$168$1_0$213	= .
	.globl	C$board.c$168$1_0$213
;src/engine/board.c:168: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -3 (ix)
	add	a, #0x0b
	ld	-3 (ix), a
	C$board.c$169$1_0$213	= .
	.globl	C$board.c$169$1_0$213
;src/engine/board.c:169: mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	d, -17 (ix)
	inc	d
	C$board.c$166$1_0$213	= .
	.globl	C$board.c$166$1_0$213
;src/engine/board.c:166: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
	ld	a, -16 (ix)
	add	a, #0x0a
	ld	-18 (ix), a
	ld	a, -15 (ix)
	adc	a, #0x00
	ld	-17 (ix), a
	C$board.c$168$1_0$213	= .
	.globl	C$board.c$168$1_0$213
;src/engine/board.c:168: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	l, -14 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -13 (ix)
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	a, -3 (ix)
	push	iy
	ex	(sp), hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ex	(sp), hl
	pop	iy
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	iy
	ld	a, -19 (ix)
	pop	iy
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	a, h
	ld	-14 (ix), l
	or	a, #0x78
	ld	-13 (ix), a
	C$board.c$166$1_0$213	= .
	.globl	C$board.c$166$1_0$213
;src/engine/board.c:166: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
	ld	a, -4 (ix)
	or	a, a
	jr	Z, 00117$
	ld	a, #<(_board)
	add	a, -18 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, -17 (ix)
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (hl)
	sub	a, #0x03
	jr	NZ, 00117$
	C$board.c$167$2_0$218	= .
	.globl	C$board.c$167$2_0$218
;src/engine/board.c:167: board[position+10] = NOT_PROCESSED_WHITE_CELL;
	ld	(hl), #0x01
	C$board.c$168$3_0$219	= .
	.globl	C$board.c$168$3_0$219
;src/engine/board.c:168: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	push	bc
	ld	l, -14 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -13 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	pop	bc
	ld	hl, #0x0801
	rst	#0x18
	C$board.c$169$2_0$218	= .
	.globl	C$board.c$169$2_0$218
;src/engine/board.c:169: mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_WHITE_CELL);
	push	bc
	push	de
	ld	a, #0x01
	push	af
	inc	sp
	ld	l, d
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_mark_position_for_change_in_board_rec
	pop	de
	pop	bc
00117$:
	C$board.c$171$1_0$213	= .
	.globl	C$board.c$171$1_0$213
;src/engine/board.c:171: if(x > 0 && board[position-1] == CONNECTED_BLACK_CELL) {
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00123$
	ld	a, #<(_board)
	add	a, -11 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, -10 (ix)
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (hl)
	sub	a, #0x06
	jr	NZ, 00123$
	C$board.c$172$2_0$220	= .
	.globl	C$board.c$172$2_0$220
;src/engine/board.c:172: board[position-1] = NOT_PROCESSED_BLACK_CELL;
	ld	(hl), #0x04
	C$board.c$173$3_0$221	= .
	.globl	C$board.c$173$3_0$221
;src/engine/board.c:173: draw_board_position(x-1, y, NOT_PROCESSED_BLACK_CELL);
	push	bc
	ld	l, -9 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -8 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	pop	bc
	ld	hl, #0x0804
	rst	#0x18
	C$board.c$174$2_0$220	= .
	.globl	C$board.c$174$2_0$220
;src/engine/board.c:174: mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_BLACK_CELL);
	push	bc
	push	de
	ld	a, #0x04
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -12 (ix)
	call	_mark_position_for_change_in_board_rec
	pop	de
	pop	bc
00123$:
	C$board.c$176$1_0$213	= .
	.globl	C$board.c$176$1_0$213
;src/engine/board.c:176: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_BLACK_CELL) {
	ld	a, -7 (ix)
	or	a, a
	jr	Z, 00129$
	ld	hl, #_board+0
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x06
	jr	NZ, 00129$
	C$board.c$177$2_0$222	= .
	.globl	C$board.c$177$2_0$222
;src/engine/board.c:177: board[position+1] = NOT_PROCESSED_BLACK_CELL;
	ld	(hl), #0x04
	C$board.c$178$3_0$223	= .
	.globl	C$board.c$178$3_0$223
;src/engine/board.c:178: draw_board_position(x+1, y, NOT_PROCESSED_BLACK_CELL);
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0804
	rst	#0x18
	C$board.c$179$2_0$222	= .
	.globl	C$board.c$179$2_0$222
;src/engine/board.c:179: mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_BLACK_CELL);
	push	de
	ld	a, #0x04
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, e
	call	_mark_position_for_change_in_board_rec
	pop	de
00129$:
	C$board.c$181$1_0$213	= .
	.globl	C$board.c$181$1_0$213
;src/engine/board.c:181: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_BLACK_CELL) {
	ld	a, -4 (ix)
	or	a, a
	jr	Z, 00137$
	ld	bc, #_board+0
	pop	hl
	push	hl
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x06
	jr	NZ, 00137$
	C$board.c$182$2_0$224	= .
	.globl	C$board.c$182$2_0$224
;src/engine/board.c:182: board[position+10] = NOT_PROCESSED_BLACK_CELL;
	ld	(hl), #0x04
	C$board.c$183$3_0$225	= .
	.globl	C$board.c$183$3_0$225
;src/engine/board.c:183: draw_board_position(x, y+1, NOT_PROCESSED_BLACK_CELL);
	ld	l, -14 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -13 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0804
	rst	#0x18
	C$board.c$184$2_0$224	= .
	.globl	C$board.c$184$2_0$224
;src/engine/board.c:184: mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_BLACK_CELL);
	ld	a, #0x04
	push	af
	inc	sp
	ld	l, d
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_mark_position_for_change_in_board_rec
00137$:
	C$board.c$186$1_0$213	= .
	.globl	C$board.c$186$1_0$213
;src/engine/board.c:186: }
	ld	sp, ix
	pop	ix
	C$board.c$186$1_0$213	= .
	.globl	C$board.c$186$1_0$213
	XG$mark_position_for_change_in_board$0$0	= .
	.globl	XG$mark_position_for_change_in_board$0$0
	ret
	G$mark_position_for_change_in_board_rec$0$0	= .
	.globl	G$mark_position_for_change_in_board_rec$0$0
	C$board.c$188$1_0$227	= .
	.globl	C$board.c$188$1_0$227
;src/engine/board.c:188: void mark_position_for_change_in_board_rec(u8 x, u8 y, u8 color) {
;	---------------------------------
; Function mark_position_for_change_in_board_rec
; ---------------------------------
_mark_position_for_change_in_board_rec::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-30
	add	iy, sp
	ld	sp, iy
	ld	-1 (ix), a
	C$board.c$189$1_0$227	= .
	.globl	C$board.c$189$1_0$227
;src/engine/board.c:189: u8 position = (y << 3) + (y << 1) + x;
	ld	-2 (ix), l
	ld	a, l
	ld	-8 (ix), a
	add	a, a
	add	a, a
	add	a, a
	ld	c, a
	ld	a, -8 (ix)
	add	a, a
	add	a, c
	ld	-3 (ix), a
	ld	a, -1 (ix)
	ld	-6 (ix), a
	add	a, -3 (ix)
	C$board.c$191$1_0$227	= .
	.globl	C$board.c$191$1_0$227
;src/engine/board.c:191: if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
	ld	-3 (ix), a
	ld	-5 (ix), a
	ld	-4 (ix), #0x00
	C$board.c$193$1_0$227	= .
	.globl	C$board.c$193$1_0$227
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -2 (ix)
	ld	-12 (ix), a
	ld	-11 (ix), #0x00
	ld	a, -6 (ix)
	add	a, #0x0a
	ld	-3 (ix), a
	C$board.c$194$1_0$227	= .
	.globl	C$board.c$194$1_0$227
;src/engine/board.c:194: mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -6 (ix)
	dec	a
	ld	-30 (ix), a
	C$board.c$196$1_0$227	= .
	.globl	C$board.c$196$1_0$227
;src/engine/board.c:196: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
	ld	a, -1 (ix)
	sub	a, #0x09
	ld	a, #0x00
	rla
	ld	-29 (ix), a
	C$board.c$198$1_0$227	= .
	.globl	C$board.c$198$1_0$227
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -6 (ix)
	add	a, #0x0c
	ld	-7 (ix), a
	C$board.c$199$1_0$227	= .
	.globl	C$board.c$199$1_0$227
;src/engine/board.c:199: mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -6 (ix)
	inc	a
	ld	-28 (ix), a
	C$board.c$203$1_0$227	= .
	.globl	C$board.c$203$1_0$227
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -6 (ix)
	add	a, #0x0b
	ld	-6 (ix), a
	C$board.c$204$1_0$227	= .
	.globl	C$board.c$204$1_0$227
;src/engine/board.c:204: mark_position_for_change_in_board_rec(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -8 (ix)
	dec	a
	ld	-27 (ix), a
	C$board.c$206$1_0$227	= .
	.globl	C$board.c$206$1_0$227
;src/engine/board.c:206: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
	ld	a, -2 (ix)
	sub	a, #0x15
	ld	a, #0x00
	rla
	ld	-26 (ix), a
	C$board.c$209$1_0$227	= .
	.globl	C$board.c$209$1_0$227
;src/engine/board.c:209: mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -8 (ix)
	inc	a
	ld	-25 (ix), a
	C$board.c$191$1_0$227	= .
	.globl	C$board.c$191$1_0$227
;src/engine/board.c:191: if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
	ld	a, -5 (ix)
	add	a, #0xff
	ld	-24 (ix), a
	ld	a, -4 (ix)
	adc	a, #0xff
	ld	-23 (ix), a
	C$board.c$193$1_0$227	= .
	.globl	C$board.c$193$1_0$227
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -12 (ix)
	add	a, #0x01
	ld	-10 (ix), a
	ld	a, -11 (ix)
	adc	a, #0x00
	ld	-9 (ix), a
	C$board.c$196$1_0$227	= .
	.globl	C$board.c$196$1_0$227
;src/engine/board.c:196: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
	ld	a, -5 (ix)
	add	a, #0x01
	ld	-22 (ix), a
	ld	a, -4 (ix)
	adc	a, #0x00
	ld	-21 (ix), a
	C$board.c$198$1_0$227	= .
	.globl	C$board.c$198$1_0$227
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -7 (ix)
	ld	-8 (ix), a
	C$board.c$201$1_0$227	= .
	.globl	C$board.c$201$1_0$227
;src/engine/board.c:201: if(y > 0 && board[position-10] == CONNECTED_WHITE_CELL) {
	ld	a, -5 (ix)
	add	a, #0xf6
	ld	-20 (ix), a
	ld	a, -4 (ix)
	adc	a, #0xff
	ld	-19 (ix), a
	C$board.c$203$1_0$227	= .
	.globl	C$board.c$203$1_0$227
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -12 (ix)
	ld	-14 (ix), a
	ld	a, -11 (ix)
	ld	-13 (ix), a
	ld	a, -6 (ix)
	ld	-7 (ix), a
	C$board.c$206$1_0$227	= .
	.globl	C$board.c$206$1_0$227
;src/engine/board.c:206: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
	ld	a, -5 (ix)
	add	a, #0x0a
	ld	-18 (ix), a
	ld	a, -4 (ix)
	adc	a, #0x00
	ld	-17 (ix), a
	C$board.c$208$1_0$227	= .
	.globl	C$board.c$208$1_0$227
;src/engine/board.c:208: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -12 (ix)
	add	a, #0x02
	ld	-6 (ix), a
	ld	a, -11 (ix)
	adc	a, #0x00
	ld	-5 (ix), a
	C$board.c$193$1_0$227	= .
	.globl	C$board.c$193$1_0$227
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -10 (ix)
	ld	-12 (ix), a
	ld	a, -9 (ix)
	ld	-11 (ix), a
	ld	a, -3 (ix)
	ld	-4 (ix), a
	ld	-3 (ix), #0x00
	C$board.c$198$1_0$227	= .
	.globl	C$board.c$198$1_0$227
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -8 (ix)
	ld	-10 (ix), a
	ld	-9 (ix), #0x00
	C$board.c$203$1_0$227	= .
	.globl	C$board.c$203$1_0$227
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -14 (ix)
	ld	-16 (ix), a
	ld	a, -13 (ix)
	ld	-15 (ix), a
	ld	b, #0x05
00246$:
	sla	-16 (ix)
	rl	-15 (ix)
	djnz	00246$
	ld	a, -7 (ix)
	ld	-8 (ix), a
	ld	-7 (ix), #0x00
	C$board.c$208$1_0$227	= .
	.globl	C$board.c$208$1_0$227
;src/engine/board.c:208: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	C$board.c$193$1_0$227	= .
	.globl	C$board.c$193$1_0$227
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -12 (ix)
	ld	-14 (ix), a
	ld	a, -11 (ix)
	ld	-13 (ix), a
	ld	b, #0x05
00247$:
	sla	-14 (ix)
	rl	-13 (ix)
	djnz	00247$
	C$board.c$198$1_0$227	= .
	.globl	C$board.c$198$1_0$227
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -10 (ix)
	ld	-12 (ix), a
	ld	a, -9 (ix)
	ld	-11 (ix), a
	C$board.c$203$1_0$227	= .
	.globl	C$board.c$203$1_0$227
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -8 (ix)
	ld	-10 (ix), a
	ld	a, -7 (ix)
	ld	-9 (ix), a
	C$board.c$208$1_0$227	= .
	.globl	C$board.c$208$1_0$227
;src/engine/board.c:208: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -6 (ix)
	ld	-8 (ix), a
	ld	a, -5 (ix)
	ld	-7 (ix), a
	ld	b, #0x05
00248$:
	sla	-8 (ix)
	rl	-7 (ix)
	djnz	00248$
	C$board.c$193$1_0$227	= .
	.globl	C$board.c$193$1_0$227
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -14 (ix)
	add	a, -4 (ix)
	ld	-6 (ix), a
	ld	a, -13 (ix)
	adc	a, -3 (ix)
	ld	-5 (ix), a
	C$board.c$198$1_0$227	= .
	.globl	C$board.c$198$1_0$227
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -12 (ix)
	add	a, -14 (ix)
	ld	-4 (ix), a
	ld	a, -11 (ix)
	adc	a, -13 (ix)
	ld	-3 (ix), a
	C$board.c$203$1_0$227	= .
	.globl	C$board.c$203$1_0$227
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -10 (ix)
	add	a, -16 (ix)
	ld	-12 (ix), a
	ld	a, -9 (ix)
	adc	a, -15 (ix)
	ld	-11 (ix), a
	C$board.c$208$1_0$227	= .
	.globl	C$board.c$208$1_0$227
;src/engine/board.c:208: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -10 (ix)
	add	a, -8 (ix)
	ld	-14 (ix), a
	ld	a, -9 (ix)
	adc	a, -7 (ix)
	ld	-13 (ix), a
	C$board.c$193$1_0$227	= .
	.globl	C$board.c$193$1_0$227
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	sla	-6 (ix)
	rl	-5 (ix)
	C$board.c$198$1_0$227	= .
	.globl	C$board.c$198$1_0$227
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	sla	-4 (ix)
	rl	-3 (ix)
	C$board.c$203$1_0$227	= .
	.globl	C$board.c$203$1_0$227
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -12 (ix)
	ld	-8 (ix), a
	ld	a, -11 (ix)
	ld	-7 (ix), a
	sla	-8 (ix)
	rl	-7 (ix)
	C$board.c$208$1_0$227	= .
	.globl	C$board.c$208$1_0$227
;src/engine/board.c:208: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -14 (ix)
	ld	-10 (ix), a
	ld	a, -13 (ix)
	ld	-9 (ix), a
	sla	-10 (ix)
	rl	-9 (ix)
	C$board.c$193$1_0$227	= .
	.globl	C$board.c$193$1_0$227
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -6 (ix)
	ld	-12 (ix), a
	ld	a, -5 (ix)
	or	a, #0x78
	ld	-11 (ix), a
	C$board.c$198$1_0$227	= .
	.globl	C$board.c$198$1_0$227
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, -4 (ix)
	ld	-6 (ix), a
	ld	a, -3 (ix)
	or	a, #0x78
	ld	-5 (ix), a
	C$board.c$203$1_0$227	= .
	.globl	C$board.c$203$1_0$227
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -8 (ix)
	ld	-4 (ix), a
	ld	a, -7 (ix)
	or	a, #0x78
	ld	-3 (ix), a
	C$board.c$208$1_0$227	= .
	.globl	C$board.c$208$1_0$227
;src/engine/board.c:208: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, -10 (ix)
	ld	-8 (ix), a
	ld	a, -9 (ix)
	or	a, #0x78
	ld	-7 (ix), a
	C$board.c$190$1_0$227	= .
	.globl	C$board.c$190$1_0$227
;src/engine/board.c:190: if(color == NOT_PROCESSED_WHITE_CELL) {
	ld	a, 4 (ix)
	dec	a
	jp	NZ,00152$
	C$board.c$191$2_0$228	= .
	.globl	C$board.c$191$2_0$228
;src/engine/board.c:191: if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00105$
	ld	a, #<(_board)
	add	a, -24 (ix)
	ld	-10 (ix), a
	ld	a, #>(_board)
	adc	a, -23 (ix)
	ld	-9 (ix), a
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	ld	a, (hl)
	sub	a, #0x03
	jr	NZ, 00105$
	C$board.c$192$3_0$229	= .
	.globl	C$board.c$192$3_0$229
;src/engine/board.c:192: board[position-1] = NOT_PROCESSED_WHITE_CELL;
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	ld	(hl), #0x01
	C$board.c$193$4_0$230	= .
	.globl	C$board.c$193$4_0$230
;src/engine/board.c:193: draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	l, -12 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -11 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0801
	rst	#0x18
	C$board.c$194$3_0$229	= .
	.globl	C$board.c$194$3_0$229
;src/engine/board.c:194: mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, #0x01
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -30 (ix)
	call	_mark_position_for_change_in_board_rec
00105$:
	C$board.c$196$2_0$228	= .
	.globl	C$board.c$196$2_0$228
;src/engine/board.c:196: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
	ld	a, -29 (ix)
	or	a, a
	jr	Z, 00111$
	ld	bc, #_board+0
	ld	l, -22 (ix)
	ld	h, -21 (ix)
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x03
	jr	NZ, 00111$
	C$board.c$197$3_0$231	= .
	.globl	C$board.c$197$3_0$231
;src/engine/board.c:197: board[position+1] = NOT_PROCESSED_WHITE_CELL;
	ld	(hl), #0x01
	C$board.c$198$4_0$232	= .
	.globl	C$board.c$198$4_0$232
;src/engine/board.c:198: draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0801
	rst	#0x18
	C$board.c$199$3_0$231	= .
	.globl	C$board.c$199$3_0$231
;src/engine/board.c:199: mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_WHITE_CELL);
	ld	a, #0x01
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -28 (ix)
	call	_mark_position_for_change_in_board_rec
00111$:
	C$board.c$201$2_0$228	= .
	.globl	C$board.c$201$2_0$228
;src/engine/board.c:201: if(y > 0 && board[position-10] == CONNECTED_WHITE_CELL) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00117$
	ld	bc, #_board+0
	ld	l, -20 (ix)
	ld	h, -19 (ix)
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x03
	jr	NZ, 00117$
	C$board.c$202$3_0$233	= .
	.globl	C$board.c$202$3_0$233
;src/engine/board.c:202: board[position-10] = NOT_PROCESSED_WHITE_CELL;
	ld	(hl), #0x01
	C$board.c$203$4_0$234	= .
	.globl	C$board.c$203$4_0$234
;src/engine/board.c:203: draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0801
	rst	#0x18
	C$board.c$204$3_0$233	= .
	.globl	C$board.c$204$3_0$233
;src/engine/board.c:204: mark_position_for_change_in_board_rec(x, y-1, NOT_PROCESSED_WHITE_CELL);
	ld	a, #0x01
	push	af
	inc	sp
	ld	l, -27 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_mark_position_for_change_in_board_rec
00117$:
	C$board.c$206$2_0$228	= .
	.globl	C$board.c$206$2_0$228
;src/engine/board.c:206: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
	ld	a, -26 (ix)
	or	a, a
	jp	Z, 00154$
	ld	a, -18 (ix)
	add	a, #<(_board)
	ld	-4 (ix), a
	ld	a, -17 (ix)
	adc	a, #>(_board)
	ld	-3 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, (hl)
	sub	a, #0x03
	jp	NZ,00154$
	C$board.c$207$3_0$235	= .
	.globl	C$board.c$207$3_0$235
;src/engine/board.c:207: board[position+10] = NOT_PROCESSED_WHITE_CELL;
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	(hl), #0x01
	C$board.c$208$4_0$236	= .
	.globl	C$board.c$208$4_0$236
;src/engine/board.c:208: draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	l, -8 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -7 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0801
	rst	#0x18
	C$board.c$209$3_0$235	= .
	.globl	C$board.c$209$3_0$235
;src/engine/board.c:209: mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_WHITE_CELL);
	ld	a, #0x01
	push	af
	inc	sp
	ld	l, -25 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_mark_position_for_change_in_board_rec
	jp	00154$
00152$:
	C$board.c$211$1_0$227	= .
	.globl	C$board.c$211$1_0$227
;src/engine/board.c:211: } else if(color == NOT_PROCESSED_BLACK_CELL) {
	ld	a, 4 (ix)
	sub	a, #0x04
	jp	NZ,00154$
	C$board.c$212$2_0$237	= .
	.globl	C$board.c$212$2_0$237
;src/engine/board.c:212: if(x > 0 && board[position-1] == CONNECTED_BLACK_CELL) {
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00129$
	ld	bc, #_board+0
	ld	l, -24 (ix)
	ld	h, -23 (ix)
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x06
	jr	NZ, 00129$
	C$board.c$213$3_0$238	= .
	.globl	C$board.c$213$3_0$238
;src/engine/board.c:213: board[position-1] = NOT_PROCESSED_BLACK_CELL;
	ld	(hl), #0x04
	C$board.c$214$4_0$239	= .
	.globl	C$board.c$214$4_0$239
;src/engine/board.c:214: draw_board_position(x-1, y, NOT_PROCESSED_BLACK_CELL);
	ld	l, -12 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -11 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0804
	rst	#0x18
	C$board.c$215$3_0$238	= .
	.globl	C$board.c$215$3_0$238
;src/engine/board.c:215: mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_BLACK_CELL);
	ld	a, #0x04
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -30 (ix)
	call	_mark_position_for_change_in_board_rec
00129$:
	C$board.c$217$2_0$237	= .
	.globl	C$board.c$217$2_0$237
;src/engine/board.c:217: if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_BLACK_CELL) {
	ld	a, -29 (ix)
	or	a, a
	jr	Z, 00135$
	ld	a, #<(_board)
	add	a, -22 (ix)
	ld	-10 (ix), a
	ld	a, #>(_board)
	adc	a, -21 (ix)
	ld	-9 (ix), a
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	ld	a, (hl)
	sub	a, #0x06
	jr	NZ, 00135$
	C$board.c$218$3_0$240	= .
	.globl	C$board.c$218$3_0$240
;src/engine/board.c:218: board[position+1] = NOT_PROCESSED_BLACK_CELL;
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	ld	(hl), #0x04
	C$board.c$219$4_0$241	= .
	.globl	C$board.c$219$4_0$241
;src/engine/board.c:219: draw_board_position(x+1, y, NOT_PROCESSED_BLACK_CELL);
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0804
	rst	#0x18
	C$board.c$220$3_0$240	= .
	.globl	C$board.c$220$3_0$240
;src/engine/board.c:220: mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_BLACK_CELL);
	ld	a, #0x04
	push	af
	inc	sp
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -28 (ix)
	call	_mark_position_for_change_in_board_rec
00135$:
	C$board.c$222$2_0$237	= .
	.globl	C$board.c$222$2_0$237
;src/engine/board.c:222: if(y > 0 && board[position-10] == CONNECTED_BLACK_CELL) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00141$
	ld	bc, #_board+0
	ld	l, -20 (ix)
	ld	h, -19 (ix)
	add	hl, bc
	ld	a, (hl)
	sub	a, #0x06
	jr	NZ, 00141$
	C$board.c$223$3_0$242	= .
	.globl	C$board.c$223$3_0$242
;src/engine/board.c:223: board[position-10] = NOT_PROCESSED_BLACK_CELL;
	ld	(hl), #0x04
	C$board.c$224$4_0$243	= .
	.globl	C$board.c$224$4_0$243
;src/engine/board.c:224: draw_board_position(x, y-1, NOT_PROCESSED_BLACK_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0804
	rst	#0x18
	C$board.c$225$3_0$242	= .
	.globl	C$board.c$225$3_0$242
;src/engine/board.c:225: mark_position_for_change_in_board_rec(x, y-1, NOT_PROCESSED_BLACK_CELL);
	ld	a, #0x04
	push	af
	inc	sp
	ld	l, -27 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_mark_position_for_change_in_board_rec
00141$:
	C$board.c$227$2_0$237	= .
	.globl	C$board.c$227$2_0$237
;src/engine/board.c:227: if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_BLACK_CELL) {
	ld	a, -26 (ix)
	or	a, a
	jr	Z, 00154$
	ld	a, #<(_board)
	add	a, -18 (ix)
	ld	-4 (ix), a
	ld	a, #>(_board)
	adc	a, -17 (ix)
	ld	-3 (ix), a
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, (hl)
	ld	-5 (ix), a
	sub	a, #0x06
	jr	NZ, 00154$
	C$board.c$228$3_0$244	= .
	.globl	C$board.c$228$3_0$244
;src/engine/board.c:228: board[position+10] = NOT_PROCESSED_BLACK_CELL;
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	(hl), #0x04
	C$board.c$229$4_0$245	= .
	.globl	C$board.c$229$4_0$245
;src/engine/board.c:229: draw_board_position(x, y+1, NOT_PROCESSED_BLACK_CELL);
	ld	l, -8 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -7 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0804
	rst	#0x18
	C$board.c$230$3_0$244	= .
	.globl	C$board.c$230$3_0$244
;src/engine/board.c:230: mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_BLACK_CELL);
	ld	a, #0x04
	push	af
	inc	sp
	ld	l, -25 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_mark_position_for_change_in_board_rec
00154$:
	C$board.c$233$1_0$227	= .
	.globl	C$board.c$233$1_0$227
;src/engine/board.c:233: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$put_square_pieces_board$0$0	= .
	.globl	G$put_square_pieces_board$0$0
	C$board.c$235$1_0$247	= .
	.globl	C$board.c$235$1_0$247
;src/engine/board.c:235: void put_square_pieces_board(void) {
;	---------------------------------
; Function put_square_pieces_board
; ---------------------------------
_put_square_pieces_board::
	C$board.c$236$1_0$247	= .
	.globl	C$board.c$236$1_0$247
;src/engine/board.c:236: u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
	ld	a, (#(_current_square + 1) + 0)
	ld	c, a
	add	a, a
	add	a, a
	add	a, a
	sla	c
	add	a, c
	ld	hl, #_current_square
	ld	c, (hl)
	add	a, c
	C$board.c$237$1_0$247	= .
	.globl	C$board.c$237$1_0$247
;src/engine/board.c:237: board[position] = current_square.color00;
	ld	c, a
	add	a, #<(_board)
	ld	e, a
	ld	a, #0x00
	adc	a, #>(_board)
	ld	d, a
	ld	a, (#(_current_square + 2) + 0)
	ld	(de), a
	C$board.c$238$1_0$247	= .
	.globl	C$board.c$238$1_0$247
;src/engine/board.c:238: board[position+1] = current_square.color01;
	ld	b, #0x00
	ld	e, c
	ld	d, b
	inc	de
	ld	hl, #_board
	add	hl, de
	ex	de, hl
	ld	a, (#(_current_square + 3) + 0)
	ld	(de), a
	C$board.c$239$1_0$247	= .
	.globl	C$board.c$239$1_0$247
;src/engine/board.c:239: board[position+10] = current_square.color10;
	ld	hl, #0x000a
	add	hl, bc
	ex	de, hl
	ld	hl, #_board
	add	hl, de
	ex	de, hl
	ld	a, (#(_current_square + 4) + 0)
	ld	(de), a
	C$board.c$240$1_0$247	= .
	.globl	C$board.c$240$1_0$247
;src/engine/board.c:240: board[position+11] = current_square.color11;
	ld	hl, #0x000b
	add	hl, bc
	ld	de, #_board
	add	hl, de
	ld	de, #_current_square + 5
	ld	a, (de)
	ld	(hl), a
	C$board.c$241$2_0$248	= .
	.globl	C$board.c$241$2_0$248
;src/engine/board.c:241: draw_board_position(current_square.x, current_square.y, current_square.color00);
	ld	a, (#(_current_square + 1) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0b
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, (#(_current_square + 2) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$board.c$242$2_0$249	= .
	.globl	C$board.c$242$2_0$249
;src/engine/board.c:242: draw_board_position(current_square.x+1, current_square.y, current_square.color01);
	ld	a, (#(_current_square + 1) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, (#(_current_square + 3) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$board.c$243$2_0$250	= .
	.globl	C$board.c$243$2_0$250
;src/engine/board.c:243: draw_board_position(current_square.x, current_square.y+1, current_square.color10);
	ld	a, (#(_current_square + 1) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0b
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, (#(_current_square + 4) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$board.c$244$2_0$251	= .
	.globl	C$board.c$244$2_0$251
;src/engine/board.c:244: draw_board_position(current_square.x+1, current_square.y+1, current_square.color11);
	ld	a, (#(_current_square + 1) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (#_current_square + 0)
	add	a, #0x0c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, (de)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	C$board.c$245$1_0$247	= .
	.globl	C$board.c$245$1_0$247
;src/engine/board.c:245: }
	C$board.c$245$1_0$247	= .
	.globl	C$board.c$245$1_0$247
	XG$put_square_pieces_board$0$0	= .
	.globl	XG$put_square_pieces_board$0$0
	jp	_SMS_crt0_RST18
	G$put_bonus_board$0$0	= .
	.globl	G$put_bonus_board$0$0
	C$board.c$247$1_0$253	= .
	.globl	C$board.c$247$1_0$253
;src/engine/board.c:247: void put_bonus_board(void) {
;	---------------------------------
; Function put_bonus_board
; ---------------------------------
_put_bonus_board::
	C$board.c$248$1_0$253	= .
	.globl	C$board.c$248$1_0$253
;src/engine/board.c:248: u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
	ld	bc, #_current_square+0
	ld	a, (#(_current_square + 1) + 0)
	ld	e, a
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	ld	e, a
	ld	a, (bc)
	add	a, e
	ld	e, a
	C$board.c$249$1_0$253	= .
	.globl	C$board.c$249$1_0$253
;src/engine/board.c:249: board[position] = current_square.color00;
	ld	hl, #_board+0
	ld	d, #0x00
	add	hl, de
	ld	e, c
	ld	d, b
	inc	de
	inc	de
	ld	a, (de)
	ld	(hl), a
	C$board.c$250$2_0$254	= .
	.globl	C$board.c$250$2_0$254
;src/engine/board.c:250: draw_board_position(current_square.x, current_square.y, current_square.color00);
	ld	a, (#(_current_square + 1) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, (bc)
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, (de)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	C$board.c$251$1_0$253	= .
	.globl	C$board.c$251$1_0$253
;src/engine/board.c:251: }
	C$board.c$251$1_0$253	= .
	.globl	C$board.c$251$1_0$253
	XG$put_bonus_board$0$0	= .
	.globl	XG$put_bonus_board$0$0
	jp	_SMS_crt0_RST18
	G$compute_path$0$0	= .
	.globl	G$compute_path$0$0
	C$board.c$253$1_0$256	= .
	.globl	C$board.c$253$1_0$256
;src/engine/board.c:253: bool compute_path(u8 x, u8 y) {
;	---------------------------------
; Function compute_path
; ---------------------------------
_compute_path::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-18
	add	iy, sp
	ld	sp, iy
	ld	-1 (ix), a
	C$board.c$254$1_0$256	= .
	.globl	C$board.c$254$1_0$256
;src/engine/board.c:254: u8 position = (y << 3) + (y << 1) + x;
	ld	-2 (ix), l
	ld	c, l
	ld	a, c
	add	a, a
	add	a, a
	add	a, a
	sla	c
	add	a, c
	ld	c, -1 (ix)
	add	a, c
	C$board.c$256$1_0$256	= .
	.globl	C$board.c$256$1_0$256
;src/engine/board.c:256: if(board[position] == NOT_PROCESSED_WHITE_CELL) {
	ld	-18 (ix), a
	add	a, #<(_board)
	ld	-17 (ix), a
	ld	a, #0x00
	adc	a, #>(_board)
	ld	-16 (ix), a
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	a, (hl)
	ld	-15 (ix), a
	C$board.c$257$1_0$256	= .
	.globl	C$board.c$257$1_0$256
;src/engine/board.c:257: if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
	ld	a, -2 (ix)
	sub	a, #0x15
	ld	a, #0x01
	jr	Z, 00353$
	xor	a, a
00353$:
	ld	-14 (ix), a
	ld	a, -1 (ix)
	sub	a, #0x09
	ld	a, #0x01
	jr	Z, 00355$
	xor	a, a
00355$:
	ld	-13 (ix), a
	C$board.c$259$1_0$256	= .
	.globl	C$board.c$259$1_0$256
;src/engine/board.c:259: draw_board_position(x, y, CONNECTED_WHITE_CELL);
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	add	a, #0x0b
	C$board.c$276$1_0$256	= .
	.globl	C$board.c$276$1_0$256
;src/engine/board.c:276: if(is_free_board_position(position+9)) {
	ld	e, -18 (ix)
	C$board.c$277$1_0$256	= .
	.globl	C$board.c$277$1_0$256
;src/engine/board.c:277: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	C$board.c$283$1_0$256	= .
	.globl	C$board.c$283$1_0$256
;src/engine/board.c:283: suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	C$board.c$259$1_0$256	= .
	.globl	C$board.c$259$1_0$256
;src/engine/board.c:259: draw_board_position(x, y, CONNECTED_WHITE_CELL);
	inc	hl
	ld	d, #0x00
	ld	b, c
	dec	b
	inc	c
	ld	-5 (ix), a
	C$board.c$276$1_0$256	= .
	.globl	C$board.c$276$1_0$256
;src/engine/board.c:276: if(is_free_board_position(position+9)) {
	ld	a, e
	add	a, #0x09
	ld	-4 (ix), a
	ld	a, d
	adc	a, #0x00
	ld	-3 (ix), a
	C$board.c$277$1_0$256	= .
	.globl	C$board.c$277$1_0$256
;src/engine/board.c:277: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	ld	a, b
	ld	-12 (ix), a
	rlca
	sbc	a, a
	ld	-11 (ix), a
	C$board.c$282$1_0$256	= .
	.globl	C$board.c$282$1_0$256
;src/engine/board.c:282: if(is_free_board_position(position+11)) {
	ld	a, e
	add	a, #0x0b
	ld	e, a
	jr	NC, 00356$
	inc	d
00356$:
	C$board.c$283$1_0$256	= .
	.globl	C$board.c$283$1_0$256
;src/engine/board.c:283: suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	ld	a, c
	ld	-10 (ix), a
	rlca
	sbc	a, a
	ld	-9 (ix), a
	C$board.c$259$1_0$256	= .
	.globl	C$board.c$259$1_0$256
;src/engine/board.c:259: draw_board_position(x, y, CONNECTED_WHITE_CELL);
	ld	c, -5 (ix)
	ld	b, #0x00
	C$board.c$276$1_0$256	= .
	.globl	C$board.c$276$1_0$256
;src/engine/board.c:276: if(is_free_board_position(position+9)) {
	ld	a, -4 (ix)
	add	a, #<(_board)
	ld	-8 (ix), a
	ld	a, -3 (ix)
	adc	a, #>(_board)
	ld	-7 (ix), a
	C$board.c$282$1_0$256	= .
	.globl	C$board.c$282$1_0$256
;src/engine/board.c:282: if(is_free_board_position(position+11)) {
	ld	a, #<(_board)
	add	a, e
	ld	-6 (ix), a
	ld	a, #>(_board)
	adc	a, d
	ld	-5 (ix), a
	C$board.c$259$1_0$256	= .
	.globl	C$board.c$259$1_0$256
;src/engine/board.c:259: draw_board_position(x, y, CONNECTED_WHITE_CELL);
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	ld	-4 (ix), l
	or	a, #0x78
	ld	-3 (ix), a
	C$board.c$256$1_0$256	= .
	.globl	C$board.c$256$1_0$256
;src/engine/board.c:256: if(board[position] == NOT_PROCESSED_WHITE_CELL) {
	ld	a, -15 (ix)
	dec	a
	jp	NZ,00193$
	C$board.c$257$2_0$257	= .
	.globl	C$board.c$257$2_0$257
;src/engine/board.c:257: if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00138$
	ld	a, -14 (ix)
	or	a, a
	jr	NZ, 00138$
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00138$
	ld	a, -13 (ix)
	or	a, a
	jr	Z, 00139$
00138$:
	C$board.c$258$3_0$258	= .
	.globl	C$board.c$258$3_0$258
;src/engine/board.c:258: board[position] = CONNECTED_WHITE_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x03
	C$board.c$259$4_0$259	= .
	.globl	C$board.c$259$4_0$259
;src/engine/board.c:259: draw_board_position(x, y, CONNECTED_WHITE_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0803
	rst	#0x18
	C$board.c$260$3_0$258	= .
	.globl	C$board.c$260$3_0$258
;src/engine/board.c:260: suitable_columns[x] = NO_SUITABLE;
	ld	bc, #_suitable_columns+0
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$261$3_0$258	= .
	.globl	C$board.c$261$3_0$258
;src/engine/board.c:261: return true;
	ld	a, #0x01
	jp	00195$
00139$:
	C$board.c$263$3_0$260	= .
	.globl	C$board.c$263$3_0$260
;src/engine/board.c:263: if(neighbour_connected(position)) {
	ld	a, -18 (ix)
	call	_neighbour_connected
	bit	0,a
	jr	Z, 00136$
	C$board.c$264$4_0$261	= .
	.globl	C$board.c$264$4_0$261
;src/engine/board.c:264: board[position] = CONNECTED_WHITE_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x03
	C$board.c$265$5_0$262	= .
	.globl	C$board.c$265$5_0$262
;src/engine/board.c:265: draw_board_position(x, y, CONNECTED_WHITE_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0803
	rst	#0x18
	C$board.c$266$4_0$261	= .
	.globl	C$board.c$266$4_0$261
;src/engine/board.c:266: suitable_columns[x] = 0;
	ld	bc, #_suitable_columns+0
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$267$4_0$261	= .
	.globl	C$board.c$267$4_0$261
;src/engine/board.c:267: return true;
	ld	a, #0x01
	jp	00195$
00136$:
	C$board.c$269$4_0$263	= .
	.globl	C$board.c$269$4_0$263
;src/engine/board.c:269: empty_directions = neighbour_emptyspace(position);
	ld	a, -18 (ix)
	call	_neighbour_emptyspace
	C$board.c$270$4_0$263	= .
	.globl	C$board.c$270$4_0$263
;src/engine/board.c:270: if(empty_directions) {
	ld	c, a
	or	a, a
	jp	Z, 00133$
	C$board.c$273$5_0$264	= .
	.globl	C$board.c$273$5_0$264
;src/engine/board.c:273: if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
	bit	0, c
	jr	Z, 00121$
	ld	a, c
	xor	a, #0x01
	jr	NZ, 00121$
	C$board.c$274$6_0$265	= .
	.globl	C$board.c$274$6_0$265
;src/engine/board.c:274: suitable_columns[x] = ((suitable_columns[x] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x]+ 1) &0xF) ;
	ld	bc, #_suitable_columns+0
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x21
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	jp	00122$
00121$:
	C$board.c$275$5_0$264	= .
	.globl	C$board.c$275$5_0$264
;src/engine/board.c:275: } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {
	bit	1, c
	jr	Z, 00117$
	ld	a, c
	xor	a, #0x02
	jr	NZ, 00117$
	C$board.c$276$6_0$266	= .
	.globl	C$board.c$276$6_0$266
;src/engine/board.c:276: if(is_free_board_position(position+9)) {
	ld	l, -8 (ix)
	ld	h, -7 (ix)
	ld	a, (hl)
	or	a, a
	jr	NZ, 00108$
	C$board.c$277$7_0$267	= .
	.globl	C$board.c$277$7_0$267
;src/engine/board.c:277: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -12 (ix)
	ld	h, -11 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x81
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	jr	00122$
00108$:
	C$board.c$279$7_0$268	= .
	.globl	C$board.c$279$7_0$268
;src/engine/board.c:279: suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x-1]+1)& 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -12 (ix)
	ld	h, -11 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x21
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	jr	00122$
00117$:
	C$board.c$281$5_0$264	= .
	.globl	C$board.c$281$5_0$264
;src/engine/board.c:281: } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
	bit	2, c
	jr	Z, 00122$
	ld	a, c
	xor	a, #0x04
	jr	NZ, 00122$
	C$board.c$282$6_0$269	= .
	.globl	C$board.c$282$6_0$269
;src/engine/board.c:282: if(is_free_board_position(position+11)) {
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	or	a, a
	jr	NZ, 00111$
	C$board.c$283$7_0$270	= .
	.globl	C$board.c$283$7_0$270
;src/engine/board.c:283: suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x81
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
	jr	00122$
00111$:
	C$board.c$285$7_0$271	= .
	.globl	C$board.c$285$7_0$271
;src/engine/board.c:285: suitable_columns[x+1] = ((suitable_columns[x+1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
	ld	bc, #_suitable_columns+0
	ld	l, -10 (ix)
	ld	h, -9 (ix)
	add	hl, bc
	ld	e, (hl)
	ld	a, e
	or	a, #0x21
	and	a, #0xf0
	ld	c, a
	ld	a, e
	inc	a
	and	a, #0x0f
	add	a, c
	ld	(hl), a
00122$:
	C$board.c$288$5_0$264	= .
	.globl	C$board.c$288$5_0$264
;src/engine/board.c:288: board[position] = CONNECTED_WHITE_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x03
	C$board.c$289$6_0$272	= .
	.globl	C$board.c$289$6_0$272
;src/engine/board.c:289: draw_board_position(x, y, CONNECTED_WHITE_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0803
	rst	#0x18
	C$board.c$290$5_0$264	= .
	.globl	C$board.c$290$5_0$264
;src/engine/board.c:290: return true;
	ld	a, #0x01
	jp	00195$
00133$:
	C$board.c$291$4_0$263	= .
	.globl	C$board.c$291$4_0$263
;src/engine/board.c:291: } else if(!neighbour_notprocessed(position)) {
	ld	a, -18 (ix)
	call	_neighbour_notprocessed
	bit	0,a
	jp	NZ, 00194$
	C$board.c$292$5_0$273	= .
	.globl	C$board.c$292$5_0$273
;src/engine/board.c:292: board[position] = NOT_CONNECTED_WHITE_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x02
	C$board.c$293$6_0$274	= .
	.globl	C$board.c$293$6_0$274
;src/engine/board.c:293: draw_board_position(x, y, NOT_CONNECTED_WHITE_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0802
	rst	#0x18
	C$board.c$294$5_0$273	= .
	.globl	C$board.c$294$5_0$273
;src/engine/board.c:294: return true;
	ld	a, #0x01
	jp	00195$
00193$:
	C$board.c$298$1_0$256	= .
	.globl	C$board.c$298$1_0$256
;src/engine/board.c:298: } else if(board[position] == NOT_PROCESSED_BLACK_CELL) {
	ld	a, -15 (ix)
	sub	a, #0x04
	jp	NZ,00190$
	C$board.c$299$2_0$275	= .
	.globl	C$board.c$299$2_0$275
;src/engine/board.c:299: if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00181$
	ld	a, -14 (ix)
	or	a, a
	jr	NZ, 00181$
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00181$
	ld	a, -13 (ix)
	or	a, a
	jr	Z, 00182$
00181$:
	C$board.c$300$3_0$276	= .
	.globl	C$board.c$300$3_0$276
;src/engine/board.c:300: board[position] = CONNECTED_BLACK_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x06
	C$board.c$301$4_0$277	= .
	.globl	C$board.c$301$4_0$277
;src/engine/board.c:301: draw_board_position(x, y, CONNECTED_BLACK_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0806
	rst	#0x18
	C$board.c$302$3_0$276	= .
	.globl	C$board.c$302$3_0$276
;src/engine/board.c:302: suitable_columns[x] = 0;
	ld	bc, #_suitable_columns+0
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	(hl), #0x00
	C$board.c$303$3_0$276	= .
	.globl	C$board.c$303$3_0$276
;src/engine/board.c:303: return true;
	ld	a, #0x01
	jp	00195$
00182$:
	C$board.c$305$3_0$278	= .
	.globl	C$board.c$305$3_0$278
;src/engine/board.c:305: if(neighbour_connected(position)) {
	ld	a, -18 (ix)
	call	_neighbour_connected
	bit	0,a
	jr	Z, 00179$
	C$board.c$306$4_0$279	= .
	.globl	C$board.c$306$4_0$279
;src/engine/board.c:306: board[position] = CONNECTED_BLACK_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x06
	C$board.c$307$5_0$280	= .
	.globl	C$board.c$307$5_0$280
;src/engine/board.c:307: draw_board_position(x, y, CONNECTED_BLACK_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0806
	rst	#0x18
	C$board.c$309$4_0$279	= .
	.globl	C$board.c$309$4_0$279
;src/engine/board.c:309: return true;
	ld	a, #0x01
	jp	00195$
00179$:
	C$board.c$311$4_0$281	= .
	.globl	C$board.c$311$4_0$281
;src/engine/board.c:311: empty_directions = neighbour_emptyspace(position);
	ld	a, -18 (ix)
	call	_neighbour_emptyspace
	C$board.c$312$4_0$281	= .
	.globl	C$board.c$312$4_0$281
;src/engine/board.c:312: if(empty_directions) {
	ld	-13 (ix), a
	or	a, a
	jp	Z, 00176$
	C$board.c$315$5_0$282	= .
	.globl	C$board.c$315$5_0$282
;src/engine/board.c:315: if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
	bit	0, -13 (ix)
	jr	Z, 00164$
	ld	a, -13 (ix)
	xor	a, #0x01
	jr	NZ, 00164$
	C$board.c$316$6_0$283	= .
	.globl	C$board.c$316$6_0$283
;src/engine/board.c:316: suitable_columns[x] = ((suitable_columns[x] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x] + 4) & 0xF);                        
	ld	a, #<(_suitable_columns)
	add	a, -1 (ix)
	ld	-8 (ix), a
	ld	a, #>(_suitable_columns)
	adc	a, #0x00
	ld	-7 (ix), a
	ld	l, -8 (ix)
	ld	h, -7 (ix)
	ld	a, (hl)
	ld	-5 (ix), a
	or	a, #0x11
	ld	-6 (ix), a
	and	a, #0xf0
	ld	-6 (ix), a
	inc	-5 (ix)
	inc	-5 (ix)
	inc	-5 (ix)
	inc	-5 (ix)
	ld	a, -5 (ix)
	and	a, #0x0f
	ld	-5 (ix), a
	add	a, -6 (ix)
	ld	l, -8 (ix)
	ld	h, -7 (ix)
	ld	(hl), a
	jp	00165$
00164$:
	C$board.c$317$5_0$282	= .
	.globl	C$board.c$317$5_0$282
;src/engine/board.c:317: } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {                        
	bit	1, -13 (ix)
	jp	Z,00160$
	ld	a, -13 (ix)
	xor	a, #0x02
	jp	NZ,00160$
	C$board.c$318$6_0$284	= .
	.globl	C$board.c$318$6_0$284
;src/engine/board.c:318: if(is_free_board_position(position+9)) {
	ld	l, -8 (ix)
	ld	h, -7 (ix)
	ld	a, (hl)
	ld	-5 (ix), a
	or	a, a
	jr	NZ, 00151$
	C$board.c$319$7_0$285	= .
	.globl	C$board.c$319$7_0$285
;src/engine/board.c:319: suitable_columns[x-1] =((suitable_columns[x-1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 4) & 0xF);
	ld	a, -12 (ix)
	add	a, #<(_suitable_columns)
	ld	-6 (ix), a
	ld	a, -11 (ix)
	adc	a, #>(_suitable_columns)
	ld	-5 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-7 (ix), a
	or	a, #0x41
	ld	-8 (ix), a
	and	a, #0xf0
	ld	-8 (ix), a
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	ld	a, -7 (ix)
	and	a, #0x0f
	ld	-7 (ix), a
	add	a, -8 (ix)
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	(hl), a
	jp	00165$
00151$:
	C$board.c$321$7_0$286	= .
	.globl	C$board.c$321$7_0$286
;src/engine/board.c:321: suitable_columns[x-1] = ((suitable_columns[x-1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x-1] + 4) & 0xF);
	ld	a, -12 (ix)
	add	a, #<(_suitable_columns)
	ld	-6 (ix), a
	ld	a, -11 (ix)
	adc	a, #>(_suitable_columns)
	ld	-5 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-7 (ix), a
	or	a, #0x11
	ld	-8 (ix), a
	and	a, #0xf0
	ld	-8 (ix), a
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	ld	a, -7 (ix)
	and	a, #0x0f
	ld	-7 (ix), a
	add	a, -8 (ix)
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	(hl), a
	jp	00165$
00160$:
	C$board.c$323$5_0$282	= .
	.globl	C$board.c$323$5_0$282
;src/engine/board.c:323: } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
	bit	2, -13 (ix)
	jp	Z,00165$
	ld	a, -13 (ix)
	xor	a, #0x04
	jp	NZ,00165$
	C$board.c$324$6_0$287	= .
	.globl	C$board.c$324$6_0$287
;src/engine/board.c:324: if(is_free_board_position(position+11)) {
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	or	a, a
	jr	NZ, 00154$
	C$board.c$325$7_0$288	= .
	.globl	C$board.c$325$7_0$288
;src/engine/board.c:325: suitable_columns[x+1] =((suitable_columns[x+1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] + 4) & 0xF);
	ld	a, -10 (ix)
	add	a, #<(_suitable_columns)
	ld	-6 (ix), a
	ld	a, -9 (ix)
	adc	a, #>(_suitable_columns)
	ld	-5 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-7 (ix), a
	or	a, #0x41
	ld	-8 (ix), a
	and	a, #0xf0
	ld	-8 (ix), a
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	ld	a, -7 (ix)
	and	a, #0x0f
	ld	-7 (ix), a
	add	a, -8 (ix)
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	(hl), a
	jr	00165$
00154$:
	C$board.c$327$7_0$289	= .
	.globl	C$board.c$327$7_0$289
;src/engine/board.c:327: suitable_columns[x+1] = ((suitable_columns[x+1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x+1] + 4) & 0xF);
	ld	a, -10 (ix)
	add	a, #<(_suitable_columns)
	ld	-6 (ix), a
	ld	a, -9 (ix)
	adc	a, #>(_suitable_columns)
	ld	-5 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-7 (ix), a
	or	a, #0x11
	ld	-8 (ix), a
	and	a, #0xf0
	ld	-8 (ix), a
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	inc	-7 (ix)
	ld	a, -7 (ix)
	and	a, #0x0f
	ld	-7 (ix), a
	add	a, -8 (ix)
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	(hl), a
00165$:
	C$board.c$330$5_0$282	= .
	.globl	C$board.c$330$5_0$282
;src/engine/board.c:330: board[position] = CONNECTED_BLACK_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x06
	C$board.c$331$6_0$290	= .
	.globl	C$board.c$331$6_0$290
;src/engine/board.c:331: draw_board_position(x, y, CONNECTED_BLACK_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0806
	rst	#0x18
	C$board.c$332$5_0$282	= .
	.globl	C$board.c$332$5_0$282
;src/engine/board.c:332: return true;
	ld	a, #0x01
	jr	00195$
00176$:
	C$board.c$333$4_0$281	= .
	.globl	C$board.c$333$4_0$281
;src/engine/board.c:333: } else if(!neighbour_notprocessed(position)) {
	ld	a, -18 (ix)
	call	_neighbour_notprocessed
	bit	0,a
	jr	NZ, 00194$
	C$board.c$334$5_0$291	= .
	.globl	C$board.c$334$5_0$291
;src/engine/board.c:334: board[position] = NOT_CONNECTED_BLACK_CELL;
	ld	l, -17 (ix)
	ld	h, -16 (ix)
	ld	(hl), #0x05
	C$board.c$335$6_0$292	= .
	.globl	C$board.c$335$6_0$292
;src/engine/board.c:335: draw_board_position(x, y, NOT_CONNECTED_BLACK_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0805
	rst	#0x18
	C$board.c$336$5_0$291	= .
	.globl	C$board.c$336$5_0$291
;src/engine/board.c:336: return true;
	ld	a, #0x01
	jr	00195$
00190$:
	C$board.c$340$1_0$256	= .
	.globl	C$board.c$340$1_0$256
;src/engine/board.c:340: } else if(board[position] == EMPTY_CELL) {
	ld	a, -15 (ix)
	or	a, a
	jr	NZ, 00194$
	C$board.c$341$2_0$293	= .
	.globl	C$board.c$341$2_0$293
;src/engine/board.c:341: suitable_columns[x] = 0;
	ld	bc, #_suitable_columns+0
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, bc
	ld	(hl), #0x00
00194$:
	C$board.c$343$1_0$256	= .
	.globl	C$board.c$343$1_0$256
;src/engine/board.c:343: return false;
	xor	a, a
00195$:
	C$board.c$344$1_0$256	= .
	.globl	C$board.c$344$1_0$256
;src/engine/board.c:344: }
	ld	sp, ix
	pop	ix
	C$board.c$344$1_0$256	= .
	.globl	C$board.c$344$1_0$256
	XG$compute_path$0$0	= .
	.globl	XG$compute_path$0$0
	ret
	G$neighbour_emptyspace$0$0	= .
	.globl	G$neighbour_emptyspace$0$0
	C$board.c$346$1_0$295	= .
	.globl	C$board.c$346$1_0$295
;src/engine/board.c:346: u8 neighbour_emptyspace(u8 position2check) {
;	---------------------------------
; Function neighbour_emptyspace
; ---------------------------------
_neighbour_emptyspace::
	ld	b, a
	C$board.c$347$2_0$295	= .
	.globl	C$board.c$347$2_0$295
;src/engine/board.c:347: u8 result = 0;
	ld	c, #0x00
	C$board.c$348$1_0$295	= .
	.globl	C$board.c$348$1_0$295
;src/engine/board.c:348: if(board[position2check - 10] == EMPTY_CELL) 
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, l
	add	a, #0xf6
	ld	e, a
	ld	a, h
	adc	a, #0xff
	ld	d, a
	ld	iy, #_board
	add	iy, de
	ld	a, 0 (iy)
	or	a, a
	jr	NZ, 00102$
	C$board.c$349$1_0$295	= .
	.globl	C$board.c$349$1_0$295
;src/engine/board.c:349: result = result | EMPTY_UP;
	ld	c, #0x01
00102$:
	C$board.c$350$1_0$295	= .
	.globl	C$board.c$350$1_0$295
;src/engine/board.c:350: if(board[position2check - 1] == EMPTY_CELL) 
	ld	e, l
	ld	d, h
	dec	de
	ld	iy, #_board
	add	iy, de
	ld	a, 0 (iy)
	or	a, a
	jr	NZ, 00104$
	C$board.c$351$1_0$295	= .
	.globl	C$board.c$351$1_0$295
;src/engine/board.c:351: result = result | EMPTY_LEFT;
	set	1, c
00104$:
	C$board.c$352$1_0$295	= .
	.globl	C$board.c$352$1_0$295
;src/engine/board.c:352: if(board[position2check + 1] == EMPTY_CELL) 
	ld	e, l
	ld	d, h
	inc	de
	ld	iy, #_board
	add	iy, de
	ld	a, 0 (iy)
	or	a, a
	jr	NZ, 00106$
	C$board.c$353$1_0$295	= .
	.globl	C$board.c$353$1_0$295
;src/engine/board.c:353: result = result | EMPTY_RIGHT;
	set	2, c
00106$:
	C$board.c$354$1_0$295	= .
	.globl	C$board.c$354$1_0$295
;src/engine/board.c:354: if(board[position2check + 10] == EMPTY_CELL) 
	ld	de, #0x000a
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00108$
	C$board.c$355$1_0$295	= .
	.globl	C$board.c$355$1_0$295
;src/engine/board.c:355: result = result | EMPTY_DOWN;
	set	3, c
00108$:
	C$board.c$356$1_0$295	= .
	.globl	C$board.c$356$1_0$295
;src/engine/board.c:356: return result;
	ld	a, c
	C$board.c$364$1_0$295	= .
	.globl	C$board.c$364$1_0$295
;src/engine/board.c:364: }
	C$board.c$364$1_0$295	= .
	.globl	C$board.c$364$1_0$295
	XG$neighbour_emptyspace$0$0	= .
	.globl	XG$neighbour_emptyspace$0$0
	ret
	G$neighbour_connected$0$0	= .
	.globl	G$neighbour_connected$0$0
	C$board.c$366$1_0$297	= .
	.globl	C$board.c$366$1_0$297
;src/engine/board.c:366: bool neighbour_connected(u8 position2check) {
;	---------------------------------
; Function neighbour_connected
; ---------------------------------
_neighbour_connected::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	e, a
	C$board.c$367$1_0$297	= .
	.globl	C$board.c$367$1_0$297
;src/engine/board.c:367: if(board[position2check] == NOT_PROCESSED_WHITE_CELL && 
	ld	hl, #_board
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	ld	-1 (ix), a
	C$board.c$368$1_0$297	= .
	.globl	C$board.c$368$1_0$297
;src/engine/board.c:368: (board[position2check - 10] == CONNECTED_WHITE_CELL || board[position2check - 1] == CONNECTED_WHITE_CELL 
	ld	d, #0x00
	ld	a, e
	add	a, #0xf6
	ld	c, a
	ld	a, d
	adc	a, #0xff
	ld	b, a
	ld	l, e
	ld	h, d
	dec	hl
	C$board.c$369$1_0$297	= .
	.globl	C$board.c$369$1_0$297
;src/engine/board.c:369: || board[position2check + 1] == CONNECTED_WHITE_CELL || board[position2check + 10] == CONNECTED_WHITE_CELL)) {
	push	de
	pop	iy
	inc	iy
	ld	a, e
	add	a, #0x0a
	ld	e, a
	jr	NC, 00155$
	inc	d
00155$:
	C$board.c$368$1_0$297	= .
	.globl	C$board.c$368$1_0$297
;src/engine/board.c:368: (board[position2check - 10] == CONNECTED_WHITE_CELL || board[position2check - 1] == CONNECTED_WHITE_CELL 
	ld	a, #<(_board)
	add	a, c
	ld	c, a
	ld	a, #>(_board)
	adc	a, b
	ld	b, a
	ld	a, #<(_board)
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	C$board.c$369$1_0$297	= .
	.globl	C$board.c$369$1_0$297
;src/engine/board.c:369: || board[position2check + 1] == CONNECTED_WHITE_CELL || board[position2check + 10] == CONNECTED_WHITE_CELL)) {
	push	bc
	ld	bc, #_board
	add	iy, bc
	pop	bc
	ld	a, #<(_board)
	add	a, e
	ld	e, a
	ld	a, #>(_board)
	adc	a, d
	ld	d, a
	C$board.c$367$1_0$297	= .
	.globl	C$board.c$367$1_0$297
;src/engine/board.c:367: if(board[position2check] == NOT_PROCESSED_WHITE_CELL && 
	ld	a, -1 (ix)
	dec	a
	jr	NZ, 00102$
	C$board.c$368$1_0$297	= .
	.globl	C$board.c$368$1_0$297
;src/engine/board.c:368: (board[position2check - 10] == CONNECTED_WHITE_CELL || board[position2check - 1] == CONNECTED_WHITE_CELL 
	ld	a, (bc)
	sub	a, #0x03
	jr	Z, 00101$
	ld	a, (hl)
	sub	a, #0x03
	jr	Z, 00101$
	C$board.c$369$1_0$297	= .
	.globl	C$board.c$369$1_0$297
;src/engine/board.c:369: || board[position2check + 1] == CONNECTED_WHITE_CELL || board[position2check + 10] == CONNECTED_WHITE_CELL)) {
	ld	a, 0 (iy)
	sub	a, #0x03
	jr	Z, 00101$
	ld	a, (de)
	sub	a, #0x03
	jr	NZ, 00102$
00101$:
	C$board.c$370$2_0$298	= .
	.globl	C$board.c$370$2_0$298
;src/engine/board.c:370: return true;
	ld	a, #0x01
	jr	00113$
00102$:
	C$board.c$372$1_0$297	= .
	.globl	C$board.c$372$1_0$297
;src/engine/board.c:372: if(board[position2check] == NOT_PROCESSED_BLACK_CELL && 
	ld	a, -1 (ix)
	sub	a, #0x04
	jr	NZ, 00108$
	C$board.c$373$1_0$297	= .
	.globl	C$board.c$373$1_0$297
;src/engine/board.c:373: (board[position2check - 10] == CONNECTED_BLACK_CELL || board[position2check - 1] == CONNECTED_BLACK_CELL 
	ld	a, (bc)
	sub	a, #0x06
	jr	Z, 00107$
	ld	a, (hl)
	sub	a, #0x06
	jr	Z, 00107$
	C$board.c$374$1_0$297	= .
	.globl	C$board.c$374$1_0$297
;src/engine/board.c:374: || board[position2check + 1] == CONNECTED_BLACK_CELL || board[position2check + 10] == CONNECTED_BLACK_CELL)) {
	ld	a, 0 (iy)
	sub	a, #0x06
	jr	Z, 00107$
	ld	a, (de)
	sub	a, #0x06
	jr	NZ, 00108$
00107$:
	C$board.c$375$2_0$299	= .
	.globl	C$board.c$375$2_0$299
;src/engine/board.c:375: return true;
	ld	a, #0x01
	jr	00113$
00108$:
	C$board.c$377$1_0$297	= .
	.globl	C$board.c$377$1_0$297
;src/engine/board.c:377: return false;
	xor	a, a
00113$:
	C$board.c$378$1_0$297	= .
	.globl	C$board.c$378$1_0$297
;src/engine/board.c:378: }
	inc	sp
	pop	ix
	C$board.c$378$1_0$297	= .
	.globl	C$board.c$378$1_0$297
	XG$neighbour_connected$0$0	= .
	.globl	XG$neighbour_connected$0$0
	ret
	G$neighbour_notprocessed$0$0	= .
	.globl	G$neighbour_notprocessed$0$0
	C$board.c$380$1_0$301	= .
	.globl	C$board.c$380$1_0$301
;src/engine/board.c:380: bool neighbour_notprocessed(u8 position2check) {
;	---------------------------------
; Function neighbour_notprocessed
; ---------------------------------
_neighbour_notprocessed::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	e, a
	C$board.c$381$1_0$301	= .
	.globl	C$board.c$381$1_0$301
;src/engine/board.c:381: if(board[position2check] == NOT_PROCESSED_WHITE_CELL && 
	ld	hl, #_board
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	ld	-1 (ix), a
	C$board.c$382$1_0$301	= .
	.globl	C$board.c$382$1_0$301
;src/engine/board.c:382: (board[position2check - 10] == NOT_PROCESSED_WHITE_CELL || board[position2check - 1] == NOT_PROCESSED_WHITE_CELL 
	ld	d, #0x00
	ld	a, e
	add	a, #0xf6
	ld	c, a
	ld	a, d
	adc	a, #0xff
	ld	b, a
	ld	l, e
	ld	h, d
	dec	hl
	C$board.c$383$1_0$301	= .
	.globl	C$board.c$383$1_0$301
;src/engine/board.c:383: || board[position2check + 1] == NOT_PROCESSED_WHITE_CELL || board[position2check + 10] == NOT_PROCESSED_WHITE_CELL)) {
	push	de
	pop	iy
	inc	iy
	ld	a, e
	add	a, #0x0a
	ld	e, a
	jr	NC, 00155$
	inc	d
00155$:
	C$board.c$382$1_0$301	= .
	.globl	C$board.c$382$1_0$301
;src/engine/board.c:382: (board[position2check - 10] == NOT_PROCESSED_WHITE_CELL || board[position2check - 1] == NOT_PROCESSED_WHITE_CELL 
	ld	a, #<(_board)
	add	a, c
	ld	c, a
	ld	a, #>(_board)
	adc	a, b
	ld	b, a
	ld	a, #<(_board)
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	C$board.c$383$1_0$301	= .
	.globl	C$board.c$383$1_0$301
;src/engine/board.c:383: || board[position2check + 1] == NOT_PROCESSED_WHITE_CELL || board[position2check + 10] == NOT_PROCESSED_WHITE_CELL)) {
	push	bc
	ld	bc, #_board
	add	iy, bc
	pop	bc
	ld	a, #<(_board)
	add	a, e
	ld	e, a
	ld	a, #>(_board)
	adc	a, d
	ld	d, a
	C$board.c$381$1_0$301	= .
	.globl	C$board.c$381$1_0$301
;src/engine/board.c:381: if(board[position2check] == NOT_PROCESSED_WHITE_CELL && 
	ld	a, -1 (ix)
	dec	a
	jr	NZ, 00102$
	C$board.c$382$1_0$301	= .
	.globl	C$board.c$382$1_0$301
;src/engine/board.c:382: (board[position2check - 10] == NOT_PROCESSED_WHITE_CELL || board[position2check - 1] == NOT_PROCESSED_WHITE_CELL 
	ld	a, (bc)
	dec	a
	jr	Z, 00101$
	ld	a, (hl)
	dec	a
	jr	Z, 00101$
	C$board.c$383$1_0$301	= .
	.globl	C$board.c$383$1_0$301
;src/engine/board.c:383: || board[position2check + 1] == NOT_PROCESSED_WHITE_CELL || board[position2check + 10] == NOT_PROCESSED_WHITE_CELL)) {
	ld	a, 0 (iy)
	dec	a
	jr	Z, 00101$
	ld	a, (de)
	dec	a
	jr	NZ, 00102$
00101$:
	C$board.c$384$2_0$302	= .
	.globl	C$board.c$384$2_0$302
;src/engine/board.c:384: return true;
	ld	a, #0x01
	jr	00113$
00102$:
	C$board.c$386$1_0$301	= .
	.globl	C$board.c$386$1_0$301
;src/engine/board.c:386: if(board[position2check] == NOT_PROCESSED_BLACK_CELL && 
	ld	a, -1 (ix)
	sub	a, #0x04
	jr	NZ, 00108$
	C$board.c$387$1_0$301	= .
	.globl	C$board.c$387$1_0$301
;src/engine/board.c:387: (board[position2check - 10] == NOT_PROCESSED_BLACK_CELL || board[position2check - 1] == NOT_PROCESSED_BLACK_CELL 
	ld	a, (bc)
	sub	a, #0x04
	jr	Z, 00107$
	ld	a, (hl)
	sub	a, #0x04
	jr	Z, 00107$
	C$board.c$388$1_0$301	= .
	.globl	C$board.c$388$1_0$301
;src/engine/board.c:388: || board[position2check + 1] == NOT_PROCESSED_BLACK_CELL || board[position2check + 10] == NOT_PROCESSED_BLACK_CELL)) {
	ld	a, 0 (iy)
	sub	a, #0x04
	jr	Z, 00107$
	ld	a, (de)
	sub	a, #0x04
	jr	NZ, 00108$
00107$:
	C$board.c$389$2_0$303	= .
	.globl	C$board.c$389$2_0$303
;src/engine/board.c:389: return true;
	ld	a, #0x01
	jr	00113$
00108$:
	C$board.c$391$1_0$301	= .
	.globl	C$board.c$391$1_0$301
;src/engine/board.c:391: return false;
	xor	a, a
00113$:
	C$board.c$392$1_0$301	= .
	.globl	C$board.c$392$1_0$301
;src/engine/board.c:392: }
	inc	sp
	pop	ix
	C$board.c$392$1_0$301	= .
	.globl	C$board.c$392$1_0$301
	XG$neighbour_notprocessed$0$0	= .
	.globl	XG$neighbour_notprocessed$0$0
	ret
	G$mark_as_disconnected$0$0	= .
	.globl	G$mark_as_disconnected$0$0
	C$board.c$394$1_0$305	= .
	.globl	C$board.c$394$1_0$305
;src/engine/board.c:394: bool mark_as_disconnected(u8 x, u8 y, u8 position2check) {
;	---------------------------------
; Function mark_as_disconnected
; ---------------------------------
_mark_as_disconnected::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, a
	ld	-1 (ix), l
	C$board.c$395$1_0$305	= .
	.globl	C$board.c$395$1_0$305
;src/engine/board.c:395: if(board[position2check] == NOT_PROCESSED_WHITE_CELL || board[position2check] == NOT_PROCESSED_BLACK_CELL) {
	ld	de, #_board+0
	ld	a, 4 (ix)
	add	a, e
	ld	e, a
	ld	a, #0x00
	adc	a, d
	ld	d, a
	ld	a, (de)
	cp	a, #0x01
	jr	Z, 00104$
	cp	a, #0x04
	jr	NZ, 00105$
00104$:
	C$board.c$396$2_0$306	= .
	.globl	C$board.c$396$2_0$306
;src/engine/board.c:396: board[position2check] = board[position2check] + 1;
	inc	a
	ld	(de), a
	C$board.c$397$3_0$307	= .
	.globl	C$board.c$397$3_0$307
;src/engine/board.c:397: draw_board_position(x, y, board[position2check]);
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, (de)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$board.c$398$2_0$306	= .
	.globl	C$board.c$398$2_0$306
;src/engine/board.c:398: return true;
	ld	a, #0x01
	jr	00110$
00105$:
	C$board.c$400$1_0$305	= .
	.globl	C$board.c$400$1_0$305
;src/engine/board.c:400: if(board[position2check] == NOT_CONNECTED_WHITE_CELL || board[position2check] == NOT_CONNECTED_BLACK_CELL) {
	cp	a, #0x02
	jr	Z, 00107$
	sub	a, #0x05
	jr	NZ, 00108$
00107$:
	C$board.c$401$2_0$308	= .
	.globl	C$board.c$401$2_0$308
;src/engine/board.c:401: return true;
	ld	a, #0x01
	jr	00110$
00108$:
	C$board.c$403$1_0$305	= .
	.globl	C$board.c$403$1_0$305
;src/engine/board.c:403: return false;
	xor	a, a
00110$:
	C$board.c$404$1_0$305	= .
	.globl	C$board.c$404$1_0$305
;src/engine/board.c:404: }
	inc	sp
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$destroy_disconnected$0$0	= .
	.globl	G$destroy_disconnected$0$0
	C$board.c$406$1_0$310	= .
	.globl	C$board.c$406$1_0$310
;src/engine/board.c:406: u8 destroy_disconnected(u8 x, u8 y, u8 position2check) {
;	---------------------------------
; Function destroy_disconnected
; ---------------------------------
_destroy_disconnected::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-25
	add	iy, sp
	ld	sp, iy
	ld	c, a
	ld	-2 (ix), l
	C$board.c$407$2_0$310	= .
	.globl	C$board.c$407$2_0$310
;src/engine/board.c:407: u8 numPiecesDestroyed = 0;
	ld	-1 (ix), #0x00
	C$board.c$408$1_0$310	= .
	.globl	C$board.c$408$1_0$310
;src/engine/board.c:408: if(board[position2check] == NOT_CONNECTED_WHITE_CELL) {
	ld	a, 4 (ix)
	add	a, #<(_board)
	ld	-25 (ix), a
	ld	a, #0x00
	adc	a, #>(_board)
	ld	-24 (ix), a
	pop	hl
	push	hl
	ld	a, (hl)
	ld	-23 (ix), a
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	b, c
	C$board.c$412$1_0$310	= .
	.globl	C$board.c$412$1_0$310
;src/engine/board.c:412: if((x > 0) && board[position2check-1] != NOT_CONNECTED_WHITE_CELL) {
	ld	e, 4 (ix)
	ld	d, #0x00
	C$board.c$417$1_0$310	= .
	.globl	C$board.c$417$1_0$310
;src/engine/board.c:417: if((x < BOARD_COLUMNS - 1) && board[position2check+1] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, c
	sub	a, #0x09
	ld	a, #0x00
	rla
	ld	-22 (ix), a
	C$board.c$427$1_0$310	= .
	.globl	C$board.c$427$1_0$310
;src/engine/board.c:427: if((y < BOARD_ROWS-1) && board[position2check+10] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, -2 (ix)
	sub	a, #0x15
	ld	a, #0x00
	rla
	ld	-21 (ix), a
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	push	hl
	pop	iy
	inc	iy
	ld	a, b
	add	a, #0x0b
	ld	-4 (ix), a
	C$board.c$412$1_0$310	= .
	.globl	C$board.c$412$1_0$310
;src/engine/board.c:412: if((x > 0) && board[position2check-1] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, e
	add	a, #0xff
	ld	-12 (ix), a
	ld	a, d
	adc	a, #0xff
	ld	-11 (ix), a
	C$board.c$414$1_0$310	= .
	.globl	C$board.c$414$1_0$310
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	ld	a, b
	add	a, #0x0a
	ld	-3 (ix), a
	C$board.c$417$1_0$310	= .
	.globl	C$board.c$417$1_0$310
;src/engine/board.c:417: if((x < BOARD_COLUMNS - 1) && board[position2check+1] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, e
	add	a, #0x01
	ld	-10 (ix), a
	ld	a, d
	adc	a, #0x00
	ld	-9 (ix), a
	C$board.c$419$1_0$310	= .
	.globl	C$board.c$419$1_0$310
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	ld	a, b
	add	a, #0x0c
	ld	b, a
	C$board.c$422$1_0$310	= .
	.globl	C$board.c$422$1_0$310
;src/engine/board.c:422: if((y>0) && board[position2check-10] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, e
	add	a, #0xf6
	ld	-8 (ix), a
	ld	a, d
	adc	a, #0xff
	ld	-7 (ix), a
	C$board.c$424$1_0$310	= .
	.globl	C$board.c$424$1_0$310
;src/engine/board.c:424: draw_board_position(x, y-1, EXPLODING_CELL);
	ld	-6 (ix), l
	ld	-5 (ix), h
	C$board.c$427$1_0$310	= .
	.globl	C$board.c$427$1_0$310
;src/engine/board.c:427: if((y < BOARD_ROWS-1) && board[position2check+10] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, e
	add	a, #0x0a
	ld	e, a
	jr	NC, 00252$
	inc	d
00252$:
	C$board.c$429$1_0$310	= .
	.globl	C$board.c$429$1_0$310
;src/engine/board.c:429: draw_board_position(x, y+1, EXPLODING_CELL);
	inc	hl
	inc	hl
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	C$board.c$412$1_0$310	= .
	.globl	C$board.c$412$1_0$310
;src/engine/board.c:412: if((x > 0) && board[position2check-1] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, #<(_board)
	add	a, -12 (ix)
	ld	-20 (ix), a
	ld	a, #>(_board)
	adc	a, -11 (ix)
	ld	-19 (ix), a
	C$board.c$414$1_0$310	= .
	.globl	C$board.c$414$1_0$310
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	C$board.c$417$1_0$310	= .
	.globl	C$board.c$417$1_0$310
;src/engine/board.c:417: if((x < BOARD_COLUMNS - 1) && board[position2check+1] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, -10 (ix)
	add	a, #<(_board)
	ld	-18 (ix), a
	ld	a, -9 (ix)
	adc	a, #>(_board)
	ld	-17 (ix), a
	C$board.c$419$1_0$310	= .
	.globl	C$board.c$419$1_0$310
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	C$board.c$422$1_0$310	= .
	.globl	C$board.c$422$1_0$310
;src/engine/board.c:422: if((y>0) && board[position2check-10] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, #<(_board)
	add	a, -8 (ix)
	ld	-16 (ix), a
	ld	a, #>(_board)
	adc	a, -7 (ix)
	ld	-15 (ix), a
	C$board.c$424$1_0$310	= .
	.globl	C$board.c$424$1_0$310
;src/engine/board.c:424: draw_board_position(x, y-1, EXPLODING_CELL);
	ld	a, -6 (ix)
	ld	-8 (ix), a
	ld	a, -5 (ix)
	ld	-7 (ix), a
	ld	a, #0x05
00253$:
	sla	-8 (ix)
	rl	-7 (ix)
	dec	a
	jr	NZ, 00253$
	C$board.c$427$1_0$310	= .
	.globl	C$board.c$427$1_0$310
;src/engine/board.c:427: if((y < BOARD_ROWS-1) && board[position2check+10] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, #<(_board)
	add	a, e
	ld	-14 (ix), a
	ld	a, #>(_board)
	adc	a, d
	ld	-13 (ix), a
	C$board.c$429$1_0$310	= .
	.globl	C$board.c$429$1_0$310
;src/engine/board.c:429: draw_board_position(x, y+1, EXPLODING_CELL);
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	push	iy
	pop	de
	ex	de, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	ld	a, -4 (ix)
	ld	-6 (ix), a
	ld	-5 (ix), #0x00
	C$board.c$414$1_0$310	= .
	.globl	C$board.c$414$1_0$310
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	ld	a, -3 (ix)
	ld	-4 (ix), a
	ld	-3 (ix), #0x00
	C$board.c$419$1_0$310	= .
	.globl	C$board.c$419$1_0$310
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	ld	a, b
	push	iy
	ex	(sp), hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ex	(sp), hl
	pop	iy
	C$board.c$429$1_0$310	= .
	.globl	C$board.c$429$1_0$310
;src/engine/board.c:429: draw_board_position(x, y+1, EXPLODING_CELL);
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	-10 (ix), l
	ld	-9 (ix), h
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	C$board.c$414$1_0$310	= .
	.globl	C$board.c$414$1_0$310
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	C$board.c$419$1_0$310	= .
	.globl	C$board.c$419$1_0$310
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	push	iy
	ld	-27 (ix), a
	pop	iy
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	ld	a, e
	add	a, -6 (ix)
	ld	-4 (ix), a
	ld	a, d
	adc	a, -5 (ix)
	ld	-3 (ix), a
	C$board.c$414$1_0$310	= .
	.globl	C$board.c$414$1_0$310
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	add	hl, de
	C$board.c$419$1_0$310	= .
	.globl	C$board.c$419$1_0$310
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	add	iy, de
	C$board.c$424$1_0$310	= .
	.globl	C$board.c$424$1_0$310
;src/engine/board.c:424: draw_board_position(x, y-1, EXPLODING_CELL);
	ld	a, -6 (ix)
	add	a, -8 (ix)
	ld	e, a
	ld	a, -5 (ix)
	adc	a, -7 (ix)
	ld	d, a
	C$board.c$429$1_0$310	= .
	.globl	C$board.c$429$1_0$310
;src/engine/board.c:429: draw_board_position(x, y+1, EXPLODING_CELL);
	ld	a, -10 (ix)
	add	a, -6 (ix)
	ld	-8 (ix), a
	ld	a, -9 (ix)
	adc	a, -5 (ix)
	ld	-7 (ix), a
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	ld	a, -4 (ix)
	ld	b, -3 (ix)
	add	a, a
	rl	b
	C$board.c$414$1_0$310	= .
	.globl	C$board.c$414$1_0$310
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	add	hl, hl
	ld	-6 (ix), l
	ld	-5 (ix), h
	C$board.c$419$1_0$310	= .
	.globl	C$board.c$419$1_0$310
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	push	iy
	pop	hl
	add	hl, hl
	C$board.c$424$1_0$310	= .
	.globl	C$board.c$424$1_0$310
;src/engine/board.c:424: draw_board_position(x, y-1, EXPLODING_CELL);
	ex	de, hl
	add	hl, hl
	ex	de, hl
	ld	-4 (ix), e
	ld	-3 (ix), d
	C$board.c$429$1_0$310	= .
	.globl	C$board.c$429$1_0$310
;src/engine/board.c:429: draw_board_position(x, y+1, EXPLODING_CELL);
	ld	e, -8 (ix)
	ld	d, -7 (ix)
	ex	de, hl
	add	hl, hl
	ex	de, hl
	C$board.c$410$1_0$310	= .
	.globl	C$board.c$410$1_0$310
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	ld	-12 (ix), a
	ld	a, b
	or	a, #0x78
	ld	-11 (ix), a
	C$board.c$414$1_0$310	= .
	.globl	C$board.c$414$1_0$310
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	ld	a, -6 (ix)
	ld	-10 (ix), a
	ld	a, -5 (ix)
	or	a, #0x78
	ld	-9 (ix), a
	C$board.c$419$1_0$310	= .
	.globl	C$board.c$419$1_0$310
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	ld	-8 (ix), l
	ld	a, h
	or	a, #0x78
	ld	-7 (ix), a
	C$board.c$424$1_0$310	= .
	.globl	C$board.c$424$1_0$310
;src/engine/board.c:424: draw_board_position(x, y-1, EXPLODING_CELL);
	ld	a, -4 (ix)
	ld	-6 (ix), a
	ld	a, -3 (ix)
	or	a, #0x78
	ld	-5 (ix), a
	C$board.c$429$1_0$310	= .
	.globl	C$board.c$429$1_0$310
;src/engine/board.c:429: draw_board_position(x, y+1, EXPLODING_CELL);
	ld	-4 (ix), e
	ld	a, d
	or	a, #0x78
	ld	-3 (ix), a
	C$board.c$408$1_0$310	= .
	.globl	C$board.c$408$1_0$310
;src/engine/board.c:408: if(board[position2check] == NOT_CONNECTED_WHITE_CELL) {
	ld	a, -23 (ix)
	sub	a, #0x02
	jp	NZ,00158$
	C$board.c$409$2_0$311	= .
	.globl	C$board.c$409$2_0$311
;src/engine/board.c:409: board[position2check] = EXPLODING_CELL;
	pop	hl
	ld	(hl), #0x0b
	push	hl
	C$board.c$410$3_0$312	= .
	.globl	C$board.c$410$3_0$312
;src/engine/board.c:410: draw_board_position(x, y, EXPLODING_CELL);
	push	bc
	ld	l, -12 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -11 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	pop	bc
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$411$2_0$311	= .
	.globl	C$board.c$411$2_0$311
;src/engine/board.c:411: numPiecesDestroyed++;
	ld	-1 (ix), #0x01
	C$board.c$412$2_0$311	= .
	.globl	C$board.c$412$2_0$311
;src/engine/board.c:412: if((x > 0) && board[position2check-1] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, c
	or	a, a
	jr	Z, 00108$
	ld	l, -20 (ix)
	ld	h, -19 (ix)
	ld	a, (hl)
	sub	a, #0x02
	jr	Z, 00108$
	C$board.c$413$3_0$313	= .
	.globl	C$board.c$413$3_0$313
;src/engine/board.c:413: board[position2check-1] = EXPLODING_CELL;
	ld	l, -20 (ix)
	ld	h, -19 (ix)
	ld	(hl), #0x0b
	C$board.c$414$4_0$314	= .
	.globl	C$board.c$414$4_0$314
;src/engine/board.c:414: draw_board_position(x-1, y, EXPLODING_CELL);
	ld	l, -10 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -9 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$415$3_0$313	= .
	.globl	C$board.c$415$3_0$313
;src/engine/board.c:415: numPiecesDestroyed++;
	ld	-1 (ix), #0x02
00108$:
	C$board.c$417$2_0$311	= .
	.globl	C$board.c$417$2_0$311
;src/engine/board.c:417: if((x < BOARD_COLUMNS - 1) && board[position2check+1] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, -22 (ix)
	or	a, a
	jr	Z, 00114$
	ld	l, -18 (ix)
	ld	h, -17 (ix)
	ld	a, (hl)
	sub	a, #0x02
	jr	Z, 00114$
	C$board.c$418$3_0$315	= .
	.globl	C$board.c$418$3_0$315
;src/engine/board.c:418: board[position2check+1] = EXPLODING_CELL;
	ld	l, -18 (ix)
	ld	h, -17 (ix)
	ld	(hl), #0x0b
	C$board.c$419$4_0$316	= .
	.globl	C$board.c$419$4_0$316
;src/engine/board.c:419: draw_board_position(x+1, y, EXPLODING_CELL);
	ld	l, -8 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -7 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$420$3_0$315	= .
	.globl	C$board.c$420$3_0$315
;src/engine/board.c:420: numPiecesDestroyed++;
	inc	-1 (ix)
00114$:
	C$board.c$422$2_0$311	= .
	.globl	C$board.c$422$2_0$311
;src/engine/board.c:422: if((y>0) && board[position2check-10] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00120$
	ld	l, -16 (ix)
	ld	h, -15 (ix)
	ld	a, (hl)
	sub	a, #0x02
	jr	Z, 00120$
	C$board.c$423$3_0$317	= .
	.globl	C$board.c$423$3_0$317
;src/engine/board.c:423: board[position2check-10] = EXPLODING_CELL;
	ld	l, -16 (ix)
	ld	h, -15 (ix)
	ld	(hl), #0x0b
	C$board.c$424$4_0$318	= .
	.globl	C$board.c$424$4_0$318
;src/engine/board.c:424: draw_board_position(x, y-1, EXPLODING_CELL);
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$425$3_0$317	= .
	.globl	C$board.c$425$3_0$317
;src/engine/board.c:425: numPiecesDestroyed++;
	inc	-1 (ix)
00120$:
	C$board.c$427$2_0$311	= .
	.globl	C$board.c$427$2_0$311
;src/engine/board.c:427: if((y < BOARD_ROWS-1) && board[position2check+10] != NOT_CONNECTED_WHITE_CELL) {
	ld	a, -21 (ix)
	or	a, a
	jp	Z, 00159$
	ld	l, -14 (ix)
	ld	h, -13 (ix)
	ld	a, (hl)
	sub	a, #0x02
	jp	Z,00159$
	C$board.c$428$3_0$319	= .
	.globl	C$board.c$428$3_0$319
;src/engine/board.c:428: board[position2check+10] = EXPLODING_CELL;
	ld	l, -14 (ix)
	ld	h, -13 (ix)
	ld	(hl), #0x0b
	C$board.c$429$4_0$320	= .
	.globl	C$board.c$429$4_0$320
;src/engine/board.c:429: draw_board_position(x, y+1, EXPLODING_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$430$3_0$319	= .
	.globl	C$board.c$430$3_0$319
;src/engine/board.c:430: numPiecesDestroyed++;
	inc	-1 (ix)
	jp	00159$
00158$:
	C$board.c$432$1_0$310	= .
	.globl	C$board.c$432$1_0$310
;src/engine/board.c:432: } else if(board[position2check] == NOT_CONNECTED_BLACK_CELL) {
	ld	a, -23 (ix)
	sub	a, #0x05
	jp	NZ,00159$
	C$board.c$433$2_0$321	= .
	.globl	C$board.c$433$2_0$321
;src/engine/board.c:433: board[position2check] = EXPLODING_CELL;
	pop	hl
	ld	(hl), #0x0b
	push	hl
	C$board.c$434$3_0$322	= .
	.globl	C$board.c$434$3_0$322
;src/engine/board.c:434: draw_board_position(x, y, EXPLODING_CELL);
	push	bc
	ld	l, -12 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -11 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	pop	bc
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$435$2_0$321	= .
	.globl	C$board.c$435$2_0$321
;src/engine/board.c:435: numPiecesDestroyed++;
	ld	-1 (ix), #0x01
	C$board.c$436$2_0$321	= .
	.globl	C$board.c$436$2_0$321
;src/engine/board.c:436: if((x > 0) && board[position2check-1] != NOT_CONNECTED_BLACK_CELL) {
	ld	a, c
	or	a, a
	jr	Z, 00135$
	ld	l, -20 (ix)
	ld	h, -19 (ix)
	ld	a, (hl)
	sub	a, #0x05
	jr	Z, 00135$
	C$board.c$437$3_0$323	= .
	.globl	C$board.c$437$3_0$323
;src/engine/board.c:437: board[position2check-1] = EXPLODING_CELL;
	ld	l, -20 (ix)
	ld	h, -19 (ix)
	ld	(hl), #0x0b
	C$board.c$438$4_0$324	= .
	.globl	C$board.c$438$4_0$324
;src/engine/board.c:438: draw_board_position(x-1, y, EXPLODING_CELL);
	ld	l, -10 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -9 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$439$3_0$323	= .
	.globl	C$board.c$439$3_0$323
;src/engine/board.c:439: numPiecesDestroyed++;
	ld	-1 (ix), #0x02
00135$:
	C$board.c$441$2_0$321	= .
	.globl	C$board.c$441$2_0$321
;src/engine/board.c:441: if((x < BOARD_COLUMNS - 1) && board[position2check+1] != NOT_CONNECTED_BLACK_CELL) {
	ld	a, -22 (ix)
	or	a, a
	jr	Z, 00141$
	ld	l, -18 (ix)
	ld	h, -17 (ix)
	ld	a, (hl)
	sub	a, #0x05
	jr	Z, 00141$
	C$board.c$442$3_0$325	= .
	.globl	C$board.c$442$3_0$325
;src/engine/board.c:442: board[position2check+1] = EXPLODING_CELL;
	ld	l, -18 (ix)
	ld	h, -17 (ix)
	ld	(hl), #0x0b
	C$board.c$443$4_0$326	= .
	.globl	C$board.c$443$4_0$326
;src/engine/board.c:443: draw_board_position(x+1, y, EXPLODING_CELL);
	ld	l, -8 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -7 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$444$3_0$325	= .
	.globl	C$board.c$444$3_0$325
;src/engine/board.c:444: numPiecesDestroyed++;
	inc	-1 (ix)
00141$:
	C$board.c$446$2_0$321	= .
	.globl	C$board.c$446$2_0$321
;src/engine/board.c:446: if((y>0) && board[position2check-10] != NOT_CONNECTED_BLACK_CELL) {
	ld	a, -2 (ix)
	or	a, a
	jr	Z, 00147$
	ld	l, -16 (ix)
	ld	h, -15 (ix)
	ld	a, (hl)
	sub	a, #0x05
	jr	Z, 00147$
	C$board.c$447$3_0$327	= .
	.globl	C$board.c$447$3_0$327
;src/engine/board.c:447: board[position2check-10] = EXPLODING_CELL;
	ld	l, -16 (ix)
	ld	h, -15 (ix)
	ld	(hl), #0x0b
	C$board.c$448$4_0$328	= .
	.globl	C$board.c$448$4_0$328
;src/engine/board.c:448: draw_board_position(x, y-1, EXPLODING_CELL);
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$449$3_0$327	= .
	.globl	C$board.c$449$3_0$327
;src/engine/board.c:449: numPiecesDestroyed++;
	inc	-1 (ix)
00147$:
	C$board.c$451$2_0$321	= .
	.globl	C$board.c$451$2_0$321
;src/engine/board.c:451: if((y < BOARD_ROWS-1) && board[position2check+10] != NOT_CONNECTED_BLACK_CELL) {
	ld	a, -21 (ix)
	or	a, a
	jr	Z, 00159$
	ld	l, -14 (ix)
	ld	h, -13 (ix)
	ld	a, (hl)
	sub	a, #0x05
	jr	Z, 00159$
	C$board.c$452$3_0$329	= .
	.globl	C$board.c$452$3_0$329
;src/engine/board.c:452: board[position2check+10] = EXPLODING_CELL;
	ld	l, -14 (ix)
	ld	h, -13 (ix)
	ld	(hl), #0x0b
	C$board.c$453$4_0$330	= .
	.globl	C$board.c$453$4_0$330
;src/engine/board.c:453: draw_board_position(x, y+1, EXPLODING_CELL);
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080b
	rst	#0x18
	C$board.c$454$3_0$329	= .
	.globl	C$board.c$454$3_0$329
;src/engine/board.c:454: numPiecesDestroyed++;
	inc	-1 (ix)
00159$:
	C$board.c$457$1_0$310	= .
	.globl	C$board.c$457$1_0$310
;src/engine/board.c:457: return numPiecesDestroyed;
	ld	a, -1 (ix)
	C$board.c$458$1_0$310	= .
	.globl	C$board.c$458$1_0$310
;src/engine/board.c:458: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$clean_explosion$0$0	= .
	.globl	G$clean_explosion$0$0
	C$board.c$460$1_0$332	= .
	.globl	C$board.c$460$1_0$332
;src/engine/board.c:460: void clean_explosion(u8 x, u8 y, u8 position2check) {
;	---------------------------------
; Function clean_explosion
; ---------------------------------
_clean_explosion::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	c, a
	ld	b, l
	C$board.c$461$1_0$332	= .
	.globl	C$board.c$461$1_0$332
;src/engine/board.c:461: board[position2check] = EMPTY_CELL;
	ld	de, #_board+0
	ld	l, 4 (ix)
	ld	h, #0x00
	add	hl, de
	ld	(hl), #0x00
	C$board.c$462$2_0$333	= .
	.globl	C$board.c$462$2_0$333
;src/engine/board.c:462: draw_board_position(x, y, EMPTY_CELL);
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	rst	#0x18
	C$board.c$463$1_0$332	= .
	.globl	C$board.c$463$1_0$332
;src/engine/board.c:463: }
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$clear_board_position$0$0	= .
	.globl	G$clear_board_position$0$0
	C$board.c$465$1_0$335	= .
	.globl	C$board.c$465$1_0$335
;src/engine/board.c:465: void clear_board_position(u8 x, u8 y) {
;	---------------------------------
; Function clear_board_position
; ---------------------------------
_clear_board_position::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, a
	C$board.c$466$1_0$335	= .
	.globl	C$board.c$466$1_0$335
;src/engine/board.c:466: u8 position = (y << 3) + (y << 1) + x;
	ld	-1 (ix), l
	ld	e, l
	ld	a, e
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	add	a, c
	ld	e, a
	C$board.c$467$1_0$335	= .
	.globl	C$board.c$467$1_0$335
;src/engine/board.c:467: board[position] = EMPTY_CELL;
	ld	hl, #_board+0
	ld	d, #0x00
	add	hl, de
	ld	(hl), #0x00
	C$board.c$468$2_0$336	= .
	.globl	C$board.c$468$2_0$336
;src/engine/board.c:468: draw_board_position(x,y,EMPTY_CELL);
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0800
	rst	#0x18
	C$board.c$469$1_0$335	= .
	.globl	C$board.c$469$1_0$335
;src/engine/board.c:469: }
	inc	sp
	pop	ix
	C$board.c$469$1_0$335	= .
	.globl	C$board.c$469$1_0$335
	XG$clear_board_position$0$0	= .
	.globl	XG$clear_board_position$0$0
	ret
	G$block_board_position$0$0	= .
	.globl	G$block_board_position$0$0
	C$board.c$471$1_0$338	= .
	.globl	C$board.c$471$1_0$338
;src/engine/board.c:471: void block_board_position(u8 x, u8 y) {
;	---------------------------------
; Function block_board_position
; ---------------------------------
_block_board_position::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, a
	C$board.c$472$1_0$338	= .
	.globl	C$board.c$472$1_0$338
;src/engine/board.c:472: u8 position = (y << 3) + (y << 1) + x;
	ld	-1 (ix), l
	ld	e, l
	ld	a, e
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	add	a, c
	ld	e, a
	C$board.c$473$1_0$338	= .
	.globl	C$board.c$473$1_0$338
;src/engine/board.c:473: board[position] = BLOCKED_CELL;
	ld	hl, #_board+0
	ld	d, #0x00
	add	hl, de
	ld	(hl), #0x0a
	C$board.c$474$2_0$339	= .
	.globl	C$board.c$474$2_0$339
;src/engine/board.c:474: draw_board_position(x,y,BLOCKED_CELL);
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x080a
	rst	#0x18
	C$board.c$475$1_0$338	= .
	.globl	C$board.c$475$1_0$338
;src/engine/board.c:475: }
	inc	sp
	pop	ix
	C$board.c$475$1_0$338	= .
	.globl	C$board.c$475$1_0$338
	XG$block_board_position$0$0	= .
	.globl	XG$block_board_position$0$0
	ret
	G$add_piece_board_position$0$0	= .
	.globl	G$add_piece_board_position$0$0
	C$board.c$477$1_0$341	= .
	.globl	C$board.c$477$1_0$341
;src/engine/board.c:477: void add_piece_board_position(u8 x, u8 y, u8 color) {
;	---------------------------------
; Function add_piece_board_position
; ---------------------------------
_add_piece_board_position::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, a
	C$board.c$478$1_0$341	= .
	.globl	C$board.c$478$1_0$341
;src/engine/board.c:478: u8 position = (y << 3) + (y << 1) + x;
	ld	-1 (ix), l
	ld	e, l
	ld	a, e
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	add	a, c
	ld	e, a
	C$board.c$479$1_0$341	= .
	.globl	C$board.c$479$1_0$341
;src/engine/board.c:479: board[position] = color;
	ld	hl, #_board+0
	ld	d, #0x00
	add	hl, de
	ld	a, 4 (ix)
	ld	(hl), a
	C$board.c$480$2_0$342	= .
	.globl	C$board.c$480$2_0$342
;src/engine/board.c:480: draw_board_position(x,y,color);
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	l, 4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$board.c$481$1_0$341	= .
	.globl	C$board.c$481$1_0$341
;src/engine/board.c:481: }
	inc	sp
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$getCollisionRowFromColumn$0$0	= .
	.globl	G$getCollisionRowFromColumn$0$0
	C$board.c$483$1_0$344	= .
	.globl	C$board.c$483$1_0$344
;src/engine/board.c:483: u8 getCollisionRowFromColumn(u8 position2check) {
;	---------------------------------
; Function getCollisionRowFromColumn
; ---------------------------------
_getCollisionRowFromColumn::
	ld	c, a
	C$board.c$484$2_0$344	= .
	.globl	C$board.c$484$2_0$344
;src/engine/board.c:484: u8 current_row = 0;
	C$board.c$485$1_0$344	= .
	.globl	C$board.c$485$1_0$344
;src/engine/board.c:485: while(position2check < 209) { //220 - 1 - 10 ya que nunca estamos en la última fila
	ld	de, #0x0
00104$:
	ld	a, c
	sub	a, #0xd1
	jr	NC, 00106$
	C$board.c$486$2_0$345	= .
	.globl	C$board.c$486$2_0$345
;src/engine/board.c:486: if(board[position2check] != EMPTY_CELL || board[++position2check] != EMPTY_CELL) {
	ld	hl, #_board
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00101$
	inc	c
	ld	hl, #_board
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
00101$:
	C$board.c$487$3_0$346	= .
	.globl	C$board.c$487$3_0$346
;src/engine/board.c:487: return current_row;
	ld	a, e
	ret
00102$:
	C$board.c$489$2_0$345	= .
	.globl	C$board.c$489$2_0$345
;src/engine/board.c:489: position2check = position2check + 9;
	ld	a, c
	add	a, #0x09
	ld	c, a
	C$board.c$490$2_0$345	= .
	.globl	C$board.c$490$2_0$345
;src/engine/board.c:490: current_row++;
	inc	d
	ld	e, d
	jr	00104$
00106$:
	C$board.c$492$1_0$344	= .
	.globl	C$board.c$492$1_0$344
;src/engine/board.c:492: return current_row;
	ld	a, e
	C$board.c$493$1_0$344	= .
	.globl	C$board.c$493$1_0$344
;src/engine/board.c:493: }
	C$board.c$493$1_0$344	= .
	.globl	C$board.c$493$1_0$344
	XG$getCollisionRowFromColumn$0$0	= .
	.globl	XG$getCollisionRowFromColumn$0$0
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
