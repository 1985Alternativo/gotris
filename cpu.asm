;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module cpu
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _get_rand
	.globl _getCollisionRowFromColumn
	.globl _getTargetBottomPart
	.globl _getTargetUpperPart
	.globl _decideNextMove
	.globl _decideDirection
	.globl _decideRotation
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$getTargetBottomPart$0$0	= .
	.globl	G$getTargetBottomPart$0$0
	C$cpu.c$11$0_0$84	= .
	.globl	C$cpu.c$11$0_0$84
;src/engine/cpu.c:11: void getTargetBottomPart(u8 color, u8 column) {
;	---------------------------------
; Function getTargetBottomPart
; ---------------------------------
_getTargetBottomPart::
	ld	c, a
	ld	b, l
	C$cpu.c$12$1_0$84	= .
	.globl	C$cpu.c$12$1_0$84
;src/engine/cpu.c:12: if(current_square.color10 == color) {
	ld	a, (#_current_square + 4)
	sub	a, c
	jr	NZ, 00116$
	C$cpu.c$13$2_0$85	= .
	.globl	C$cpu.c$13$2_0$85
;src/engine/cpu.c:13: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	ret
00116$:
	C$cpu.c$14$1_0$84	= .
	.globl	C$cpu.c$14$1_0$84
;src/engine/cpu.c:14: } else if(current_square.color11 == color) {
	ld	hl, #_current_square + 5
	ld	e, (hl)
	C$cpu.c$16$1_0$84	= .
	.globl	C$cpu.c$16$1_0$84
;src/engine/cpu.c:16: target_column = column-1;
	ld	d, b
	dec	d
	C$cpu.c$14$1_0$84	= .
	.globl	C$cpu.c$14$1_0$84
;src/engine/cpu.c:14: } else if(current_square.color11 == color) {
	ld	a, c
	C$cpu.c$15$2_0$86	= .
	.globl	C$cpu.c$15$2_0$86
;src/engine/cpu.c:15: if(column) {
	sub	a,e
	jr	NZ, 00113$
	or	a,b
	jr	Z, 00102$
	C$cpu.c$16$3_0$87	= .
	.globl	C$cpu.c$16$3_0$87
;src/engine/cpu.c:16: target_column = column-1;
	ld	hl, #_target_column
	ld	(hl), d
	ret
00102$:
	C$cpu.c$18$3_0$88	= .
	.globl	C$cpu.c$18$3_0$88
;src/engine/cpu.c:18: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	C$cpu.c$19$3_0$88	= .
	.globl	C$cpu.c$19$3_0$88
;src/engine/cpu.c:19: target_rotation = 4;
	ld	hl, #_target_rotation
	ld	(hl), #0x04
	ret
00113$:
	C$cpu.c$21$1_0$84	= .
	.globl	C$cpu.c$21$1_0$84
;src/engine/cpu.c:21: } else if(current_square.color00 == color) {
	ld	hl, #_current_square + 2
	ld	e, (hl)
	ld	a, c
	sub	a, e
	jr	NZ, 00110$
	C$cpu.c$22$2_0$89	= .
	.globl	C$cpu.c$22$2_0$89
;src/engine/cpu.c:22: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	C$cpu.c$23$2_0$89	= .
	.globl	C$cpu.c$23$2_0$89
;src/engine/cpu.c:23: target_rotation = 2;
	ld	hl, #_target_rotation
	ld	(hl), #0x02
	ret
00110$:
	C$cpu.c$24$1_0$84	= .
	.globl	C$cpu.c$24$1_0$84
;src/engine/cpu.c:24: } else if(current_square.color01 == color) {
	ld	hl, #_current_square + 3
	ld	e, (hl)
	ld	a, c
	C$cpu.c$25$2_0$90	= .
	.globl	C$cpu.c$25$2_0$90
;src/engine/cpu.c:25: if(column) {
	sub	a,e
	ret	NZ
	or	a,b
	jr	Z, 00105$
	C$cpu.c$26$3_0$91	= .
	.globl	C$cpu.c$26$3_0$91
;src/engine/cpu.c:26: target_column = column-1;
	ld	hl, #_target_column
	ld	(hl), d
	C$cpu.c$27$3_0$91	= .
	.globl	C$cpu.c$27$3_0$91
;src/engine/cpu.c:27: target_rotation = 1;
	ld	hl, #_target_rotation
	ld	(hl), #0x01
	ret
00105$:
	C$cpu.c$29$3_0$92	= .
	.globl	C$cpu.c$29$3_0$92
;src/engine/cpu.c:29: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	C$cpu.c$30$3_0$92	= .
	.globl	C$cpu.c$30$3_0$92
;src/engine/cpu.c:30: target_rotation = 4;
	ld	hl, #_target_rotation
	ld	(hl), #0x04
	C$cpu.c$33$1_0$84	= .
	.globl	C$cpu.c$33$1_0$84
;src/engine/cpu.c:33: }
	C$cpu.c$33$1_0$84	= .
	.globl	C$cpu.c$33$1_0$84
	XG$getTargetBottomPart$0$0	= .
	.globl	XG$getTargetBottomPart$0$0
	ret
	G$getTargetUpperPart$0$0	= .
	.globl	G$getTargetUpperPart$0$0
	C$cpu.c$35$1_0$94	= .
	.globl	C$cpu.c$35$1_0$94
;src/engine/cpu.c:35: void getTargetUpperPart(u8 color, u8 column) {
;	---------------------------------
; Function getTargetUpperPart
; ---------------------------------
_getTargetUpperPart::
	ld	c, a
	ld	b, l
	C$cpu.c$36$1_0$94	= .
	.globl	C$cpu.c$36$1_0$94
;src/engine/cpu.c:36: if(current_square.color10 == color) {
	ld	a, (#_current_square + 4)
	sub	a, c
	jr	NZ, 00116$
	C$cpu.c$37$2_0$95	= .
	.globl	C$cpu.c$37$2_0$95
;src/engine/cpu.c:37: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	C$cpu.c$38$2_0$95	= .
	.globl	C$cpu.c$38$2_0$95
;src/engine/cpu.c:38: target_rotation = 4;
	ld	hl, #_target_rotation
	ld	(hl), #0x04
	ret
00116$:
	C$cpu.c$39$1_0$94	= .
	.globl	C$cpu.c$39$1_0$94
;src/engine/cpu.c:39: } else if(current_square.color11 == color) {
	ld	hl, #_current_square + 5
	ld	e, (hl)
	C$cpu.c$41$1_0$94	= .
	.globl	C$cpu.c$41$1_0$94
;src/engine/cpu.c:41: target_column = column-1;
	ld	d, b
	dec	d
	C$cpu.c$39$1_0$94	= .
	.globl	C$cpu.c$39$1_0$94
;src/engine/cpu.c:39: } else if(current_square.color11 == color) {
	ld	a, c
	C$cpu.c$40$2_0$96	= .
	.globl	C$cpu.c$40$2_0$96
;src/engine/cpu.c:40: if(column) {
	sub	a,e
	jr	NZ, 00113$
	or	a,b
	jr	Z, 00102$
	C$cpu.c$41$3_0$97	= .
	.globl	C$cpu.c$41$3_0$97
;src/engine/cpu.c:41: target_column = column-1;
	ld	hl, #_target_column
	ld	(hl), d
	C$cpu.c$42$3_0$97	= .
	.globl	C$cpu.c$42$3_0$97
;src/engine/cpu.c:42: target_rotation = 2;
	ld	hl, #_target_rotation
	ld	(hl), #0x02
	ret
00102$:
	C$cpu.c$44$3_0$98	= .
	.globl	C$cpu.c$44$3_0$98
;src/engine/cpu.c:44: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	C$cpu.c$45$3_0$98	= .
	.globl	C$cpu.c$45$3_0$98
;src/engine/cpu.c:45: target_rotation = 5;
	ld	hl, #_target_rotation
	ld	(hl), #0x05
	ret
00113$:
	C$cpu.c$47$1_0$94	= .
	.globl	C$cpu.c$47$1_0$94
;src/engine/cpu.c:47: } else if(current_square.color00 == color) {
	ld	hl, #_current_square + 2
	ld	e, (hl)
	ld	a, c
	sub	a, e
	jr	NZ, 00110$
	C$cpu.c$48$2_0$99	= .
	.globl	C$cpu.c$48$2_0$99
;src/engine/cpu.c:48: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	ret
00110$:
	C$cpu.c$49$1_0$94	= .
	.globl	C$cpu.c$49$1_0$94
;src/engine/cpu.c:49: } else if(current_square.color01 == color) {
	ld	hl, #_current_square + 3
	ld	e, (hl)
	ld	a, c
	C$cpu.c$50$2_0$100	= .
	.globl	C$cpu.c$50$2_0$100
;src/engine/cpu.c:50: if(column) {
	sub	a,e
	ret	NZ
	or	a,b
	jr	Z, 00105$
	C$cpu.c$51$3_0$101	= .
	.globl	C$cpu.c$51$3_0$101
;src/engine/cpu.c:51: target_column = column-1;
	ld	hl, #_target_column
	ld	(hl), d
	ret
00105$:
	C$cpu.c$53$3_0$102	= .
	.globl	C$cpu.c$53$3_0$102
;src/engine/cpu.c:53: target_column = column;
	ld	hl, #_target_column
	ld	(hl), b
	C$cpu.c$54$3_0$102	= .
	.globl	C$cpu.c$54$3_0$102
;src/engine/cpu.c:54: target_rotation = 2;
	ld	hl, #_target_rotation
	ld	(hl), #0x02
	C$cpu.c$57$1_0$94	= .
	.globl	C$cpu.c$57$1_0$94
;src/engine/cpu.c:57: }
	C$cpu.c$57$1_0$94	= .
	.globl	C$cpu.c$57$1_0$94
	XG$getTargetUpperPart$0$0	= .
	.globl	XG$getTargetUpperPart$0$0
	ret
	G$decideNextMove$0$0	= .
	.globl	G$decideNextMove$0$0
	C$cpu.c$59$1_0$104	= .
	.globl	C$cpu.c$59$1_0$104
;src/engine/cpu.c:59: void decideNextMove(void) {
;	---------------------------------
; Function decideNextMove
; ---------------------------------
_decideNextMove::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	C$cpu.c$63$1_0$104	= .
	.globl	C$cpu.c$63$1_0$104
;src/engine/cpu.c:63: current_rotation = 3;
	ld	hl, #_current_rotation
	ld	(hl), #0x03
	C$cpu.c$64$1_0$104	= .
	.globl	C$cpu.c$64$1_0$104
;src/engine/cpu.c:64: target_rotation = 3;
	ld	hl, #_target_rotation
	ld	(hl), #0x03
	C$cpu.c$65$1_0$104	= .
	.globl	C$cpu.c$65$1_0$104
;src/engine/cpu.c:65: target_column = 0;
	ld	hl, #_target_column
	ld	(hl), #0x00
	C$cpu.c$66$1_0$104	= .
	.globl	C$cpu.c$66$1_0$104
;src/engine/cpu.c:66: tmpColor = WHITE_PIECE;
	ld	-4 (ix), #0x01
	C$cpu.c$67$1_0$104	= .
	.globl	C$cpu.c$67$1_0$104
;src/engine/cpu.c:67: if((current_square.color00 != BONUS_CELL_INVERSE) && (current_square.color00 != BONUS_CELL_DESTROY)) {
	ld	a, (#(_current_square + 2) + 0)
	ld	-2 (ix), a
	sub	a, #0x0c
	ld	a, #0x01
	jr	Z, 00251$
	xor	a, a
00251$:
	ld	-1 (ix), a
	bit	0, -1 (ix)
	jp	NZ, 00139$
	ld	a, -2 (ix)
	sub	a, #0x0d
	jp	Z,00139$
	C$cpu.c$68$2_0$105	= .
	.globl	C$cpu.c$68$2_0$105
;src/engine/cpu.c:68: maxSuitableColumn = get_rand() & 7;
	call	_get_rand
	and	a, #0x07
	ld	-3 (ix), a
	C$cpu.c$69$2_0$105	= .
	.globl	C$cpu.c$69$2_0$105
;src/engine/cpu.c:69: maxSuitableColor = WHITE_SUITABLE;
	ld	-2 (ix), #0x11
	C$cpu.c$70$2_0$105	= .
	.globl	C$cpu.c$70$2_0$105
;src/engine/cpu.c:70: maxSuitableElements = 0;
	ld	-1 (ix), #0x00
	C$cpu.c$73$4_0$107	= .
	.globl	C$cpu.c$73$4_0$107
;src/engine/cpu.c:73: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	c, #0x00
00142$:
	C$cpu.c$74$1_0$104	= .
	.globl	C$cpu.c$74$1_0$104
;src/engine/cpu.c:74: if(suitable_columns[i] != NO_SUITABLE) {
	ld	hl, #_suitable_columns
	ld	b, #0x00
	add	hl, bc
	ld	l, (hl)
;	spillPairReg hl
	ld	a, l
	or	a, a
	jr	Z, 00143$
	C$cpu.c$75$5_0$108	= .
	.globl	C$cpu.c$75$5_0$108
;src/engine/cpu.c:75: maxBlack = (suitable_columns[i]&0x3);//las two bits
	ld	a, l
	and	a, #0x03
	ld	-4 (ix), a
	C$cpu.c$76$5_0$108	= .
	.globl	C$cpu.c$76$5_0$108
;src/engine/cpu.c:76: maxWhite = (suitable_columns[i]&0xC)>>2;//3rd and 4th bit
	ld	a, l
	and	a, #0x0c
	ld	e, a
	ld	d, #0x00
	sra	d
	rr	e
	sra	d
	rr	e
	C$cpu.c$77$5_0$108	= .
	.globl	C$cpu.c$77$5_0$108
;src/engine/cpu.c:77: if(maxBlack > maxWhite) { 
	ld	a, e
	sub	a, -4 (ix)
	jr	NC, 00102$
	C$cpu.c$78$6_0$109	= .
	.globl	C$cpu.c$78$6_0$109
;src/engine/cpu.c:78: tmpValue = maxBlack;
	ld	e, -4 (ix)
	C$cpu.c$79$6_0$109	= .
	.globl	C$cpu.c$79$6_0$109
;src/engine/cpu.c:79: tmpColor = BLACK_PIECE;
	ld	-4 (ix), #0x04
	jr	00103$
00102$:
	C$cpu.c$81$6_0$110	= .
	.globl	C$cpu.c$81$6_0$110
;src/engine/cpu.c:81: tmpValue = maxWhite;
	C$cpu.c$82$6_0$110	= .
	.globl	C$cpu.c$82$6_0$110
;src/engine/cpu.c:82: tmpColor = WHITE_PIECE;
	ld	-4 (ix), #0x01
00103$:
	C$cpu.c$84$5_0$108	= .
	.globl	C$cpu.c$84$5_0$108
;src/engine/cpu.c:84: if(tmpValue > maxSuitableElements) {
	ld	a, -1 (ix)
	sub	a, e
	jr	NC, 00143$
	C$cpu.c$85$6_0$111	= .
	.globl	C$cpu.c$85$6_0$111
;src/engine/cpu.c:85: maxSuitableColumn = i;
	ld	-3 (ix), c
	C$cpu.c$86$6_0$111	= .
	.globl	C$cpu.c$86$6_0$111
;src/engine/cpu.c:86: maxSuitableColor = (suitable_columns[i]&0xf0); 
	ld	a, l
	and	a, #0xf0
	ld	-2 (ix), a
	C$cpu.c$87$6_0$111	= .
	.globl	C$cpu.c$87$6_0$111
;src/engine/cpu.c:87: maxSuitableElements = tmpValue;
	ld	-1 (ix), e
00143$:
	C$cpu.c$73$3_0$106	= .
	.globl	C$cpu.c$73$3_0$106
;src/engine/cpu.c:73: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	c
	ld	a, c
	sub	a, #0x0a
	jr	C, 00142$
	C$cpu.c$91$2_0$105	= .
	.globl	C$cpu.c$91$2_0$105
;src/engine/cpu.c:91: i=maxSuitableColumn;
	ld	a, -3 (ix)
	ld	-1 (ix), a
	C$cpu.c$93$2_0$105	= .
	.globl	C$cpu.c$93$2_0$105
;src/engine/cpu.c:93: if(tmpColor == BLACK_PIECE) {
	ld	a, -4 (ix)
	sub	a, #0x04
	jr	NZ, 00120$
	C$cpu.c$94$3_0$112	= .
	.globl	C$cpu.c$94$3_0$112
;src/engine/cpu.c:94: if(maxSuitableColor & WHITE_SUITABLE) {
	ld	a, -2 (ix)
	and	a, #0x11
	jr	Z, 00112$
	C$cpu.c$95$4_0$113	= .
	.globl	C$cpu.c$95$4_0$113
;src/engine/cpu.c:95: getTargetBottomPart(WHITE_PIECE,i);                    
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x01
	call	_getTargetBottomPart
	jr	00121$
00112$:
	C$cpu.c$96$3_0$112	= .
	.globl	C$cpu.c$96$3_0$112
;src/engine/cpu.c:96: } else if(maxSuitableColor & WHITE_SUITABLE_UPPER_PART) {
	ld	a, -2 (ix)
	and	a, #0x41
	jr	Z, 00121$
	C$cpu.c$97$4_0$114	= .
	.globl	C$cpu.c$97$4_0$114
;src/engine/cpu.c:97: getTargetUpperPart(WHITE_PIECE,i);
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x01
	call	_getTargetUpperPart
	jr	00121$
00120$:
	C$cpu.c$100$3_0$115	= .
	.globl	C$cpu.c$100$3_0$115
;src/engine/cpu.c:100: if(maxSuitableColor & BLACK_SUITABLE) {
	ld	a, -2 (ix)
	and	a, #0x21
	jr	Z, 00117$
	C$cpu.c$101$4_0$116	= .
	.globl	C$cpu.c$101$4_0$116
;src/engine/cpu.c:101: getTargetBottomPart(BLACK_PIECE,i);
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x04
	call	_getTargetBottomPart
	jr	00121$
00117$:
	C$cpu.c$102$3_0$115	= .
	.globl	C$cpu.c$102$3_0$115
;src/engine/cpu.c:102: } else if(maxSuitableColor & BLACK_SUITABLE_UPPER_PART) {
	ld	a, -2 (ix)
	and	a, #0x81
	jr	Z, 00121$
	C$cpu.c$103$4_0$117	= .
	.globl	C$cpu.c$103$4_0$117
;src/engine/cpu.c:103: getTargetUpperPart(BLACK_PIECE,i);
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x04
	call	_getTargetUpperPart
00121$:
	C$cpu.c$109$2_0$105	= .
	.globl	C$cpu.c$109$2_0$105
;src/engine/cpu.c:109: target_row = getCollisionRowFromColumn(target_column);
	ld	a, (_target_column+0)
	call	_getCollisionRowFromColumn
	ld	(_target_row+0), a
	C$cpu.c$110$2_0$105	= .
	.globl	C$cpu.c$110$2_0$105
;src/engine/cpu.c:110: if(i==BOARD_COLUMNS || (target_row < 8 && current_square.color00 != BONUS_CELL_INVERSE) ){
	ld	a, -1 (ix)
	sub	a, #0x0a
	jr	Z, 00124$
	ld	a, (_target_row+0)
	sub	a, #0x08
	jp	NC, 00140$
	ld	a, (#(_current_square + 2) + 0)
	sub	a, #0x0c
	jp	Z,00140$
00124$:
	C$cpu.c$111$3_0$118	= .
	.globl	C$cpu.c$111$3_0$118
;src/engine/cpu.c:111: target_column = get_rand() & 7;
	call	_get_rand
	and	a, #0x07
	ld	(_target_column+0), a
	C$cpu.c$112$3_0$118	= .
	.globl	C$cpu.c$112$3_0$118
;src/engine/cpu.c:112: target_rotation = get_rand() & 7; //value from 0-7
	call	_get_rand
	and	a, #0x07
	C$cpu.c$113$3_0$118	= .
	.globl	C$cpu.c$113$3_0$118
;src/engine/cpu.c:113: if(!target_rotation) target_rotation++; //value from 1-7
	ld	(_target_rotation+0), a
	or	a, a
	jr	NZ, 00123$
	ld	hl, #_target_rotation
	inc	(hl)
00123$:
	C$cpu.c$114$3_0$118	= .
	.globl	C$cpu.c$114$3_0$118
;src/engine/cpu.c:114: target_rotation--; //value from 0-6
	ld	hl, #_target_rotation
	dec	(hl)
	C$cpu.c$115$3_0$118	= .
	.globl	C$cpu.c$115$3_0$118
;src/engine/cpu.c:115: target_row = getCollisionRowFromColumn(target_column);
	ld	a, (_target_column+0)
	call	_getCollisionRowFromColumn
	ld	(_target_row+0), a
	jr	00140$
00139$:
	C$cpu.c$118$2_0$119	= .
	.globl	C$cpu.c$118$2_0$119
;src/engine/cpu.c:118: if(current_square.color00 == BONUS_CELL_INVERSE) {
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00129$
	C$cpu.c$119$3_0$120	= .
	.globl	C$cpu.c$119$3_0$120
;src/engine/cpu.c:119: target_row = 10;
	ld	hl, #_target_row
	ld	(hl), #0x0a
	jr	00167$
00129$:
	C$cpu.c$121$3_0$121	= .
	.globl	C$cpu.c$121$3_0$121
;src/engine/cpu.c:121: target_row = 0;
	ld	hl, #_target_row
	ld	(hl), #0x00
	C$cpu.c$124$1_0$104	= .
	.globl	C$cpu.c$124$1_0$104
;src/engine/cpu.c:124: for(i=0;i<BOARD_COLUMNS;i++) {
00167$:
	ld	-1 (ix), #0x00
00144$:
	C$cpu.c$125$4_0$123	= .
	.globl	C$cpu.c$125$4_0$123
;src/engine/cpu.c:125: tmpValue = getCollisionRowFromColumn(i);
	ld	a, -1 (ix)
	call	_getCollisionRowFromColumn
	C$cpu.c$126$4_0$123	= .
	.globl	C$cpu.c$126$4_0$123
;src/engine/cpu.c:126: if((tmpValue < target_row && current_square.color00 == BONUS_CELL_INVERSE) || (tmpValue > target_row && tmpValue < BOARD_ROWS && current_square.color00 == BONUS_CELL_DESTROY)) {
	ld	-2 (ix), a
	ld	iy, #_target_row
	sub	a, 0 (iy)
	jr	NC, 00136$
	ld	a, (#(_current_square + 2) + 0)
	sub	a, #0x0c
	jr	Z, 00131$
00136$:
	ld	a, (_target_row+0)
	sub	a, -2 (ix)
	jr	NC, 00145$
	ld	a, -2 (ix)
	sub	a, #0x16
	jr	NC, 00145$
	ld	a, (#(_current_square + 2) + 0)
	sub	a, #0x0d
	jr	NZ, 00145$
00131$:
	C$cpu.c$127$5_0$124	= .
	.globl	C$cpu.c$127$5_0$124
;src/engine/cpu.c:127: target_column = i;
	ld	a, -1 (ix)
	C$cpu.c$128$5_0$124	= .
	.globl	C$cpu.c$128$5_0$124
;src/engine/cpu.c:128: target_row = getCollisionRowFromColumn(target_column);
	ld	(_target_column+0), a
	call	_getCollisionRowFromColumn
	ld	(_target_row+0), a
00145$:
	C$cpu.c$124$3_0$122	= .
	.globl	C$cpu.c$124$3_0$122
;src/engine/cpu.c:124: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	-1 (ix)
	ld	a, -1 (ix)
	sub	a, #0x0a
	jr	C, 00144$
00140$:
	C$cpu.c$133$1_0$104	= .
	.globl	C$cpu.c$133$1_0$104
;src/engine/cpu.c:133: movement_counter = target_row;
	ld	a, (_target_row+0)
	ld	(_movement_counter+0), a
	C$cpu.c$134$1_0$104	= .
	.globl	C$cpu.c$134$1_0$104
;src/engine/cpu.c:134: rotation_counter = target_row;
	ld	(_rotation_counter+0), a
	C$cpu.c$135$1_0$104	= .
	.globl	C$cpu.c$135$1_0$104
;src/engine/cpu.c:135: }
	ld	sp, ix
	pop	ix
	C$cpu.c$135$1_0$104	= .
	.globl	C$cpu.c$135$1_0$104
	XG$decideNextMove$0$0	= .
	.globl	XG$decideNextMove$0$0
	ret
	G$decideDirection$0$0	= .
	.globl	G$decideDirection$0$0
	C$cpu.c$138$1_0$126	= .
	.globl	C$cpu.c$138$1_0$126
;src/engine/cpu.c:138: u16 decideDirection(u8 current_column, u8 current_row) {
;	---------------------------------
; Function decideDirection
; ---------------------------------
_decideDirection::
	ld	b, a
	ld	c, l
	C$cpu.c$141$1_0$126	= .
	.globl	C$cpu.c$141$1_0$126
;src/engine/cpu.c:141: if(!movement_counter) {
	ld	a, (_movement_counter+0)
	or	a, a
	jr	NZ, 00111$
	C$cpu.c$142$2_0$127	= .
	.globl	C$cpu.c$142$2_0$127
;src/engine/cpu.c:142: row_delta = target_row - current_row;
	ld	a, (_target_row+0)
	sub	a, c
	ld	c, a
	C$cpu.c$143$2_0$127	= .
	.globl	C$cpu.c$143$2_0$127
;src/engine/cpu.c:143: column_delta = target_column - current_column;
	ld	a, (_target_column+0)
	sub	a, b
	ld	e, a
	C$cpu.c$144$2_0$127	= .
	.globl	C$cpu.c$144$2_0$127
;src/engine/cpu.c:144: if(row_delta > 0) {
	xor	a, a
	sub	a, c
	jp	PO, 00135$
	xor	a, #0x80
00135$:
	jp	P, 00102$
	C$cpu.c$145$3_0$128	= .
	.globl	C$cpu.c$145$3_0$128
;src/engine/cpu.c:145: movement_counter = row_delta;
	ld	hl, #_movement_counter
	ld	(hl), c
	jr	00103$
00102$:
	C$cpu.c$147$3_0$129	= .
	.globl	C$cpu.c$147$3_0$129
;src/engine/cpu.c:147: movement_counter = 1;
	ld	hl, #_movement_counter
	ld	(hl), #0x01
00103$:
	C$cpu.c$149$2_0$127	= .
	.globl	C$cpu.c$149$2_0$127
;src/engine/cpu.c:149: if(column_delta > 0) {
	xor	a, a
	sub	a, e
	jp	PO, 00136$
	xor	a, #0x80
00136$:
	jp	P, 00108$
	C$cpu.c$150$3_0$130	= .
	.globl	C$cpu.c$150$3_0$130
;src/engine/cpu.c:150: return RIGHT_ACTION;
	ld	de, #0x0008
	ret
00108$:
	C$cpu.c$151$2_0$127	= .
	.globl	C$cpu.c$151$2_0$127
;src/engine/cpu.c:151: } else if(column_delta < 0) {
	bit	7, e
	jr	Z, 00105$
	C$cpu.c$152$3_0$131	= .
	.globl	C$cpu.c$152$3_0$131
;src/engine/cpu.c:152: return LEFT_ACTION;
	ld	de, #0x0004
	ret
00105$:
	C$cpu.c$154$3_0$132	= .
	.globl	C$cpu.c$154$3_0$132
;src/engine/cpu.c:154: movement_counter = 0;
	ld	hl, #_movement_counter
	C$cpu.c$155$3_0$132	= .
	.globl	C$cpu.c$155$3_0$132
;src/engine/cpu.c:155: return DOWN_ACTION;
	ld	de, #0x0002
	ld	(hl), d
	ret
00111$:
	C$cpu.c$158$2_0$133	= .
	.globl	C$cpu.c$158$2_0$133
;src/engine/cpu.c:158: movement_counter--;
	ld	hl, #_movement_counter
	dec	(hl)
	C$cpu.c$159$2_0$133	= .
	.globl	C$cpu.c$159$2_0$133
;src/engine/cpu.c:159: return NEUTRAL_ACTION;
	ld	de, #0x0000
	C$cpu.c$162$1_0$126	= .
	.globl	C$cpu.c$162$1_0$126
;src/engine/cpu.c:162: }
	C$cpu.c$162$1_0$126	= .
	.globl	C$cpu.c$162$1_0$126
	XG$decideDirection$0$0	= .
	.globl	XG$decideDirection$0$0
	ret
	G$decideRotation$0$0	= .
	.globl	G$decideRotation$0$0
	C$cpu.c$164$1_0$135	= .
	.globl	C$cpu.c$164$1_0$135
;src/engine/cpu.c:164: u16 decideRotation(u8 current_row) {
;	---------------------------------
; Function decideRotation
; ---------------------------------
_decideRotation::
	ld	c, a
	C$cpu.c$166$1_0$135	= .
	.globl	C$cpu.c$166$1_0$135
;src/engine/cpu.c:166: if(!rotation_counter) {
	ld	a, (_rotation_counter+0)
	or	a, a
	jr	NZ, 00110$
	C$cpu.c$167$2_0$136	= .
	.globl	C$cpu.c$167$2_0$136
;src/engine/cpu.c:167: rotation_delta = target_rotation - current_rotation;
	ld	hl, #_current_rotation
	ld	a, (_target_rotation+0)
	sub	a, (hl)
	ld	b, a
	C$cpu.c$168$2_0$136	= .
	.globl	C$cpu.c$168$2_0$136
;src/engine/cpu.c:168: row_delta = target_row - current_row;
	ld	a, (_target_row+0)
	sub	a, c
	ld	c, a
	C$cpu.c$169$2_0$136	= .
	.globl	C$cpu.c$169$2_0$136
;src/engine/cpu.c:169: if(row_delta > 0) {
	xor	a, a
	sub	a, c
	jp	PO, 00134$
	xor	a, #0x80
00134$:
	jp	P, 00102$
	C$cpu.c$170$3_0$137	= .
	.globl	C$cpu.c$170$3_0$137
;src/engine/cpu.c:170: rotation_counter = row_delta;
	ld	hl, #_rotation_counter
	ld	(hl), c
	jr	00103$
00102$:
	C$cpu.c$172$3_0$138	= .
	.globl	C$cpu.c$172$3_0$138
;src/engine/cpu.c:172: rotation_counter = 1;
	ld	hl, #_rotation_counter
	ld	(hl), #0x01
00103$:
	C$cpu.c$174$2_0$136	= .
	.globl	C$cpu.c$174$2_0$136
;src/engine/cpu.c:174: if(rotation_delta > 0) {
	xor	a, a
	sub	a, b
	jp	PO, 00135$
	xor	a, #0x80
00135$:
	jp	P, 00107$
	C$cpu.c$175$3_0$139	= .
	.globl	C$cpu.c$175$3_0$139
;src/engine/cpu.c:175: current_rotation++;
	ld	hl, #_current_rotation
	inc	(hl)
	C$cpu.c$176$3_0$139	= .
	.globl	C$cpu.c$176$3_0$139
;src/engine/cpu.c:176: return ROTATION_ONE;
	ld	de, #0x0010
	ret
00107$:
	C$cpu.c$177$2_0$136	= .
	.globl	C$cpu.c$177$2_0$136
;src/engine/cpu.c:177: } else if(rotation_delta < 0) {
	bit	7, b
	jr	Z, 00108$
	C$cpu.c$178$3_0$140	= .
	.globl	C$cpu.c$178$3_0$140
;src/engine/cpu.c:178: current_rotation--;
	ld	hl, #_current_rotation
	dec	(hl)
	C$cpu.c$179$3_0$140	= .
	.globl	C$cpu.c$179$3_0$140
;src/engine/cpu.c:179: return ROTATION_TWO;
	ld	de, #0x0020
	ret
00108$:
	C$cpu.c$181$2_0$136	= .
	.globl	C$cpu.c$181$2_0$136
;src/engine/cpu.c:181: return NEUTRAL_ACTION;
	ld	de, #0x0000
	ret
00110$:
	C$cpu.c$183$2_0$141	= .
	.globl	C$cpu.c$183$2_0$141
;src/engine/cpu.c:183: rotation_counter--;
	ld	hl, #_rotation_counter
	dec	(hl)
	C$cpu.c$184$2_0$141	= .
	.globl	C$cpu.c$184$2_0$141
;src/engine/cpu.c:184: return NEUTRAL_ACTION;
	ld	de, #0x0000
	C$cpu.c$187$1_0$135	= .
	.globl	C$cpu.c$187$1_0$135
;src/engine/cpu.c:187: }
	C$cpu.c$187$1_0$135	= .
	.globl	C$cpu.c$187$1_0$135
	XG$decideRotation$0$0	= .
	.globl	XG$decideRotation$0$0
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
