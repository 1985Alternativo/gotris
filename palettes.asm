;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module palettes
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _waitForFrame
	.globl _SMS_loadSpritePalette
	.globl _SMS_loadBGPalette
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _setBGPalette
	.globl _setBGPaletteColor
	.globl _getBGPalette
	.globl _setSpritePalette
	.globl _setSpritePaletteColor
	.globl _getSpritePalette
	.globl _createSepiaPaletteVersion
	.globl _createBluePaletteVersion
	.globl _fadeToTargetBGPalette
	.globl _fadeToTargetSpritePalette
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$setBGPalette$0$0	= .
	.globl	G$setBGPalette$0$0
	C$palettes.c$8$0_0$107	= .
	.globl	C$palettes.c$8$0_0$107
;src/engine/palettes.c:8: void setBGPalette(u8 *palette, u8 numcolors) {
;	---------------------------------
; Function setBGPalette
; ---------------------------------
_setBGPalette::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	ex	de, hl
	C$palettes.c$10$3_0$109	= .
	.globl	C$palettes.c$10$3_0$109
;src/engine/palettes.c:10: for(i = 0;i<numcolors;i++) {
	ld	c, #0x00
00103$:
	ld	a, c
	sub	a, 4 (ix)
	jr	NC, 00101$
	C$palettes.c$11$3_0$109	= .
	.globl	C$palettes.c$11$3_0$109
;src/engine/palettes.c:11: background_palette[i] = palette[i];
	ld	a, #<(_background_palette)
	add	a, c
	ld	-2 (ix), a
	ld	a, #>(_background_palette)
	adc	a, #0x00
	ld	-1 (ix), a
	ld	l, c
	ld	h, #0x00
	add	hl, de
	ld	a, (hl)
	pop	hl
	push	hl
	ld	(hl), a
	C$palettes.c$10$2_0$108	= .
	.globl	C$palettes.c$10$2_0$108
;src/engine/palettes.c:10: for(i = 0;i<numcolors;i++) {
	inc	c
	jr	00103$
00101$:
	C$palettes.c$13$1_0$107	= .
	.globl	C$palettes.c$13$1_0$107
;src/engine/palettes.c:13: SMS_loadBGPalette(palette);
	ex	de, hl
	call	_SMS_loadBGPalette
	C$palettes.c$14$1_0$107	= .
	.globl	C$palettes.c$14$1_0$107
;src/engine/palettes.c:14: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$setBGPaletteColor$0$0	= .
	.globl	G$setBGPaletteColor$0$0
	C$palettes.c$16$1_0$111	= .
	.globl	C$palettes.c$16$1_0$111
;src/engine/palettes.c:16: void setBGPaletteColor(u8 position, u8 value) {
;	---------------------------------
; Function setBGPaletteColor
; ---------------------------------
_setBGPaletteColor::
	ld	e, a
	ld	c, l
	C$palettes.c$17$1_0$111	= .
	.globl	C$palettes.c$17$1_0$111
;src/engine/palettes.c:17: background_palette[position] = value;
	ld	hl, #_background_palette+0
	ld	d, #0x00
	add	hl, de
	ld	(hl), c
	C$palettes.c$18$1_0$111	= .
	.globl	C$palettes.c$18$1_0$111
;src/engine/palettes.c:18: }
	C$palettes.c$18$1_0$111	= .
	.globl	C$palettes.c$18$1_0$111
	XG$setBGPaletteColor$0$0	= .
	.globl	XG$setBGPaletteColor$0$0
	ret
	G$getBGPalette$0$0	= .
	.globl	G$getBGPalette$0$0
	C$palettes.c$20$1_0$113	= .
	.globl	C$palettes.c$20$1_0$113
;src/engine/palettes.c:20: u8* getBGPalette(void) {
;	---------------------------------
; Function getBGPalette
; ---------------------------------
_getBGPalette::
	C$palettes.c$21$1_0$113	= .
	.globl	C$palettes.c$21$1_0$113
;src/engine/palettes.c:21: return background_palette;
	ld	de, #_background_palette
	C$palettes.c$22$1_0$113	= .
	.globl	C$palettes.c$22$1_0$113
;src/engine/palettes.c:22: }
	C$palettes.c$22$1_0$113	= .
	.globl	C$palettes.c$22$1_0$113
	XG$getBGPalette$0$0	= .
	.globl	XG$getBGPalette$0$0
	ret
	G$setSpritePalette$0$0	= .
	.globl	G$setSpritePalette$0$0
	C$palettes.c$24$1_0$115	= .
	.globl	C$palettes.c$24$1_0$115
;src/engine/palettes.c:24: void setSpritePalette(u8 *palette, u8 numcolors) {
;	---------------------------------
; Function setSpritePalette
; ---------------------------------
_setSpritePalette::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	ex	de, hl
	C$palettes.c$26$3_0$117	= .
	.globl	C$palettes.c$26$3_0$117
;src/engine/palettes.c:26: for(i = 0;i<numcolors;i++) {
	ld	c, #0x00
00103$:
	ld	a, c
	sub	a, 4 (ix)
	jr	NC, 00101$
	C$palettes.c$27$3_0$117	= .
	.globl	C$palettes.c$27$3_0$117
;src/engine/palettes.c:27: sprite_palette[i] = palette[i];
	ld	a, #<(_sprite_palette)
	add	a, c
	ld	-2 (ix), a
	ld	a, #>(_sprite_palette)
	adc	a, #0x00
	ld	-1 (ix), a
	ld	l, c
	ld	h, #0x00
	add	hl, de
	ld	a, (hl)
	pop	hl
	push	hl
	ld	(hl), a
	C$palettes.c$26$2_0$116	= .
	.globl	C$palettes.c$26$2_0$116
;src/engine/palettes.c:26: for(i = 0;i<numcolors;i++) {
	inc	c
	jr	00103$
00101$:
	C$palettes.c$29$1_0$115	= .
	.globl	C$palettes.c$29$1_0$115
;src/engine/palettes.c:29: SMS_loadSpritePalette(palette);
	ex	de, hl
	call	_SMS_loadSpritePalette
	C$palettes.c$30$1_0$115	= .
	.globl	C$palettes.c$30$1_0$115
;src/engine/palettes.c:30: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$setSpritePaletteColor$0$0	= .
	.globl	G$setSpritePaletteColor$0$0
	C$palettes.c$32$1_0$119	= .
	.globl	C$palettes.c$32$1_0$119
;src/engine/palettes.c:32: void setSpritePaletteColor(u8 position, u8 value) {
;	---------------------------------
; Function setSpritePaletteColor
; ---------------------------------
_setSpritePaletteColor::
	ld	e, a
	ld	c, l
	C$palettes.c$33$1_0$119	= .
	.globl	C$palettes.c$33$1_0$119
;src/engine/palettes.c:33: sprite_palette[position] = value;
	ld	hl, #_sprite_palette+0
	ld	d, #0x00
	add	hl, de
	ld	(hl), c
	C$palettes.c$34$1_0$119	= .
	.globl	C$palettes.c$34$1_0$119
;src/engine/palettes.c:34: }
	C$palettes.c$34$1_0$119	= .
	.globl	C$palettes.c$34$1_0$119
	XG$setSpritePaletteColor$0$0	= .
	.globl	XG$setSpritePaletteColor$0$0
	ret
	G$getSpritePalette$0$0	= .
	.globl	G$getSpritePalette$0$0
	C$palettes.c$36$1_0$121	= .
	.globl	C$palettes.c$36$1_0$121
;src/engine/palettes.c:36: u8* getSpritePalette(void) {
;	---------------------------------
; Function getSpritePalette
; ---------------------------------
_getSpritePalette::
	C$palettes.c$37$1_0$121	= .
	.globl	C$palettes.c$37$1_0$121
;src/engine/palettes.c:37: return sprite_palette;
	ld	de, #_sprite_palette
	C$palettes.c$38$1_0$121	= .
	.globl	C$palettes.c$38$1_0$121
;src/engine/palettes.c:38: }
	C$palettes.c$38$1_0$121	= .
	.globl	C$palettes.c$38$1_0$121
	XG$getSpritePalette$0$0	= .
	.globl	XG$getSpritePalette$0$0
	ret
	G$createSepiaPaletteVersion$0$0	= .
	.globl	G$createSepiaPaletteVersion$0$0
	C$palettes.c$85$1_0$124	= .
	.globl	C$palettes.c$85$1_0$124
;src/engine/palettes.c:85: void createSepiaPaletteVersion(const u8 *original_palette, u8 *bw_palette, u8 numcolors) {
;	---------------------------------
; Function createSepiaPaletteVersion
; ---------------------------------
_createSepiaPaletteVersion::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	push	af
	dec	sp
	ld	-3 (ix), l
	ld	-2 (ix), h
	ld	-5 (ix), e
	ld	-4 (ix), d
	C$palettes.c$87$2_0$124	= .
	.globl	C$palettes.c$87$2_0$124
;src/engine/palettes.c:87: for(j = 0;j<numcolors;j++) {
	ld	-1 (ix), #0x00
00115$:
	ld	a, -1 (ix)
	sub	a, 4 (ix)
	jp	NC, 00117$
	C$palettes.c$88$3_0$125	= .
	.globl	C$palettes.c$88$3_0$125
;src/engine/palettes.c:88: redComponent = getRedFromRGB(original_palette[j]);
	ld	a, -3 (ix)
	add	a, -1 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -2 (ix)
	adc	a, #0x00
	ld	h, a
	ld	c, (hl)
	ld	a, c
	and	a, #0x0b
	ld	e, a
	C$palettes.c$89$3_0$125	= .
	.globl	C$palettes.c$89$3_0$125
;src/engine/palettes.c:89: greenComponent = getGreenFromRGB(original_palette[j]);
	ld	a, c
	ld	b, #0x00
	and	a, #0x4c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	and	a, #0x04
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	sra	h
	rr	l
	sra	h
	rr	l
	C$palettes.c$90$2_0$124	= .
	.globl	C$palettes.c$90$2_0$124
;src/engine/palettes.c:90: blueComponent = getBlueFromRGB(original_palette[j]);
	ld	b, #0x00
	ld	a, c
	and	a, #0xb0
	ld	c, a
	ld	a, b
	and	a, #0xad
	ld	b, a
	srl	b
	rr	c
	srl	b
	rr	c
	srl	b
	rr	c
	srl	b
	rr	c
	C$palettes.c$91$3_0$125	= .
	.globl	C$palettes.c$91$3_0$125
;src/engine/palettes.c:91: addedColor = redComponent + greenComponent + blueComponent;
	add	hl, de
	add	hl, bc
	ld	-6 (ix), l
	C$palettes.c$92$3_0$125	= .
	.globl	C$palettes.c$92$3_0$125
;src/engine/palettes.c:92: switch (addedColor)
	ld	a, #0x09
	sub	a, -6 (ix)
	jr	C, 00111$
	ld	c, -6 (ix)
	ld	b, #0x00
	ld	hl, #00135$
	add	hl, bc
	add	hl, bc
	add	hl, bc
	jp	(hl)
00135$:
	jp	00101$
	jp	00104$
	jp	00104$
	jp	00104$
	jp	00107$
	jp	00107$
	jp	00107$
	jp	00110$
	jp	00110$
	jp	00110$
	C$palettes.c$94$4_0$126	= .
	.globl	C$palettes.c$94$4_0$126
;src/engine/palettes.c:94: case 0:
00101$:
	C$palettes.c$95$4_0$126	= .
	.globl	C$palettes.c$95$4_0$126
;src/engine/palettes.c:95: redComponent = 0;
	ld	-7 (ix), #0x00
	C$palettes.c$96$4_0$126	= .
	.globl	C$palettes.c$96$4_0$126
;src/engine/palettes.c:96: greenComponent = 0;
	ld	-6 (ix), #0x00
	C$palettes.c$97$4_0$126	= .
	.globl	C$palettes.c$97$4_0$126
;src/engine/palettes.c:97: blueComponent = 0;
	ld	c, #0x00
	C$palettes.c$98$4_0$126	= .
	.globl	C$palettes.c$98$4_0$126
;src/engine/palettes.c:98: break;
	jr	00112$
	C$palettes.c$101$4_0$126	= .
	.globl	C$palettes.c$101$4_0$126
;src/engine/palettes.c:101: case 3:
00104$:
	C$palettes.c$102$4_0$126	= .
	.globl	C$palettes.c$102$4_0$126
;src/engine/palettes.c:102: redComponent = 1;
	ld	-7 (ix), #0x01
	C$palettes.c$103$4_0$126	= .
	.globl	C$palettes.c$103$4_0$126
;src/engine/palettes.c:103: greenComponent = 1;
	ld	-6 (ix), #0x01
	C$palettes.c$104$4_0$126	= .
	.globl	C$palettes.c$104$4_0$126
;src/engine/palettes.c:104: blueComponent = 0;
	ld	c, #0x00
	C$palettes.c$105$4_0$126	= .
	.globl	C$palettes.c$105$4_0$126
;src/engine/palettes.c:105: break;
	jr	00112$
	C$palettes.c$108$4_0$126	= .
	.globl	C$palettes.c$108$4_0$126
;src/engine/palettes.c:108: case 6:
00107$:
	C$palettes.c$109$4_0$126	= .
	.globl	C$palettes.c$109$4_0$126
;src/engine/palettes.c:109: redComponent = 2;
	ld	-7 (ix), #0x02
	C$palettes.c$110$4_0$126	= .
	.globl	C$palettes.c$110$4_0$126
;src/engine/palettes.c:110: greenComponent = 2;
	ld	-6 (ix), #0x02
	C$palettes.c$111$4_0$126	= .
	.globl	C$palettes.c$111$4_0$126
;src/engine/palettes.c:111: blueComponent = 1;
	ld	c, #0x01
	C$palettes.c$112$4_0$126	= .
	.globl	C$palettes.c$112$4_0$126
;src/engine/palettes.c:112: break;  
	jr	00112$
	C$palettes.c$115$4_0$126	= .
	.globl	C$palettes.c$115$4_0$126
;src/engine/palettes.c:115: case 9:
00110$:
	C$palettes.c$116$4_0$126	= .
	.globl	C$palettes.c$116$4_0$126
;src/engine/palettes.c:116: redComponent = 3;
	ld	-7 (ix), #0x03
	C$palettes.c$117$4_0$126	= .
	.globl	C$palettes.c$117$4_0$126
;src/engine/palettes.c:117: greenComponent = 3;
	ld	-6 (ix), #0x03
	C$palettes.c$118$4_0$126	= .
	.globl	C$palettes.c$118$4_0$126
;src/engine/palettes.c:118: blueComponent = 2;
	ld	c, #0x02
	C$palettes.c$119$4_0$126	= .
	.globl	C$palettes.c$119$4_0$126
;src/engine/palettes.c:119: break;
	jr	00112$
	C$palettes.c$120$4_0$126	= .
	.globl	C$palettes.c$120$4_0$126
;src/engine/palettes.c:120: default:
00111$:
	C$palettes.c$121$4_0$126	= .
	.globl	C$palettes.c$121$4_0$126
;src/engine/palettes.c:121: redComponent = 3;
	ld	-7 (ix), #0x03
	C$palettes.c$122$4_0$126	= .
	.globl	C$palettes.c$122$4_0$126
;src/engine/palettes.c:122: greenComponent = 3;
	ld	-6 (ix), #0x03
	C$palettes.c$123$4_0$126	= .
	.globl	C$palettes.c$123$4_0$126
;src/engine/palettes.c:123: blueComponent = 3;
	ld	c, #0x03
	C$palettes.c$125$3_0$125	= .
	.globl	C$palettes.c$125$3_0$125
;src/engine/palettes.c:125: }
00112$:
	C$palettes.c$126$3_0$125	= .
	.globl	C$palettes.c$126$3_0$125
;src/engine/palettes.c:126: bw_palette[j] = RGB(redComponent,greenComponent,blueComponent);
	ld	a, -5 (ix)
	add	a, -1 (ix)
	ld	e, a
	ld	a, -4 (ix)
	adc	a, #0x00
	ld	d, a
	ld	a, -6 (ix)
	add	a, a
	add	a, a
	or	a, -7 (ix)
	ld	b, a
	ld	a, c
	add	a, a
	add	a, a
	add	a, a
	add	a, a
	or	a, b
	ld	(de), a
	C$palettes.c$87$2_0$124	= .
	.globl	C$palettes.c$87$2_0$124
;src/engine/palettes.c:87: for(j = 0;j<numcolors;j++) {
	inc	-1 (ix)
	jp	00115$
00117$:
	C$palettes.c$128$2_0$124	= .
	.globl	C$palettes.c$128$2_0$124
;src/engine/palettes.c:128: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$createBluePaletteVersion$0$0	= .
	.globl	G$createBluePaletteVersion$0$0
	C$palettes.c$130$2_0$129	= .
	.globl	C$palettes.c$130$2_0$129
;src/engine/palettes.c:130: void createBluePaletteVersion(const u8 *original_palette, u8 *bw_palette, u8 numcolors) {
;	---------------------------------
; Function createBluePaletteVersion
; ---------------------------------
_createBluePaletteVersion::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	push	af
	dec	sp
	ld	-3 (ix), l
	ld	-2 (ix), h
	ld	-5 (ix), e
	ld	-4 (ix), d
	C$palettes.c$132$2_0$129	= .
	.globl	C$palettes.c$132$2_0$129
;src/engine/palettes.c:132: for(j = 0;j<numcolors;j++) {
	ld	-1 (ix), #0x00
00115$:
	ld	a, -1 (ix)
	sub	a, 4 (ix)
	jp	NC, 00117$
	C$palettes.c$133$3_0$130	= .
	.globl	C$palettes.c$133$3_0$130
;src/engine/palettes.c:133: redComponent = getRedFromRGB(original_palette[j]);
	ld	a, -3 (ix)
	add	a, -1 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -2 (ix)
	adc	a, #0x00
	ld	h, a
	ld	c, (hl)
	ld	a, c
	and	a, #0x0b
	ld	e, a
	C$palettes.c$134$3_0$130	= .
	.globl	C$palettes.c$134$3_0$130
;src/engine/palettes.c:134: greenComponent = getGreenFromRGB(original_palette[j]);
	ld	a, c
	ld	b, #0x00
	and	a, #0x4c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	and	a, #0x04
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	sra	h
	rr	l
	sra	h
	rr	l
	C$palettes.c$135$2_0$129	= .
	.globl	C$palettes.c$135$2_0$129
;src/engine/palettes.c:135: blueComponent = getBlueFromRGB(original_palette[j]);
	ld	b, #0x00
	ld	a, c
	and	a, #0xb0
	ld	c, a
	ld	a, b
	and	a, #0xad
	ld	b, a
	srl	b
	rr	c
	srl	b
	rr	c
	srl	b
	rr	c
	srl	b
	rr	c
	C$palettes.c$136$3_0$130	= .
	.globl	C$palettes.c$136$3_0$130
;src/engine/palettes.c:136: addedColor = redComponent + greenComponent + blueComponent;
	add	hl, de
	add	hl, bc
	ld	-6 (ix), l
	C$palettes.c$137$3_0$130	= .
	.globl	C$palettes.c$137$3_0$130
;src/engine/palettes.c:137: switch (addedColor)
	ld	a, #0x09
	sub	a, -6 (ix)
	jr	C, 00111$
	ld	c, -6 (ix)
	ld	b, #0x00
	ld	hl, #00135$
	add	hl, bc
	add	hl, bc
	add	hl, bc
	jp	(hl)
00135$:
	jp	00101$
	jp	00103$
	jp	00103$
	jp	00105$
	jp	00105$
	jp	00107$
	jp	00107$
	jp	00110$
	jp	00110$
	jp	00110$
	C$palettes.c$139$4_0$131	= .
	.globl	C$palettes.c$139$4_0$131
;src/engine/palettes.c:139: case 0:
00101$:
	C$palettes.c$140$4_0$131	= .
	.globl	C$palettes.c$140$4_0$131
;src/engine/palettes.c:140: redComponent = 0;
	ld	-7 (ix), #0x00
	C$palettes.c$141$4_0$131	= .
	.globl	C$palettes.c$141$4_0$131
;src/engine/palettes.c:141: greenComponent = 0;
	ld	-6 (ix), #0x00
	C$palettes.c$142$4_0$131	= .
	.globl	C$palettes.c$142$4_0$131
;src/engine/palettes.c:142: blueComponent = 0;
	ld	c, #0x00
	C$palettes.c$143$4_0$131	= .
	.globl	C$palettes.c$143$4_0$131
;src/engine/palettes.c:143: break;
	jr	00112$
	C$palettes.c$145$4_0$131	= .
	.globl	C$palettes.c$145$4_0$131
;src/engine/palettes.c:145: case 2:
00103$:
	C$palettes.c$146$4_0$131	= .
	.globl	C$palettes.c$146$4_0$131
;src/engine/palettes.c:146: redComponent = 0;
	ld	-7 (ix), #0x00
	C$palettes.c$147$4_0$131	= .
	.globl	C$palettes.c$147$4_0$131
;src/engine/palettes.c:147: greenComponent = 0;
	ld	-6 (ix), #0x00
	C$palettes.c$148$4_0$131	= .
	.globl	C$palettes.c$148$4_0$131
;src/engine/palettes.c:148: blueComponent = 1;
	ld	c, #0x01
	C$palettes.c$149$4_0$131	= .
	.globl	C$palettes.c$149$4_0$131
;src/engine/palettes.c:149: break;
	jr	00112$
	C$palettes.c$151$4_0$131	= .
	.globl	C$palettes.c$151$4_0$131
;src/engine/palettes.c:151: case 4:
00105$:
	C$palettes.c$152$4_0$131	= .
	.globl	C$palettes.c$152$4_0$131
;src/engine/palettes.c:152: redComponent = 0;
	ld	-7 (ix), #0x00
	C$palettes.c$153$4_0$131	= .
	.globl	C$palettes.c$153$4_0$131
;src/engine/palettes.c:153: greenComponent = 0;
	ld	-6 (ix), #0x00
	C$palettes.c$154$4_0$131	= .
	.globl	C$palettes.c$154$4_0$131
;src/engine/palettes.c:154: blueComponent = 2;
	ld	c, #0x02
	C$palettes.c$155$4_0$131	= .
	.globl	C$palettes.c$155$4_0$131
;src/engine/palettes.c:155: break;
	jr	00112$
	C$palettes.c$158$4_0$131	= .
	.globl	C$palettes.c$158$4_0$131
;src/engine/palettes.c:158: case 6:
00107$:
	C$palettes.c$159$4_0$131	= .
	.globl	C$palettes.c$159$4_0$131
;src/engine/palettes.c:159: redComponent = 0;
	ld	-7 (ix), #0x00
	C$palettes.c$160$4_0$131	= .
	.globl	C$palettes.c$160$4_0$131
;src/engine/palettes.c:160: greenComponent = 0;
	ld	-6 (ix), #0x00
	C$palettes.c$161$4_0$131	= .
	.globl	C$palettes.c$161$4_0$131
;src/engine/palettes.c:161: blueComponent = 3;
	ld	c, #0x03
	C$palettes.c$162$4_0$131	= .
	.globl	C$palettes.c$162$4_0$131
;src/engine/palettes.c:162: break;  
	jr	00112$
	C$palettes.c$168$4_0$131	= .
	.globl	C$palettes.c$168$4_0$131
;src/engine/palettes.c:168: case 9:
00110$:
	C$palettes.c$169$4_0$131	= .
	.globl	C$palettes.c$169$4_0$131
;src/engine/palettes.c:169: redComponent = 0;
	ld	-7 (ix), #0x00
	C$palettes.c$170$4_0$131	= .
	.globl	C$palettes.c$170$4_0$131
;src/engine/palettes.c:170: greenComponent = 2;
	ld	-6 (ix), #0x02
	C$palettes.c$171$4_0$131	= .
	.globl	C$palettes.c$171$4_0$131
;src/engine/palettes.c:171: blueComponent = 3;  
	ld	c, #0x03
	C$palettes.c$172$4_0$131	= .
	.globl	C$palettes.c$172$4_0$131
;src/engine/palettes.c:172: break;
	jr	00112$
	C$palettes.c$173$4_0$131	= .
	.globl	C$palettes.c$173$4_0$131
;src/engine/palettes.c:173: default:
00111$:
	C$palettes.c$174$4_0$131	= .
	.globl	C$palettes.c$174$4_0$131
;src/engine/palettes.c:174: redComponent = 0;
	ld	-7 (ix), #0x00
	C$palettes.c$175$4_0$131	= .
	.globl	C$palettes.c$175$4_0$131
;src/engine/palettes.c:175: greenComponent = 3;
	ld	-6 (ix), #0x03
	C$palettes.c$176$4_0$131	= .
	.globl	C$palettes.c$176$4_0$131
;src/engine/palettes.c:176: blueComponent = 3;
	ld	c, #0x03
	C$palettes.c$178$3_0$130	= .
	.globl	C$palettes.c$178$3_0$130
;src/engine/palettes.c:178: }
00112$:
	C$palettes.c$179$3_0$130	= .
	.globl	C$palettes.c$179$3_0$130
;src/engine/palettes.c:179: bw_palette[j] = RGB(redComponent,greenComponent,blueComponent);
	ld	a, -5 (ix)
	add	a, -1 (ix)
	ld	e, a
	ld	a, -4 (ix)
	adc	a, #0x00
	ld	d, a
	ld	a, -6 (ix)
	add	a, a
	add	a, a
	or	a, -7 (ix)
	ld	b, a
	ld	a, c
	add	a, a
	add	a, a
	add	a, a
	add	a, a
	or	a, b
	ld	(de), a
	C$palettes.c$132$2_0$129	= .
	.globl	C$palettes.c$132$2_0$129
;src/engine/palettes.c:132: for(j = 0;j<numcolors;j++) {
	inc	-1 (ix)
	jp	00115$
00117$:
	C$palettes.c$181$2_0$129	= .
	.globl	C$palettes.c$181$2_0$129
;src/engine/palettes.c:181: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$fadeToTargetBGPalette$0$0	= .
	.globl	G$fadeToTargetBGPalette$0$0
	C$palettes.c$355$2_0$133	= .
	.globl	C$palettes.c$355$2_0$133
;src/engine/palettes.c:355: void fadeToTargetBGPalette(const u8 *target_palette, u8 numcolors, u8 framesperstep) {
;	---------------------------------
; Function fadeToTargetBGPalette
; ---------------------------------
_fadeToTargetBGPalette::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-29
	add	iy, sp
	ld	sp, iy
	ld	-7 (ix), l
	ld	-6 (ix), h
	C$palettes.c$360$3_0$135	= .
	.globl	C$palettes.c$360$3_0$135
;src/engine/palettes.c:360: for(j = 0;j<numcolors;j++) {
	ld	-1 (ix), #0x00
00127$:
	ld	a, -1 (ix)
	sub	a, 4 (ix)
	jr	NC, 00101$
	C$palettes.c$361$3_0$135	= .
	.globl	C$palettes.c$361$3_0$135
;src/engine/palettes.c:361: temporal_palette[j] = background_palette[j];
	ld	e, -1 (ix)
	ld	d, #0x00
	ld	hl, #2
	add	hl, sp
	add	hl, de
	ld	-5 (ix), l
	ld	-4 (ix), h
	ld	a, #<(_background_palette)
	add	a, -1 (ix)
	ld	-3 (ix), a
	ld	a, #>(_background_palette)
	adc	a, #0x00
	ld	-2 (ix), a
	ld	l, -3 (ix)
	ld	h, -2 (ix)
	ld	a, (hl)
	ld	l, -5 (ix)
	ld	h, -4 (ix)
	ld	(hl), a
	C$palettes.c$360$2_0$134	= .
	.globl	C$palettes.c$360$2_0$134
;src/engine/palettes.c:360: for(j = 0;j<numcolors;j++) {
	inc	-1 (ix)
	jr	00127$
00101$:
	C$palettes.c$363$1_0$133	= .
	.globl	C$palettes.c$363$1_0$133
;src/engine/palettes.c:363: SMS_loadBGPalette(background_palette);
	ld	hl, #_background_palette
	call	_SMS_loadBGPalette
	C$palettes.c$365$3_0$137	= .
	.globl	C$palettes.c$365$3_0$137
;src/engine/palettes.c:365: while(i<4) {
	ld	-5 (ix), #0x00
00123$:
	ld	a, -5 (ix)
	sub	a, #0x04
	jp	NC, 00125$
	C$palettes.c$367$1_0$133	= .
	.globl	C$palettes.c$367$1_0$133
;src/engine/palettes.c:367: while(j<numcolors) {
	ld	-4 (ix), #0x00
00117$:
	ld	a, -4 (ix)
	sub	a, 4 (ix)
	jp	NC, 00119$
	C$palettes.c$368$1_0$133	= .
	.globl	C$palettes.c$368$1_0$133
;src/engine/palettes.c:368: redComponent = getRedFromRGB(temporal_palette[j]);
	ld	e, -4 (ix)
	ld	d, #0x00
	ld	hl, #2
	add	hl, sp
	add	hl, de
	ld	-11 (ix), l
	ld	-10 (ix), h
	ld	a, (hl)
	ld	-1 (ix), a
	and	a, #0x0b
	ld	-3 (ix), a
	C$palettes.c$369$3_0$137	= .
	.globl	C$palettes.c$369$3_0$137
;src/engine/palettes.c:369: greenComponent = getGreenFromRGB(temporal_palette[j]);
	ld	a, -1 (ix)
	ld	b, #0x00
	and	a, #0x4c
	ld	c, a
	ld	a, b
	and	a, #0x04
	ld	b, a
	ld	-9 (ix), c
	ld	-8 (ix), b
	sra	-8 (ix)
	rr	-9 (ix)
	sra	-8 (ix)
	rr	-9 (ix)
	ld	a, -9 (ix)
	ld	-2 (ix), a
	C$palettes.c$370$1_0$133	= .
	.globl	C$palettes.c$370$1_0$133
;src/engine/palettes.c:370: blueComponent = getBlueFromRGB(temporal_palette[j]);
	ld	a, -1 (ix)
	ld	-29 (ix), a
	ld	-28 (ix), #0x00
	ld	a, -29 (ix)
	and	a, #0xb0
	ld	-9 (ix), a
	ld	a, -28 (ix)
	and	a, #0xad
	ld	-8 (ix), a
	srl	-8 (ix)
	rr	-9 (ix)
	srl	-8 (ix)
	rr	-9 (ix)
	srl	-8 (ix)
	rr	-9 (ix)
	srl	-8 (ix)
	rr	-9 (ix)
	ld	a, -9 (ix)
	ld	-1 (ix), a
	C$palettes.c$371$3_0$137	= .
	.globl	C$palettes.c$371$3_0$137
;src/engine/palettes.c:371: targetComponent = getGreenFromRGB(target_palette[j]);
	ld	a, -7 (ix)
	add	a, -4 (ix)
	ld	-9 (ix), a
	ld	a, -6 (ix)
	adc	a, #0x00
	ld	-8 (ix), a
	ld	l, -9 (ix)
	ld	h, -8 (ix)
	ld	a, (hl)
	ld	-8 (ix), a
	ld	-29 (ix), a
	ld	-28 (ix), #0x00
	ld	a, -29 (ix)
	and	a, #0x4c
	ld	-9 (ix), a
	ld	a, -28 (ix)
	and	a, #0x04
	ld	-8 (ix), a
	sra	-8 (ix)
	rr	-9 (ix)
	sra	-8 (ix)
	rr	-9 (ix)
	ld	c, -9 (ix)
	C$palettes.c$372$3_0$137	= .
	.globl	C$palettes.c$372$3_0$137
;src/engine/palettes.c:372: if(greenComponent>targetComponent) {
	ld	a, c
	sub	a, -2 (ix)
	jr	NC, 00105$
	C$palettes.c$373$4_0$138	= .
	.globl	C$palettes.c$373$4_0$138
;src/engine/palettes.c:373: greenComponent--;
	dec	-2 (ix)
	jr	00106$
00105$:
	C$palettes.c$374$3_0$137	= .
	.globl	C$palettes.c$374$3_0$137
;src/engine/palettes.c:374: } else if(greenComponent<targetComponent) {
	ld	a, -2 (ix)
	sub	a, c
	jr	NC, 00106$
	C$palettes.c$375$4_0$139	= .
	.globl	C$palettes.c$375$4_0$139
;src/engine/palettes.c:375: greenComponent++;
	inc	-2 (ix)
00106$:
	C$palettes.c$377$3_0$137	= .
	.globl	C$palettes.c$377$3_0$137
;src/engine/palettes.c:377: if(redComponent>targetComponent) {
	ld	a, c
	sub	a, -3 (ix)
	jr	NC, 00110$
	C$palettes.c$378$4_0$140	= .
	.globl	C$palettes.c$378$4_0$140
;src/engine/palettes.c:378: redComponent--;
	dec	-3 (ix)
	jr	00111$
00110$:
	C$palettes.c$379$3_0$137	= .
	.globl	C$palettes.c$379$3_0$137
;src/engine/palettes.c:379: } else if(redComponent<targetComponent) {
	ld	a, -3 (ix)
	sub	a, c
	jr	NC, 00111$
	C$palettes.c$380$4_0$141	= .
	.globl	C$palettes.c$380$4_0$141
;src/engine/palettes.c:380: redComponent++;
	inc	-3 (ix)
00111$:
	C$palettes.c$382$3_0$137	= .
	.globl	C$palettes.c$382$3_0$137
;src/engine/palettes.c:382: if(blueComponent>targetComponent) {
	ld	a, c
	sub	a, -1 (ix)
	jr	NC, 00115$
	C$palettes.c$383$4_0$142	= .
	.globl	C$palettes.c$383$4_0$142
;src/engine/palettes.c:383: blueComponent--;
	dec	-1 (ix)
	jr	00116$
00115$:
	C$palettes.c$384$3_0$137	= .
	.globl	C$palettes.c$384$3_0$137
;src/engine/palettes.c:384: } else if(blueComponent<targetComponent){
	ld	a, -1 (ix)
	sub	a, c
	jr	NC, 00116$
	C$palettes.c$385$4_0$143	= .
	.globl	C$palettes.c$385$4_0$143
;src/engine/palettes.c:385: blueComponent++;
	inc	-1 (ix)
00116$:
	C$palettes.c$387$3_0$137	= .
	.globl	C$palettes.c$387$3_0$137
;src/engine/palettes.c:387: temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
	ld	a, -2 (ix)
	add	a, a
	add	a, a
	or	a, -3 (ix)
	ld	c, a
	ld	a, -1 (ix)
	add	a, a
	add	a, a
	add	a, a
	add	a, a
	or	a, c
	ld	l, -11 (ix)
	ld	h, -10 (ix)
	ld	(hl), a
	C$palettes.c$388$3_0$137	= .
	.globl	C$palettes.c$388$3_0$137
;src/engine/palettes.c:388: j++;
	inc	-4 (ix)
	jp	00117$
00119$:
	C$palettes.c$391$1_0$133	= .
	.globl	C$palettes.c$391$1_0$133
;src/engine/palettes.c:391: while(j<framesperstep) {
	ld	c, #0x00
00120$:
	ld	a, c
	sub	a, 5 (ix)
	jr	NC, 00122$
	C$palettes.c$392$3_0$144	= .
	.globl	C$palettes.c$392$3_0$144
;src/engine/palettes.c:392: waitForFrame();
	push	bc
	call	_waitForFrame
	pop	bc
	C$palettes.c$393$3_0$144	= .
	.globl	C$palettes.c$393$3_0$144
;src/engine/palettes.c:393: j++;
	inc	c
	jr	00120$
00122$:
	C$palettes.c$395$2_0$136	= .
	.globl	C$palettes.c$395$2_0$136
;src/engine/palettes.c:395: SMS_loadBGPalette(temporal_palette);
	ld	hl, #2
	add	hl, sp
	call	_SMS_loadBGPalette
	C$palettes.c$396$2_0$136	= .
	.globl	C$palettes.c$396$2_0$136
;src/engine/palettes.c:396: i++;
	inc	-5 (ix)
	jp	00123$
00125$:
	C$palettes.c$398$1_0$133	= .
	.globl	C$palettes.c$398$1_0$133
;src/engine/palettes.c:398: setBGPalette(target_palette, numcolors);
	ld	a, 4 (ix)
	push	af
	inc	sp
	ld	l, -7 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_setBGPalette
	C$palettes.c$399$1_0$133	= .
	.globl	C$palettes.c$399$1_0$133
;src/engine/palettes.c:399: }
	ld	sp, ix
	pop	ix
	pop	hl
	pop	af
	jp	(hl)
	G$fadeToTargetSpritePalette$0$0	= .
	.globl	G$fadeToTargetSpritePalette$0$0
	C$palettes.c$401$1_0$146	= .
	.globl	C$palettes.c$401$1_0$146
;src/engine/palettes.c:401: void fadeToTargetSpritePalette(const u8 *target_palette, u8 numcolors, u8 framesperstep) {
;	---------------------------------
; Function fadeToTargetSpritePalette
; ---------------------------------
_fadeToTargetSpritePalette::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-29
	add	iy, sp
	ld	sp, iy
	ld	-7 (ix), l
	ld	-6 (ix), h
	C$palettes.c$406$3_0$148	= .
	.globl	C$palettes.c$406$3_0$148
;src/engine/palettes.c:406: for(j = 0;j<numcolors;j++) {
	ld	-1 (ix), #0x00
00127$:
	ld	a, -1 (ix)
	sub	a, 4 (ix)
	jr	NC, 00101$
	C$palettes.c$407$3_0$148	= .
	.globl	C$palettes.c$407$3_0$148
;src/engine/palettes.c:407: temporal_palette[j] = sprite_palette[j];
	ld	e, -1 (ix)
	ld	d, #0x00
	ld	hl, #2
	add	hl, sp
	add	hl, de
	ld	-5 (ix), l
	ld	-4 (ix), h
	ld	a, #<(_sprite_palette)
	add	a, -1 (ix)
	ld	-3 (ix), a
	ld	a, #>(_sprite_palette)
	adc	a, #0x00
	ld	-2 (ix), a
	ld	l, -3 (ix)
	ld	h, -2 (ix)
	ld	a, (hl)
	ld	l, -5 (ix)
	ld	h, -4 (ix)
	ld	(hl), a
	C$palettes.c$406$2_0$147	= .
	.globl	C$palettes.c$406$2_0$147
;src/engine/palettes.c:406: for(j = 0;j<numcolors;j++) {
	inc	-1 (ix)
	jr	00127$
00101$:
	C$palettes.c$409$1_0$146	= .
	.globl	C$palettes.c$409$1_0$146
;src/engine/palettes.c:409: SMS_loadSpritePalette(sprite_palette);
	ld	hl, #_sprite_palette
	call	_SMS_loadSpritePalette
	C$palettes.c$411$3_0$150	= .
	.globl	C$palettes.c$411$3_0$150
;src/engine/palettes.c:411: while(i<4) {
	ld	-5 (ix), #0x00
00123$:
	ld	a, -5 (ix)
	sub	a, #0x04
	jp	NC, 00125$
	C$palettes.c$413$1_0$146	= .
	.globl	C$palettes.c$413$1_0$146
;src/engine/palettes.c:413: while(j<numcolors) {
	ld	-4 (ix), #0x00
00117$:
	ld	a, -4 (ix)
	sub	a, 4 (ix)
	jp	NC, 00119$
	C$palettes.c$414$1_0$146	= .
	.globl	C$palettes.c$414$1_0$146
;src/engine/palettes.c:414: redComponent = getRedFromRGB(temporal_palette[j]);
	ld	e, -4 (ix)
	ld	d, #0x00
	ld	hl, #2
	add	hl, sp
	add	hl, de
	ld	-11 (ix), l
	ld	-10 (ix), h
	ld	a, (hl)
	ld	-1 (ix), a
	and	a, #0x0b
	ld	-3 (ix), a
	C$palettes.c$415$3_0$150	= .
	.globl	C$palettes.c$415$3_0$150
;src/engine/palettes.c:415: greenComponent = getGreenFromRGB(temporal_palette[j]);
	ld	a, -1 (ix)
	ld	b, #0x00
	and	a, #0x4c
	ld	c, a
	ld	a, b
	and	a, #0x04
	ld	b, a
	ld	-9 (ix), c
	ld	-8 (ix), b
	sra	-8 (ix)
	rr	-9 (ix)
	sra	-8 (ix)
	rr	-9 (ix)
	ld	a, -9 (ix)
	ld	-2 (ix), a
	C$palettes.c$416$1_0$146	= .
	.globl	C$palettes.c$416$1_0$146
;src/engine/palettes.c:416: blueComponent = getBlueFromRGB(temporal_palette[j]);
	ld	a, -1 (ix)
	ld	-29 (ix), a
	ld	-28 (ix), #0x00
	ld	a, -29 (ix)
	and	a, #0xb0
	ld	-9 (ix), a
	ld	a, -28 (ix)
	and	a, #0xad
	ld	-8 (ix), a
	srl	-8 (ix)
	rr	-9 (ix)
	srl	-8 (ix)
	rr	-9 (ix)
	srl	-8 (ix)
	rr	-9 (ix)
	srl	-8 (ix)
	rr	-9 (ix)
	ld	a, -9 (ix)
	ld	-1 (ix), a
	C$palettes.c$417$3_0$150	= .
	.globl	C$palettes.c$417$3_0$150
;src/engine/palettes.c:417: targetComponent = getGreenFromRGB(target_palette[j]);
	ld	a, -7 (ix)
	add	a, -4 (ix)
	ld	-9 (ix), a
	ld	a, -6 (ix)
	adc	a, #0x00
	ld	-8 (ix), a
	ld	l, -9 (ix)
	ld	h, -8 (ix)
	ld	a, (hl)
	ld	-8 (ix), a
	ld	-29 (ix), a
	ld	-28 (ix), #0x00
	ld	a, -29 (ix)
	and	a, #0x4c
	ld	-9 (ix), a
	ld	a, -28 (ix)
	and	a, #0x04
	ld	-8 (ix), a
	sra	-8 (ix)
	rr	-9 (ix)
	sra	-8 (ix)
	rr	-9 (ix)
	ld	c, -9 (ix)
	C$palettes.c$418$3_0$150	= .
	.globl	C$palettes.c$418$3_0$150
;src/engine/palettes.c:418: if(greenComponent>targetComponent) {
	ld	a, c
	sub	a, -2 (ix)
	jr	NC, 00105$
	C$palettes.c$419$4_0$151	= .
	.globl	C$palettes.c$419$4_0$151
;src/engine/palettes.c:419: greenComponent--;
	dec	-2 (ix)
	jr	00106$
00105$:
	C$palettes.c$420$3_0$150	= .
	.globl	C$palettes.c$420$3_0$150
;src/engine/palettes.c:420: } else if(greenComponent<targetComponent) {
	ld	a, -2 (ix)
	sub	a, c
	jr	NC, 00106$
	C$palettes.c$421$4_0$152	= .
	.globl	C$palettes.c$421$4_0$152
;src/engine/palettes.c:421: greenComponent++;
	inc	-2 (ix)
00106$:
	C$palettes.c$423$3_0$150	= .
	.globl	C$palettes.c$423$3_0$150
;src/engine/palettes.c:423: if(redComponent>targetComponent) {
	ld	a, c
	sub	a, -3 (ix)
	jr	NC, 00110$
	C$palettes.c$424$4_0$153	= .
	.globl	C$palettes.c$424$4_0$153
;src/engine/palettes.c:424: redComponent--;
	dec	-3 (ix)
	jr	00111$
00110$:
	C$palettes.c$425$3_0$150	= .
	.globl	C$palettes.c$425$3_0$150
;src/engine/palettes.c:425: } else if(redComponent<targetComponent) {
	ld	a, -3 (ix)
	sub	a, c
	jr	NC, 00111$
	C$palettes.c$426$4_0$154	= .
	.globl	C$palettes.c$426$4_0$154
;src/engine/palettes.c:426: redComponent++;
	inc	-3 (ix)
00111$:
	C$palettes.c$428$3_0$150	= .
	.globl	C$palettes.c$428$3_0$150
;src/engine/palettes.c:428: if(blueComponent>targetComponent) {
	ld	a, c
	sub	a, -1 (ix)
	jr	NC, 00115$
	C$palettes.c$429$4_0$155	= .
	.globl	C$palettes.c$429$4_0$155
;src/engine/palettes.c:429: blueComponent--;
	dec	-1 (ix)
	jr	00116$
00115$:
	C$palettes.c$430$3_0$150	= .
	.globl	C$palettes.c$430$3_0$150
;src/engine/palettes.c:430: } else if(blueComponent<targetComponent){
	ld	a, -1 (ix)
	sub	a, c
	jr	NC, 00116$
	C$palettes.c$431$4_0$156	= .
	.globl	C$palettes.c$431$4_0$156
;src/engine/palettes.c:431: blueComponent++;
	inc	-1 (ix)
00116$:
	C$palettes.c$433$3_0$150	= .
	.globl	C$palettes.c$433$3_0$150
;src/engine/palettes.c:433: temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
	ld	a, -2 (ix)
	add	a, a
	add	a, a
	or	a, -3 (ix)
	ld	c, a
	ld	a, -1 (ix)
	add	a, a
	add	a, a
	add	a, a
	add	a, a
	or	a, c
	ld	l, -11 (ix)
	ld	h, -10 (ix)
	ld	(hl), a
	C$palettes.c$434$3_0$150	= .
	.globl	C$palettes.c$434$3_0$150
;src/engine/palettes.c:434: j++;
	inc	-4 (ix)
	jp	00117$
00119$:
	C$palettes.c$437$1_0$146	= .
	.globl	C$palettes.c$437$1_0$146
;src/engine/palettes.c:437: while(j<framesperstep) {
	ld	c, #0x00
00120$:
	ld	a, c
	sub	a, 5 (ix)
	jr	NC, 00122$
	C$palettes.c$438$3_0$157	= .
	.globl	C$palettes.c$438$3_0$157
;src/engine/palettes.c:438: waitForFrame();
	push	bc
	call	_waitForFrame
	pop	bc
	C$palettes.c$439$3_0$157	= .
	.globl	C$palettes.c$439$3_0$157
;src/engine/palettes.c:439: j++;
	inc	c
	jr	00120$
00122$:
	C$palettes.c$442$2_0$149	= .
	.globl	C$palettes.c$442$2_0$149
;src/engine/palettes.c:442: SMS_loadSpritePalette(temporal_palette);
	ld	hl, #2
	add	hl, sp
	call	_SMS_loadSpritePalette
	C$palettes.c$443$2_0$149	= .
	.globl	C$palettes.c$443$2_0$149
;src/engine/palettes.c:443: i++;
	inc	-5 (ix)
	jp	00123$
00125$:
	C$palettes.c$445$1_0$146	= .
	.globl	C$palettes.c$445$1_0$146
;src/engine/palettes.c:445: setSpritePalette(target_palette, numcolors);
	ld	a, 4 (ix)
	push	af
	inc	sp
	ld	l, -7 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_setSpritePalette
	C$palettes.c$446$1_0$146	= .
	.globl	C$palettes.c$446$1_0$146
;src/engine/palettes.c:446: }
	ld	sp, ix
	pop	ix
	pop	hl
	pop	af
	jp	(hl)
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
