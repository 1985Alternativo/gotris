import sys
import argparse
from pathlib import Path

def generateValue(element, numberocurrencies, nibblenumber):
    value = numberocurrencies*4
    if(element == "B"):
        value = value + 3
    else:
        if(element == "E"):
            value = value + 1
        else:
            if(element == "X"):
                value = value + 2
    if(nibblenumber == 1):
        value = value * 16
    return value

def main():
    print("\n****Kusfo's Gotris Levelificator***********\n")
    parser = argparse.ArgumentParser(description='Process levels for Gotris project')
    parser.add_argument('inputfilename', metavar='i',
	        help='the filename with level definition')
    args = parser.parse_args()
    print("processing: " +args.inputfilename)
    inputfile = open(args.inputfilename,'r')
    inputfilelines = inputfile.readlines()
    inputfile.close()
    inputfilelines.reverse()
    output = ""
    
    nibblenumber = 1
    for inputfileline in inputfilelines:
        inputfileline = inputfileline.strip()
        linelements = inputfileline.split(",")
        tempvalue = 0
        currentelement = ""
        consecutiveocurrences = 0
        for linelement in linelements:
            if(linelement in ["B","W","E"]):
                if(currentelement != linelement):
                    if(consecutiveocurrences > 0):
                        tempvalue = tempvalue + generateValue(currentelement, consecutiveocurrences, nibblenumber)
                        if(nibblenumber == 1):
                            nibblenumber = 2
                        else:
                            if(nibblenumber == 2):
                                output = output + "," + hex(tempvalue)
                                nibblenumber = 1
                                tempvalue = 0
                    currentelement = linelement
                    consecutiveocurrences = 1
                else:
                    consecutiveocurrences = consecutiveocurrences + 1
                    if(consecutiveocurrences == 4):
                        tempvalue = tempvalue + generateValue(currentelement, 3, nibblenumber)
                        if(nibblenumber == 1):
                            nibblenumber = 2
                        else:
                            if(nibblenumber == 2):
                                output = output + "," + hex(tempvalue)
                                nibblenumber = 1
                                tempvalue = 0
                        consecutiveocurrences = 1
            else:
                print("Error on input file!")
                exit()
        tempvalue = tempvalue + generateValue(currentelement, consecutiveocurrences, nibblenumber)
        if(nibblenumber == 1):
            nibblenumber = 2
            tempvalue = tempvalue + generateValue("X", 0, nibblenumber)
        output = output + "," + hex(tempvalue)
        nibblenumber = 1
        tempvalue = 0
            
        output = output + ",0X22"
    output = output + ",0XEE"
    print(output)


if __name__ == '__main__':
    main() 