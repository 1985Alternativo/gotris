;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module game_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGSFXStop
	.globl _PSGStop
	.globl _SMS_resetPauseRequest
	.globl _SMS_queryPauseRequested
	.globl _SMS_zeroSpritePalette
	.globl _SMS_zeroBGPalette
	.globl _waitForFrame
	.globl _clear_input
	.globl _transition_to_state
	.globl _restore_board
	.globl _get_game_actions
	.globl _execute_game_logic
	.globl _init_gamestatus
	.globl _init_board
	.globl _play_sample
	.globl _play_fx
	.globl _play_song
	.globl _start_fadein_music
	.globl _print_string
	.globl _print_level_number
	.globl _print_score_value
	.globl _draw_game_screen
	.globl _draw_game_background
	.globl _game_full_palette
	.globl _game_half_palette
	.globl _clear_game_lists
	.globl _initialize_game_lists
	.globl _game_fade_in
	.globl _general_fade_out
	.globl _load_game_assets
	.globl _game_state_status
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _game_state_start
	.globl _game_state_update
	.globl _game_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$game_state_status$0_0$0==.
_game_state_status::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$game_state_start$0$0	= .
	.globl	G$game_state_start$0$0
	C$game_state.c$18$0_0$236	= .
	.globl	C$game_state.c$18$0_0$236
;src/states/game_state.c:18: void game_state_start(void) {
;	---------------------------------
; Function game_state_start
; ---------------------------------
_game_state_start::
	C$game_state.c$19$1_0$236	= .
	.globl	C$game_state.c$19$1_0$236
;src/states/game_state.c:19: load_game_assets();
	call	_load_game_assets
	C$game_state.c$20$1_0$236	= .
	.globl	C$game_state.c$20$1_0$236
;src/states/game_state.c:20: draw_game_background();
	call	_draw_game_background
	C$game_state.c$21$1_0$236	= .
	.globl	C$game_state.c$21$1_0$236
;src/states/game_state.c:21: init_gamestatus(0);
	xor	a, a
	call	_init_gamestatus
	C$game_state.c$22$1_0$236	= .
	.globl	C$game_state.c$22$1_0$236
;src/states/game_state.c:22: init_board();
	call	_init_board
	C$game_state.c$23$1_0$236	= .
	.globl	C$game_state.c$23$1_0$236
;src/states/game_state.c:23: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$game_state.c$24$1_0$236	= .
	.globl	C$game_state.c$24$1_0$236
;src/states/game_state.c:24: game_state_status = 0;
	ld	hl, #_game_state_status
	ld	(hl), #0x00
	C$game_state.c$25$1_0$236	= .
	.globl	C$game_state.c$25$1_0$236
;src/states/game_state.c:25: }
	C$game_state.c$25$1_0$236	= .
	.globl	C$game_state.c$25$1_0$236
	XG$game_state_start$0$0	= .
	.globl	XG$game_state_start$0$0
	ret
	G$game_state_update$0$0	= .
	.globl	G$game_state_update$0$0
	C$game_state.c$27$1_0$238	= .
	.globl	C$game_state.c$27$1_0$238
;src/states/game_state.c:27: void game_state_update(void) {
;	---------------------------------
; Function game_state_update
; ---------------------------------
_game_state_update::
	C$game_state.c$28$1_0$238	= .
	.globl	C$game_state.c$28$1_0$238
;src/states/game_state.c:28: if(game_state_status == 0) {
	ld	a, (_game_state_status+0)
	or	a, a
	jr	NZ, 00151$
	C$game_state.c$29$2_0$239	= .
	.globl	C$game_state.c$29$2_0$239
;src/states/game_state.c:29: start_fadein_music();
	call	_start_fadein_music
	C$game_state.c$30$2_0$239	= .
	.globl	C$game_state.c$30$2_0$239
;src/states/game_state.c:30: game_fade_in(0);
	xor	a, a
	call	_game_fade_in
	C$game_state.c$31$2_0$239	= .
	.globl	C$game_state.c$31$2_0$239
;src/states/game_state.c:31: play_song(START_OST);
	ld	a, #0x05
	call	_play_song
	C$game_state.c$32$2_0$239	= .
	.globl	C$game_state.c$32$2_0$239
;src/states/game_state.c:32: game_state_status = 1;
	ld	hl, #_game_state_status
	ld	(hl), #0x01
	ret
00151$:
	C$game_state.c$33$1_0$238	= .
	.globl	C$game_state.c$33$1_0$238
;src/states/game_state.c:33: } else if(game_state_status == 1) {
	ld	a, (_game_state_status+0)
	dec	a
	jr	NZ, 00148$
	C$game_state.c$34$2_0$240	= .
	.globl	C$game_state.c$34$2_0$240
;src/states/game_state.c:34: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$game_state.c$35$2_0$240	= .
	.globl	C$game_state.c$35$2_0$240
;src/states/game_state.c:35: while(frame_cnt < XS_PERIOD) {
00101$:
	ld	de, #0x0018
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00103$
	C$game_state.c$36$3_0$241	= .
	.globl	C$game_state.c$36$3_0$241
;src/states/game_state.c:36: waitForFrame();
	call	_waitForFrame
	jr	00101$
00103$:
	C$game_state.c$38$2_0$240	= .
	.globl	C$game_state.c$38$2_0$240
;src/states/game_state.c:38: game_state_status = 2;
	ld	hl, #_game_state_status
	ld	(hl), #0x02
	ret
00148$:
	C$game_state.c$39$1_0$238	= .
	.globl	C$game_state.c$39$1_0$238
;src/states/game_state.c:39: } else if(game_state_status == 2) {
	ld	a, (_game_state_status+0)
	sub	a, #0x02
	jr	NZ, 00145$
	C$game_state.c$40$2_0$242	= .
	.globl	C$game_state.c$40$2_0$242
;src/states/game_state.c:40: print_string(12,10,"GET^");
	ld	hl, #___str_0
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0c
	call	_print_string
	C$game_state.c$41$2_0$242	= .
	.globl	C$game_state.c$41$2_0$242
;src/states/game_state.c:41: print_string(16,10,"READY[^");
	ld	hl, #___str_1
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x10
	call	_print_string
	C$game_state.c$42$2_0$242	= .
	.globl	C$game_state.c$42$2_0$242
;src/states/game_state.c:42: game_state_status = 3;
	ld	hl, #_game_state_status
	ld	(hl), #0x03
	ret
00145$:
	C$game_state.c$43$1_0$238	= .
	.globl	C$game_state.c$43$1_0$238
;src/states/game_state.c:43: } else if(game_state_status == 3) {
	ld	a, (_game_state_status+0)
	sub	a, #0x03
	jr	NZ, 00142$
	C$game_state.c$44$2_0$243	= .
	.globl	C$game_state.c$44$2_0$243
;src/states/game_state.c:44: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$game_state.c$45$2_0$243	= .
	.globl	C$game_state.c$45$2_0$243
;src/states/game_state.c:45: while(frame_cnt < L_PERIOD) {
00104$:
	ld	de, #0x00b4
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00106$
	C$game_state.c$46$3_0$244	= .
	.globl	C$game_state.c$46$3_0$244
;src/states/game_state.c:46: waitForFrame();
	call	_waitForFrame
	jr	00104$
00106$:
	C$game_state.c$48$2_0$243	= .
	.globl	C$game_state.c$48$2_0$243
;src/states/game_state.c:48: game_state_status = 4;
	ld	hl, #_game_state_status
	ld	(hl), #0x04
	ret
00142$:
	C$game_state.c$49$1_0$238	= .
	.globl	C$game_state.c$49$1_0$238
;src/states/game_state.c:49: } else if(game_state_status == 4) {
	ld	a, (_game_state_status+0)
	sub	a, #0x04
	jr	NZ, 00139$
	C$game_state.c$50$2_0$245	= .
	.globl	C$game_state.c$50$2_0$245
;src/states/game_state.c:50: stop_music();
	call	_PSGStop
	C$game_state.c$51$2_0$245	= .
	.globl	C$game_state.c$51$2_0$245
;src/states/game_state.c:51: stop_fx();
	call	_PSGSFXStop
	C$game_state.c$52$2_0$245	= .
	.globl	C$game_state.c$52$2_0$245
;src/states/game_state.c:52: waitForFrame();
	call	_waitForFrame
	C$game_state.c$53$2_0$245	= .
	.globl	C$game_state.c$53$2_0$245
;src/states/game_state.c:53: play_sample(GETREADY_SAMPLE);
	ld	a, #0x02
	call	_play_sample
	C$game_state.c$54$2_0$245	= .
	.globl	C$game_state.c$54$2_0$245
;src/states/game_state.c:54: waitForFrame();
	call	_waitForFrame
	C$game_state.c$55$2_0$245	= .
	.globl	C$game_state.c$55$2_0$245
;src/states/game_state.c:55: play_song(INGAME_OST);
	ld	a, #0x06
	call	_play_song
	C$game_state.c$56$2_0$245	= .
	.globl	C$game_state.c$56$2_0$245
;src/states/game_state.c:56: draw_game_screen();
	call	_draw_game_screen
	C$game_state.c$57$2_0$245	= .
	.globl	C$game_state.c$57$2_0$245
;src/states/game_state.c:57: print_level_number(2,23,level+1);
	ld	a, (_level+0)
	inc	a
	push	af
	inc	sp
	ld	l, #0x17
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_level_number
	C$game_state.c$58$2_0$245	= .
	.globl	C$game_state.c$58$2_0$245
;src/states/game_state.c:58: print_string(2,19,"LVL]GOAL^");
	ld	hl, #___str_2
	push	hl
	ld	l, #0x13
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	C$game_state.c$59$2_0$245	= .
	.globl	C$game_state.c$59$2_0$245
;src/states/game_state.c:59: print_score_value(2,21,(level+1)<<LEVEL_DELIMITER);
	ld	a, (_level+0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	push	hl
	ld	l, #0x15
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_score_value
	C$game_state.c$60$2_0$245	= .
	.globl	C$game_state.c$60$2_0$245
;src/states/game_state.c:60: print_score_value(24,19,score_p1);
	ld	hl, (_score_p1)
	push	hl
	ld	l, #0x13
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x18
	call	_print_score_value
	C$game_state.c$61$2_0$245	= .
	.globl	C$game_state.c$61$2_0$245
;src/states/game_state.c:61: print_score_value(24,23,score_p2);
	ld	hl, (_score_p2)
	push	hl
	ld	l, #0x17
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x18
	call	_print_score_value
	C$game_state.c$62$2_0$245	= .
	.globl	C$game_state.c$62$2_0$245
;src/states/game_state.c:62: initialize_game_lists();
	call	_initialize_game_lists
	C$game_state.c$63$2_0$245	= .
	.globl	C$game_state.c$63$2_0$245
;src/states/game_state.c:63: pause = false;
	ld	hl, #_pause
	ld	(hl), #0x00
	C$game_state.c$64$2_0$245	= .
	.globl	C$game_state.c$64$2_0$245
;src/states/game_state.c:64: SMS_resetPauseRequest();
	call	_SMS_resetPauseRequest
	C$game_state.c$65$2_0$245	= .
	.globl	C$game_state.c$65$2_0$245
;src/states/game_state.c:65: game_state_status = 5;
	ld	hl, #_game_state_status
	ld	(hl), #0x05
	ret
00139$:
	C$game_state.c$67$1_0$238	= .
	.globl	C$game_state.c$67$1_0$238
;src/states/game_state.c:67: if (game_status!=GAME_STATUS_RESET && game_status != GAME_STATUS_ENDGAME2) { 
	ld	a, (_game_status+0)
	sub	a, #0x15
	ld	a, #0x01
	jr	Z, 00279$
	xor	a, a
00279$:
	ld	c, a
	ld	a, (_game_status+0)
	sub	a, #0x14
	ld	a, #0x01
	jr	Z, 00281$
	xor	a, a
00281$:
	ld	b, a
	C$game_state.c$66$1_0$238	= .
	.globl	C$game_state.c$66$1_0$238
;src/states/game_state.c:66: }  else if (game_state_status == 5) {
	ld	a, (_game_state_status+0)
	sub	a, #0x05
	jp	NZ,00136$
	C$game_state.c$67$2_0$246	= .
	.globl	C$game_state.c$67$2_0$246
;src/states/game_state.c:67: if (game_status!=GAME_STATUS_RESET && game_status != GAME_STATUS_ENDGAME2) { 
	bit	0, c
	jr	NZ, 00122$
	bit	0, b
	jr	NZ, 00122$
	C$game_state.c$68$3_0$247	= .
	.globl	C$game_state.c$68$3_0$247
;src/states/game_state.c:68: if(!pause) {
	ld	hl, #_pause
	bit	0, (hl)
	jr	NZ, 00108$
	C$game_state.c$69$4_0$248	= .
	.globl	C$game_state.c$69$4_0$248
;src/states/game_state.c:69: execute_game_logic();
	call	_execute_game_logic
	C$game_state.c$70$4_0$248	= .
	.globl	C$game_state.c$70$4_0$248
;src/states/game_state.c:70: get_game_actions();
	call	_get_game_actions
00108$:
	C$game_state.c$72$3_0$247	= .
	.globl	C$game_state.c$72$3_0$247
;src/states/game_state.c:72: if(SMS_queryPauseRequested()) {
	call	_SMS_queryPauseRequested
	ld	c, a
	bit	0, c
	jp	Z,_waitForFrame
	C$game_state.c$73$4_0$249	= .
	.globl	C$game_state.c$73$4_0$249
;src/states/game_state.c:73: if(pause) {
	ld	iy, #_pause
	bit	0, 0 (iy)
	jr	Z, 00114$
	C$game_state.c$74$5_0$250	= .
	.globl	C$game_state.c$74$5_0$250
;src/states/game_state.c:74: pause = false;
	ld	0 (iy), #0x00
	C$game_state.c$75$5_0$250	= .
	.globl	C$game_state.c$75$5_0$250
;src/states/game_state.c:75: game_full_palette();
	call	_game_full_palette
	C$game_state.c$76$5_0$250	= .
	.globl	C$game_state.c$76$5_0$250
;src/states/game_state.c:76: if(game_status != GAME_STATUS_GAMEOVER) {
	ld	a, (_game_status+0)
	sub	a, #0x12
	jr	Z, 00110$
	C$game_state.c$77$6_0$251	= .
	.globl	C$game_state.c$77$6_0$251
;src/states/game_state.c:77: restore_board();
	call	_restore_board
00110$:
	C$game_state.c$79$5_0$250	= .
	.globl	C$game_state.c$79$5_0$250
;src/states/game_state.c:79: play_fx(SELECT_FX);
	ld	a, #0x01
	call	_play_fx
	jr	00115$
00114$:
	C$game_state.c$82$5_0$252	= .
	.globl	C$game_state.c$82$5_0$252
;src/states/game_state.c:82: pause = true;
	ld	hl, #_pause
	ld	(hl), #0x01
	C$game_state.c$83$5_0$252	= .
	.globl	C$game_state.c$83$5_0$252
;src/states/game_state.c:83: game_half_palette();
	call	_game_half_palette
	C$game_state.c$84$5_0$252	= .
	.globl	C$game_state.c$84$5_0$252
;src/states/game_state.c:84: if(game_status != GAME_STATUS_GAMEOVER) {
	ld	a, (_game_status+0)
	sub	a, #0x12
	jr	Z, 00112$
	C$game_state.c$85$6_0$253	= .
	.globl	C$game_state.c$85$6_0$253
;src/states/game_state.c:85: print_string(13,10,"PAUSE^");
	ld	hl, #___str_3
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0d
	call	_print_string
00112$:
	C$game_state.c$88$5_0$252	= .
	.globl	C$game_state.c$88$5_0$252
;src/states/game_state.c:88: play_fx(SELECT_FX);
	ld	a, #0x01
	call	_play_fx
00115$:
	C$game_state.c$91$4_0$249	= .
	.globl	C$game_state.c$91$4_0$249
;src/states/game_state.c:91: SMS_resetPauseRequest();
	call	_SMS_resetPauseRequest
	C$game_state.c$93$3_0$247	= .
	.globl	C$game_state.c$93$3_0$247
;src/states/game_state.c:93: waitForFrame();
	jp	_waitForFrame
00122$:
	C$game_state.c$95$3_0$254	= .
	.globl	C$game_state.c$95$3_0$254
;src/states/game_state.c:95: general_fade_out();
	call	_general_fade_out
	C$game_state.c$96$3_0$254	= .
	.globl	C$game_state.c$96$3_0$254
;src/states/game_state.c:96: blackBackgroundPalette();
	call	_SMS_zeroBGPalette
	C$game_state.c$97$3_0$254	= .
	.globl	C$game_state.c$97$3_0$254
;src/states/game_state.c:97: blackSpritePalette();
	call	_SMS_zeroSpritePalette
	C$game_state.c$98$3_0$254	= .
	.globl	C$game_state.c$98$3_0$254
;src/states/game_state.c:98: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$game_state.c$99$3_0$254	= .
	.globl	C$game_state.c$99$3_0$254
;src/states/game_state.c:99: while(frame_cnt < M_PERIOD) {
00118$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00120$
	C$game_state.c$100$4_0$255	= .
	.globl	C$game_state.c$100$4_0$255
;src/states/game_state.c:100: waitForFrame();
	call	_waitForFrame
	jr	00118$
00120$:
	C$game_state.c$102$3_0$254	= .
	.globl	C$game_state.c$102$3_0$254
;src/states/game_state.c:102: clear_game_lists();
	call	_clear_game_lists
	C$game_state.c$103$3_0$254	= .
	.globl	C$game_state.c$103$3_0$254
;src/states/game_state.c:103: clear_input();
	call	_clear_input
	C$game_state.c$104$3_0$254	= .
	.globl	C$game_state.c$104$3_0$254
;src/states/game_state.c:104: game_state_status = 6;
	ld	hl, #_game_state_status
	ld	(hl), #0x06
	ret
00136$:
	C$game_state.c$106$1_0$238	= .
	.globl	C$game_state.c$106$1_0$238
;src/states/game_state.c:106: } else if(game_state_status == 6) {
	ld	a, (_game_state_status+0)
	C$game_state.c$107$2_0$256	= .
	.globl	C$game_state.c$107$2_0$256
;src/states/game_state.c:107: if(game_status == GAME_STATUS_RESET) {
	sub	a,#0x06
	ret	NZ
	or	a,c
	jr	Z, 00131$
	C$game_state.c$108$3_0$257	= .
	.globl	C$game_state.c$108$3_0$257
;src/states/game_state.c:108: if(highScoreReached) {
	ld	iy, #_highScoreReached
	bit	0, 0 (iy)
	jr	Z, 00126$
	C$game_state.c$109$4_0$258	= .
	.globl	C$game_state.c$109$4_0$258
;src/states/game_state.c:109: transition_to_state(ENTER_SCORE_STATE);
	ld	a, #0x08
	jp	_transition_to_state
00126$:
	C$game_state.c$111$4_0$259	= .
	.globl	C$game_state.c$111$4_0$259
;src/states/game_state.c:111: transition_to_state(LOGO_STATE);
	ld	a, #0x01
	jp	_transition_to_state
00131$:
	C$game_state.c$113$2_0$256	= .
	.globl	C$game_state.c$113$2_0$256
;src/states/game_state.c:113: } else if(game_status == GAME_STATUS_ENDGAME2) {
	ld	a, b
	or	a, a
	ret	Z
	C$game_state.c$114$3_0$260	= .
	.globl	C$game_state.c$114$3_0$260
;src/states/game_state.c:114: transition_to_state(END_GAME_STATE);
	ld	a, #0x09
	C$game_state.c$117$1_0$238	= .
	.globl	C$game_state.c$117$1_0$238
;src/states/game_state.c:117: }
	C$game_state.c$117$1_0$238	= .
	.globl	C$game_state.c$117$1_0$238
	XG$game_state_update$0$0	= .
	.globl	XG$game_state_update$0$0
	jp	_transition_to_state
Fgame_state$__str_0$0_0$0 == .
___str_0:
	.ascii "GET^"
	.db 0x00
Fgame_state$__str_1$0_0$0 == .
___str_1:
	.ascii "READY[^"
	.db 0x00
Fgame_state$__str_2$0_0$0 == .
___str_2:
	.ascii "LVL]GOAL^"
	.db 0x00
Fgame_state$__str_3$0_0$0 == .
___str_3:
	.ascii "PAUSE^"
	.db 0x00
	G$game_state_stop$0$0	= .
	.globl	G$game_state_stop$0$0
	C$game_state.c$119$1_0$262	= .
	.globl	C$game_state.c$119$1_0$262
;src/states/game_state.c:119: void game_state_stop(void) {
;	---------------------------------
; Function game_state_stop
; ---------------------------------
_game_state_stop::
	C$game_state.c$120$1_0$262	= .
	.globl	C$game_state.c$120$1_0$262
;src/states/game_state.c:120: clear_input();
	C$game_state.c$121$1_0$262	= .
	.globl	C$game_state.c$121$1_0$262
;src/states/game_state.c:121: }
	C$game_state.c$121$1_0$262	= .
	.globl	C$game_state.c$121$1_0$262
	XG$game_state_stop$0$0	= .
	.globl	XG$game_state_stop$0$0
	jp	_clear_input
	.area _CODE
	.area _INITIALIZER
Fgame_state$__xinit_game_state_status$0_0$0 == .
__xinit__game_state_status:
	.db #0x00	; 0
	.area _CABS (ABS)
