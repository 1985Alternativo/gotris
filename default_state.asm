;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module default_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _transition_to_state
	.globl _default_state_start
	.globl _default_state_update
	.globl _default_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$default_state_start$0$0	= .
	.globl	G$default_state_start$0$0
	C$default_state.c$7$0_0$15	= .
	.globl	C$default_state.c$7$0_0$15
;src/states/default_state.c:7: void default_state_start(void) {
;	---------------------------------
; Function default_state_start
; ---------------------------------
_default_state_start::
	C$default_state.c$9$0_0$15	= .
	.globl	C$default_state.c$9$0_0$15
;src/states/default_state.c:9: }
	C$default_state.c$9$0_0$15	= .
	.globl	C$default_state.c$9$0_0$15
	XG$default_state_start$0$0	= .
	.globl	XG$default_state_start$0$0
	ret
	G$default_state_update$0$0	= .
	.globl	G$default_state_update$0$0
	C$default_state.c$11$0_0$17	= .
	.globl	C$default_state.c$11$0_0$17
;src/states/default_state.c:11: void default_state_update(void) {
;	---------------------------------
; Function default_state_update
; ---------------------------------
_default_state_update::
	C$default_state.c$12$1_0$17	= .
	.globl	C$default_state.c$12$1_0$17
;src/states/default_state.c:12: transition_to_state(LOGO_STATE);
	ld	a, #0x01
	C$default_state.c$13$1_0$17	= .
	.globl	C$default_state.c$13$1_0$17
;src/states/default_state.c:13: }
	C$default_state.c$13$1_0$17	= .
	.globl	C$default_state.c$13$1_0$17
	XG$default_state_update$0$0	= .
	.globl	XG$default_state_update$0$0
	jp	_transition_to_state
	G$default_state_stop$0$0	= .
	.globl	G$default_state_stop$0$0
	C$default_state.c$15$1_0$20	= .
	.globl	C$default_state.c$15$1_0$20
;src/states/default_state.c:15: void default_state_stop(void) {
;	---------------------------------
; Function default_state_stop
; ---------------------------------
_default_state_stop::
	C$default_state.c$17$1_0$20	= .
	.globl	C$default_state.c$17$1_0$20
;src/states/default_state.c:17: }
	C$default_state.c$17$1_0$20	= .
	.globl	C$default_state.c$17$1_0$20
	XG$default_state_stop$0$0	= .
	.globl	XG$default_state_stop$0$0
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
