;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module logo_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGStop
	.globl _SMS_zeroBGPalette
	.globl _waitForFrame
	.globl _transition_to_state
	.globl _play_sample
	.globl _play_song
	.globl _full_volume
	.globl _background_fade_in
	.globl _background_fade_out
	.globl _turn_black
	.globl _load_tilemap
	.globl _load_tileset
	.globl _logo_state_status
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _logo_state_start
	.globl _logo_state_update
	.globl _logo_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$logo_state_status$0_0$0==.
_logo_state_status::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$logo_state_start$0$0	= .
	.globl	G$logo_state_start$0$0
	C$logo_state.c$14$0_0$195	= .
	.globl	C$logo_state.c$14$0_0$195
;src/states/logo_state.c:14: void logo_state_start(void) {
;	---------------------------------
; Function logo_state_start
; ---------------------------------
_logo_state_start::
	C$logo_state.c$15$1_0$195	= .
	.globl	C$logo_state.c$15$1_0$195
;src/states/logo_state.c:15: load_tileset(LOGO_ASSETS,0);
;	spillPairReg hl
;	spillPairReg hl
	xor	a, a
	ld	l, a
	call	_load_tileset
	C$logo_state.c$16$1_0$195	= .
	.globl	C$logo_state.c$16$1_0$195
;src/states/logo_state.c:16: load_tilemap(LOGO_ASSETS);
	xor	a, a
	call	_load_tilemap
	C$logo_state.c$17$1_0$195	= .
	.globl	C$logo_state.c$17$1_0$195
;src/states/logo_state.c:17: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$logo_state.c$18$1_0$195	= .
	.globl	C$logo_state.c$18$1_0$195
;src/states/logo_state.c:18: logo_state_status = 0;
	ld	hl, #_logo_state_status
	ld	(hl), #0x00
	C$logo_state.c$19$1_0$195	= .
	.globl	C$logo_state.c$19$1_0$195
;src/states/logo_state.c:19: }
	C$logo_state.c$19$1_0$195	= .
	.globl	C$logo_state.c$19$1_0$195
	XG$logo_state_start$0$0	= .
	.globl	XG$logo_state_start$0$0
	ret
	G$logo_state_update$0$0	= .
	.globl	G$logo_state_update$0$0
	C$logo_state.c$21$1_0$197	= .
	.globl	C$logo_state.c$21$1_0$197
;src/states/logo_state.c:21: void logo_state_update(void) {
;	---------------------------------
; Function logo_state_update
; ---------------------------------
_logo_state_update::
	C$logo_state.c$22$1_0$197	= .
	.globl	C$logo_state.c$22$1_0$197
;src/states/logo_state.c:22: if(logo_state_status == 0) {
	ld	a, (_logo_state_status+0)
	or	a, a
	jr	NZ, 00128$
	C$logo_state.c$23$2_0$198	= .
	.globl	C$logo_state.c$23$2_0$198
;src/states/logo_state.c:23: turn_black();
	call	_turn_black
	C$logo_state.c$24$2_0$198	= .
	.globl	C$logo_state.c$24$2_0$198
;src/states/logo_state.c:24: background_fade_in(LOGO_PALETTE,2);
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x01
	call	_background_fade_in
	C$logo_state.c$25$2_0$198	= .
	.globl	C$logo_state.c$25$2_0$198
;src/states/logo_state.c:25: full_volume();
	call	_full_volume
	C$logo_state.c$26$2_0$198	= .
	.globl	C$logo_state.c$26$2_0$198
;src/states/logo_state.c:26: play_sample(TUXEDO_SAMPLE);
	ld	a, #0x08
	call	_play_sample
	C$logo_state.c$27$2_0$198	= .
	.globl	C$logo_state.c$27$2_0$198
;src/states/logo_state.c:27: logo_state_status = 1;
	ld	hl, #_logo_state_status
	ld	(hl), #0x01
	ret
00128$:
	C$logo_state.c$28$1_0$197	= .
	.globl	C$logo_state.c$28$1_0$197
;src/states/logo_state.c:28: } else if(logo_state_status == 1) {
	ld	a, (_logo_state_status+0)
	dec	a
	jr	NZ, 00125$
	C$logo_state.c$29$2_0$199	= .
	.globl	C$logo_state.c$29$2_0$199
;src/states/logo_state.c:29: while(frame_cnt < M_PERIOD) {
00101$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00103$
	C$logo_state.c$30$3_0$200	= .
	.globl	C$logo_state.c$30$3_0$200
;src/states/logo_state.c:30: waitForFrame();
	call	_waitForFrame
	jr	00101$
00103$:
	C$logo_state.c$32$2_0$199	= .
	.globl	C$logo_state.c$32$2_0$199
;src/states/logo_state.c:32: logo_state_status = 2;
	ld	hl, #_logo_state_status
	ld	(hl), #0x02
	ret
00125$:
	C$logo_state.c$33$1_0$197	= .
	.globl	C$logo_state.c$33$1_0$197
;src/states/logo_state.c:33: } else if(logo_state_status == 2){
	ld	a, (_logo_state_status+0)
	sub	a, #0x02
	jr	NZ, 00122$
	C$logo_state.c$34$2_0$201	= .
	.globl	C$logo_state.c$34$2_0$201
;src/states/logo_state.c:34: background_fade_out();
	call	_background_fade_out
	C$logo_state.c$35$2_0$201	= .
	.globl	C$logo_state.c$35$2_0$201
;src/states/logo_state.c:35: logo_state_status = 3;
	ld	hl, #_logo_state_status
	ld	(hl), #0x03
	ret
00122$:
	C$logo_state.c$36$1_0$197	= .
	.globl	C$logo_state.c$36$1_0$197
;src/states/logo_state.c:36: }  else if(logo_state_status == 3) {
	ld	a, (_logo_state_status+0)
	C$logo_state.c$37$2_0$202	= .
	.globl	C$logo_state.c$37$2_0$202
;src/states/logo_state.c:37: load_tileset(DEVKIT_ASSETS,0);
	sub	a,#0x03
	jr	NZ, 00119$
	ld	l,a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x01
	call	_load_tileset
	C$logo_state.c$38$2_0$202	= .
	.globl	C$logo_state.c$38$2_0$202
;src/states/logo_state.c:38: load_tilemap(DEVKIT_ASSETS);
	ld	a, #0x01
	call	_load_tilemap
	C$logo_state.c$39$2_0$202	= .
	.globl	C$logo_state.c$39$2_0$202
;src/states/logo_state.c:39: logo_state_status = 4;
	ld	hl, #_logo_state_status
	ld	(hl), #0x04
	ret
00119$:
	C$logo_state.c$40$1_0$197	= .
	.globl	C$logo_state.c$40$1_0$197
;src/states/logo_state.c:40: } else if(logo_state_status == 4) {
	ld	a, (_logo_state_status+0)
	sub	a, #0x04
	jr	NZ, 00116$
	C$logo_state.c$41$2_0$203	= .
	.globl	C$logo_state.c$41$2_0$203
;src/states/logo_state.c:41: background_fade_in(DEVKIT_PALETTE,2);
;	spillPairReg hl
;	spillPairReg hl
	ld	a,#0x02
	ld	l,a
	call	_background_fade_in
	C$logo_state.c$42$2_0$203	= .
	.globl	C$logo_state.c$42$2_0$203
;src/states/logo_state.c:42: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$logo_state.c$43$2_0$203	= .
	.globl	C$logo_state.c$43$2_0$203
;src/states/logo_state.c:43: logo_state_status = 5;
	ld	hl, #_logo_state_status
	ld	(hl), #0x05
	ret
00116$:
	C$logo_state.c$44$1_0$197	= .
	.globl	C$logo_state.c$44$1_0$197
;src/states/logo_state.c:44: } else if(logo_state_status == 5) {
	ld	a, (_logo_state_status+0)
	sub	a, #0x05
	jr	NZ, 00113$
	C$logo_state.c$45$2_0$204	= .
	.globl	C$logo_state.c$45$2_0$204
;src/states/logo_state.c:45: play_song(LOGO_OST);
	xor	a, a
	call	_play_song
	C$logo_state.c$46$2_0$204	= .
	.globl	C$logo_state.c$46$2_0$204
;src/states/logo_state.c:46: while(frame_cnt < M_PERIOD) {
00104$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00106$
	C$logo_state.c$47$3_0$205	= .
	.globl	C$logo_state.c$47$3_0$205
;src/states/logo_state.c:47: waitForFrame();
	call	_waitForFrame
	jr	00104$
00106$:
	C$logo_state.c$49$2_0$204	= .
	.globl	C$logo_state.c$49$2_0$204
;src/states/logo_state.c:49: logo_state_status = 6;
	ld	hl, #_logo_state_status
	ld	(hl), #0x06
	ret
00113$:
	C$logo_state.c$50$1_0$197	= .
	.globl	C$logo_state.c$50$1_0$197
;src/states/logo_state.c:50: }  else if(logo_state_status == 6){
	ld	a, (_logo_state_status+0)
	sub	a, #0x06
	jr	NZ, 00110$
	C$logo_state.c$51$2_0$206	= .
	.globl	C$logo_state.c$51$2_0$206
;src/states/logo_state.c:51: background_fade_out();
	call	_background_fade_out
	C$logo_state.c$52$2_0$206	= .
	.globl	C$logo_state.c$52$2_0$206
;src/states/logo_state.c:52: logo_state_status = 7;
	ld	hl, #_logo_state_status
	ld	(hl), #0x07
	ret
00110$:
	C$logo_state.c$53$1_0$197	= .
	.globl	C$logo_state.c$53$1_0$197
;src/states/logo_state.c:53: } else if(logo_state_status == 7) {
	ld	a, (_logo_state_status+0)
	sub	a, #0x07
	ret	NZ
	C$logo_state.c$54$2_0$207	= .
	.globl	C$logo_state.c$54$2_0$207
;src/states/logo_state.c:54: transition_to_state(INTRO_STATE);
	ld	a, #0x02
	C$logo_state.c$56$1_0$197	= .
	.globl	C$logo_state.c$56$1_0$197
;src/states/logo_state.c:56: }
	C$logo_state.c$56$1_0$197	= .
	.globl	C$logo_state.c$56$1_0$197
	XG$logo_state_update$0$0	= .
	.globl	XG$logo_state_update$0$0
	jp	_transition_to_state
	G$logo_state_stop$0$0	= .
	.globl	G$logo_state_stop$0$0
	C$logo_state.c$58$1_0$209	= .
	.globl	C$logo_state.c$58$1_0$209
;src/states/logo_state.c:58: void logo_state_stop(void) {
;	---------------------------------
; Function logo_state_stop
; ---------------------------------
_logo_state_stop::
	C$logo_state.c$59$1_0$209	= .
	.globl	C$logo_state.c$59$1_0$209
;src/states/logo_state.c:59: stop_music();
	call	_PSGStop
	C$logo_state.c$60$1_0$209	= .
	.globl	C$logo_state.c$60$1_0$209
;src/states/logo_state.c:60: blackBackgroundPalette();
	C$logo_state.c$61$1_0$209	= .
	.globl	C$logo_state.c$61$1_0$209
;src/states/logo_state.c:61: }
	C$logo_state.c$61$1_0$209	= .
	.globl	C$logo_state.c$61$1_0$209
	XG$logo_state_stop$0$0	= .
	.globl	XG$logo_state_stop$0$0
	jp	_SMS_zeroBGPalette
	.area _CODE
	.area _INITIALIZER
Flogo_state$__xinit_logo_state_status$0_0$0 == .
__xinit__logo_state_status:
	.db #0x00	; 0
	.area _CABS (ABS)
